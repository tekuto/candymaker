﻿#include "fg/util/export.h"
#include "fg/core/basesystem/context.h"

namespace {
    auto    i = 0;
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::BasesystemContext< int > &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData(
        )
    )

    void initialize(
        fg::BasesystemContext< int > &
    )
    {
        i = 20;
    }

    int getData(
    )
    {
        return i;
    }
}
