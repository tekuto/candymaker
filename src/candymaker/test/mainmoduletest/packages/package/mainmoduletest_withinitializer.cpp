﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"

namespace {
    auto    i = 0;
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData(
        )
    )

    void initialize(
        fg::ModuleContext &
    )
    {
        i = 10;
    }

    int getData(
    )
    {
        return i;
    }
}
