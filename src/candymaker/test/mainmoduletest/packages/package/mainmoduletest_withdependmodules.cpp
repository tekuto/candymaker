﻿#include "fg/util/export.h"
#include "candymaker/test/mainmoduletest/packages/package/mainmoduletest_dependmodule.h"
#include "fg/core/game/context.h"

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::GameContext< int > &
        )
    )

    void initialize(
        fg::GameContext< int > &
    )
    {
        dependFunc();
    }
}
