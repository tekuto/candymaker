﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"
#include "candymaker/test/modulemanagertest/loadtest4.h"

namespace {
    void freeData(
        int *
    )
    {
        candymaker::setData( 30 );
    }
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    void initialize(
        fg::ModuleContext & _moduleContext
    )
    {
        _moduleContext.setData(
            static_cast< int * >( nullptr )
            , freeData
        );
    }
}
