﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"
#include "candymaker/test/modulemanagertest/unloadtest1.h"

namespace {
    auto    i = 0;
}

namespace candymaker {
    void initialize(
        fg::ModuleContext &
    )
    {
        i = 20;
    }

    int getData(
    )
    {
        return i;
    }

    void setData(
        int _i
    )
    {
        i = _i;
    }
}
