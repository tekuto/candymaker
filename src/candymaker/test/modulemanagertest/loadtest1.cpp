﻿#include "fg/util/export.h"
#include "candymaker/test/modulemanagertest/loadtest1.h"
#include "candymaker/test/modulemanagertest/loadtest2.h"
#include "fg/core/module/context.h"

#include <memory>

namespace {
    void freeData(
        int *   _iPtr
    )
    {
        delete _iPtr;
    }
}

namespace candymaker {
    void initialize(
        fg::ModuleContext & _moduleContext
    )
    {
        auto    iUnique = std::unique_ptr< int >( new int( getData2() ) );

        _moduleContext.setData(
            iUnique.release()
            , freeData
        );
    }

    int getData1(
    )
    {
        return 20;
    }
}
