﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"

namespace {
    auto    iPtr = static_cast< int * >( nullptr );

    void freeData(
        int *
    )
    {
        if( iPtr != nullptr ) {
            *iPtr = 10;
        }
    }
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    FG_FUNCTION_VOID(
        setData(
            int &
        )
    )

    void initialize(
        fg::ModuleContext & _moduleContext
    )
    {
        _moduleContext.setData(
            static_cast< int * >( nullptr )
            , freeData
        );
    }

    void setData(
        int &   _i
    )
    {
        iPtr = &_i;
    }
}
