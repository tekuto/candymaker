﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"

namespace {
    auto    i = 0;

    void freeData(
        int *
    )
    {
        i = 10;
    }
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData(
        )
    )

    void initialize(
        fg::ModuleContext & _moduleContext
    )
    {
        _moduleContext.setData(
            static_cast< int * >( nullptr )
            , freeData
        );
    }

    int getData(
    )
    {
        return i;
    }
}
