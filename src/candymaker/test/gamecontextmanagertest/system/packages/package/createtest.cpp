﻿#include "fg/util/export.h"
#include "fg/core/game/context.h"

namespace {
    auto    i = 0;
}

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::GameContext< int > &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData(
        )
    )

    void initialize(
        fg::GameContext< int > &
    )
    {
        i = 20;
    }

    int getData(
    )
    {
        return i;
    }
}
