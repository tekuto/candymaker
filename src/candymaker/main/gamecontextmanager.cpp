﻿#include "candymaker/main/gamecontextmanager.h"
#include "candymaker/main/module.h"
#include "candymaker/main/packagefile.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/module/contexts.h"

#include <string>
#include <utility>

namespace {
    auto createInitializerSymbol(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _CURRENT_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _NAME
    )
    {
        const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _AT
            , _PATH
        );

        const auto &    GAME = PACKAGE_FILE_UNIQUE->getGame( _NAME );

        return GAME.getInitializer();
    }

    auto createInitializerSymbol(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _NAME
    )
    {
        const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
            _SYSTEM_DIRECTORY
            , _AT
            , _PATH
        );

        const auto &    GAME = PACKAGE_FILE_UNIQUE->getGame( _NAME );

        return GAME.getInitializer();
    }
}

namespace candymaker {
    GameContextManager::GameContextManager(
        ModuleContexts::Unique &&   _moduleContextsUnique
        , FgState &                 _state
        , const std::string &       _SAVE_DIRECTORY_PATH
        , const std::string &       _INITIALIZER_SYMBOL
    )
        : moduleContextsUnique( std::move( _moduleContextsUnique ) )
        , gameContext(
            _state
            , this->moduleContextsUnique->getMainModuleContext()
            , _SAVE_DIRECTORY_PATH
        )
    {
        const auto  INITIALIZER_PROC_PTR = this->moduleContextsUnique->getProc< void ( * )( FgGameContext & ) >( _INITIALIZER_SYMBOL );

        INITIALIZER_PROC_PTR( this->gameContext );
    }

    GameContextManager::Unique GameContextManager::create(
        const ExecuteFile &     _EXECUTE_FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , const std::string &   _CURRENT_DIRECTORY
        , ModuleManager &       _moduleManager
        , FgState &             _state
    )
    {
        const auto &    GAME = _EXECUTE_FILE.game;

        const auto  SAVE_DIRECTORY_PATH = GAME.relativeSaveDirectoryPathToString();

        auto    moduleContextsUnique = createModuleContexts(
            _moduleManager
            , _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _EXECUTE_FILE.packages
            , GAME.at
            , GAME.package
            , GAME.name
        );

        const auto  INITIALIZER_SYMBOL = createInitializerSymbol(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , GAME.at
            , GAME.package
            , GAME.name
        );

        return new GameContextManager(
            std::move( moduleContextsUnique )
            , _state
            , SAVE_DIRECTORY_PATH
            , INITIALIZER_SYMBOL
        );
    }

    GameContextManager::Unique GameContextManager::create(
        const ExecuteFile &     _EXECUTE_FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , ModuleManager &       _moduleManager
        , FgState &             _state
    )
    {
        const auto &    GAME = _EXECUTE_FILE.game;

        const auto  SAVE_DIRECTORY_PATH = GAME.relativeSaveDirectoryPathToString();

        auto    moduleContextsUnique = createModuleContexts(
            _moduleManager
            , _SYSTEM_DIRECTORY
            , _EXECUTE_FILE.packages
            , GAME.at
            , GAME.package
            , GAME.name
        );

        const auto  INITIALIZER_SYMBOL = createInitializerSymbol(
            _SYSTEM_DIRECTORY
            , GAME.at
            , GAME.package
            , GAME.name
        );

        return new GameContextManager(
            std::move( moduleContextsUnique )
            , _state
            , SAVE_DIRECTORY_PATH
            , INITIALIZER_SYMBOL
        );
    }

    void GameContextManager::start(
    )
    {
        this->gameContext.start();
    }
}
