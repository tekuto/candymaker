﻿#include "fg/util/test.h"
#include "candymaker/main/statemanager.h"

TEST(
    StateManagerTest
    , GetBasesystemState
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateManagerUnique = candymaker::StateManager::create( threadCache );
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    const auto &    STATE = stateManagerUnique->getBasesystemState();
    ASSERT_EQ( nullptr, STATE.prevStatePtr );
    ASSERT_TRUE( STATE.disabled );
    ASSERT_EQ( nullptr, STATE.dataPtr );
    ASSERT_NE( nullptr, STATE.nextStateUnique.get() );
}

TEST(
    StateManagerTest
    , GetGameState
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateManagerUnique = candymaker::StateManager::create( threadCache );
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    const auto &    STATE = stateManagerUnique->getGameState();
    ASSERT_EQ( &( stateManagerUnique->getBasesystemState() ), STATE.prevStatePtr );
    ASSERT_TRUE( STATE.disabled );
    ASSERT_EQ( nullptr, STATE.dataPtr );
    ASSERT_EQ( nullptr, STATE.nextStateUnique.get() );
}
