﻿#include "candymaker/main/args.h"

#include <string>
#include <utility>
#include <stdexcept>

namespace {
    const auto  OPTION_SYSTEM_DIRECTORY_PATH = std::string( "--system-directory-path" );
    const auto  OPTION_HOME_DIRECTORY_PATH = std::string( "--home-directory-path" );
}

namespace candymaker {
    Args::Args(
        bool                _existsShortcutFilePath
        , std::string &&    _shortcutFilePath
        , bool              _existsSystemDirectoryPath
        , std::string &&    _systemDirectoryPath
        , bool              _existsHomeDirectoryPath
        , std::string &&    _homeDirectoryPath
    )
        : existsShortcutFilePath( _existsShortcutFilePath )
        , shortcutFilePath( std::move( _shortcutFilePath ) )
        , existsSystemDirectoryPath( _existsSystemDirectoryPath )
        , systemDirectoryPath( std::move( _systemDirectoryPath ) )
        , existsHomeDirectoryPath( _existsHomeDirectoryPath )
        , homeDirectoryPath( std::move( _homeDirectoryPath ) )
    {
    }

    Args::ConstUnique Args::create(
        int         _argc
        , char **   _argv
    )
    {
        auto    optionSystemDirectoryPath = false;
        auto    optionHomeDirectoryPath = false;

        auto    existsShortcutFilePath = false;
        auto    shortcutFilePath = std::string();
        auto    existsSystemDirectoryPath = false;
        auto    systemDirectoryPath = std::string();
        auto    existsHomeDirectoryPath = false;
        auto    homeDirectoryPath = std::string();

        for( auto i = 1 ; i < _argc ; i++ ) {
            const auto  ARG = _argv[ i ];

            if( ARG == OPTION_SYSTEM_DIRECTORY_PATH ) {
                optionSystemDirectoryPath = true;
            } else if( ARG == OPTION_HOME_DIRECTORY_PATH ) {
                optionHomeDirectoryPath = true;
            } else if( optionSystemDirectoryPath == true ) {
                existsSystemDirectoryPath = true;
                systemDirectoryPath = ARG;

                optionSystemDirectoryPath = false;
            } else if( optionHomeDirectoryPath == true ) {
                existsHomeDirectoryPath = true;
                homeDirectoryPath = ARG;

                optionHomeDirectoryPath = false;
            } else {
                existsShortcutFilePath = true;
                shortcutFilePath = ARG;
            }
        }

        if( optionSystemDirectoryPath == true ) {
            throw std::runtime_error( "システムディレクトリパス指定が不正" );
        }

        if( optionHomeDirectoryPath == true ) {
            throw std::runtime_error( "ホームディレクトリパス指定が不正" );
        }

        return new Args(
            std::move( existsShortcutFilePath )
            , std::move( shortcutFilePath )
            , std::move( existsSystemDirectoryPath )
            , std::move( systemDirectoryPath )
            , std::move( existsHomeDirectoryPath )
            , std::move( homeDirectoryPath )
        );
    }

    bool Args::isExistsShortcutFilePath(
    ) const
    {
        return this->existsShortcutFilePath;
    }

    const std::string & Args::getShortcutFilePath(
    ) const
    {
        if( this->existsShortcutFilePath == false ) {
            throw std::runtime_error( "ショートカットファイルパスが存在しない" );
        }

        return this->shortcutFilePath;
    }

    bool Args::isExistsSystemDirectoryPath(
    ) const
    {
        return this->existsSystemDirectoryPath;
    }

    const std::string & Args::getSystemDirectoryPath(
    ) const
    {
        if( this->existsSystemDirectoryPath == false ) {
            throw std::runtime_error( "システムディレクトリパスが存在しない" );
        }

        return this->systemDirectoryPath;
    }

    bool Args::isExistsHomeDirectoryPath(
    ) const
    {
        return this->existsHomeDirectoryPath;
    }

    const std::string & Args::getHomeDirectoryPath(
    ) const
    {
        if( this->existsHomeDirectoryPath == false ) {
            throw std::runtime_error( "ホームディレクトリパスが存在しない" );
        }

        return this->homeDirectoryPath;
    }
}
