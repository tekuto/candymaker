﻿#include "fg/util/test.h"
#include "candymaker/main/args.h"

#include <vector>
#include <string>

auto createArgs(
    const std::vector< std::string > &  _ARGS
)
{
    auto    argsPtrs = std::vector< char * >();

    for( const auto & ARG : _ARGS ) {
        argsPtrs.push_back( const_cast< char * >( ARG.c_str() ) );
    }

    return candymaker::Args::create(
        argsPtrs.size()
        , argsPtrs.data()
    );
}

auto createArgs(
)
{
    return createArgs(
        {
            "execute",
            "--system-directory-path",
            "systemdirectorypath_dummy",
            "--system-directory-path",
            "systemdirectorypath",
            "--home-directory-path",
            "homedirectorypath_dummy",
            "--home-directory-path",
            "homedirectorypath",
            "shortcutfilepath_dummy",
            "shortcutfilepath",
        }
    );
}

auto createArgs2(
)
{
    return createArgs(
        {
            "execute",
            "--system-directory-path",
            "systemdirectorypath2_dummy",
            "--system-directory-path",
            "systemdirectorypath2",
            "--home-directory-path",
            "homedirectorypath2_dummy",
            "--home-directory-path",
            "homedirectorypath2",
            "shortcutfilepath2_dummy",
            "shortcutfilepath2",
        }
    );
}

auto createArgsWithoutShortcutFilePath(
)
{
    return createArgs(
        {
            "execute",
            "--system-directory-path",
            "systemdirectorypath_dummy",
            "--system-directory-path",
            "systemdirectorypath",
            "--home-directory-path",
            "homedirectorypath_dummy",
            "--home-directory-path",
            "homedirectorypath",
        }
    );
}

auto createArgsWithoutSystemDirectoryPath(
)
{
    return createArgs(
        {
            "execute",
            "--home-directory-path",
            "homedirectorypath_dummy",
            "--home-directory-path",
            "homedirectorypath",
            "shortcutfilepath_dummy",
            "shortcutfilepath",
        }
    );
}

auto createArgsWithoutHomeDirectoryPath(
)
{
    return createArgs(
        {
            "execute",
            "--system-directory-path",
            "systemdirectorypath_dummy",
            "--system-directory-path",
            "systemdirectorypath",
            "shortcutfilepath_dummy",
            "shortcutfilepath",
        }
    );
}

TEST(
    ArgsTest
    , Create_failedSystemDirectoryPath
)
{
    try {
        createArgs(
            {
                "execute",
                "--system-directory-path",
                "systemdirectorypath",
                "--system-directory-path",
            }
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ArgsTest
    , Create_failedHomeDirectoryPath
)
{
    try {
        createArgs(
            {
                "execute",
                "--home-directory-path",
                "homedirectorypath",
                "--home-directory-path",
            }
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ArgsTest
    , IsExistsShortcutFilePath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_TRUE( ARGS_UNIQUE->isExistsShortcutFilePath() );
}

TEST(
    ArgsTest
    , IsExistsShortcutFilePath_false
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutShortcutFilePath();

    ASSERT_FALSE( ARGS_UNIQUE->isExistsShortcutFilePath() );
}

TEST(
    ArgsTest
    , GetShortcutFilePath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_STREQ( "shortcutfilepath", ARGS_UNIQUE->getShortcutFilePath().c_str() );
}

TEST(
    ArgsTest
    , GetShortcutFilePath2
)
{
    const auto  ARGS_UNIQUE = createArgs2();

    ASSERT_STREQ( "shortcutfilepath2", ARGS_UNIQUE->getShortcutFilePath().c_str() );
}

TEST(
    ArgsTest
    , GetShortcutFilePath_failedNotExists
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutShortcutFilePath();

    try {
        ARGS_UNIQUE->getShortcutFilePath();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ArgsTest
    , IsExistsSystemDirectoryPath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_TRUE( ARGS_UNIQUE->isExistsSystemDirectoryPath() );
}

TEST(
    ArgsTest
    , IsExistsSystemDirectoryPath_false
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutSystemDirectoryPath();

    ASSERT_FALSE( ARGS_UNIQUE->isExistsSystemDirectoryPath() );
}

TEST(
    ArgsTest
    , GetSystemDirectoryPath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_STREQ( "systemdirectorypath", ARGS_UNIQUE->getSystemDirectoryPath().c_str() );
}

TEST(
    ArgsTest
    , GetSystemDirectoryPath2
)
{
    const auto  ARGS_UNIQUE = createArgs2();

    ASSERT_STREQ( "systemdirectorypath2", ARGS_UNIQUE->getSystemDirectoryPath().c_str() );
}

TEST(
    ArgsTest
    , GetSystemDirectoryPath_failedNotExists
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutSystemDirectoryPath();

    try {
        ARGS_UNIQUE->getSystemDirectoryPath();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ArgsTest
    , IsExistsHomeDirectoryPath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_TRUE( ARGS_UNIQUE->isExistsHomeDirectoryPath() );
}

TEST(
    ArgsTest
    , IsExistsHomeDirectoryPath_false
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutHomeDirectoryPath();

    ASSERT_FALSE( ARGS_UNIQUE->isExistsHomeDirectoryPath() );
}

TEST(
    ArgsTest
    , GetHomeDirectoryPath
)
{
    const auto  ARGS_UNIQUE = createArgs();

    ASSERT_STREQ( "homedirectorypath", ARGS_UNIQUE->getHomeDirectoryPath().c_str() );
}

TEST(
    ArgsTest
    , GetHomeDirectoryPath2
)
{
    const auto  ARGS_UNIQUE = createArgs2();

    ASSERT_STREQ( "homedirectorypath2", ARGS_UNIQUE->getHomeDirectoryPath().c_str() );
}

TEST(
    ArgsTest
    , GetHomeDirectoryPath_failedNotExists
)
{
    const auto  ARGS_UNIQUE = createArgsWithoutHomeDirectoryPath();

    try {
        ARGS_UNIQUE->getHomeDirectoryPath();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
