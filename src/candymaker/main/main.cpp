﻿#include "fg/util/export.h"
#include "candymaker/main/main.h"
#include "candymaker/main/args.h"
#include "candymaker/main/statemanager.h"
#include "candymaker/main/basesystemcontextmanager.h"
#include "candymaker/main/gamecontextmanager.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/file/shortcut.h"
#include "candymaker/core/file/path.h"

#include <string>
#include <cstdlib>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  DEFAULT_SYSTEM_DIRECTORY_PATH = std::string( "/usr/lib/candymaker/" );
    const auto  DEFAULT_HOME_DIRECTORY_PATH = std::string( "/candymaker/" );

    auto createSystemDirectoryPath(
        const candymaker::Args &    _ARGS
    )
    {
        if( _ARGS.isExistsSystemDirectoryPath() == true ) {
            return candymaker::createDirectoryPath( _ARGS.getSystemDirectoryPath() );
        } else {
            return DEFAULT_SYSTEM_DIRECTORY_PATH;
        }
    }

    auto createHomeDirectoryPath(
        const candymaker::Args &    _ARGS
    )
    {
        if( _ARGS.isExistsHomeDirectoryPath() == true ) {
            return candymaker::createDirectoryPath( _ARGS.getHomeDirectoryPath() );
        } else {
            const auto  HOME_PATH = std::string( std::getenv( "HOME" ) );

            auto    homeDirectory = HOME_PATH + DEFAULT_HOME_DIRECTORY_PATH;

            return homeDirectory;
        }
    }
}

namespace candymaker {
    bool main(
        int         _argc
        , char **   _argv
    )
    {
        const auto  ARGS_UNIQUE = Args::create(
            _argc
            , _argv
        );
        const auto &    ARGS = *ARGS_UNIQUE;

        if( ARGS.isExistsShortcutFilePath() == false ) {
#ifdef  DEBUG
            std::printf(
                "使い方: %s ショートカットファイルパス\n"
                , _argv[ 0 ]
            );
#endif  // DEBUG

            return 1;
        }

        const auto &    SHORTCUT_FILE_PATH = ARGS.getShortcutFilePath();
#ifdef  DEBUG
        std::printf(
            "D:ショートカットファイルパス: %s\n"
            , SHORTCUT_FILE_PATH.c_str()
        );
#endif  // DEBUG

        const auto  SYSTEM_DIRECTORY_PATH = createSystemDirectoryPath( ARGS );
        const auto  HOME_DIRECTORY_PATH = createHomeDirectoryPath( ARGS );

        const auto      SHORTCUT_FILE_UNIQUE = ShortcutFile::create( SHORTCUT_FILE_PATH );
        const auto &    SHORTCUT_FILE = *SHORTCUT_FILE_UNIQUE;

        auto    threadCacheUnique = ThreadCache::create();
        auto &  threadCache = *threadCacheUnique;

        auto    moduleManagerUnique = ModuleManager::create( HOME_DIRECTORY_PATH );
        auto &  moduleManager = *moduleManagerUnique;

        auto    basesystemContextManagerUnique = BasesystemContextManager::Unique();
        auto    gameContextManagerUnique = GameContextManager::Unique();

        auto    stateManagerUnique = StateManager::create( threadCache );

        const auto  RELATIVE_EXECUTE_FILE_PATH = SHORTCUT_FILE.createExecuteFilePath();

        if( SHORTCUT_FILE.atSystem() == true ) {
            const auto  EXECUTE_FILE_PATH = SYSTEM_DIRECTORY_PATH + RELATIVE_EXECUTE_FILE_PATH;

            const auto      EXECUTE_FILE_UNIQUE = ExecuteFile::create(
                EXECUTE_FILE_PATH
                , SYSTEM_DIRECTORY_PATH
            );
            const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

            basesystemContextManagerUnique = BasesystemContextManager::create(
                EXECUTE_FILE
                , SYSTEM_DIRECTORY_PATH
                , moduleManager
                , stateManagerUnique->getBasesystemState()
            );

            gameContextManagerUnique = GameContextManager::create(
                EXECUTE_FILE
                , SYSTEM_DIRECTORY_PATH
                , moduleManager
                , stateManagerUnique->getGameState()
            );
        } else {
            const auto  CURRENT_DIRECTORY_PATH = SHORTCUT_FILE.atHome() == true
                ? HOME_DIRECTORY_PATH
                : SHORTCUT_FILE.createCurrentDirectory( SHORTCUT_FILE_PATH )
            ;

            const auto  EXECUTE_FILE_PATH = CURRENT_DIRECTORY_PATH + RELATIVE_EXECUTE_FILE_PATH;

            const auto      EXECUTE_FILE_UNIQUE = ExecuteFile::create(
                EXECUTE_FILE_PATH
                , SYSTEM_DIRECTORY_PATH
                , CURRENT_DIRECTORY_PATH
            );
            const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

            basesystemContextManagerUnique = BasesystemContextManager::create(
                EXECUTE_FILE
                , SYSTEM_DIRECTORY_PATH
                , CURRENT_DIRECTORY_PATH
                , moduleManager
                , stateManagerUnique->getBasesystemState()
            );

            gameContextManagerUnique = GameContextManager::create(
                EXECUTE_FILE
                , SYSTEM_DIRECTORY_PATH
                , CURRENT_DIRECTORY_PATH
                , moduleManager
                , stateManagerUnique->getGameState()
            );
        }

        gameContextManagerUnique->start();

        basesystemContextManagerUnique->waitEnd();

        return 0;
    }
}
