﻿#include "fg/util/test.h"
#include "candymaker/main/module.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/module/contexts.h"
#include "candymaker/core/module/context.h"

#include <string>
#include <algorithm>

void testModule(
    const candymaker::ModuleManager::ModuleMap &    _MODULE_MAP
    , const std::string &                           _PACKAGE_PATH
    , const std::string &                           _MODULE_PATH
    , int                                           _useCount
    , const std::string &                           _HOME_DIRECTORY_PATH
    , const std::string &                           _SAVE_DIRECTORY_PATH
)
{
    const auto  END = _MODULE_MAP.end();
    const auto  IT = std::find_if(
        _MODULE_MAP.begin()
        , END
        , [
            &_PACKAGE_PATH
            , &_MODULE_PATH
        ]
        (
            const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
        )
        {
            const auto &    KEY = _PAIR.first;

            if( KEY.PACKAGE_PATH != _PACKAGE_PATH ) {
                return false;
            }

            if( KEY.MODULE_PATH != _MODULE_PATH ) {
                return false;
            }

            return true;
        }
    );
    ASSERT_NE( END, IT );

    ASSERT_EQ( _useCount, IT->second.use_count() );
    const auto &    MODULE_CONTEXT = *( IT->second );
    ASSERT_STREQ( _HOME_DIRECTORY_PATH.c_str(), MODULE_CONTEXT.HOME_DIRECTORY_PATH.c_str() );
    ASSERT_STREQ( _SAVE_DIRECTORY_PATH.c_str(), MODULE_CONTEXT.SAVE_DIRECTORY_PATH.c_str() );
}

TEST(
    MainModuleTest
    , Create_currentPackage
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_currentpackage.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "current"
        , {
            "package",
        }
        , "mainmoduletest"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "currentPackageSavePath1/currentPackageSavePath2/"
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , Create_systemPackage
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_systempackage.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "system"
        , {
            "package",
        }
        , "mainmoduletest"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/system/packages/package/"
            , "mainmoduletest/system/packages/package/libmainmoduletest.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "systemPackageSavePath1/systemPackageSavePath2/"
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , Create_absoluteModulePath
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_absolutemodulepath.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "current"
        , {
            "package",
        }
        , "mainmoduletest_absolutemodulepath"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "/usr/lib64/libpthread.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , Create_withInitializer
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_withinitializer.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "current"
        , {
            "package",
        }
        , "mainmoduletest_withinitializer"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest_withinitializer.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );

    const auto  GET_DATA_PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 10, GET_DATA_PROC_PTR() );
}

TEST(
    MainModuleTest
    , Create_withDependModules
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_withdependmodules.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "current"
        , {
            "package",
        }
        , "mainmoduletest_withdependmodules"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 2, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest_withdependmodules.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest_dependmodule.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );

    ASSERT_EQ( 2, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , Create_withDependLocalModules
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_withdependlocalmodules.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , "mainmoduletest/"
        , EXECUTE_FILE_UNIQUE->packages
        , "current"
        , {
            "package",
        }
        , "mainmoduletest_withdependlocalmodules"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 2, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest_withdependlocalmodules.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/packages/package/"
            , "mainmoduletest/packages/package/libmainmoduletest_dependlocalmodule.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "package/"
        )
    );

    ASSERT_EQ( 2, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , Create_failedIlligalPackageAt
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_failedilligalpackageat.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    try {
        candymaker::createModuleContexts(
            moduleManager
            , "mainmoduletest/system/"
            , "mainmoduletest/"
            , EXECUTE_FILE_UNIQUE->packages
            , "current"
            , {
                "package",
            }
            , "mainmoduletest"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    MainModuleTest
    , Create_failedIlligalModuleAt
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_failedilligalmoduleat.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    try {
        candymaker::createModuleContexts(
            moduleManager
            , "mainmoduletest/system/"
            , "mainmoduletest/"
            , EXECUTE_FILE_UNIQUE->packages
            , "current"
            , {
                "package",
            }
            , "mainmoduletest_failedilligalmoduleat"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    MainModuleTest
    , Create_failedDisableModule
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_faileddisablemodule.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    try {
        candymaker::createModuleContexts(
            moduleManager
            , "mainmoduletest/system/"
            , "mainmoduletest/"
            , EXECUTE_FILE_UNIQUE->packages
            , "current"
            , {
                "package",
            }
            , "mainmoduletest"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    MainModuleTest
    , Create_failedConflictDepend
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_failedconflictdepend.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    try {
        candymaker::createModuleContexts(
            moduleManager
            , "mainmoduletest/system/"
            , "mainmoduletest/"
            , EXECUTE_FILE_UNIQUE->packages
            , "current"
            , {
                "package",
            }
            , "mainmoduletest_withdependmodules"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    MainModuleTest
    , CreateWithoutCurrentDirectory
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_systempackage.fge"
        , "mainmoduletest/system/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    auto    moduleContextsUnique = candymaker::createModuleContexts(
        moduleManager
        , "mainmoduletest/system/"
        , EXECUTE_FILE_UNIQUE->packages
        , "system"
        , {
            "package",
        }
        , "mainmoduletest"
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModule(
            MODULE_MAP
            , "mainmoduletest/system/packages/package/"
            , "mainmoduletest/system/packages/package/libmainmoduletest.so"
            , 2
            , "HOME_DIRECTORY_PATH"
            , "systemPackageSavePath1/systemPackageSavePath2/"
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );
}

TEST(
    MainModuleTest
    , CreateWithoutCurrentDirectory_failedExistsCurrentModule
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "mainmoduletest/mainmoduletest_currentpackage.fge"
        , "mainmoduletest/system/"
        , "mainmoduletest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    try {
        candymaker::createModuleContexts(
            moduleManager
            , "mainmoduletest/system/"
            , EXECUTE_FILE_UNIQUE->packages
            , "current"
            , {
                "package",
            }
            , "mainmoduletest"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
