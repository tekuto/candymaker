﻿#include "candymaker/main/basesystemcontextmanager.h"
#include "candymaker/main/module.h"
#include "candymaker/main/packagefile.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/module/contexts.h"

#include <string>
#include <utility>

namespace {
    auto createInitializerSymbol(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _CURRENT_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _NAME
    )
    {
        const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _AT
            , _PATH
        );

        const auto &    BASESYSTEM = PACKAGE_FILE_UNIQUE->getBasesystem( _NAME );

        return BASESYSTEM.getInitializer();
    }

    auto createInitializerSymbol(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _NAME
    )
    {
        const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
            _SYSTEM_DIRECTORY
            , _AT
            , _PATH
        );

        const auto &    BASESYSTEM = PACKAGE_FILE_UNIQUE->getBasesystem( _NAME );

        return BASESYSTEM.getInitializer();
    }
}

namespace candymaker {
    BasesystemContextManager::BasesystemContextManager(
        ModuleContexts::Unique &&   _moduleContextsUnique
        , FgState &                 _state
        , const std::string &       _SAVE_DIRECTORY_PATH
        , const std::string &       _INITIALIZER_SYMBOL
    )
        : moduleContextsUnique( std::move( _moduleContextsUnique ) )
        , basesystemContext(
            _state
            , this->moduleContextsUnique->getMainModuleContext()
            , _SAVE_DIRECTORY_PATH
        )
    {
        const auto  INITIALIZER_PROC_PTR = this->moduleContextsUnique->getProc< void ( * )( FgBasesystemContext & ) >( _INITIALIZER_SYMBOL );

        INITIALIZER_PROC_PTR( this->basesystemContext );
    }

    BasesystemContextManager::Unique BasesystemContextManager::create(
        const ExecuteFile &     _EXECUTE_FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , const std::string &   _CURRENT_DIRECTORY
        , ModuleManager &       _moduleManager
        , FgState &             _state
    )
    {
        const auto &    BASESYSTEM = _EXECUTE_FILE.basesystem;

        const auto  SAVE_DIRECTORY_PATH = BASESYSTEM.relativeSaveDirectoryPathToString();

        auto    moduleContextsUnique = createModuleContexts(
            _moduleManager
            , _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _EXECUTE_FILE.packages
            , BASESYSTEM.at
            , BASESYSTEM.package
            , BASESYSTEM.name
        );

        const auto  INITIALIZER_SYMBOL = createInitializerSymbol(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , BASESYSTEM.at
            , BASESYSTEM.package
            , BASESYSTEM.name
        );

        return new BasesystemContextManager(
            std::move( moduleContextsUnique )
            , _state
            , SAVE_DIRECTORY_PATH
            , INITIALIZER_SYMBOL
        );
    }

    BasesystemContextManager::Unique BasesystemContextManager::create(
        const ExecuteFile &     _EXECUTE_FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , ModuleManager &       _moduleManager
        , FgState &             _state
    )
    {
        const auto &    BASESYSTEM = _EXECUTE_FILE.basesystem;

        const auto  SAVE_DIRECTORY_PATH = BASESYSTEM.relativeSaveDirectoryPathToString();

        auto    moduleContextsUnique = createModuleContexts(
            _moduleManager
            , _SYSTEM_DIRECTORY
            , _EXECUTE_FILE.packages
            , BASESYSTEM.at
            , BASESYSTEM.package
            , BASESYSTEM.name
        );

        const auto  INITIALIZER_SYMBOL = createInitializerSymbol(
            _SYSTEM_DIRECTORY
            , BASESYSTEM.at
            , BASESYSTEM.package
            , BASESYSTEM.name
        );

        return new BasesystemContextManager(
            std::move( moduleContextsUnique )
            , _state
            , SAVE_DIRECTORY_PATH
            , INITIALIZER_SYMBOL
        );
    }

    void BasesystemContextManager::waitEnd(
    )
    {
        this->basesystemContext.waitEnd();
    }
}
