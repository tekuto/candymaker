﻿#include "candymaker/main/statemanager.h"
#include "candymaker/core/state/state.h"

#include <utility>

namespace {
    void dummyDestroy(
        void *
    )
    {
    }

    auto createState(
        candymaker::ThreadCache &   _threadCache
    )
    {
        auto    basesystemStateUnique = FgState::create(
            _threadCache
            , nullptr
            , true
            , nullptr
            , dummyDestroy
        );
        auto &  basesystemState = *basesystemStateUnique;

        auto    gameStateUnique = FgState::create(
            _threadCache
            , &basesystemState
            , true
            , nullptr
            , dummyDestroy
        );

        basesystemState.setNextState( std::move( gameStateUnique ) );

        return basesystemStateUnique;
    }
}

namespace candymaker {
    StateManager::StateManager(
        ThreadCache &   _threadCache
    )
        : stateUnique( createState( _threadCache ) )
    {
    }

    StateManager::Unique StateManager::create(
        ThreadCache &   _threadCache
    )
    {
        return new StateManager( _threadCache );
    }

    FgState & StateManager::getBasesystemState(
    )
    {
        return *( this->stateUnique );
    }

    FgState & StateManager::getGameState(
    )
    {
        return this->stateUnique->getNextState();
    }
}
