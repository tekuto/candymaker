﻿#include "fg/util/test.h"
#include "candymaker/main/basesystemcontextmanager.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/state/state.h"
#include "fg/common/unique.h"

#include <string>
#include <algorithm>

struct BasesystemContextManagerTest : public fg::UniqueWrapper< BasesystemContextManagerTest >
{
    candymaker::ModuleContexts::Unique  moduleContextsUnique;
    FgBasesystemContext                 basesystemContext;
};

void testModuleUseCount(
    const candymaker::ModuleManager::ModuleMap &    _MODULE_MAP
    , const std::string &                           _PACKAGE_PATH
    , const std::string &                           _MODULE_PATH
    , int                                           _useCount
)
{
    const auto  END = _MODULE_MAP.end();
    const auto  IT = std::find_if(
        _MODULE_MAP.begin()
        , END
        , [
            &_PACKAGE_PATH
            , &_MODULE_PATH
        ]
        (
            const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
        )
        {
            const auto &    KEY = _PAIR.first;

            if( KEY.PACKAGE_PATH != _PACKAGE_PATH ) {
                return false;
            }

            if( KEY.MODULE_PATH != _MODULE_PATH ) {
                return false;
            }

            return true;
        }
    );
    ASSERT_NE( END, IT );

    ASSERT_EQ( _useCount, IT->second.use_count() );
}

TEST(
    BasesystemContextManagerTest
    , Create
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "basesystemcontextmanagertest/createtest.fge"
        , "basesystemcontextmanagertest/system/"
        , "basesystemcontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextManagerUnique = candymaker::BasesystemContextManager::create(
        EXECUTE_FILE
        , "basesystemcontextmanagertest/system/"
        , "basesystemcontextmanagertest/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, basesystemContextManagerUnique.get() );
    auto &  basesystemContextManager = reinterpret_cast< BasesystemContextManagerTest & >( *basesystemContextManagerUnique );

    auto &  moduleContextsUnique = basesystemContextManager.moduleContextsUnique;
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleUseCount(
            MODULE_MAP
            , "basesystemcontextmanagertest/packages/package/"
            , "basesystemcontextmanagertest/packages/package/libcreatetest.so"
            , 2
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );

    const auto  GET_DATA_PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );

    ASSERT_EQ( 10, GET_DATA_PROC_PTR() );

    const auto &    BASESYSTEM_CONTEXT = basesystemContextManager.basesystemContext;
    ASSERT_STREQ( "basesystemSavePath1/basesystemSavePath2/", BASESYSTEM_CONTEXT.getSaveDirectoryPath().c_str() );
}

TEST(
    BasesystemContextManagerTest
    , CreateWithoutCurrentDirectory
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "basesystemcontextmanagertest/createtest_withoutcurrentdirectory.fge"
        , "basesystemcontextmanagertest/system/"
        , "basesystemcontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextManagerUnique = candymaker::BasesystemContextManager::create(
        EXECUTE_FILE
        , "basesystemcontextmanagertest/system/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, basesystemContextManagerUnique.get() );
    auto &  basesystemContextManager = reinterpret_cast< BasesystemContextManagerTest & >( *basesystemContextManagerUnique );

    auto &  moduleContextsUnique = basesystemContextManager.moduleContextsUnique;
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleUseCount(
            MODULE_MAP
            , "basesystemcontextmanagertest/system/packages/package/"
            , "basesystemcontextmanagertest/system/packages/package/libcreatetest.so"
            , 2
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );

    const auto  GET_DATA_PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );

    ASSERT_EQ( 20, GET_DATA_PROC_PTR() );

    const auto &    BASESYSTEM_CONTEXT = basesystemContextManager.basesystemContext;
    ASSERT_STREQ( "package/", BASESYSTEM_CONTEXT.getSaveDirectoryPath().c_str() );
}

void waitEndTestProc(
    int &   _i
)
{
    _i = 20;
}

TEST(
    BasesystemContextManagerTest
    , WaitEnd
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "basesystemcontextmanagertest/waitendtest.fge"
        , "basesystemcontextmanagertest/system/"
        , "basesystemcontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextManagerUnique = candymaker::BasesystemContextManager::create(
        EXECUTE_FILE
        , "basesystemcontextmanagertest/system/"
        , "basesystemcontextmanagertest/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, basesystemContextManagerUnique.get() );
    auto &  basesystemContextManager = reinterpret_cast< BasesystemContextManagerTest & >( *basesystemContextManagerUnique );

    auto    i = 10;

    auto &  basesystemContext = reinterpret_cast< fg::BasesystemContext< int > & >( basesystemContextManager.basesystemContext );
    basesystemContext.setWaitEndProc(
        waitEndTestProc
        , i
    );

    basesystemContextManagerUnique->waitEnd();

    ASSERT_EQ( 20, i );
}
