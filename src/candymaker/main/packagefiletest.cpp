﻿#include "fg/util/test.h"
#include "candymaker/main/packagefile.h"

TEST(
    MainPackageFileTest
    , CreatePackageDirectoryString_currentPackage
)
{
    ASSERT_STREQ(
        "mainpackagefiletest/packages/package/"
        , candymaker::createPackageDirectoryString(
            "mainpackagefiletest/system/"
            , "mainpackagefiletest/"
            , "current"
            , {
                "package"
            }
        ).c_str()
    );
}

TEST(
    MainPackageFileTest
    , CreatePackageDirectoryString_systemPackage
)
{
    ASSERT_STREQ(
        "mainpackagefiletest/system/packages/package/"
        , candymaker::createPackageDirectoryString(
            "mainpackagefiletest/system/"
            , "mainpackagefiletest/"
            , "system"
            , {
                "package"
            }
        ).c_str()
    );
}

TEST(
    MainPackageFileTest
    , CreatePackageDirectoryStringWithoutCurrentDirectory
)
{
    ASSERT_STREQ(
        "mainpackagefiletest/system/packages/package/"
        , candymaker::createPackageDirectoryString(
            "mainpackagefiletest/system/"
            , "system"
            , {
                "package"
            }
        ).c_str()
    );
}

TEST(
    MainPackageFileTest
    , CreatePackageDirectoryStringWithoutCurrentDirectory_failedCurrentPackage
)
{
    try {
        candymaker::createPackageDirectoryString(
            "mainpackagefiletest/system/"
            , "current"
            , {
                "package"
            }
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    MainPackageFileTest
    , Create
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile( "mainpackagefiletest/packages/package/" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 2, MODULES.size() );

    const auto  MODULES_END = MODULES.end();

    const auto  MODULES_IT1 = MODULES.find( "currentModule1" );
    ASSERT_NE( MODULES_END, MODULES_IT1 );
    const auto &    MODULE1 = MODULES_IT1->second;

    ASSERT_STREQ( "relative", MODULE1.at.c_str() );

    const auto &    PATH1 = MODULE1.path;
    ASSERT_EQ( 2, PATH1.size() );
    ASSERT_STREQ( "currentModulePath1_1", PATH1[ 0 ].c_str() );
    ASSERT_STREQ( "currentModulePath1_2", PATH1[ 1 ].c_str() );

    ASSERT_TRUE( MODULE1.existsInitializer );
    ASSERT_STREQ( "currentModuleInitializer1", MODULE1.initializer.c_str() );

    const auto &    INTERFACES1 = MODULE1.interfaces;
    ASSERT_EQ( 2, INTERFACES1.size() );
    const auto &    INTERFACE1_1 = INTERFACES1[ 0 ];
    ASSERT_STREQ( "currentModuleInterfacePackage1_1", INTERFACE1_1.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule1_1", INTERFACE1_1.module.c_str() );
    const auto &    INTERFACE1_2 = INTERFACES1[ 1 ];
    ASSERT_STREQ( "currentModuleInterfacePackage1_2", INTERFACE1_2.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule1_2", INTERFACE1_2.module.c_str() );

    const auto &    DEPENDS1 = MODULE1.depends;
    ASSERT_EQ( 2, DEPENDS1.size() );
    const auto &    DEPEND1_1 = DEPENDS1[ 0 ];
    ASSERT_STREQ( "currentModuleDependPackage1_1", DEPEND1_1.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule1_1", DEPEND1_1.module.c_str() );
    const auto &    DEPEND1_2 = DEPENDS1[ 1 ];
    ASSERT_STREQ( "currentModuleDependPackage1_2", DEPEND1_2.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule1_2", DEPEND1_2.module.c_str() );

    const auto  MODULES_IT2 = MODULES.find( "currentModule2" );
    ASSERT_NE( MODULES_END, MODULES_IT2 );
    const auto &    MODULE2 = MODULES_IT2->second;

    ASSERT_STREQ( "absolute", MODULE2.at.c_str() );

    const auto &    PATH2 = MODULE2.path;
    ASSERT_EQ( 2, PATH2.size() );
    ASSERT_STREQ( "currentModulePath2_1", PATH2[ 0 ].c_str() );
    ASSERT_STREQ( "currentModulePath2_2", PATH2[ 1 ].c_str() );

    ASSERT_TRUE( MODULE2.existsInitializer );
    ASSERT_STREQ( "currentModuleInitializer2", MODULE2.initializer.c_str() );

    const auto &    INTERFACES2 = MODULE2.interfaces;
    ASSERT_EQ( 2, INTERFACES2.size() );
    const auto &    INTERFACE2_1 = INTERFACES2[ 0 ];
    ASSERT_STREQ( "currentModuleInterfacePackage2_1", INTERFACE2_1.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule2_1", INTERFACE2_1.module.c_str() );
    const auto &    INTERFACE2_2 = INTERFACES2[ 1 ];
    ASSERT_STREQ( "currentModuleInterfacePackage2_2", INTERFACE2_2.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule2_2", INTERFACE2_2.module.c_str() );

    const auto &    DEPENDS2 = MODULE2.depends;
    ASSERT_EQ( 2, DEPENDS2.size() );
    const auto &    DEPEND2_1 = DEPENDS2[ 0 ];
    ASSERT_STREQ( "currentModuleDependPackage2_1", DEPEND2_1.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule2_1", DEPEND2_1.module.c_str() );
    const auto &    DEPEND2_2 = DEPENDS2[ 1 ];
    ASSERT_STREQ( "currentModuleDependPackage2_2", DEPEND2_2.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule2_2", DEPEND2_2.module.c_str() );

    const auto &    BASESYSTEMS = PACKAGE_FILE_UNIQUE->basesystems;
    ASSERT_EQ( 2, BASESYSTEMS.size() );

    const auto  BASESYSTEMS_END = BASESYSTEMS.end();

    const auto  BASESYSTEMS_IT1 = BASESYSTEMS.find( "currentBasesystem1" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT1 );
    const auto &    BASESYSTEM1 = BASESYSTEMS_IT1->second;
    ASSERT_STREQ( "currentModule1", BASESYSTEM1.module.c_str() );
    ASSERT_STREQ( "currentBasesystemInitializer1", BASESYSTEM1.initializer.c_str() );

    const auto  BASESYSTEMS_IT2 = BASESYSTEMS.find( "currentBasesystem2" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT2 );
    const auto &    BASESYSTEM2 = BASESYSTEMS_IT2->second;
    ASSERT_STREQ( "currentModule2", BASESYSTEM2.module.c_str() );
    ASSERT_STREQ( "currentBasesystemInitializer2", BASESYSTEM2.initializer.c_str() );

    const auto &    GAMES = PACKAGE_FILE_UNIQUE->games;
    ASSERT_EQ( 2, GAMES.size() );

    const auto  GAMES_END = GAMES.end();

    const auto  GAMES_IT1 = GAMES.find( "currentGame1" );
    ASSERT_NE( GAMES_END, GAMES_IT1 );
    const auto &    GAME1 = GAMES_IT1->second;
    ASSERT_STREQ( "currentModule1", GAME1.module.c_str() );
    ASSERT_STREQ( "currentGameInitializer1", GAME1.initializer.c_str() );

    const auto  GAMES_IT2 = GAMES.find( "currentGame2" );
    ASSERT_NE( GAMES_END, GAMES_IT2 );
    const auto &    GAME2 = GAMES_IT2->second;
    ASSERT_STREQ( "currentModule2", GAME2.module.c_str() );
    ASSERT_STREQ( "currentGameInitializer2", GAME2.initializer.c_str() );
}

TEST(
    MainPackageFileTest
    , Create_currentPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
        "mainpackagefiletest/system/"
        , "mainpackagefiletest/"
        , "current"
        , {
            "package"
        }
    );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 2, MODULES.size() );

    const auto  MODULES_END = MODULES.end();

    const auto  MODULES_IT1 = MODULES.find( "currentModule1" );
    ASSERT_NE( MODULES_END, MODULES_IT1 );
    const auto &    MODULE1 = MODULES_IT1->second;

    ASSERT_STREQ( "relative", MODULE1.at.c_str() );

    const auto &    PATH1 = MODULE1.path;
    ASSERT_EQ( 2, PATH1.size() );
    ASSERT_STREQ( "currentModulePath1_1", PATH1[ 0 ].c_str() );
    ASSERT_STREQ( "currentModulePath1_2", PATH1[ 1 ].c_str() );

    ASSERT_TRUE( MODULE1.existsInitializer );
    ASSERT_STREQ( "currentModuleInitializer1", MODULE1.initializer.c_str() );

    const auto &    INTERFACES1 = MODULE1.interfaces;
    ASSERT_EQ( 2, INTERFACES1.size() );
    const auto &    INTERFACE1_1 = INTERFACES1[ 0 ];
    ASSERT_STREQ( "currentModuleInterfacePackage1_1", INTERFACE1_1.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule1_1", INTERFACE1_1.module.c_str() );
    const auto &    INTERFACE1_2 = INTERFACES1[ 1 ];
    ASSERT_STREQ( "currentModuleInterfacePackage1_2", INTERFACE1_2.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule1_2", INTERFACE1_2.module.c_str() );

    const auto &    DEPENDS1 = MODULE1.depends;
    ASSERT_EQ( 2, DEPENDS1.size() );
    const auto &    DEPEND1_1 = DEPENDS1[ 0 ];
    ASSERT_STREQ( "currentModuleDependPackage1_1", DEPEND1_1.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule1_1", DEPEND1_1.module.c_str() );
    const auto &    DEPEND1_2 = DEPENDS1[ 1 ];
    ASSERT_STREQ( "currentModuleDependPackage1_2", DEPEND1_2.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule1_2", DEPEND1_2.module.c_str() );

    const auto  MODULES_IT2 = MODULES.find( "currentModule2" );
    ASSERT_NE( MODULES_END, MODULES_IT2 );
    const auto &    MODULE2 = MODULES_IT2->second;

    ASSERT_STREQ( "absolute", MODULE2.at.c_str() );

    const auto &    PATH2 = MODULE2.path;
    ASSERT_EQ( 2, PATH2.size() );
    ASSERT_STREQ( "currentModulePath2_1", PATH2[ 0 ].c_str() );
    ASSERT_STREQ( "currentModulePath2_2", PATH2[ 1 ].c_str() );

    ASSERT_TRUE( MODULE2.existsInitializer );
    ASSERT_STREQ( "currentModuleInitializer2", MODULE2.initializer.c_str() );

    const auto &    INTERFACES2 = MODULE2.interfaces;
    ASSERT_EQ( 2, INTERFACES2.size() );
    const auto &    INTERFACE2_1 = INTERFACES2[ 0 ];
    ASSERT_STREQ( "currentModuleInterfacePackage2_1", INTERFACE2_1.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule2_1", INTERFACE2_1.module.c_str() );
    const auto &    INTERFACE2_2 = INTERFACES2[ 1 ];
    ASSERT_STREQ( "currentModuleInterfacePackage2_2", INTERFACE2_2.package.c_str() );
    ASSERT_STREQ( "currentModuleInterfaceModule2_2", INTERFACE2_2.module.c_str() );

    const auto &    DEPENDS2 = MODULE2.depends;
    ASSERT_EQ( 2, DEPENDS2.size() );
    const auto &    DEPEND2_1 = DEPENDS2[ 0 ];
    ASSERT_STREQ( "currentModuleDependPackage2_1", DEPEND2_1.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule2_1", DEPEND2_1.module.c_str() );
    const auto &    DEPEND2_2 = DEPENDS2[ 1 ];
    ASSERT_STREQ( "currentModuleDependPackage2_2", DEPEND2_2.package.c_str() );
    ASSERT_STREQ( "currentModuleDependModule2_2", DEPEND2_2.module.c_str() );

    const auto &    BASESYSTEMS = PACKAGE_FILE_UNIQUE->basesystems;
    ASSERT_EQ( 2, BASESYSTEMS.size() );

    const auto  BASESYSTEMS_END = BASESYSTEMS.end();

    const auto  BASESYSTEMS_IT1 = BASESYSTEMS.find( "currentBasesystem1" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT1 );
    const auto &    BASESYSTEM1 = BASESYSTEMS_IT1->second;
    ASSERT_STREQ( "currentModule1", BASESYSTEM1.module.c_str() );
    ASSERT_STREQ( "currentBasesystemInitializer1", BASESYSTEM1.initializer.c_str() );

    const auto  BASESYSTEMS_IT2 = BASESYSTEMS.find( "currentBasesystem2" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT2 );
    const auto &    BASESYSTEM2 = BASESYSTEMS_IT2->second;
    ASSERT_STREQ( "currentModule2", BASESYSTEM2.module.c_str() );
    ASSERT_STREQ( "currentBasesystemInitializer2", BASESYSTEM2.initializer.c_str() );

    const auto &    GAMES = PACKAGE_FILE_UNIQUE->games;
    ASSERT_EQ( 2, GAMES.size() );

    const auto  GAMES_END = GAMES.end();

    const auto  GAMES_IT1 = GAMES.find( "currentGame1" );
    ASSERT_NE( GAMES_END, GAMES_IT1 );
    const auto &    GAME1 = GAMES_IT1->second;
    ASSERT_STREQ( "currentModule1", GAME1.module.c_str() );
    ASSERT_STREQ( "currentGameInitializer1", GAME1.initializer.c_str() );

    const auto  GAMES_IT2 = GAMES.find( "currentGame2" );
    ASSERT_NE( GAMES_END, GAMES_IT2 );
    const auto &    GAME2 = GAMES_IT2->second;
    ASSERT_STREQ( "currentModule2", GAME2.module.c_str() );
    ASSERT_STREQ( "currentGameInitializer2", GAME2.initializer.c_str() );
}

TEST(
    MainPackageFileTest
    , Create_systemPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
        "mainpackagefiletest/system/"
        , "mainpackagefiletest/"
        , "system"
        , {
            "package"
        }
    );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 2, MODULES.size() );

    const auto  MODULES_END = MODULES.end();

    const auto  MODULES_IT1 = MODULES.find( "systemModule1" );
    ASSERT_NE( MODULES_END, MODULES_IT1 );
    const auto &    MODULE1 = MODULES_IT1->second;

    ASSERT_STREQ( "relative", MODULE1.at.c_str() );

    const auto &    PATH1 = MODULE1.path;
    ASSERT_EQ( 2, PATH1.size() );
    ASSERT_STREQ( "systemModulePath1_1", PATH1[ 0 ].c_str() );
    ASSERT_STREQ( "systemModulePath1_2", PATH1[ 1 ].c_str() );

    ASSERT_TRUE( MODULE1.existsInitializer );
    ASSERT_STREQ( "systemModuleInitializer1", MODULE1.initializer.c_str() );

    const auto &    INTERFACES1 = MODULE1.interfaces;
    ASSERT_EQ( 2, INTERFACES1.size() );
    const auto &    INTERFACE1_1 = INTERFACES1[ 0 ];
    ASSERT_STREQ( "systemModuleInterfacePackage1_1", INTERFACE1_1.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule1_1", INTERFACE1_1.module.c_str() );
    const auto &    INTERFACE1_2 = INTERFACES1[ 1 ];
    ASSERT_STREQ( "systemModuleInterfacePackage1_2", INTERFACE1_2.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule1_2", INTERFACE1_2.module.c_str() );

    const auto &    DEPENDS1 = MODULE1.depends;
    ASSERT_EQ( 2, DEPENDS1.size() );
    const auto &    DEPEND1_1 = DEPENDS1[ 0 ];
    ASSERT_STREQ( "systemModuleDependPackage1_1", DEPEND1_1.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule1_1", DEPEND1_1.module.c_str() );
    const auto &    DEPEND1_2 = DEPENDS1[ 1 ];
    ASSERT_STREQ( "systemModuleDependPackage1_2", DEPEND1_2.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule1_2", DEPEND1_2.module.c_str() );

    const auto  MODULES_IT2 = MODULES.find( "systemModule2" );
    ASSERT_NE( MODULES_END, MODULES_IT2 );
    const auto &    MODULE2 = MODULES_IT2->second;

    ASSERT_STREQ( "absolute", MODULE2.at.c_str() );

    const auto &    PATH2 = MODULE2.path;
    ASSERT_EQ( 2, PATH2.size() );
    ASSERT_STREQ( "systemModulePath2_1", PATH2[ 0 ].c_str() );
    ASSERT_STREQ( "systemModulePath2_2", PATH2[ 1 ].c_str() );

    ASSERT_TRUE( MODULE2.existsInitializer );
    ASSERT_STREQ( "systemModuleInitializer2", MODULE2.initializer.c_str() );

    const auto &    INTERFACES2 = MODULE2.interfaces;
    ASSERT_EQ( 2, INTERFACES2.size() );
    const auto &    INTERFACE2_1 = INTERFACES2[ 0 ];
    ASSERT_STREQ( "systemModuleInterfacePackage2_1", INTERFACE2_1.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule2_1", INTERFACE2_1.module.c_str() );
    const auto &    INTERFACE2_2 = INTERFACES2[ 1 ];
    ASSERT_STREQ( "systemModuleInterfacePackage2_2", INTERFACE2_2.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule2_2", INTERFACE2_2.module.c_str() );

    const auto &    DEPENDS2 = MODULE2.depends;
    ASSERT_EQ( 2, DEPENDS2.size() );
    const auto &    DEPEND2_1 = DEPENDS2[ 0 ];
    ASSERT_STREQ( "systemModuleDependPackage2_1", DEPEND2_1.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule2_1", DEPEND2_1.module.c_str() );
    const auto &    DEPEND2_2 = DEPENDS2[ 1 ];
    ASSERT_STREQ( "systemModuleDependPackage2_2", DEPEND2_2.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule2_2", DEPEND2_2.module.c_str() );

    const auto &    BASESYSTEMS = PACKAGE_FILE_UNIQUE->basesystems;
    ASSERT_EQ( 2, BASESYSTEMS.size() );

    const auto  BASESYSTEMS_END = BASESYSTEMS.end();

    const auto  BASESYSTEMS_IT1 = BASESYSTEMS.find( "systemBasesystem1" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT1 );
    const auto &    BASESYSTEM1 = BASESYSTEMS_IT1->second;
    ASSERT_STREQ( "systemModule1", BASESYSTEM1.module.c_str() );
    ASSERT_STREQ( "systemBasesystemInitializer1", BASESYSTEM1.initializer.c_str() );

    const auto  BASESYSTEMS_IT2 = BASESYSTEMS.find( "systemBasesystem2" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT2 );
    const auto &    BASESYSTEM2 = BASESYSTEMS_IT2->second;
    ASSERT_STREQ( "systemModule2", BASESYSTEM2.module.c_str() );
    ASSERT_STREQ( "systemBasesystemInitializer2", BASESYSTEM2.initializer.c_str() );

    const auto &    GAMES = PACKAGE_FILE_UNIQUE->games;
    ASSERT_EQ( 2, GAMES.size() );

    const auto  GAMES_END = GAMES.end();

    const auto  GAMES_IT1 = GAMES.find( "systemGame1" );
    ASSERT_NE( GAMES_END, GAMES_IT1 );
    const auto &    GAME1 = GAMES_IT1->second;
    ASSERT_STREQ( "systemModule1", GAME1.module.c_str() );
    ASSERT_STREQ( "systemGameInitializer1", GAME1.initializer.c_str() );

    const auto  GAMES_IT2 = GAMES.find( "systemGame2" );
    ASSERT_NE( GAMES_END, GAMES_IT2 );
    const auto &    GAME2 = GAMES_IT2->second;
    ASSERT_STREQ( "systemModule2", GAME2.module.c_str() );
    ASSERT_STREQ( "systemGameInitializer2", GAME2.initializer.c_str() );
}

TEST(
    MainPackageFileTest
    , CreateWithoutCurrentDirectory
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::createPackageFile(
        "mainpackagefiletest/system/"
        , "system"
        , {
            "package"
        }
    );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 2, MODULES.size() );

    const auto  MODULES_END = MODULES.end();

    const auto  MODULES_IT1 = MODULES.find( "systemModule1" );
    ASSERT_NE( MODULES_END, MODULES_IT1 );
    const auto &    MODULE1 = MODULES_IT1->second;

    ASSERT_STREQ( "relative", MODULE1.at.c_str() );

    const auto &    PATH1 = MODULE1.path;
    ASSERT_EQ( 2, PATH1.size() );
    ASSERT_STREQ( "systemModulePath1_1", PATH1[ 0 ].c_str() );
    ASSERT_STREQ( "systemModulePath1_2", PATH1[ 1 ].c_str() );

    ASSERT_TRUE( MODULE1.existsInitializer );
    ASSERT_STREQ( "systemModuleInitializer1", MODULE1.initializer.c_str() );

    const auto &    INTERFACES1 = MODULE1.interfaces;
    ASSERT_EQ( 2, INTERFACES1.size() );
    const auto &    INTERFACE1_1 = INTERFACES1[ 0 ];
    ASSERT_STREQ( "systemModuleInterfacePackage1_1", INTERFACE1_1.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule1_1", INTERFACE1_1.module.c_str() );
    const auto &    INTERFACE1_2 = INTERFACES1[ 1 ];
    ASSERT_STREQ( "systemModuleInterfacePackage1_2", INTERFACE1_2.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule1_2", INTERFACE1_2.module.c_str() );

    const auto &    DEPENDS1 = MODULE1.depends;
    ASSERT_EQ( 2, DEPENDS1.size() );
    const auto &    DEPEND1_1 = DEPENDS1[ 0 ];
    ASSERT_STREQ( "systemModuleDependPackage1_1", DEPEND1_1.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule1_1", DEPEND1_1.module.c_str() );
    const auto &    DEPEND1_2 = DEPENDS1[ 1 ];
    ASSERT_STREQ( "systemModuleDependPackage1_2", DEPEND1_2.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule1_2", DEPEND1_2.module.c_str() );

    const auto  MODULES_IT2 = MODULES.find( "systemModule2" );
    ASSERT_NE( MODULES_END, MODULES_IT2 );
    const auto &    MODULE2 = MODULES_IT2->second;

    ASSERT_STREQ( "absolute", MODULE2.at.c_str() );

    const auto &    PATH2 = MODULE2.path;
    ASSERT_EQ( 2, PATH2.size() );
    ASSERT_STREQ( "systemModulePath2_1", PATH2[ 0 ].c_str() );
    ASSERT_STREQ( "systemModulePath2_2", PATH2[ 1 ].c_str() );

    ASSERT_TRUE( MODULE2.existsInitializer );
    ASSERT_STREQ( "systemModuleInitializer2", MODULE2.initializer.c_str() );

    const auto &    INTERFACES2 = MODULE2.interfaces;
    ASSERT_EQ( 2, INTERFACES2.size() );
    const auto &    INTERFACE2_1 = INTERFACES2[ 0 ];
    ASSERT_STREQ( "systemModuleInterfacePackage2_1", INTERFACE2_1.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule2_1", INTERFACE2_1.module.c_str() );
    const auto &    INTERFACE2_2 = INTERFACES2[ 1 ];
    ASSERT_STREQ( "systemModuleInterfacePackage2_2", INTERFACE2_2.package.c_str() );
    ASSERT_STREQ( "systemModuleInterfaceModule2_2", INTERFACE2_2.module.c_str() );

    const auto &    DEPENDS2 = MODULE2.depends;
    ASSERT_EQ( 2, DEPENDS2.size() );
    const auto &    DEPEND2_1 = DEPENDS2[ 0 ];
    ASSERT_STREQ( "systemModuleDependPackage2_1", DEPEND2_1.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule2_1", DEPEND2_1.module.c_str() );
    const auto &    DEPEND2_2 = DEPENDS2[ 1 ];
    ASSERT_STREQ( "systemModuleDependPackage2_2", DEPEND2_2.package.c_str() );
    ASSERT_STREQ( "systemModuleDependModule2_2", DEPEND2_2.module.c_str() );

    const auto &    BASESYSTEMS = PACKAGE_FILE_UNIQUE->basesystems;
    ASSERT_EQ( 2, BASESYSTEMS.size() );

    const auto  BASESYSTEMS_END = BASESYSTEMS.end();

    const auto  BASESYSTEMS_IT1 = BASESYSTEMS.find( "systemBasesystem1" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT1 );
    const auto &    BASESYSTEM1 = BASESYSTEMS_IT1->second;
    ASSERT_STREQ( "systemModule1", BASESYSTEM1.module.c_str() );
    ASSERT_STREQ( "systemBasesystemInitializer1", BASESYSTEM1.initializer.c_str() );

    const auto  BASESYSTEMS_IT2 = BASESYSTEMS.find( "systemBasesystem2" );
    ASSERT_NE( BASESYSTEMS_END, BASESYSTEMS_IT2 );
    const auto &    BASESYSTEM2 = BASESYSTEMS_IT2->second;
    ASSERT_STREQ( "systemModule2", BASESYSTEM2.module.c_str() );
    ASSERT_STREQ( "systemBasesystemInitializer2", BASESYSTEM2.initializer.c_str() );

    const auto &    GAMES = PACKAGE_FILE_UNIQUE->games;
    ASSERT_EQ( 2, GAMES.size() );

    const auto  GAMES_END = GAMES.end();

    const auto  GAMES_IT1 = GAMES.find( "systemGame1" );
    ASSERT_NE( GAMES_END, GAMES_IT1 );
    const auto &    GAME1 = GAMES_IT1->second;
    ASSERT_STREQ( "systemModule1", GAME1.module.c_str() );
    ASSERT_STREQ( "systemGameInitializer1", GAME1.initializer.c_str() );

    const auto  GAMES_IT2 = GAMES.find( "systemGame2" );
    ASSERT_NE( GAMES_END, GAMES_IT2 );
    const auto &    GAME2 = GAMES_IT2->second;
    ASSERT_STREQ( "systemModule2", GAME2.module.c_str() );
    ASSERT_STREQ( "systemGameInitializer2", GAME2.initializer.c_str() );
}

TEST(
    MainPackageFileTest
    , CreateWithoutCurrentDirectory_failedCurrentPackage
)
{
    try {
        candymaker::createPackageFile(
            "mainpackagefiletest/system/"
            , "current"
            , {
                "package"
            }
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
