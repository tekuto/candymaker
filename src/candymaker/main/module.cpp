﻿#include "candymaker/main/module.h"
#include "candymaker/main/packagefile.h"
#include "candymaker/core/file/package.h"
#include "candymaker/core/file/path.h"

#include <string>
#include <map>
#include <algorithm>
#include <utility>
#include <stdexcept>

namespace {
    const auto  MODULE_AT_ABSOLUTE = "absolute";
    const auto  MODULE_AT_RELATIVE = "relative";

    struct EnableModule
    {
        const std::string &                     PACKAGE_DIRECTORY;
        const std::string                       SAVE_DIRECTORY_PATH;
        const std::string &                     MODULE_NAME;
        const candymaker::PackageFile::Module & MODULE;
    };

    using EnableModules = std::vector< EnableModule >;

    using EnableModulePtrs = std::vector< const EnableModule * >;

    void addEnableModules(
        EnableModules &                             _enableModules
        , const std::string &                       _PACKAGE_DIRECTORY
        , const candymaker::PackageFile &           _PACKAGE_FILE
        , const candymaker::ExecuteFile::Package &  _PACKAGE
    )
    {
        const auto  SAVE_DIRECTORY_PATH = _PACKAGE.relativeSaveDirectoryPathToString();

        for( const auto & ENABLE : _PACKAGE.modules ) {
            if( ENABLE.second != true ) {
                continue;
            }

            const auto &    MODULE_NAME = ENABLE.first;

            const auto &    MODULE = _PACKAGE_FILE.getModule( MODULE_NAME );

            _enableModules.push_back(
                EnableModule{
                    _PACKAGE_DIRECTORY,
                    SAVE_DIRECTORY_PATH,
                    MODULE_NAME,
                    MODULE,
                }
            );
        }
    }

    using PackageFileUniques = std::map<
        std::string
        , candymaker::PackageFile::ConstUnique
    >;

    template< typename CREATE_PACKAGE_DIRECTORY_STRING_PROC_T >
    auto createEnableModules(
        const candymaker::ExecuteFile::Packages &           _PACKAGES
        , PackageFileUniques &                              _packageFileUniques
        , const CREATE_PACKAGE_DIRECTORY_STRING_PROC_T &    _CREATE_PACKAGE_DIRECTORY_STRING_PROC
    )
    {
        auto    enableModules = EnableModules();
        auto    packageFileUniques = PackageFileUniques();

        for( const auto & PACKAGE : _PACKAGES ) {
            auto    packageDirectory = _CREATE_PACKAGE_DIRECTORY_STRING_PROC( PACKAGE );

            auto            packageFileUnique = candymaker::createPackageFile( packageDirectory );
            const auto &    PACKAGE_FILE = *packageFileUnique;

            const auto  RESULT = packageFileUniques.insert(
                std::make_pair(
                    std::move( packageDirectory )
                    , std::move( packageFileUnique )
                )
            );
            if( RESULT.second == false ) {
                throw std::runtime_error( "パッケージファイル集合への要素追加に失敗" );
            }

            const auto &    PACKAGE_DIRECTORY = RESULT.first->first;

            addEnableModules(
                enableModules
                , PACKAGE_DIRECTORY
                , PACKAGE_FILE
                , PACKAGE
            );
        }

        _packageFileUniques = std::move( packageFileUniques );

        return enableModules;
    }

    auto createEnableModules(
        const std::string &                         _SYSTEM_DIRECTORY
        , const std::string &                       _CURRENT_DIRECTORY
        , const candymaker::ExecuteFile::Packages & _PACKAGES
        , PackageFileUniques &                      _packageFileUniques
    )
    {
        return createEnableModules(
            _PACKAGES
            , _packageFileUniques
            , [
                &_SYSTEM_DIRECTORY
                , &_CURRENT_DIRECTORY
            ]
            (
                const candymaker::ExecuteFile::Package &    _PACKAGE
            )
            {
                return candymaker::createPackageDirectoryString(
                    _SYSTEM_DIRECTORY
                    , _CURRENT_DIRECTORY
                    , _PACKAGE.at
                    , _PACKAGE.path
                );
            }
        );
    }

    auto createEnableModules(
        const std::string &                         _SYSTEM_DIRECTORY
        , const candymaker::ExecuteFile::Packages & _PACKAGES
        , PackageFileUniques &                      _packageFileUniques
    )
    {
        return createEnableModules(
            _PACKAGES
            , _packageFileUniques
            , [
                &_SYSTEM_DIRECTORY
            ]
            (
                const candymaker::ExecuteFile::Package &    _PACKAGE
            )
            {
                return candymaker::createPackageDirectoryString(
                    _SYSTEM_DIRECTORY
                    , _PACKAGE.at
                    , _PACKAGE.path
                );
            }
        );
    }

    template<
        typename GET_ENABLE_MODULE_T
        , typename ENABLE_MODULES_T
    >
    typename ENABLE_MODULES_T::const_iterator findEnableModule(
        const ENABLE_MODULES_T &    _ENABLE_MODULES
        , const std::string &       _PACKAGE_DIRECTORY
        , const std::string &       _MODULE_NAME
    )
    {
        return std::find_if(
            _ENABLE_MODULES.begin()
            , _ENABLE_MODULES.end()
            , [
                &_PACKAGE_DIRECTORY
                , &_MODULE_NAME
            ]
            (
                const typename ENABLE_MODULES_T::value_type &   _ENABLE_MODULE
            )
            {
                const auto &    ENABLE_MODULE = GET_ENABLE_MODULE_T()( _ENABLE_MODULE );

                if( ENABLE_MODULE.PACKAGE_DIRECTORY != _PACKAGE_DIRECTORY ) {
                    return false;
                }

                if( ENABLE_MODULE.MODULE_NAME != _MODULE_NAME ) {
                    return false;
                }

                return true;
            }
        );
    }

    bool isExistsEnableModule(
        const EnableModulePtrs &    _ENABLE_MODULE_PTRS
        , const std::string &       _PACKAGE_DIRECTORY
        , const std::string &       _MODULE_NAME
    )
    {
        struct GetEnableModule
        {
            const EnableModule & operator()(
                const EnableModule *    _ENABLE_MODULE_PTR
            ) const
            {
                return *_ENABLE_MODULE_PTR;
            };
        };

        const auto  IT = findEnableModule< GetEnableModule >(
            _ENABLE_MODULE_PTRS
            , _PACKAGE_DIRECTORY
            , _MODULE_NAME
        );

        return IT != _ENABLE_MODULE_PTRS.end();
    }

    const auto & getEnableModule(
        const EnableModules &   _ENABLE_MODULES
        , const std::string &   _PACKAGE_DIRECTORY
        , const std::string &   _MODULE_NAME
    )
    {
        struct GetEnableModule
        {
            const EnableModule & operator()(
                const EnableModule &    _ENABLE_MODULE
            ) const
            {
                return _ENABLE_MODULE;
            };
        };

        const auto  IT = findEnableModule< GetEnableModule >(
            _ENABLE_MODULES
            , _PACKAGE_DIRECTORY
            , _MODULE_NAME
        );
        if( IT == _ENABLE_MODULES.end() ) {
            throw std::runtime_error( "対象モジュールが有効モジュールではない" );
        }

        return *IT;
    }

    const auto & getDependModule(
        const EnableModules &                               _ENABLE_MODULES
        , const candymaker::PackageFile::Module::Depend &   _DEPEND
        , const std::string &                               _PACKAGE_DIRECTORY
    )
    {
        auto    enableModulePtr = static_cast< const EnableModule * >( nullptr );

        for( const auto & ENABLE_MODULE : _ENABLE_MODULES ) {
            if( _DEPEND.isLocalPackage() == true && ENABLE_MODULE.PACKAGE_DIRECTORY != _PACKAGE_DIRECTORY ) {
                continue;
            }

            if( ENABLE_MODULE.MODULE.isImplementedModule( _DEPEND ) == false ) {
                continue;
            }

            if( enableModulePtr != nullptr ) {
                throw std::runtime_error( "インターフェースを実装しているモジュールが複数存在する" );
            }

            enableModulePtr = &ENABLE_MODULE;
        }

        return *enableModulePtr;
    }

    void addEnableModulePtr(
        EnableModulePtrs &      _enableModulePtrs
        , const EnableModules & _ENABLE_MODULES
        , const std::string &   _PACKAGE_DIRECTORY
        , const std::string &   _MODULE_NAME
    )
    {
        if( isExistsEnableModule(
            _enableModulePtrs
            , _PACKAGE_DIRECTORY
            , _MODULE_NAME
        ) == true ) {
            return;
        }

        const auto &    ENABLE_MODULE = getEnableModule(
            _ENABLE_MODULES
            , _PACKAGE_DIRECTORY
            , _MODULE_NAME
        );

        _enableModulePtrs.push_back( &ENABLE_MODULE );

        const auto &    DEPENDS = ENABLE_MODULE.MODULE.getDepends();
        for( const auto & DEPEND : DEPENDS ) {
            const auto &    DEPEND_MODULE = getDependModule(
                _ENABLE_MODULES
                , DEPEND
                , _PACKAGE_DIRECTORY
            );

            addEnableModulePtr(
                _enableModulePtrs
                , _ENABLE_MODULES
                , DEPEND_MODULE.PACKAGE_DIRECTORY
                , DEPEND_MODULE.MODULE_NAME
            );
        }
    }

    auto createEnableModulePtrs(
        const EnableModules &   _ENABLE_MODULES
        , const std::string &   _PACKAGE_DIRECTORY
        , const std::string &   _MODULE_NAME
    )
    {
        auto    enableModulePtrs = EnableModulePtrs();

        addEnableModulePtr(
            enableModulePtrs
            , _ENABLE_MODULES
            , _PACKAGE_DIRECTORY
            , _MODULE_NAME
        );

        return enableModulePtrs;
    }

    auto createModulePathString(
        const std::string &         _MODULE_AT
        , const candymaker::Path &  _MODULE_PATH
        , const std::string &       _PACKAGE_DIRECTORY
    )
    {
        auto    modulePath = std::string();

        if( _MODULE_AT == MODULE_AT_ABSOLUTE ) {
            modulePath = candymaker::PackageFile::absoluteModulePathToString( _MODULE_PATH );
        } else if( _MODULE_AT == MODULE_AT_RELATIVE ) {
            modulePath = _PACKAGE_DIRECTORY;
            modulePath += candymaker::PackageFile::relativeModulePathToString( _MODULE_PATH );
        } else {
            throw std::runtime_error( "モジュール位置が不正" );
        }

        return std::string( modulePath );
    }

    auto createModuleInfo(
        const EnableModule &    _ENABLE_MODULE
    )
    {
        const auto &    PACKAGE_DIRECTORY = _ENABLE_MODULE.PACKAGE_DIRECTORY;
        const auto &    SAVE_DIRECTORY_PATH = _ENABLE_MODULE.SAVE_DIRECTORY_PATH;
        const auto &    MODULE = _ENABLE_MODULE.MODULE;

        const auto  MODULE_PATH = createModulePathString(
            MODULE.getAt()
            , MODULE.getPath()
            , PACKAGE_DIRECTORY
        );

        if( MODULE.isExistsInitializer() == true ) {
            return candymaker::ModuleInfo::create(
                PACKAGE_DIRECTORY
                , MODULE_PATH
                , SAVE_DIRECTORY_PATH
                , MODULE.getInitializer()
            );
        } else {
            return candymaker::ModuleInfo::create(
                PACKAGE_DIRECTORY
                , MODULE_PATH
                , SAVE_DIRECTORY_PATH
            );
        }
    }

    template< typename CREATE_PACKAGE_DIRECTORY_STRING_PROC_T >
    auto createModuleInfoUniques(
        const EnableModules &                               _ENABLE_MODULES
        , const std::string &                               _AT
        , const candymaker::Path &                          _PATH
        , const std::string &                               _MODULE_NAME
        , const CREATE_PACKAGE_DIRECTORY_STRING_PROC_T &    _CREATE_PACKAGE_DIRECTORY_STRING_PROC
    )
    {
        const auto  PACKAGE_DIRECTORY = _CREATE_PACKAGE_DIRECTORY_STRING_PROC(
            _AT
            , _PATH
        );

        const auto  ENABLE_MODULE_PTRS = createEnableModulePtrs(
            _ENABLE_MODULES
            , PACKAGE_DIRECTORY
            , _MODULE_NAME
        );

        auto    moduleInfoUniques = candymaker::ModuleInfoUniques();

        for( const auto & ENABLE_MODULE_PTR : ENABLE_MODULE_PTRS ) {
            const auto &    ENABLE_MODULE = *ENABLE_MODULE_PTR;

            auto    moduleInfoUnique = createModuleInfo( ENABLE_MODULE );

            moduleInfoUniques.push_back( std::move( moduleInfoUnique ) );
        }

        return moduleInfoUniques;
    }

    auto createModuleInfoUniques(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _CURRENT_DIRECTORY
        , const EnableModules &     _ENABLE_MODULES
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _MODULE_NAME
    )
    {
        return createModuleInfoUniques(
            _ENABLE_MODULES
            , _AT
            , _PATH
            , _MODULE_NAME
            , [
                &_SYSTEM_DIRECTORY
                , &_CURRENT_DIRECTORY
            ]
            (
                const std::string &         _AT
                , const candymaker::Path &  _PATH
            )
            {
                return candymaker::createPackageDirectoryString(
                    _SYSTEM_DIRECTORY
                    , _CURRENT_DIRECTORY
                    , _AT
                    , _PATH
                );
            }
        );
    }

    auto createModuleInfoUniques(
        const std::string &         _SYSTEM_DIRECTORY
        , const EnableModules &     _ENABLE_MODULES
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
        , const std::string &       _MODULE_NAME
    )
    {
        return createModuleInfoUniques(
            _ENABLE_MODULES
            , _AT
            , _PATH
            , _MODULE_NAME
            , [
                &_SYSTEM_DIRECTORY
            ]
            (
                const std::string &         _AT
                , const candymaker::Path &  _PATH
            )
            {
                return candymaker::createPackageDirectoryString(
                    _SYSTEM_DIRECTORY
                    , _AT
                    , _PATH
                );
            }
        );
    }
}

namespace candymaker {
    ModuleContexts::Unique createModuleContexts(
        ModuleManager &                 _moduleManager
        , const std::string &           _SYSTEM_DIRECTORY
        , const std::string &           _CURRENT_DIRECTORY
        , const ExecuteFile::Packages & _PACKAGES
        , const std::string &           _AT
        , const Path &                  _PATH
        , const std::string &           _MODULE_NAME
    )
    {
        auto    packageFileUniques = PackageFileUniques();

        const auto  ENABLE_MODULES = createEnableModules(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _PACKAGES
            , packageFileUniques
        );

        const auto  MODULE_INFO_UNIQUES = createModuleInfoUniques(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , ENABLE_MODULES
            , _AT
            , _PATH
            , _MODULE_NAME
        );

        return ModuleContexts::create(
            _moduleManager
            , MODULE_INFO_UNIQUES
        );
    }

    ModuleContexts::Unique createModuleContexts(
        ModuleManager &                 _moduleManager
        , const std::string &           _SYSTEM_DIRECTORY
        , const ExecuteFile::Packages & _PACKAGES
        , const std::string &           _AT
        , const Path &                  _PATH
        , const std::string &           _MODULE_NAME
    )
    {
        auto    packageFileUniques = PackageFileUniques();

        const auto  ENABLE_MODULES = createEnableModules(
            _SYSTEM_DIRECTORY
            , _PACKAGES
            , packageFileUniques
        );

        const auto  MODULE_INFO_UNIQUES = createModuleInfoUniques(
            _SYSTEM_DIRECTORY
            , ENABLE_MODULES
            , _AT
            , _PATH
            , _MODULE_NAME
        );

        return ModuleContexts::create(
            _moduleManager
            , MODULE_INFO_UNIQUES
        );
    }
}
