﻿#include "fg/util/test.h"
#include "candymaker/main/gamecontextmanager.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/state/state.h"
#include "fg/core/state/enterevent.h"
#include "fg/common/unique.h"

#include <string>
#include <algorithm>

struct GameContextManagerTest : public fg::UniqueWrapper< GameContextManagerTest >
{
    candymaker::ModuleContexts::Unique  moduleContextsUnique;
    FgGameContext                       gameContext;
};

void testModuleUseCount(
    const candymaker::ModuleManager::ModuleMap &    _MODULE_MAP
    , const std::string &                           _PACKAGE_PATH
    , const std::string &                           _MODULE_PATH
    , int                                           _useCount
)
{
    const auto  END = _MODULE_MAP.end();
    const auto  IT = std::find_if(
        _MODULE_MAP.begin()
        , END
        , [
            &_PACKAGE_PATH
            , &_MODULE_PATH
        ]
        (
            const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
        )
        {
            const auto &    KEY = _PAIR.first;

            if( KEY.PACKAGE_PATH != _PACKAGE_PATH ) {
                return false;
            }

            if( KEY.MODULE_PATH != _MODULE_PATH ) {
                return false;
            }

            return true;
        }
    );
    ASSERT_NE( END, IT );

    ASSERT_EQ( _useCount, IT->second.use_count() );
}

TEST(
    GameContextManagerTest
    , Create
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "gamecontextmanagertest/createtest.fge"
        , "gamecontextmanagertest/system/"
        , "gamecontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextManagerUnique = candymaker::GameContextManager::create(
        EXECUTE_FILE
        , "gamecontextmanagertest/system/"
        , "gamecontextmanagertest/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, gameContextManagerUnique.get() );
    auto &  gameContextManager = reinterpret_cast< GameContextManagerTest & >( *gameContextManagerUnique );

    auto &  moduleContextsUnique = gameContextManager.moduleContextsUnique;
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleUseCount(
            MODULE_MAP
            , "gamecontextmanagertest/packages/package/"
            , "gamecontextmanagertest/packages/package/libcreatetest.so"
            , 2
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );

    const auto  GET_DATA_PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );

    ASSERT_EQ( 10, GET_DATA_PROC_PTR() );

    const auto &    GAME_CONTEXT = gameContextManager.gameContext;
    ASSERT_STREQ( "gameSavePath1/gameSavePath2/", GAME_CONTEXT.getSaveDirectoryPath().c_str() );
}

TEST(
    GameContextManagerTest
    , CreateWithoutCurrentDirectory
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "gamecontextmanagertest/createtest_withoutcurrentdirectory.fge"
        , "gamecontextmanagertest/system/"
        , "gamecontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextManagerUnique = candymaker::GameContextManager::create(
        EXECUTE_FILE
        , "gamecontextmanagertest/system/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, gameContextManagerUnique.get() );
    auto &  gameContextManager = reinterpret_cast< GameContextManagerTest & >( *gameContextManagerUnique );

    auto &  moduleContextsUnique = gameContextManager.moduleContextsUnique;
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto &    MODULE_MAP = moduleManager.moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleUseCount(
            MODULE_MAP
            , "gamecontextmanagertest/system/packages/package/"
            , "gamecontextmanagertest/system/packages/package/libcreatetest.so"
            , 2
        )
    );

    ASSERT_EQ( 1, moduleContextsUnique->contextShares.size() );

    const auto  GET_DATA_PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );

    ASSERT_EQ( 20, GET_DATA_PROC_PTR() );

    const auto &    GAME_CONTEXT = gameContextManager.gameContext;
    ASSERT_STREQ( "package/", GAME_CONTEXT.getSaveDirectoryPath().c_str() );
}

void dummyDestroy(
    void *
)
{
}

void startTestProc(
    fg::StateEnterEvent< int > &    _event
)
{
    _event.getState().getData() = 20;
}

TEST(
    GameContextManagerTest
    , Start
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "gamecontextmanagertest/createtest.fge"
        , "gamecontextmanagertest/system/"
        , "gamecontextmanagertest/"
    );
    ASSERT_NE( nullptr,  EXECUTE_FILE_UNIQUE.get() );
    const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , nullptr
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextManagerUnique = candymaker::GameContextManager::create(
        EXECUTE_FILE
        , "gamecontextmanagertest/system/"
        , "gamecontextmanagertest/"
        , moduleManager
        , state
    );
    ASSERT_NE( nullptr, gameContextManagerUnique.get() );

    auto    i = 10;

    state.setData(
        &i
        , dummyDestroy
    );

    state.setEnterEventProc( reinterpret_cast< candymaker::StateEvent::Proc >( startTestProc ) );

    gameContextManagerUnique->start();

    state.threadJoinerUnique->join();

    ASSERT_FALSE( state.disabled );
    ASSERT_EQ( 20, i );
}
