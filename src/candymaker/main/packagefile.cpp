﻿#include "candymaker/main/packagefile.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/file/package.h"

#include <string>
#include <stdexcept>

namespace {
    const auto  PACKAGE_AT_SYSTEM = "system";
    const auto  PACKAGE_AT_CURRENT = "current";

    template< typename GET_DIRECTORY_PROC_T >
    auto createPackageDirectoryString(
        const candymaker::Path &        _PATH
        , const GET_DIRECTORY_PROC_T &  _GET_DIRECTORY_PROC
    )
    {
        const auto &    DIRECTORY = _GET_DIRECTORY_PROC();

        auto    packageDirectory = DIRECTORY;
        packageDirectory.append( candymaker::PackageFile::relativeDirectoryPathToString( _PATH ) );

        return packageDirectory;
    }
}

namespace candymaker {
    std::string createPackageDirectoryString(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _CURRENT_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
    )
    {
        return ::createPackageDirectoryString(
            _PATH
            , [
                &_SYSTEM_DIRECTORY
                , &_CURRENT_DIRECTORY
                , &_AT
            ]
            {
                if( _AT == PACKAGE_AT_SYSTEM ) {
                    return _SYSTEM_DIRECTORY;
                } else if ( _AT == PACKAGE_AT_CURRENT ) {
                    return _CURRENT_DIRECTORY;
                }

                throw std::runtime_error( "パッケージ位置が不正" );
            }
        );
    }

    std::string createPackageDirectoryString(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
    )
    {
        return ::createPackageDirectoryString(
            _PATH
            , [
                &_SYSTEM_DIRECTORY
                , &_AT
            ]
            {
                if( _AT == PACKAGE_AT_SYSTEM ) {
                    return _SYSTEM_DIRECTORY;
                }

                throw std::runtime_error( "パッケージ位置が不正" );
            }
        );
    }

    PackageFile::ConstUnique createPackageFile(
        const std::string & _PACKAGE_DIRECTORY
    )
    {
        const auto  PATH_STRING = _PACKAGE_DIRECTORY + candymaker::PackageFile::getFileName();

        return candymaker::PackageFile::create( PATH_STRING );
    }

    PackageFile::ConstUnique createPackageFile(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _CURRENT_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
    )
    {
        const auto  PACKAGE_DIRECTORY = createPackageDirectoryString(
            _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , _AT
            , _PATH
        );

        return createPackageFile( PACKAGE_DIRECTORY );
    }

    PackageFile::ConstUnique createPackageFile(
        const std::string &         _SYSTEM_DIRECTORY
        , const std::string &       _AT
        , const candymaker::Path &  _PATH
    )
    {
        const auto  PACKAGE_DIRECTORY = createPackageDirectoryString(
            _SYSTEM_DIRECTORY
            , _AT
            , _PATH
        );

        return createPackageFile( PACKAGE_DIRECTORY );
    }
}
