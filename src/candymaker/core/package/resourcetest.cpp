﻿#include "fg/util/test.h"
#include "fg/core/package/resource.h"
#include "candymaker/core/package/resource.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/state/state.h"
#include "candymaker/core/thread/cache.h"

void dummyDestroy(
    void *
)
{
}

TEST(
    PackageResourceTest
    , CreateForModuleContext
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );
    const auto &    PACKAGE_RESOURCE = *PACKAGE_RESOURCE_UNIQUE;

    const auto &    FILE_DATA = PACKAGE_RESOURCE->fileData;
    const auto      SIZE = FILE_DATA.size();

    ASSERT_EQ( 16, SIZE );

    auto    packageResourceString = std::string(
        FILE_DATA.data()
        , SIZE
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}

TEST(
    PackageResourceTest
    , CreateForBasesystemContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    moduleContextShared = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        state
        , moduleContext
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< int > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        BASESYSTEM_CONTEXT
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );
    const auto &    PACKAGE_RESOURCE = *PACKAGE_RESOURCE_UNIQUE;

    const auto &    FILE_DATA = PACKAGE_RESOURCE->fileData;
    const auto      SIZE = FILE_DATA.size();

    ASSERT_EQ( 16, SIZE );

    auto    packageResourceString = std::string(
        FILE_DATA.data()
        , SIZE
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}

TEST(
    PackageResourceTest
    , CreateForGameContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    moduleContextShared = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        state
        , moduleContext
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        GAME_CONTEXT
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );
    const auto &    PACKAGE_RESOURCE = *PACKAGE_RESOURCE_UNIQUE;

    const auto &    FILE_DATA = PACKAGE_RESOURCE->fileData;
    const auto      SIZE = FILE_DATA.size();

    ASSERT_EQ( 16, SIZE );

    auto    packageResourceString = std::string(
        FILE_DATA.data()
        , SIZE
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}

TEST(
    PackageResourceTest
    , Create_failed
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    try {
        fg::PackageResource::create(
            reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED )
            , "notfound"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageResourceTest
    , GetSize
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );

    ASSERT_EQ( 16, PACKAGE_RESOURCE_UNIQUE->getSize() );
}

TEST(
    PackageResourceTest
    , GetData
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "packageresourcetest/"
        , "packageresourcetest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );

    const auto  DATA_PTR = PACKAGE_RESOURCE_UNIQUE->getData();
    ASSERT_NE( nullptr, DATA_PTR );

    auto    packageResourceString = std::string(
        static_cast< const char * >( DATA_PTR )
        , PACKAGE_RESOURCE_UNIQUE->getSize()
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}
