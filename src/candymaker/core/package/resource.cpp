﻿#include "fg/util/export.h"
#include "fg/core/package/resource.h"
#include "candymaker/core/package/resource.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/common/filedata.h"

#include <string>
#include <utility>
#include <stdexcept>

namespace {
    std::string getPackageResourcePath(
        const FgModuleContext & _MODULE_CONTEXT
        , const char *          _FILE_NAME
    )
    {
        return candymaker::relativeFilePathToString(
            candymaker::Path(
                {
                    _MODULE_CONTEXT.getPackagePath(),
                    _FILE_NAME,
                }
            )
        );
    }

    candymaker::FileData readFile(
        const FgModuleContext & _MODULE_CONTEXT
        , const char *          _FILE_NAME
    )
    {
        const auto  PACKAGE_RESOURCE_PATH = getPackageResourcePath(
            _MODULE_CONTEXT
            , _FILE_NAME
        );

        auto    fileData = candymaker::FileData();
        if( candymaker::readFile(
            fileData
            , PACKAGE_RESOURCE_PATH
        ) == false ) {
            throw std::runtime_error( "ファイル読み込みに失敗" );
        }

        return fileData;
    }
}

FgPackageResource * fgPackageResourceCreateForModuleContext(
    const FgModuleContext * _MODULE_CONTEXT_PTR
    , const char *          _FILE_NAME
)
{
    auto    fileData = ::readFile(
        *_MODULE_CONTEXT_PTR
        , _FILE_NAME
    );

    return new FgPackageResource{
        std::move( fileData ),
    };
}

FgPackageResource * fgPackageResourceCreateForBasesystemContext(
    const FgBasesystemContext * _BASESYSTEM_CONTEXT_PTR
    , const char *              _FILE_NAME
)
{
    return fgPackageResourceCreateForModuleContext(
        &( _BASESYSTEM_CONTEXT_PTR->getModuleContext() )
        , _FILE_NAME
    );
}

FgPackageResource * fgPackageResourceCreateForGameContext(
    const FgGameContext *   _GAME_CONTEXT_PTR
    , const char *          _FILE_NAME
)
{
    return fgPackageResourceCreateForModuleContext(
        &( _GAME_CONTEXT_PTR->getModuleContext() )
        , _FILE_NAME
    );
}

void fgPackageResourceDestroy(
    FgPackageResource * _implPtr
)
{
    delete _implPtr;
}

size_t fgPackageResourceGetSize(
    const FgPackageResource *   _IMPL_PTR
)
{
    return _IMPL_PTR->fileData.size();
}

const void * fgPackageResourceGetData(
    const FgPackageResource *   _IMPL_PTR
)
{
    return _IMPL_PTR->fileData.data();
}
