﻿#include "fg/util/export.h"
#include "fg/core/state/threadbackground.h"
#include "candymaker/core/state/threadimpl.h"

FgState * fgStateThreadBackgroundGetState(
    FgStateThreadBackground *   _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadBackgroundGetData(
    FgStateThreadBackground *   _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getData();
}
