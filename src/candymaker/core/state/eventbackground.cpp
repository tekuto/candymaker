﻿#include "fg/util/export.h"
#include "fg/core/state/eventbackground.h"
#include "candymaker/core/state/eventimpl.h"

FgState * fgStateEventBackgroundGetState(
    FgStateEventBackground *    _implPtr
)
{
    return reinterpret_cast< candymaker::StateEvent * >( _implPtr )->getState();
}

void * fgStateEventBackgroundGetData(
    FgStateEventBackground *    _implPtr
)
{
    return reinterpret_cast< candymaker::StateEvent * >( _implPtr )->getData();
}
