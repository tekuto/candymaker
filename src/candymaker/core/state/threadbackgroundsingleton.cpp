﻿#include "fg/util/export.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "candymaker/core/state/threadimpl.h"

FgState * fgStateThreadBackgroundSingletonGetState(
    FgStateThreadBackgroundSingleton *  _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadBackgroundSingletonGetData(
    FgStateThreadBackgroundSingleton *  _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getData();
}
