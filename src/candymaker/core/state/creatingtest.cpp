﻿#include "fg/util/test.h"
#include "fg/core/state/creating.h"
#include "candymaker/core/state/state.h"
#include "candymaker/core/state/eventimpl.h"
#include "candymaker/core/thread/cache.h"

void dummyDestroy(
    void *
)
{
}

void creatingStateTestProc(
    int *
)
{
}

TEST(
    CreatingStateTest
    , Create
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    const auto  STATE_UNIQUE = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) ); // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, STATE_UNIQUE.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *STATE_UNIQUE );

    ASSERT_EQ( &prevState, STATE.prevStatePtr );
    ASSERT_TRUE( STATE.disabled );
    ASSERT_EQ( nullptr, STATE.dataPtr );
    ASSERT_EQ( nullptr, STATE.dataDestroyProcPtr );
    ASSERT_EQ( nullptr, STATE.dataDestroyer.get() );
}

void setDataTestProc(
    int *   _iPtr
)
{
    *_iPtr = 20;
}

TEST(
    CreatingStateTest
    , SetData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    auto    i = 10;

    fgCreatingStateSetData(
        &**stateUnique
        , &i
        , reinterpret_cast< FgCreatingStateDataDestroyProc >( setDataTestProc )
    );

    ASSERT_EQ( &prevState, STATE.prevStatePtr );
    ASSERT_TRUE( STATE.disabled );
    ASSERT_EQ( 10, *static_cast< const int * >( STATE.dataPtr ) );
    ASSERT_EQ( reinterpret_cast< FgCreatingStateDataDestroyProc >( setDataTestProc ), STATE.dataDestroyProcPtr );
    ASSERT_EQ( &STATE, STATE.dataDestroyer.get() );

    stateUnique.destroy();

    ASSERT_EQ( 20, i );
}

void setEnterEventProcTestProc(
    fg::StateEnterEvent< int > &
)
{
}

TEST(
    CreatingStateTest
    , SetEnterEventProc
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setEnterEventProcTestProc );

    ASSERT_TRUE( STATE.existsEnterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEnterEventProcTestProc ), STATE.enterEventProcPtr );
}

void setEnterEventProcWithoutStateDataTestProc(
    fg::StateEnterEvent<> &
)
{
}

TEST(
    CreatingStateTest
    , SetEnterEventProc_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState<>::Unique( fgCreatingStateCreate( &prevState ) );   // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setEnterEventProcWithoutStateDataTestProc );

    ASSERT_TRUE( STATE.existsEnterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEnterEventProcWithoutStateDataTestProc ), STATE.enterEventProcPtr );
}

void setExitEventProcTestProc(
    fg::StateExitEvent< int > &
)
{
}

TEST(
    CreatingStateTest
    , SetExitEventProc
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setExitEventProcTestProc );

    ASSERT_TRUE( STATE.existsExitEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setExitEventProcTestProc ), STATE.exitEventProcPtr );
}

void setExitEventProcWithoutStateDataTestProc(
    fg::StateExitEvent<> &
)
{
}

TEST(
    CreatingStateTest
    , SetExitEventProc_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState<>::Unique( fgCreatingStateCreate( &prevState ) );   // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setExitEventProcWithoutStateDataTestProc );

    ASSERT_TRUE( STATE.existsExitEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setExitEventProcWithoutStateDataTestProc ), STATE.exitEventProcPtr );
}

void setReenterEventProcTestProc(
    fg::StateReenterEvent< int > &
)
{
}

TEST(
    CreatingStateTest
    , SetReenterEventProc
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setReenterEventProcTestProc );

    ASSERT_TRUE( STATE.existsReenterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setReenterEventProcTestProc ), STATE.reenterEventProcPtr );
}

void setReenterEventProcWithoutStateDataTestProc(
    fg::StateReenterEvent<> &
)
{
}

TEST(
    CreatingStateTest
    , SetReenterEventProc_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState<>::Unique( fgCreatingStateCreate( &prevState ) );   // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setReenterEventProcWithoutStateDataTestProc );

    ASSERT_TRUE( STATE.existsReenterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setReenterEventProcWithoutStateDataTestProc ), STATE.reenterEventProcPtr );
}

void setLeaveEventProcTestProc(
    fg::StateLeaveEvent< int > &
)
{
}

TEST(
    CreatingStateTest
    , SetLeaveEventProc
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setLeaveEventProcTestProc );

    ASSERT_TRUE( STATE.existsLeaveEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setLeaveEventProcTestProc ), STATE.leaveEventProcPtr );
}

void setLeaveEventProcWithoutStateDataTestProc(
    fg::StateLeaveEvent<> &
)
{
}

TEST(
    CreatingStateTest
    , SetLeaveEventProc_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState<>::Unique( fgCreatingStateCreate( &prevState ) );   // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProc( setLeaveEventProcWithoutStateDataTestProc );

    ASSERT_TRUE( STATE.existsLeaveEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setLeaveEventProcWithoutStateDataTestProc ), STATE.leaveEventProcPtr );
}

void setEventProcsTestEnterEventProc(
    fg::StateEnterEvent< int > &
)
{
}

void setEventProcsTestExitEventProc(
    fg::StateExitEvent< int > &
)
{
}

void setEventProcsTestReenterEventProc(
    fg::StateReenterEvent< int > &
)
{
}

void setEventProcsTestLeaveEventProc(
    fg::StateLeaveEvent< int > &
)
{
}

TEST(
    CreatingStateTest
    , SetEventProcs
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState< int >::Unique( fgCreatingStateCreate( &prevState ) );  // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProcs(
        setEventProcsTestEnterEventProc
        , setEventProcsTestExitEventProc
        , setEventProcsTestReenterEventProc
        , setEventProcsTestLeaveEventProc
    );

    ASSERT_TRUE( STATE.existsEnterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsTestEnterEventProc ), STATE.enterEventProcPtr );
    ASSERT_TRUE( STATE.existsExitEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsTestExitEventProc ), STATE.exitEventProcPtr );
    ASSERT_TRUE( STATE.existsReenterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsTestReenterEventProc ), STATE.reenterEventProcPtr );
    ASSERT_TRUE( STATE.existsLeaveEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsTestLeaveEventProc ), STATE.leaveEventProcPtr );
}

void setEventProcsWithoutStateDataTestEnterEventProc(
    fg::StateEnterEvent<> &
)
{
}

void setEventProcsWithoutStateDataTestExitEventProc(
    fg::StateExitEvent<> &
)
{
}

void setEventProcsWithoutStateDataTestReenterEventProc(
    fg::StateReenterEvent<> &
)
{
}

void setEventProcsWithoutStateDataTestLeaveEventProc(
    fg::StateLeaveEvent<> &
)
{
}

TEST(
    CreatingStateTest
    , SetEventProcs_withoutStateData

)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    stateUnique = fg::CreatingState<>::Unique( fgCreatingStateCreate( &prevState ) );   // fg::CreatingState::create()はprivateのため、fgCreatingStateCreate()で代用
    ASSERT_NE( nullptr, stateUnique.get() );
    const auto &    STATE = reinterpret_cast< const FgState & >( *stateUnique );

    stateUnique->setEventProcs(
        setEventProcsWithoutStateDataTestEnterEventProc
        , setEventProcsWithoutStateDataTestExitEventProc
        , setEventProcsWithoutStateDataTestReenterEventProc
        , setEventProcsWithoutStateDataTestLeaveEventProc
    );

    ASSERT_TRUE( STATE.existsEnterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsWithoutStateDataTestEnterEventProc ), STATE.enterEventProcPtr );
    ASSERT_TRUE( STATE.existsExitEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsWithoutStateDataTestExitEventProc ), STATE.exitEventProcPtr );
    ASSERT_TRUE( STATE.existsReenterEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsWithoutStateDataTestReenterEventProc ), STATE.reenterEventProcPtr );
    ASSERT_TRUE( STATE.existsLeaveEvent );
    ASSERT_EQ( reinterpret_cast< candymaker::StateEvent::Proc >( setEventProcsWithoutStateDataTestLeaveEventProc ), STATE.leaveEventProcPtr );
}

TEST(
    CreatingStateTest
    , GetBasesystem
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &i
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< int > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    j = 20;

    state.enter(
        [
            &j
        ]
        (
            fg::CreatingState< int > &  _state
        )
        {
            j = _state.getBasesystem_< int >();

            return &j;
        }
        , reinterpret_cast< void ( * )( int * ) >( dummyDestroy )
    );

    ASSERT_EQ( 10, j );
}

TEST(
    CreatingStateTest
    , GetBasesystem_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &i
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< int > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    j = 20;

    state.enter(
        [
            &j
        ]
        (
            fg::CreatingState<> &   _state
        )
        {
            j = _state.getBasesystem_< int >();
        }
    );

    ASSERT_EQ( 10, j );
}
