﻿#include "fg/util/test.h"
#include "candymaker/core/state/threadimpl.h"

TEST(
    StateThreadImplTest
    , GetState
)
{
    auto    threadImpl = candymaker::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );

    ASSERT_EQ( reinterpret_cast< FgState * >( 10 ), threadImpl.getState() );
}

TEST(
    StateThreadImplTest
    , GetData
)
{
    auto    threadImpl = candymaker::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , reinterpret_cast< void * >( 20 )
    );

    ASSERT_EQ( reinterpret_cast< void * >( 20 ), threadImpl.getData() );
}
