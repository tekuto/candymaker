﻿#include "fg/util/export.h"
#include "fg/core/state/event.h"
#include "candymaker/core/state/eventimpl.h"

FgState * fgStateEventGetState(
    FgStateEvent *  _implPtr
)
{
    return reinterpret_cast< candymaker::StateEvent * >( _implPtr )->getState();
}

void * fgStateEventGetData(
    FgStateEvent *  _implPtr
)
{
    return reinterpret_cast< candymaker::StateEvent * >( _implPtr )->getData();
}
