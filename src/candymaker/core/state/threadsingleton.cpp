﻿#include "fg/util/export.h"
#include "fg/core/state/threadsingleton.h"
#include "candymaker/core/state/threadimpl.h"

FgState * fgStateThreadSingletonGetState(
    FgStateThreadSingleton *    _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadSingletonGetData(
    FgStateThreadSingleton *    _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getData();
}
