﻿#include "candymaker/core/state/eventimpl.h"

namespace candymaker {
    StateEvent::StateEvent(
        FgState &   _state
        , void *    _eventDataPtr
    )
        : state( _state )
        , eventDataPtr( _eventDataPtr )
    {
    }

    FgState * StateEvent::getState(
    )
    {
        return &( this->state );
    }

    void * StateEvent::getData(
    )
    {
        return this->eventDataPtr;
    }
}
