﻿#include "fg/util/export.h"
#include "fg/core/state/thread.h"
#include "candymaker/core/state/threadimpl.h"

FgState * fgStateThreadGetState(
    FgStateThread * _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadGetData(
    FgStateThread * _implPtr
)
{
    return reinterpret_cast< candymaker::StateThread * >( _implPtr )->getData();
}
