﻿#include "candymaker/core/file/execute.h"
#include "candymaker/core/file/template.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/file/common.h"

#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <stdexcept>

namespace {
    const auto  EXECUTES_DIRECTORY = "executes";

    const auto  SUFFIX = ".fge";

    const auto  AT_SELF = "self";
    const auto  AT_CURRENT = "current";
    const auto  AT_SYSTEM = "system";

    using PathList = std::vector< std::string >;

    std::string relativeSaveDirectoryPathToString(
        const candymaker::ExecuteFile::Save &   _SAVE
        , const candymaker::Path &              _PACKAGE_PATH
    )
    {
        const auto &    SAVE_PATH = _SAVE.existsPath == false
            ? _PACKAGE_PATH
            : _SAVE.defaultPath == true
                ? _PACKAGE_PATH
                : _SAVE.path
        ;

        return candymaker::relativeDirectoryPathToString( SAVE_PATH );
    }

    candymaker::ExecuteFile::ConstUnique createExecuteFile(
        const candymaker::TemplateFile &
        , const std::string &
        , const std::string &
        , PathList &
    );

    void selfToCurrent(
        std::string &               _at
        , bool &                    _existsPackage
        , candymaker::Path &        _package
        , const candymaker::Path *  _SELF_PACKAGE_PTR
    )
    {
        if( _at != AT_SELF ) {
            return;
        }

        if( _SELF_PACKAGE_PTR == nullptr ) {
            throw std::runtime_error( "パッケージ外のファイルでselfを指定した" );
        }

        if( _existsPackage == true ) {
            throw std::runtime_error( "パス指定がselfなのにパスリストが存在する" );
        }

        _at = AT_CURRENT;
        _existsPackage = true;
        _package = *_SELF_PACKAGE_PTR;
    }

    void selfToCurrent(
        candymaker::TemplateFile::Templates &   _templates
        , const candymaker::Path *              _SELF_PACKAGE_PTR
    )
    {
        for( auto & template_ : _templates ) {
            selfToCurrent(
                template_.at
                , template_.existsPackage
                , template_.package
                , _SELF_PACKAGE_PTR
            );
        }
    }

    void selfToCurrent(
        candymaker::ExecuteFile::Packages & _packages
        , const candymaker::Path *          _SELF_PACKAGE_PTR
    )
    {
        for( auto & package : _packages ) {
            selfToCurrent(
                package.at
                , package.existsPath
                , package.path
                , _SELF_PACKAGE_PTR
            );
        }
    }

    void selfToCurrent(
        candymaker::ExecuteFile::Basesystem &   _basesystem
        , const candymaker::Path *              _SELF_PACKAGE_PTR
    )
    {
        selfToCurrent(
            _basesystem.at
            , _basesystem.existsPackage
            , _basesystem.package
            , _SELF_PACKAGE_PTR
        );
    }

    void selfToCurrent(
        candymaker::ExecuteFile::Game & _game
        , const candymaker::Path *      _SELF_PACKAGE_PTR
    )
    {
        selfToCurrent(
            _game.at
            , _game.existsPackage
            , _game.package
            , _SELF_PACKAGE_PTR
        );
    }

    void selfToCurrent(
        candymaker::TemplateFile &  _file
        , const candymaker::Path *  _SELF_PACKAGE_PTR
    )
    {
        selfToCurrent(
            _file.templates
            , _SELF_PACKAGE_PTR
        );

        selfToCurrent(
            _file.packages
            , _SELF_PACKAGE_PTR
        );

        selfToCurrent(
            _file.basesystem
            , _SELF_PACKAGE_PTR
        );

        selfToCurrent(
            _file.game
            , _SELF_PACKAGE_PTR
        );
    }

    struct SelfToCurrent
    {
        void operator()(
            candymaker::TemplateFile &  _templateFile
            , const candymaker::Path *  _SELF_PACKAGE_PTR
        ) const
        {
            selfToCurrent(
                _templateFile
                , _SELF_PACKAGE_PTR
            );
        }
    };

    void toSystem(
        std::string &   _at
    )
    {
        if( _at != AT_CURRENT ) {
            return;
        }

        _at = AT_SYSTEM;
    }

    void toSystem(
        candymaker::TemplateFile::Templates &   _templates
    )
    {
        for( auto & template_ : _templates ) {
            toSystem( template_.at );
        }
    }

    void toSystem(
        candymaker::ExecuteFile::Packages & _packages
    )
    {
        for( auto & package : _packages ) {
            toSystem( package.at );
        }
    }

    void toSystem(
        candymaker::ExecuteFile::Basesystem &   _basesystem
    )
    {
        toSystem( _basesystem.at );
    }

    void toSystem(
        candymaker::ExecuteFile::Game & _game
    )
    {
        toSystem( _game.at );
    }

    void toSystem(
        candymaker::TemplateFile &  _file
    )
    {
        toSystem( _file.templates );
        toSystem( _file.packages );
        toSystem( _file.basesystem );
        toSystem( _file.game );
    }

    struct ToSystem
    {
        void operator()(
            candymaker::TemplateFile &  _templateFile
            , const candymaker::Path *  _SELF_PACKAGE_PTR
        ) const
        {
            selfToCurrent(
                _templateFile
                , _SELF_PACKAGE_PTR
            );

            toSystem( _templateFile );
        }
    };

    template< typename TRANSLATE_PROC_T >
    auto createTemplateFile(
        const std::string &         _PATH
        , const candymaker::Path *  _SELF_PACKAGE_PTR
    )
    {
        auto    templateFileUnique = candymaker::TemplateFile::create( _PATH );
        auto &  templateFile = const_cast< candymaker::TemplateFile & >( *templateFileUnique );

        TRANSLATE_PROC_T()(
            templateFile
            ,  _SELF_PACKAGE_PTR
        );

        return templateFileUnique;
    }

    bool isExistsPath(
        const PathList &        _PATH_LIST
        , const std::string &   _PATH_STRING
    )
    {
        const auto  END = _PATH_LIST.end();

        return std::find_if(
            _PATH_LIST.begin()
            , END
            , [
                &_PATH_STRING
            ]
            (
                const PathList::value_type &    _LIST_ELEMENT
            )
            {
                return _LIST_ELEMENT == _PATH_STRING;
            }
        ) != END;
    }

    bool isSystemTemplate(
        const candymaker::TemplateFile::Template &  _TEMPLATE
    )
    {
        return _TEMPLATE.at == AT_SYSTEM;
    }

    template< typename TRANSLATE_PROC_T >
    auto createExecuteFile(
        const candymaker::TemplateFile::Template &  _TEMPLATE
        , const std::string &                       _DIRECTORY
        , const std::string &                       _SYSTEM_DIRECTORY
        , PathList &                                _templatePathList
    )
    {
        const auto &    PACKAGE = _TEMPLATE.package;

        auto    pathString = _DIRECTORY;

        pathString.append(
            candymaker::TemplateFile::relativeFilePathToString(
                PACKAGE
                , _TEMPLATE.file
            )
        );

        if( isExistsPath(
            _templatePathList
            , pathString
        ) == true ) {
            throw std::runtime_error( "同じテンプレートファイルを複数回読み込もうとしている" );
        }

        _templatePathList.push_back( pathString );

        const auto  TEMPLATE_FILE_UNIQUE = createTemplateFile< TRANSLATE_PROC_T >(
            pathString
            , &PACKAGE
        );
        const auto &    TEMPLATE_FILE = *TEMPLATE_FILE_UNIQUE;

        return createExecuteFile(
            TEMPLATE_FILE
            , _SYSTEM_DIRECTORY
            , _DIRECTORY
            , _templatePathList
        );
    }

    auto createExecuteFile(
        const candymaker::TemplateFile::Template &  _TEMPLATE
        , const std::string &                       _SYSTEM_DIRECTORY
        , const std::string &                       _CURRENT_DIRECTORY
        , PathList &                                _templatePathList
    )
    {
        if( isSystemTemplate( _TEMPLATE ) == false ) {
            return createExecuteFile< SelfToCurrent >(
                _TEMPLATE
                , _CURRENT_DIRECTORY
                , _SYSTEM_DIRECTORY
                , _templatePathList
            );
        } else {
            return createExecuteFile< ToSystem >(
                _TEMPLATE
                , _SYSTEM_DIRECTORY
                , _SYSTEM_DIRECTORY
                , _templatePathList
            );
        }
    }

    auto findPackage(
        candymaker::ExecuteFile::Packages &         _packages
        , const candymaker::ExecuteFile::Package &  _NEW_PACKAGE
    )
    {
        return std::find_if(
            _packages.begin()
            , _packages.end()
            , [
                &_NEW_PACKAGE
            ]
            (
                const candymaker::ExecuteFile::Package &    _PACKAGE
            )
            {
                if( _PACKAGE.at != _NEW_PACKAGE.at ) {
                    return false;
                }

                if( _PACKAGE.existsPath != _NEW_PACKAGE.existsPath ) {
                    return false;
                }

                if( _PACKAGE.path != _NEW_PACKAGE.path ) {
                    return false;
                }

                return true;
            }
        );
    }

    void merge(
        candymaker::ExecuteFile::Package::Modules &         _modules
        , const candymaker::ExecuteFile::Package::Modules & _NEW_MODULES
    )
    {
        for( const auto & NEW_MODULE : _NEW_MODULES ) {
            const auto &    MODULE_NAME = NEW_MODULE.first;
            const auto &    ENABLED = NEW_MODULE.second;

            const auto  END = _modules.end();

            auto    it = _modules.find( MODULE_NAME );
            if( it != END ) {
                auto &  enabled = it->second;

                enabled = ENABLED;
            } else {
                _modules.insert(
                    std::make_pair(
                        MODULE_NAME
                        , ENABLED
                    )
                );
            }
        }
    }

    void merge(
        candymaker::ExecuteFile::Save &         _save
        , const candymaker::ExecuteFile::Save & _NEW_SAVE
    )
    {
        if( _NEW_SAVE.existsPath == false ) {
            return;
        }

        _save = _NEW_SAVE;
    }

    void merge(
        candymaker::ExecuteFile::Package &          _package
        , const candymaker::ExecuteFile::Package &  _NEW_PACKAGE
    )
    {
        merge(
            _package.modules
            , _NEW_PACKAGE.modules
        );

        merge(
            _package.save
            , _NEW_PACKAGE.save
        );
    }

    void merge(
        candymaker::ExecuteFile::Packages &         _packages
        , const candymaker::ExecuteFile::Packages & _NEW_PACKAGES
    )
    {
        for( const auto & NEW_PACKAGE : _NEW_PACKAGES ) {
            const auto  END = _packages.end();

            auto    it = findPackage(
                _packages
                , NEW_PACKAGE
            );

            if( it != END ) {
                auto &  package = *it;

                merge(
                    package
                    , NEW_PACKAGE
                );
            } else {
                _packages.push_back( NEW_PACKAGE );
            }
        }
    }

    template< typename T >
    void mergePath(
        T &         _data
        , const T & _NEW_DATA
    )
    {
        if( _NEW_DATA.existsPath == false ) {
            return;
        }

        _data.existsPath = _NEW_DATA.existsPath;
        _data.at = _NEW_DATA.at;
        _data.existsPackage = _NEW_DATA.existsPackage;
        _data.package = _NEW_DATA.package;
        _data.name = _NEW_DATA.name;
    }

    void merge(
        candymaker::ExecuteFile::Basesystem &           _basesystem
        , const candymaker::ExecuteFile::Basesystem &   _NEW_BASESYSTEM
    )
    {
        mergePath(
            _basesystem
            , _NEW_BASESYSTEM
        );

        merge(
            _basesystem.save
            , _NEW_BASESYSTEM.save
        );
    }

    void merge(
        candymaker::ExecuteFile::Game &         _game
        , const candymaker::ExecuteFile::Game & _NEW_GAME
    )
    {
        mergePath(
            _game
            , _NEW_GAME
        );

        merge(
            _game.save
            , _NEW_GAME.save
        );
    }

    template< typename FILE_T >
    void merge(
        candymaker::ExecuteFile::Packages &     _packages
        , candymaker::ExecuteFile::Basesystem & _basesystem
        , candymaker::ExecuteFile::Game &       _game
        , const FILE_T &                        _FILE
    )
    {
        merge(
            _packages
            , _FILE.packages
        );

        merge(
            _basesystem
            , _FILE.basesystem
        );

        merge(
            _game
            , _FILE.game
        );
    }

    candymaker::ExecuteFile::ConstUnique createExecuteFile(
        const candymaker::TemplateFile &    _TEMPLATE_FILE
        , const std::string &               _SYSTEM_DIRECTORY
        , const std::string &               _CURRENT_DIRECTORY
        , PathList &                        _templatePathList
    )
    {
        auto    packages = candymaker::ExecuteFile::Packages();
        auto    basesystem = candymaker::ExecuteFile::Basesystem();
        auto    game = candymaker::ExecuteFile::Game();

        for( const auto & TEMPLATE : _TEMPLATE_FILE.templates ) {
            const auto  EXECUTE_FILE_UNIQUE = createExecuteFile(
                TEMPLATE
                , _SYSTEM_DIRECTORY
                , _CURRENT_DIRECTORY
                , _templatePathList
            );
            const auto &    EXECUTE_FILE = *EXECUTE_FILE_UNIQUE;

            merge(
                packages
                , basesystem
                , game
                , EXECUTE_FILE
            );
        }

        merge(
            packages
            , basesystem
            , game
            , _TEMPLATE_FILE
        );

        return new candymaker::ExecuteFile(
            std::move( packages )
            , std::move( basesystem )
            , std::move( game )
        );
    }

    template< typename TRANSLATE_PROC_T >
    auto createExecuteFile(
        const std::string &     _FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , const std::string &   _CURRENT_DIRECTORY
    )
    {
        const auto  TEMPLATE_FILE_UNIQUE = createTemplateFile< TRANSLATE_PROC_T >(
            _FILE
            , nullptr
        );
        const auto &    TEMPLATE_FILE = *TEMPLATE_FILE_UNIQUE;

        auto    templatePathList = PathList();

        auto    thisUnique = createExecuteFile(
            TEMPLATE_FILE
            , _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
            , templatePathList
        );

        if( thisUnique->basesystem.existsPath == false ) {
            throw std::runtime_error( "ベースシステムパスが存在しない" );
        }

        if( thisUnique->game.existsPath == false ) {
            throw std::runtime_error( "ゲームパスが存在しない" );
        }

        return thisUnique;
    }
}

namespace candymaker {
    std::string ExecuteFile::Package::relativeSaveDirectoryPathToString(
    ) const
    {
        return ::relativeSaveDirectoryPathToString(
            this->save
            , this->path
        );
    }

    std::string ExecuteFile::Basesystem::relativeSaveDirectoryPathToString(
    ) const
    {
        return ::relativeSaveDirectoryPathToString(
            this->save
            , this->package
        );
    }

    std::string ExecuteFile::Game::relativeSaveDirectoryPathToString(
    ) const
    {
        return ::relativeSaveDirectoryPathToString(
            this->save
            , this->package
        );
    }

    std::string ExecuteFile::relativePathToString(
        const Path &    _PATH
    )
    {
        auto    path = Path( { EXECUTES_DIRECTORY } );
        path.insert(
            path.end()
            , _PATH.begin()
            , _PATH.end()
        );

        auto    pathString = relativeFilePathToString( path );

        pathString.append( SUFFIX );

        return pathString;
    }

    ExecuteFile::ExecuteFile(
        Packages &&     _packages
        , Basesystem && _basesystem
        , Game &&       _game
    )
        : packages( std::move( _packages ) )
        , basesystem( std::move( _basesystem ) )
        , game( std::move( _game ) )
    {
    }

    ExecuteFile::ConstUnique ExecuteFile::create(
        const std::string &     _FILE
        , const std::string &   _SYSTEM_DIRECTORY
        , const std::string &   _CURRENT_DIRECTORY
    )
    {
        return createExecuteFile< SelfToCurrent >(
            _FILE
            , _SYSTEM_DIRECTORY
            , _CURRENT_DIRECTORY
        );
    }

    ExecuteFile::ConstUnique ExecuteFile::create(
        const std::string &     _FILE
        , const std::string &   _SYSTEM_DIRECTORY
    )
    {
        return createExecuteFile< ToSystem >(
            _FILE
            , _SYSTEM_DIRECTORY
            , *static_cast< const std::string * >( nullptr )
        );
    }
}
