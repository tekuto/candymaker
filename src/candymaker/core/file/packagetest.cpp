﻿#include "fg/util/test.h"
#include "candymaker/core/file/package.h"

TEST(
    PackageFileTest
    , GetFileName
)
{
    ASSERT_STREQ( "package.fgp", candymaker::PackageFile::getFileName().c_str() );
}

TEST(
    PackageFileTest
    , RelativeDirectoryPathToString
)
{
    ASSERT_STREQ(
        "packages/packagePath1/packagePath2/"
        , candymaker::PackageFile::relativeDirectoryPathToString(
            candymaker::Path(
                {
                    "packagePath1",
                    "packagePath2",
                }
            )
        ).c_str()
    );
}

TEST(
    PackageFileTest
    , RelativeFilePathToString
)
{
    ASSERT_STREQ(
        "packages/packagePath1/packagePath2/filePath1/filePath2"
        , candymaker::PackageFile::relativeFilePathToString(
            candymaker::Path(
                {
                    "packagePath1",
                    "packagePath2",
                }
            )
            , candymaker::Path(
                {
                    "filePath1",
                    "filePath2",
                }
            )
        ).c_str()
    );
}

TEST(
    PackageFileTest
    , AbsoluteModulePathToString
)
{
    ASSERT_STREQ(
        "/modulePath1/libmodulePath2.so"
        , candymaker::PackageFile::absoluteModulePathToString(
            candymaker::Path(
                {
                    "modulePath1",
                    "modulePath2",
                }
            )
        ).c_str()
    );
}

TEST(
    PackageFileTest
    , AbsoluteModulePathToString_failedEmptyPath
)
{
    try {
        candymaker::PackageFile::absoluteModulePathToString( candymaker::Path() );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , RelativeModulePathToString
)
{
    ASSERT_STREQ(
        "modulePath1/libmodulePath2.so"
        , candymaker::PackageFile::relativeModulePathToString(
            candymaker::Path(
                {
                    "modulePath1",
                    "modulePath2",
                }
            )
        ).c_str()
    );
}

TEST(
    PackageFileTest
    , RelativeModulePathToString_failedEmptyPath
)
{
    try {
        candymaker::PackageFile::relativeModulePathToString( candymaker::Path() );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , Create_modules
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 2, MODULES.size() );

    const auto  END = MODULES.end();

    const auto  IT1 = MODULES.find( "module1" );
    ASSERT_NE( END, IT1 );
    const auto &    MODULE1 = IT1->second;

    ASSERT_STREQ( "relative", MODULE1.at.c_str() );

    const auto &    PATH1 = MODULE1.path;
    ASSERT_EQ( 2, PATH1.size() );
    ASSERT_STREQ( "path1_1", PATH1[ 0 ].c_str() );
    ASSERT_STREQ( "path1_2", PATH1[ 1 ].c_str() );

    ASSERT_TRUE( MODULE1.existsInitializer );
    ASSERT_STREQ( "initializer1", MODULE1.initializer.c_str() );

    const auto &    INTERFACES1 = MODULE1.interfaces;
    ASSERT_EQ( 2, INTERFACES1.size() );
    const auto &    INTERFACE1_1 = INTERFACES1[ 0 ];
    ASSERT_STREQ( "interfacePackage1_1", INTERFACE1_1.package.c_str() );
    ASSERT_STREQ( "interfaceModule1_1", INTERFACE1_1.module.c_str() );
    const auto &    INTERFACE1_2 = INTERFACES1[ 1 ];
    ASSERT_STREQ( "interfacePackage1_2", INTERFACE1_2.package.c_str() );
    ASSERT_STREQ( "interfaceModule1_2", INTERFACE1_2.module.c_str() );

    const auto &    DEPENDS1 = MODULE1.depends;
    ASSERT_EQ( 2, DEPENDS1.size() );
    const auto &    DEPEND1_1 = DEPENDS1[ 0 ];
    ASSERT_STREQ( "dependPackage1_1", DEPEND1_1.package.c_str() );
    ASSERT_STREQ( "dependModule1_1", DEPEND1_1.module.c_str() );
    const auto &    DEPEND1_2 = DEPENDS1[ 1 ];
    ASSERT_STREQ( "dependPackage1_2", DEPEND1_2.package.c_str() );
    ASSERT_STREQ( "dependModule1_2", DEPEND1_2.module.c_str() );

    const auto  IT2 = MODULES.find( "module2" );
    ASSERT_NE( END, IT2 );
    const auto &    MODULE2 = IT2->second;

    ASSERT_STREQ( "absolute", MODULE2.at.c_str() );

    const auto &    PATH2 = MODULE2.path;
    ASSERT_EQ( 2, PATH2.size() );
    ASSERT_STREQ( "path2_1", PATH2[ 0 ].c_str() );
    ASSERT_STREQ( "path2_2", PATH2[ 1 ].c_str() );

    ASSERT_TRUE( MODULE2.existsInitializer );
    ASSERT_STREQ( "initializer2", MODULE2.initializer.c_str() );

    const auto &    INTERFACES2 = MODULE2.interfaces;
    ASSERT_EQ( 2, INTERFACES2.size() );
    const auto &    INTERFACE2_1 = INTERFACES2[ 0 ];
    ASSERT_STREQ( "interfacePackage2_1", INTERFACE2_1.package.c_str() );
    ASSERT_STREQ( "interfaceModule2_1", INTERFACE2_1.module.c_str() );
    const auto &    INTERFACE2_2 = INTERFACES2[ 1 ];
    ASSERT_STREQ( "interfacePackage2_2", INTERFACE2_2.package.c_str() );
    ASSERT_STREQ( "interfaceModule2_2", INTERFACE2_2.module.c_str() );

    const auto &    DEPENDS2 = MODULE2.depends;
    ASSERT_EQ( 2, DEPENDS2.size() );
    const auto &    DEPEND2_1 = DEPENDS2[ 0 ];
    ASSERT_STREQ( "dependPackage2_1", DEPEND2_1.package.c_str() );
    ASSERT_STREQ( "dependModule2_1", DEPEND2_1.module.c_str() );
    const auto &    DEPEND2_2 = DEPENDS2[ 1 ];
    ASSERT_STREQ( "dependPackage2_2", DEPEND2_2.package.c_str() );
    ASSERT_STREQ( "dependModule2_2", DEPEND2_2.module.c_str() );
}

TEST(
    PackageFileTest
    , Create_moduleWithoutInitializer
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutinitializer.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 1, MODULES.size() );

    const auto  END = MODULES.end();

    const auto  IT = MODULES.find( "module" );
    ASSERT_NE( END, IT );
    const auto &    MODULE = IT->second;

    ASSERT_STREQ( "relative", MODULE.at.c_str() );

    const auto &    PATH = MODULE.path;
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path2", PATH[ 1 ].c_str() );

    ASSERT_FALSE( MODULE.existsInitializer );
    ASSERT_EQ( 0, MODULE.interfaces.size() );

    ASSERT_EQ( 0, MODULE.depends.size() );
}

TEST(
    PackageFileTest
    , Create_moduleWithoutInterfacePackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutinterfacepackage.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 1, MODULES.size() );

    const auto  END = MODULES.end();

    const auto  IT = MODULES.find( "module" );
    ASSERT_NE( END, IT );
    const auto &    MODULE = IT->second;

    ASSERT_STREQ( "relative", MODULE.at.c_str() );

    const auto &    PATH = MODULE.path;
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path2", PATH[ 1 ].c_str() );

    ASSERT_TRUE( MODULE.existsInitializer );
    ASSERT_STREQ( "initializer", MODULE.initializer.c_str() );

    const auto &    INTERFACES = MODULE.interfaces;
    ASSERT_EQ( 1, INTERFACES.size() );
    const auto &    INTERFACE = INTERFACES[ 0 ];
    ASSERT_FALSE( INTERFACE.existsPackage );
    ASSERT_STREQ( "interfaceModule", INTERFACE.module.c_str() );

    ASSERT_EQ( 0, MODULE.depends.size() );
}

TEST(
    PackageFileTest
    , Create_moduleWithoutDependPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutdependpackage.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULES = PACKAGE_FILE_UNIQUE->modules;
    ASSERT_EQ( 1, MODULES.size() );

    const auto  END = MODULES.end();

    const auto  IT = MODULES.find( "module" );
    ASSERT_NE( END, IT );
    const auto &    MODULE = IT->second;

    ASSERT_STREQ( "relative", MODULE.at.c_str() );

    const auto &    PATH = MODULE.path;
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path2", PATH[ 1 ].c_str() );

    ASSERT_TRUE( MODULE.existsInitializer );
    ASSERT_STREQ( "initializer", MODULE.initializer.c_str() );

    ASSERT_EQ( 0, MODULE.interfaces.size() );

    const auto &    DEPENDS = MODULE.depends;
    ASSERT_EQ( 1, DEPENDS.size() );
    const auto &    DEPEND = DEPENDS[ 0 ];
    ASSERT_FALSE( DEPEND.existsPackage );
    ASSERT_STREQ( "dependModule", DEPEND.module.c_str() );
}

TEST(
    PackageFileTest
    , Create_basesystems
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_basesystems.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEMS = PACKAGE_FILE_UNIQUE->basesystems;
    ASSERT_EQ( 2, BASESYSTEMS.size() );

    const auto  END = BASESYSTEMS.end();

    const auto  IT1 = BASESYSTEMS.find( "basesystem1" );
    ASSERT_NE( END, IT1 );
    const auto &    BASESYSTEM1 = IT1->second;
    ASSERT_STREQ( "module1", BASESYSTEM1.module.c_str() );
    ASSERT_STREQ( "initializer1", BASESYSTEM1.initializer.c_str() );

    const auto  IT2 = BASESYSTEMS.find( "basesystem2" );
    ASSERT_NE( END, IT2 );
    const auto &    BASESYSTEM2 = IT2->second;
    ASSERT_STREQ( "module2", BASESYSTEM2.module.c_str() );
    ASSERT_STREQ( "initializer2", BASESYSTEM2.initializer.c_str() );
}

TEST(
    PackageFileTest
    , Create_games
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_games.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    GAMES = PACKAGE_FILE_UNIQUE->games;
    ASSERT_EQ( 2, GAMES.size() );

    const auto  END = GAMES.end();

    const auto  IT1 = GAMES.find( "game1" );
    ASSERT_NE( END, IT1 );
    const auto &    GAME1 = IT1->second;
    ASSERT_STREQ( "module1", GAME1.module.c_str() );
    ASSERT_STREQ( "initializer1", GAME1.initializer.c_str() );

    const auto  IT2 = GAMES.find( "game2" );
    ASSERT_NE( END, IT2 );
    const auto &    GAME2 = IT2->second;
    ASSERT_STREQ( "module2", GAME2.module.c_str() );
    ASSERT_STREQ( "initializer2", GAME2.initializer.c_str() );
}

TEST(
    PackageFileTest
    , GetModule
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_STREQ( "relative", MODULE.at.c_str() );

    const auto &    PATH = MODULE.path;
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1_1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path1_2", PATH[ 1 ].c_str() );

    ASSERT_TRUE( MODULE.existsInitializer );
    ASSERT_STREQ( "initializer1", MODULE.initializer.c_str() );

    const auto &    INTERFACES = MODULE.interfaces;
    ASSERT_EQ( 2, INTERFACES.size() );
    const auto &    INTERFACE1 = INTERFACES[ 0 ];
    ASSERT_STREQ( "interfacePackage1_1", INTERFACE1.package.c_str() );
    ASSERT_STREQ( "interfaceModule1_1", INTERFACE1.module.c_str() );
    const auto &    INTERFACE2 = INTERFACES[ 1 ];
    ASSERT_STREQ( "interfacePackage1_2", INTERFACE2.package.c_str() );
    ASSERT_STREQ( "interfaceModule1_2", INTERFACE2.module.c_str() );

    const auto &    DEPENDS = MODULE.depends;
    ASSERT_EQ( 2, DEPENDS.size() );
    const auto &    DEPEND1 = DEPENDS[ 0 ];
    ASSERT_STREQ( "dependPackage1_1", DEPEND1.package.c_str() );
    ASSERT_STREQ( "dependModule1_1", DEPEND1.module.c_str() );
    const auto &    DEPEND2 = DEPENDS[ 1 ];
    ASSERT_STREQ( "dependPackage1_2", DEPEND2.package.c_str() );
    ASSERT_STREQ( "dependModule1_2", DEPEND2.module.c_str() );
}

TEST(
    PackageFileTest
    , GetModule_failed
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    try {
        PACKAGE_FILE_UNIQUE->getModule( "notfound" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , GetBasesystem
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_basesystems.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = PACKAGE_FILE_UNIQUE->getBasesystem( "basesystem1" );

    ASSERT_STREQ( "module1", BASESYSTEM.module.c_str() );
    ASSERT_STREQ( "initializer1", BASESYSTEM.initializer.c_str() );
}

TEST(
    PackageFileTest
    , GetBasesystem_failed
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_basesystems.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    try {
        PACKAGE_FILE_UNIQUE->getBasesystem( "notfound" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , GetGame
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_games.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    GAME = PACKAGE_FILE_UNIQUE->getGame( "game1" );

    ASSERT_STREQ( "module1", GAME.module.c_str() );
    ASSERT_STREQ( "initializer1", GAME.initializer.c_str() );
}

TEST(
    PackageFileTest
    , GetGame_failed
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_games.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    try {
        PACKAGE_FILE_UNIQUE->getGame( "notfound" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , GetAt
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_STREQ( "relative", MODULE.getAt().c_str() );
}

TEST(
    PackageFileTest
    , GetPath
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    const auto &    PATH = MODULE.getPath();
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1_1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path1_2", PATH[ 1 ].c_str() );
}

TEST(
    PackageFileTest
    , IsExistsInitializer
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_TRUE( MODULE.isExistsInitializer() );
}

TEST(
    PackageFileTest
    , IsExistsInitializer_notExists
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutinitializer.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module" );

    ASSERT_FALSE( MODULE.isExistsInitializer() );
}

TEST(
    PackageFileTest
    , GetInitializer_module
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    const auto &    INITIALIZER = MODULE.getInitializer();
    ASSERT_STREQ( "initializer1", INITIALIZER.c_str() );
}

TEST(
    PackageFileTest
    , GetInitializer_moduleNotExists
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutinitializer.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module" );

    try {
        MODULE.getInitializer();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageFileTest
    , GetDepends
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_EQ( &( MODULE.depends ), &( MODULE.getDepends() ) );
}

TEST(
    PackageFileTest
    , IsLocalPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modulewithoutdependpackage.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module" );

    const auto &    DEPENDS = MODULE.getDepends();

    ASSERT_TRUE( DEPENDS[ 0 ].isLocalPackage() );
}

TEST(
    PackageFileTest
    , IsLocalPackage_globalPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    const auto &    DEPENDS = MODULE.getDepends();

    ASSERT_FALSE( DEPENDS[ 0 ].isLocalPackage() );
}

TEST(
    PackageFileTest
    , GetPackage
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    const auto &    DEPENDS = MODULE.getDepends();

    ASSERT_STREQ( "dependPackage1_1", DEPENDS[ 0 ].getPackage().c_str() );
}

TEST(
    PackageFileTest
    , IsImplementedModule
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_TRUE(
        MODULE.isImplementedModule(
            candymaker::PackageFile::Module::Depend{
                true,
                "interfacePackage1_1",
                "interfaceModule1_1",
            }
        )
    );
    ASSERT_TRUE(
        MODULE.isImplementedModule(
            candymaker::PackageFile::Module::Depend{
                true,
                "interfacePackage1_2",
                "interfaceModule1_2",
            }
        )
    );
}

TEST(
    PackageFileTest
    , IsImplementedModule_failed
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_modules.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    MODULE = PACKAGE_FILE_UNIQUE->getModule( "module1" );

    ASSERT_FALSE(
        MODULE.isImplementedModule(
            candymaker::PackageFile::Module::Depend{
                true,
                "notImplementedPackage",
                "notImplementedModule",
            }
        )
    );
}

TEST(
    PackageFileTest
    , GetModule_basesystem
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_basesystems.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = PACKAGE_FILE_UNIQUE->getBasesystem( "basesystem1" );

    ASSERT_STREQ( "module1", BASESYSTEM.getModule().c_str() );
}

TEST(
    PackageFileTest
    , GetInitializer_basesystem
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_basesystems.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = PACKAGE_FILE_UNIQUE->getBasesystem( "basesystem1" );

    ASSERT_STREQ( "initializer1", BASESYSTEM.getInitializer().c_str() );
}

TEST(
    PackageFileTest
    , GetModule_game
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_games.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    GAME = PACKAGE_FILE_UNIQUE->getGame( "game1" );

    ASSERT_STREQ( "module1", GAME.getModule().c_str() );
}

TEST(
    PackageFileTest
    , GetInitializer_game
)
{
    const auto  PACKAGE_FILE_UNIQUE = candymaker::PackageFile::create( "packagetest/createtest_games.fgp" );
    ASSERT_NE( nullptr, PACKAGE_FILE_UNIQUE.get() );

    const auto &    GAME = PACKAGE_FILE_UNIQUE->getGame( "game1" );

    ASSERT_STREQ( "initializer1", GAME.getInitializer().c_str() );
}
