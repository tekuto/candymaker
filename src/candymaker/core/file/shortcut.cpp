﻿#include "candymaker/core/file/shortcut.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/file/common.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/map/map.h"

#include <string>
#include <utility>
#include <stdexcept>
#include <libgen.h>

namespace {
    const auto  KEY_AT = "at";
    const auto  KEY_CURRENT_DIRECTORY = "currentDirectory";
    const auto  KEY_EXECUTE_FILE = "executeFile";

    const auto  AT_ABSOLUTE = "absolute";
    const auto  AT_CURRENT = "current";
    const auto  AT_SYSTEM = "system";
    const auto  AT_HOME = "home";

    auto createAt(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  AT_PTR = _MAP.getDataElementString( KEY_AT );
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "マップに文字列のat要素が存在しない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createCurrentDirectory(
        bool &                          _existsCurrentDirectroy
        , const fg::DataElementMap &    _MAP
    )
    {
        const auto  CURRENT_DIRECTORY_ELEMENT_PTR = _MAP.getDataElement( KEY_CURRENT_DIRECTORY );
        if( CURRENT_DIRECTORY_ELEMENT_PTR == nullptr ) {
            _existsCurrentDirectroy = false;

            return candymaker::Path();
        }

        const auto  CURRENT_DIRECTORY_PTR = CURRENT_DIRECTORY_ELEMENT_PTR->getDataElementList();
        if( CURRENT_DIRECTORY_PTR == nullptr ) {
            throw std::runtime_error( "マップのcurrentDirectory要素がリストではない" );
        }
        const auto &    CURRENT_DIRECTORY = *CURRENT_DIRECTORY_PTR;

        _existsCurrentDirectroy = true;

        return candymaker::createPath( CURRENT_DIRECTORY );
    }

    auto createExecuteFile(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  EXECUTE_FILE_PTR = _MAP.getDataElementList( KEY_EXECUTE_FILE );
        if( EXECUTE_FILE_PTR == nullptr ) {
            throw std::runtime_error( "マップにリストのexecuteFile要素が存在しない" );
        }
        const auto &    EXECUTE_FILE = *EXECUTE_FILE_PTR;

        return candymaker::createPath( EXECUTE_FILE );
    }

    auto currentDirectoryToStringAbsolute(
        const candymaker::ShortcutFile &    _FILE
    )
    {
        if( _FILE.existsCurrentDirectory == false ) {
            throw std::runtime_error( "カレントディレクトリが存在しない" );
        }

        return candymaker::absoluteDirectoryPathToString( _FILE.currentDirectory );
    }

    auto currentDirectoryToStringCurrent(
        const candymaker::ShortcutFile &    _FILE
        ,  const std::string &              _FILE_PATH
    )
    {
        if( _FILE.existsCurrentDirectory == true ) {
            throw std::runtime_error( "不要なカレントディレクトリが存在する" );
        }

        return candymaker::createDirectoryName( _FILE_PATH );
    }
}

namespace candymaker {
    ShortcutFile::ShortcutFile(
        std::string &&  _at
        , bool &&       _existsCurrentDirectory
        , Path &&       _currentDirectory
        , Path &&       _executeFile
    )
        : at( std::move( _at ) )
        , existsCurrentDirectory( std::move( _existsCurrentDirectory ) )
        , currentDirectory( std::move( _currentDirectory ) )
        , executeFile( std::move( _executeFile ) )
    {
    }

    ShortcutFile::ConstUnique ShortcutFile::create(
        const std::string & _FILE_PATH
    )
    {
        const auto  ELEMENT_UNIQUE = createDataElement( _FILE_PATH );

        const auto  MAP_PTR = ELEMENT_UNIQUE->getDataElementMap();
        if( MAP_PTR == nullptr ) {
            throw std::runtime_error( "パッケージファイルがマップ要素ではない" );
        }
        const auto &    MAP = *MAP_PTR;

        auto    at = createAt( MAP );

        auto    existsCurrentDirectory = bool();
        auto    currentDirectory = ::createCurrentDirectory(
            existsCurrentDirectory
            , MAP
        );

        auto    executeFile = createExecuteFile( MAP );

        return new ShortcutFile(
            std::move( at )
            , std::move( existsCurrentDirectory )
            , std::move( currentDirectory )
            , std::move( executeFile )
        );
    }

    bool ShortcutFile::atSystem(
    ) const
    {
        if( this->at != AT_SYSTEM ) {
            return false;
        }

        if( this->existsCurrentDirectory == true ) {
            throw std::runtime_error( "不要なカレントディレクトリが存在する" );
        }

        return true;
    }

    bool ShortcutFile::atHome(
    ) const
    {
        if( this->at != AT_HOME ) {
            return false;
        }

        if( this->existsCurrentDirectory == true ) {
            throw std::runtime_error( "不要なカレントディレクトリが存在する" );
        }

        return true;
    }

    std::string ShortcutFile::createCurrentDirectory(
        const std::string & _FILE_PATH
    ) const
    {
        const auto &    AT = this->at;
        if( AT == AT_ABSOLUTE ) {
            return currentDirectoryToStringAbsolute( *this );
        } else if( AT == AT_CURRENT ) {
            return currentDirectoryToStringCurrent(
                *this
                , _FILE_PATH
            );
        }

        throw std::runtime_error( "カレントディレクトリパスを生成できないカレント位置" );
    }

    std::string ShortcutFile::createExecuteFilePath(
    ) const
    {
        return ExecuteFile::relativePathToString( this->executeFile );
    }
}
