﻿#include "fg/util/test.h"
#include "candymaker/core/file/template.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/file/path.h"

TEST(
    TemplateFileTest
    , RelativeFilePathToString
)
{
    ASSERT_STREQ(
        "packages/packagePath1/packagePath2/filePath1/filePath2.fgt"
        , candymaker::TemplateFile::relativeFilePathToString(
            candymaker::Path(
                {
                    "packagePath1",
                    "packagePath2",
                }
            )
            , candymaker::Path(
                {
                    "filePath1",
                    "filePath2",
                }
            )
        ).c_str()
    );
}

TEST(
    TemplateFileTest
    , Create_templates
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_templates.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    const auto &    TEMPLATES = TEMPLATE_FILE_UNIQUE->templates;
    ASSERT_EQ( 2, TEMPLATES.size() );

    const auto &    TEMPLATE1 = TEMPLATES[ 0 ];
    ASSERT_STREQ( "templateAt1", TEMPLATE1.at.c_str() );
    ASSERT_TRUE( TEMPLATE1.existsPackage );
    const auto &    TEMPLATE1_PACKAGE = TEMPLATE1.package;
    ASSERT_EQ( 2, TEMPLATE1_PACKAGE.size() );
    ASSERT_STREQ( "packagePath1_1", TEMPLATE1_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", TEMPLATE1_PACKAGE[ 1 ].c_str() );
    const auto &    TEMPLATE1_FILE = TEMPLATE1.file;
    ASSERT_EQ( 2, TEMPLATE1_FILE.size() );
    ASSERT_STREQ( "filePath1_1", TEMPLATE1_FILE[ 0 ].c_str() );
    ASSERT_STREQ( "filePath1_2", TEMPLATE1_FILE[ 1 ].c_str() );

    const auto &    TEMPLATE2 = TEMPLATES[ 1 ];
    ASSERT_STREQ( "templateAt2", TEMPLATE2.at.c_str() );
    ASSERT_TRUE( TEMPLATE2.existsPackage );
    const auto &    TEMPLATE2_PACKAGE = TEMPLATE2.package;
    ASSERT_EQ( 2, TEMPLATE2_PACKAGE.size() );
    ASSERT_STREQ( "packagePath2_1", TEMPLATE2_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", TEMPLATE2_PACKAGE[ 1 ].c_str() );
    const auto &    TEMPLATE2_FILE = TEMPLATE2.file;
    ASSERT_EQ( 2, TEMPLATE2_FILE.size() );
    ASSERT_STREQ( "filePath2_1", TEMPLATE2_FILE[ 0 ].c_str() );
    ASSERT_STREQ( "filePath2_2", TEMPLATE2_FILE[ 1 ].c_str() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_templateWithoutPackagePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_templatewithoutpackagepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    const auto &    TEMPLATES = TEMPLATE_FILE_UNIQUE->templates;
    ASSERT_EQ( 1, TEMPLATES.size() );

    const auto &    TEMPLATE = TEMPLATES[ 0 ];
    ASSERT_STREQ( "templateAt", TEMPLATE.at.c_str() );
    ASSERT_FALSE( TEMPLATE.existsPackage );
    ASSERT_EQ( 0, TEMPLATE.package.size() );
    const auto &    TEMPLATE_FILE = TEMPLATE.file;
    ASSERT_EQ( 2, TEMPLATE_FILE.size() );
    ASSERT_STREQ( "filePath1", TEMPLATE_FILE[ 0 ].c_str() );
    ASSERT_STREQ( "filePath2", TEMPLATE_FILE[ 1 ].c_str() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_packages
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_packages.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    const auto &    PACKAGES = TEMPLATE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "packageAt1", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "packageAt2", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_packageWithSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_packagewithsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    const auto &    PACKAGES = TEMPLATE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "packageAt", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 2, PACKAGE_PATH.size() );
    ASSERT_STREQ( "packagePath1", PACKAGE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2", PACKAGE_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE.modules.size() );
    const auto &    SAVE = PACKAGE.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "packageSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packageSavePath2", SAVE_PATH[ 1 ].c_str() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_packageDefaultSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_packagedefaultsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    const auto &    PACKAGES = TEMPLATE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "packageAt", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 2, PACKAGE_PATH.size() );
    ASSERT_STREQ( "packagePath1", PACKAGE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2", PACKAGE_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE.modules.size() );
    const auto &    SAVE = PACKAGE.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_TRUE( SAVE.defaultPath );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_packageWithoutPackagePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_packagewithoutpackagepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    const auto &    PACKAGES = TEMPLATE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "packageAt", PACKAGE.at.c_str() );
    ASSERT_FALSE( PACKAGE.existsPath );
    ASSERT_EQ( 0, PACKAGE.path.size() );
    ASSERT_EQ( 0, PACKAGE.modules.size() );
    ASSERT_FALSE( PACKAGE.save.existsPath );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_basesystem
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_basesystem.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "basesystemAt", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_basesystemWithSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_basesystemwithsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "basesystemAt", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    const auto &    SAVE = BASESYSTEM.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "basesystemSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemSavePath2", SAVE_PATH[ 1 ].c_str() );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_basesystemOnlySavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_basesystemonlysavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    const auto &    SAVE = BASESYSTEM.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "basesystemSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemSavePath2", SAVE_PATH[ 1 ].c_str() );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_basesystemDefaultSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_basesystemdefaultsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    const auto &    SAVE = BASESYSTEM.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_TRUE( SAVE.defaultPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_basesystemWithoutPackagePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_basesystemwithoutpackagepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "basesystemAt", BASESYSTEM.at.c_str() );
    ASSERT_FALSE( BASESYSTEM.existsPackage );
    ASSERT_EQ( 0, BASESYSTEM.package.size() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_game
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_game.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "gameAt", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    TemplateFileTest
    , Create_gameWithSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_gamewithsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "gameAt", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    const auto &    SAVE = GAME.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "gameSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "gameSavePath2", SAVE_PATH[ 1 ].c_str() );
}

TEST(
    TemplateFileTest
    , Create_gameOnlySavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_gameonlysavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    const auto &    SAVE = GAME.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "gameSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "gameSavePath2", SAVE_PATH[ 1 ].c_str() );
}

TEST(
    TemplateFileTest
    , Create_gameDefaultSavePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_gamedefaultsavepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_FALSE( GAME.existsPath );
    const auto &    SAVE = GAME.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_TRUE( SAVE.defaultPath );
}

TEST(
    TemplateFileTest
    , Create_gameWithoutPackagePath
)
{
    const auto  TEMPLATE_FILE_UNIQUE = candymaker::TemplateFile::create( "templatetest/createtest_gamewithoutpackagepath.fge" );
    ASSERT_NE( nullptr, TEMPLATE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->templates.size() );

    ASSERT_EQ( 0, TEMPLATE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = TEMPLATE_FILE_UNIQUE->basesystem;
    ASSERT_FALSE( BASESYSTEM.existsPath );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = TEMPLATE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "gameAt", GAME.at.c_str() );
    ASSERT_FALSE( GAME.existsPackage );
    ASSERT_EQ( 0, GAME.package.size() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}
