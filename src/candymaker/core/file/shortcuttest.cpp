﻿#include "fg/util/test.h"
#include "candymaker/core/file/shortcut.h"

TEST(
    ShortcutFileTest
    , Create
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createtest.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_STREQ( "currentAt", SHORTCUT_FILE_UNIQUE->at.c_str() );
    ASSERT_TRUE( SHORTCUT_FILE_UNIQUE->existsCurrentDirectory );
    const auto &    CURRENT_DIRECTORY = SHORTCUT_FILE_UNIQUE->currentDirectory;
    ASSERT_EQ( 2, CURRENT_DIRECTORY.size() );
    ASSERT_STREQ( "currentDirectory1", CURRENT_DIRECTORY[ 0 ].c_str() );
    ASSERT_STREQ( "currentDirectory2", CURRENT_DIRECTORY[ 1 ].c_str() );
    const auto &    EXECUTE_FILE = SHORTCUT_FILE_UNIQUE->executeFile;
    ASSERT_EQ( 2, EXECUTE_FILE.size() );
    ASSERT_STREQ( "executeFile1", EXECUTE_FILE[ 0 ].c_str() );
    ASSERT_STREQ( "executeFile2", EXECUTE_FILE[ 1 ].c_str() );
}

TEST(
    ShortcutFileTest
    , Create_withoutCurrentDirPath
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createtest_withoutcurrentdirpath.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_STREQ( "currentAt", SHORTCUT_FILE_UNIQUE->at.c_str() );
    ASSERT_FALSE( SHORTCUT_FILE_UNIQUE->existsCurrentDirectory );
    ASSERT_EQ( 0, SHORTCUT_FILE_UNIQUE->currentDirectory.size() );
    const auto &    EXECUTE_FILE = SHORTCUT_FILE_UNIQUE->executeFile;
    ASSERT_EQ( 2, EXECUTE_FILE.size() );
    ASSERT_STREQ( "executeFile1", EXECUTE_FILE[ 0 ].c_str() );
    ASSERT_STREQ( "executeFile2", EXECUTE_FILE[ 1 ].c_str() );
}

TEST(
    ShortcutFileTest
    , AtSystem_true
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/atsystemtest_true.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_TRUE( SHORTCUT_FILE_UNIQUE->atSystem() );
}

TEST(
    ShortcutFileTest
    , AtSystem_false
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/atsystemtest_false.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_FALSE( SHORTCUT_FILE_UNIQUE->atSystem() );
}

TEST(
    ShortcutFileTest
    , AtSystem_failed
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/atsystemtest_failed.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    try {
        SHORTCUT_FILE_UNIQUE->atSystem();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ShortcutFileTest
    , AtHome_true
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/athometest_true.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_TRUE( SHORTCUT_FILE_UNIQUE->atHome() );
}

TEST(
    ShortcutFileTest
    , AtHome_false
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/athometest_false.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_FALSE( SHORTCUT_FILE_UNIQUE->atHome() );
}

TEST(
    ShortcutFileTest
    , AtHome_failed
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/athometest_failed.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    try {
        SHORTCUT_FILE_UNIQUE->atHome();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ShortcutFileTest
    , CreateCurrentDirectory_atAbsolute
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createcurrentdirectorytest_atabsolute.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_STREQ( "/currentDirectory1/currentDirectory2/", SHORTCUT_FILE_UNIQUE->createCurrentDirectory( "shortcuttest/createcurrentdirectorytest_atabsolute.fgs" ).c_str() );
}

TEST(
    ShortcutFileTest
    , CreateCurrentDirectory_atCurrent
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createcurrentdirectorytest_atcurrent.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_STREQ( "shortcuttest/", SHORTCUT_FILE_UNIQUE->createCurrentDirectory( "shortcuttest/createcurrentdirectorytest_atcurrent.fgs" ).c_str() );
}

TEST(
    ShortcutFileTest
    , CreateCurrentDirectory_failed
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createcurrentdirectorytest_failed.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    try {
        SHORTCUT_FILE_UNIQUE->createCurrentDirectory( "shortcuttest/createcurrentdirectorytest_failed.fgs" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ShortcutFileTest
    , CreateCurrentDirectory_failedAtAbsolute
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createcurrentdirectorytest_failedatabsolute.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    try {
        SHORTCUT_FILE_UNIQUE->createCurrentDirectory( "shortcuttest/createcurrentdirectorytest_failedatabsolute.fgs" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ShortcutFileTest
    , CreateCurrentDirectory_failedAtCurrent
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createcurrentdirectorytest_failedatcurrent.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    try {
        SHORTCUT_FILE_UNIQUE->createCurrentDirectory( "shortcuttest/createcurrentdirectorytest_failedatcurrent.fgs" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ShortcutFileTest
    , CreateExecuteFilePath
)
{
    const auto  SHORTCUT_FILE_UNIQUE = candymaker::ShortcutFile::create( "shortcuttest/createexecutefilepathtest.fgs" );
    ASSERT_NE( nullptr, SHORTCUT_FILE_UNIQUE.get() );

    ASSERT_STREQ( "executes/executeFile1/executeFile2.fge", SHORTCUT_FILE_UNIQUE->createExecuteFilePath().c_str() );
}
