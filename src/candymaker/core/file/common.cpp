﻿#include "candymaker/core/file/common.h"
#include "fg/core/dataelement/dataelement.h"
#include "candymaker/core/common/filedata.h"
#include "candymaker/core/dataelement/dataelement.h"

#include <stdexcept>

namespace candymaker {
    fg::DataElement::ConstUnique createDataElement(
        const std::string & _FILE_PATH
    )
    {
        auto    fileData = candymaker::FileData();
        if( readFile(
            fileData
            , _FILE_PATH
        ) == false ) {
            throw std::runtime_error( "ファイル読み込みに失敗" );
        }

        return FgDataElement::create( fileData );
    }
}
