﻿#include "candymaker/core/file/template.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/core/file/package.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/file/common.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "fg/core/dataelement/dataelement.h"
#include "fg/core/dataelement/string.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/map/map.h"
#include "fg/core/dataelement/map/keys.h"

#include <string>
#include <cstddef>
#include <utility>
#include <stdexcept>

namespace {
    const auto  KEY_TEMPLATES = "templates";
    const auto  KEY_TEMPLATE_AT = "at";
    const auto  KEY_TEMPLATE_PACKAGE = "package";
    const auto  KEY_TEMPLATE_FILE = "file";

    const auto  KEY_PACKAGES = "packages";
    const auto  KEY_PACKAGE_AT = "at";
    const auto  KEY_PACKAGE_PATH = "package";
    const auto  KEY_PACKAGE_MODULES = "modules";
    const auto  KEY_PACKAGE_SAVE = "save";

    const auto  KEY_BASESYSTEM = "basesystem";
    const auto  KEY_BASESYSTEM_AT = "at";
    const auto  KEY_BASESYSTEM_PACKAGE = "package";
    const auto  KEY_BASESYSTEM_NAME = "name";
    const auto  KEY_BASESYSTEM_SAVE = "save";

    const auto  KEY_GAME = "game";
    const auto  KEY_GAME_AT = "at";
    const auto  KEY_GAME_PACKAGE = "package";
    const auto  KEY_GAME_NAME = "name";
    const auto  KEY_GAME_SAVE = "save";

    const auto  MODULE_DISABLE = "disable";
    const auto  MODULE_ENABLE = "enable";

    const auto  SAVE_DEFAULT = "default";

    const auto  SUFFIX = ".fgt";

    auto createTemplateAt(
        const fg::DataElementMap &  _TEMPLATE
    )
    {
        const auto  AT_PTR = _TEMPLATE.getDataElementString( KEY_TEMPLATE_AT );
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "テンプレート情報に文字列のat要素が存在しない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createTemplatePackage(
        bool &                          _existsPackage
        , const fg::DataElementMap &    _TEMPLATE
    )
    {
        const auto  PACKAGE_ELEMENT_PTR = _TEMPLATE.getDataElement( KEY_TEMPLATE_PACKAGE );
        if( PACKAGE_ELEMENT_PTR == nullptr ) {
            _existsPackage = false;

            return candymaker::Path();
        }

        const auto  PACKAGE_PTR = PACKAGE_ELEMENT_PTR->getDataElementList();
        if( PACKAGE_PTR == nullptr ) {
            throw std::runtime_error( "テンプレート情報のpackageがリスト要素ではない" );
        }
        const auto &    PACKAGE = *PACKAGE_PTR;

        _existsPackage = true;

        return candymaker::createPath( PACKAGE );
    }

    auto createTemplateFile(
        const fg::DataElementMap &  _TEMPLATE
    )
    {
        const auto  FILE_PTR = _TEMPLATE.getDataElementList( KEY_TEMPLATE_FILE );
        if( FILE_PTR == nullptr ) {
            throw std::runtime_error( "テンプレート情報にリストのfile要素が存在しない" );
        }
        const auto &    FILE = *FILE_PTR;

        return candymaker::createPath( FILE );
    }

    void addTemplate(
        candymaker::TemplateFile::Templates &   _templates
        , const fg::DataElementMap &            _TEMPLATE
    )
    {
        auto    at = createTemplateAt( _TEMPLATE );

        auto    existsPackage = bool();

        auto    package = createTemplatePackage(
            existsPackage
            , _TEMPLATE
        );

        auto    file = createTemplateFile( _TEMPLATE );

        _templates.push_back(
            candymaker::TemplateFile::Template{
                std::move( at ),
                std::move( existsPackage ),
                std::move( package ),
                std::move( file ),
            }
        );
    }

    auto createTemplates(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  TEMPLATES_ELEMENT_PTR = _MAP.getDataElement( KEY_TEMPLATES );
        if( TEMPLATES_ELEMENT_PTR == nullptr ) {
            return candymaker::TemplateFile::Templates();
        }

        const auto  TEMPLATES_PTR = TEMPLATES_ELEMENT_PTR->getDataElementList();
        if( TEMPLATES_PTR == nullptr ) {
            throw std::runtime_error( "templatesがリスト要素ではない" );
        }

        const auto  SIZE = TEMPLATES_PTR->getSize();

        auto    templates = candymaker::TemplateFile::Templates();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  TEMPLATE_PTR = TEMPLATES_PTR->getDataElementMap( i );
            if( TEMPLATE_PTR == nullptr ) {
                throw std::runtime_error( "テンプレートリストの要素がマップ要素ではない" );
            }
            const auto &    TEMPLATE = *TEMPLATE_PTR;

            addTemplate(
                templates
                , TEMPLATE
            );
        }

        return templates;
    }

    auto createPackageAt(
        const fg::DataElementMap &  _PACKAGE
    )
    {
        const auto  AT_PTR = _PACKAGE.getDataElementString( KEY_PACKAGE_AT );
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "パッケージ設定に文字列のat要素が存在しない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createPackagePath(
        bool &                          _existsPath
        , const fg::DataElementMap &    _PACKAGE
    )
    {
        const auto  PATH_ELEMENT_PTR = _PACKAGE.getDataElement( KEY_PACKAGE_PATH );
        if( PATH_ELEMENT_PTR == nullptr ) {
            _existsPath = false;

            return candymaker::Path();
        }

        const auto  PATH_PTR = PATH_ELEMENT_PTR->getDataElementList();
        if( PATH_PTR == nullptr ) {
            throw std::runtime_error( "パッケージ設定のpackageがリスト要素ではない" );
        }
        const auto &    PATH = *PATH_PTR;

        _existsPath = true;

        return candymaker::createPath( PATH );
    }

    void addModule(
        candymaker::ExecuteFile::Package::Modules & _modules
        , const char *                              _MODULE_NAME
        , const fg::DataElementString &             _MODULE_ENABLE
    )
    {
        const auto  ENABLE_STRING = std::string( _MODULE_ENABLE.getString() );

        auto    enable = bool();
        if( ENABLE_STRING == MODULE_ENABLE ) {
            enable = true;
        } else if( ENABLE_STRING == MODULE_DISABLE ) {
            enable = false;
        } else {
            throw std::runtime_error( "モジュール有効情報の値が不正" );
        }

        const auto  RESULT = _modules.insert(
            std::make_pair(
                _MODULE_NAME
                , std::move( enable )
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "モジュール有効集合への要素追加に失敗" );
        }
    }

    auto createPackageModules(
        const fg::DataElementMap &  _PACKAGE
    )
    {
        const auto  MODULES_ELEMENT_PTR = _PACKAGE.getDataElement( KEY_PACKAGE_MODULES );
        if( MODULES_ELEMENT_PTR == nullptr ) {
            return candymaker::ExecuteFile::Package::Modules();
        }

        const auto  MODULES_PTR = MODULES_ELEMENT_PTR->getDataElementMap();
        if( MODULES_PTR == nullptr ) {
            throw std::runtime_error( "パッケージ設定のモジュール情報がマップ要素ではない" );
        }
        const auto &    MODULES = *MODULES_PTR;

        const auto  MODULE_KEYS_UNIQUE = fg::DataElementMapKeys::create( MODULES );

        const auto  SIZE = MODULE_KEYS_UNIQUE->getSize();

        auto    modules = candymaker::ExecuteFile::Package::Modules();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  MODULE_NAME = MODULE_KEYS_UNIQUE->getKey( i );

            const auto  MODULE_ENABLE_PTR = MODULES.getDataElementString( MODULE_NAME );
            if( MODULE_ENABLE_PTR == nullptr ) {
                throw std::runtime_error( "モジュールマップの要素が文字列ではない" );
            }
            const auto &    MODULE_ENABLE = *MODULE_ENABLE_PTR;

            addModule(
                modules
                , MODULE_NAME
                , MODULE_ENABLE
            );
        }

        return modules;
    }

    auto createPackageSave(
        const fg::DataElementMap &  _PACKAGE
    )
    {
        const auto  SAVE_ELEMENT_PTR = _PACKAGE.getDataElement( KEY_PACKAGE_SAVE );
        if( SAVE_ELEMENT_PTR == nullptr ) {
            return candymaker::ExecuteFile::Save{
                false,
            };
        }

        const auto  SAVE_LIST_PTR = SAVE_ELEMENT_PTR->getDataElementList();
        if( SAVE_LIST_PTR != nullptr ) {
            const auto &    SAVE_LIST = *SAVE_LIST_PTR;

            auto    path = candymaker::createPath( SAVE_LIST );

            return candymaker::ExecuteFile::Save{
                true,
                false,
                std::move( path ),
            };
        }

        const auto  SAVE_STRING_PTR = SAVE_ELEMENT_PTR->getDataElementString();
        if( SAVE_STRING_PTR != nullptr ) {
            const auto  SAVE_STRING = std::string( SAVE_STRING_PTR->getString() );

            if( SAVE_STRING != SAVE_DEFAULT ) {
                throw std::runtime_error( "パッケージ設定のセーブ情報の文字列が不正" );
            }

            return candymaker::ExecuteFile::Save{
                true,
                true,
            };
        }

        throw std::runtime_error( "パッケージ設定のセーブ情報が不正" );
    }

    void addPackage(
        candymaker::ExecuteFile::Packages & _packages
        , const fg::DataElementMap &        _PACKAGE
    )
    {
        auto    at = createPackageAt( _PACKAGE );

        auto    existsPath = bool();

        auto    path = createPackagePath(
            existsPath
            , _PACKAGE
        );

        auto    modules = createPackageModules( _PACKAGE );

        auto    save = createPackageSave( _PACKAGE );

        _packages.push_back(
            candymaker::ExecuteFile::Package{
                std::move( at ),
                std::move( existsPath ),
                std::move( path ),
                std::move( modules ),
                std::move( save ),
            }
        );
    }

    auto createPackages(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  PACKAGES_ELEMENT_PTR = _MAP.getDataElement( KEY_PACKAGES );
        if( PACKAGES_ELEMENT_PTR == nullptr ) {
            return candymaker::ExecuteFile::Packages();
        }

        const auto  PACKAGES_PTR = PACKAGES_ELEMENT_PTR->getDataElementList();
        if( PACKAGES_PTR == nullptr ) {
            throw std::runtime_error( "packagesがリスト要素ではない" );
        }

        const auto  SIZE = PACKAGES_PTR->getSize();

        auto    packages = candymaker::ExecuteFile::Packages();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  PACKAGE_PTR = PACKAGES_PTR->getDataElementMap( i );
            if( PACKAGE_PTR == nullptr ) {
                throw std::runtime_error( "パッケージ設定リストの要素がマップ要素ではない" );
            }
            const auto &    PACKAGE = *PACKAGE_PTR;

            addPackage(
                packages
                , PACKAGE
            );
        }

        return packages;
    }

    auto createBasesystemAt(
        const fg::DataElement & _AT_ELEMENT
    )
    {
        const auto  AT_PTR = _AT_ELEMENT.getDataElementString();
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "ベースシステム設定のatが文字列要素ではない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createBasesystemPackage(
        bool &                      _existsPackage
        , const fg::DataElement *   _PACKAGE_ELEMENT_PTR
    )
    {
        if( _PACKAGE_ELEMENT_PTR == nullptr ) {
            _existsPackage = false;

            return candymaker::Path();
        }

        const auto  PACKAGE_PTR = _PACKAGE_ELEMENT_PTR->getDataElementList();
        if( PACKAGE_PTR == nullptr ) {
            throw std::runtime_error( "ベースシステム設定のpackageがリスト要素ではない" );
        }
        const auto &    PACKAGE = *PACKAGE_PTR;

        _existsPackage = true;

        return candymaker::createPath( PACKAGE );
    }

    auto createBasesystemName(
        const fg::DataElement & _NAME_ELEMENT
    )
    {
        const auto  NAME_PTR = _NAME_ELEMENT.getDataElementString();
        if( NAME_PTR == nullptr ) {
            throw std::runtime_error( "ベースシステム設定のnameが文字列要素ではない" );
        }
        const auto &    NAME = *NAME_PTR;

        return std::string( NAME.getString() );
    }

    auto createBasesystemSave(
        const fg::DataElementMap &  _BASESYSTEM
    )
    {
        const auto  SAVE_ELEMENT_PTR = _BASESYSTEM.getDataElement( KEY_BASESYSTEM_SAVE );
        if( SAVE_ELEMENT_PTR == nullptr ) {
            return candymaker::ExecuteFile::Save{
                false,
            };
        }

        const auto  SAVE_LIST_PTR = SAVE_ELEMENT_PTR->getDataElementList();
        if( SAVE_LIST_PTR != nullptr ) {
            const auto &    SAVE_LIST = *SAVE_LIST_PTR;

            auto    path = candymaker::createPath( SAVE_LIST );

            return candymaker::ExecuteFile::Save{
                true,
                false,
                std::move( path ),
            };
        }

        const auto  SAVE_STRING_PTR = SAVE_ELEMENT_PTR->getDataElementString();
        if( SAVE_STRING_PTR != nullptr ) {
            const auto  SAVE_STRING = std::string( SAVE_STRING_PTR->getString() );

            if( SAVE_STRING != SAVE_DEFAULT ) {
                throw std::runtime_error( "ベースシステム設定のセーブ情報の文字列が不正" );
            }

            return candymaker::ExecuteFile::Save{
                true,
                true,
            };
        }

        throw std::runtime_error( "ベースシステム設定のセーブ情報が不正" );
    }

    auto createBasesystem(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  BASESYSTEM_ELEMENT_PTR = _MAP.getDataElement( KEY_BASESYSTEM );
        if( BASESYSTEM_ELEMENT_PTR == nullptr ) {
            auto    basesystem = candymaker::ExecuteFile::Basesystem();
            basesystem.existsPath = false;
            basesystem.save.existsPath = false;

            return basesystem;
        }

        const auto  BASESYSTEM_PTR = BASESYSTEM_ELEMENT_PTR->getDataElementMap();
        if( BASESYSTEM_PTR == nullptr ) {
            throw std::runtime_error( "basesystemがマップ要素ではない" );
        }
        const auto &    BASESYSTEM = *BASESYSTEM_PTR;

        auto    basesystem = candymaker::ExecuteFile::Basesystem();

        const auto  AT_ELEMENT_PTR = BASESYSTEM.getDataElement( KEY_BASESYSTEM_AT );
        const auto  PACKAGE_ELEMENT_PTR = BASESYSTEM.getDataElement( KEY_BASESYSTEM_PACKAGE );
        const auto  NAME_ELEMENT_PTR = BASESYSTEM.getDataElement( KEY_BASESYSTEM_NAME );
        if( AT_ELEMENT_PTR != nullptr && NAME_ELEMENT_PTR != nullptr ) {
            const auto &    AT_ELEMENT = *AT_ELEMENT_PTR;
            const auto &    NAME_ELEMENT = *NAME_ELEMENT_PTR;

            basesystem.existsPath = true;

            basesystem.at = createBasesystemAt( AT_ELEMENT );
            basesystem.package = createBasesystemPackage(
                basesystem.existsPackage
                , PACKAGE_ELEMENT_PTR
            );
            basesystem.name = createBasesystemName( NAME_ELEMENT );
        } else if( AT_ELEMENT_PTR == nullptr && PACKAGE_ELEMENT_PTR == nullptr && NAME_ELEMENT_PTR == nullptr ) {
            basesystem.existsPath = false;
        } else {
            throw std::runtime_error( "ベースシステム設定のパス情報が不十分" );
        }

        basesystem.save = createBasesystemSave( BASESYSTEM );

        return basesystem;
    }

    auto createGameAt(
        const fg::DataElement & _AT_ELEMENT
    )
    {
        const auto  AT_PTR = _AT_ELEMENT.getDataElementString();
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "ゲーム設定のatが文字列要素ではない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createGamePackage(
        bool &                      _existsPackage
        , const fg::DataElement *   _PACKAGE_ELEMENT_PTR
    )
    {
        if( _PACKAGE_ELEMENT_PTR == nullptr ) {
            _existsPackage = false;

            return candymaker::Path();
        }

        const auto  PACKAGE_PTR = _PACKAGE_ELEMENT_PTR->getDataElementList();
        if( PACKAGE_PTR == nullptr ) {
            throw std::runtime_error( "ゲーム設定のpackageがリスト要素ではない" );
        }
        const auto &    PACKAGE = *PACKAGE_PTR;

        _existsPackage = true;

        return candymaker::createPath( PACKAGE );
    }

    auto createGameName(
        const fg::DataElement & _NAME_ELEMENT
    )
    {
        const auto  NAME_PTR = _NAME_ELEMENT.getDataElementString();
        if( NAME_PTR == nullptr ) {
            throw std::runtime_error( "ゲーム設定のnameが文字列要素ではない" );
        }
        const auto &    NAME = *NAME_PTR;

        return std::string( NAME.getString() );
    }

    auto createGameSave(
        const fg::DataElementMap &  _GAME
    )
    {
        const auto  SAVE_ELEMENT_PTR = _GAME.getDataElement( KEY_GAME_SAVE );
        if( SAVE_ELEMENT_PTR == nullptr ) {
            return candymaker::ExecuteFile::Save{
                false,
            };
        }

        const auto  SAVE_LIST_PTR = SAVE_ELEMENT_PTR->getDataElementList();
        if( SAVE_LIST_PTR != nullptr ) {
            const auto &    SAVE_LIST = *SAVE_LIST_PTR;

            auto    path = candymaker::createPath( SAVE_LIST );

            return candymaker::ExecuteFile::Save{
                true,
                false,
                std::move( path ),
            };
        }

        const auto  SAVE_STRING_PTR = SAVE_ELEMENT_PTR->getDataElementString();
        if( SAVE_STRING_PTR != nullptr ) {
            const auto  SAVE_STRING = std::string( SAVE_STRING_PTR->getString() );

            if( SAVE_STRING != SAVE_DEFAULT ) {
                throw std::runtime_error( "ゲーム設定のセーブ情報の文字列が不正" );
            }

            return candymaker::ExecuteFile::Save{
                true,
                true,
            };
        }

        throw std::runtime_error( "ゲーム設定のセーブ情報が不正" );
    }

    auto createGame(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  GAME_ELEMENT_PTR = _MAP.getDataElement( KEY_GAME );
        if( GAME_ELEMENT_PTR == nullptr ) {
            auto    game = candymaker::ExecuteFile::Game();
            game.existsPath = false;
            game.save.existsPath = false;

            return game;
        }

        const auto  GAME_PTR = GAME_ELEMENT_PTR->getDataElementMap();
        if( GAME_PTR == nullptr ) {
            throw std::runtime_error( "gameがマップ要素ではない" );
        }
        const auto &    GAME = *GAME_PTR;

        auto    game = candymaker::ExecuteFile::Game();

        const auto  AT_ELEMENT_PTR = GAME.getDataElement( KEY_GAME_AT );
        const auto  PACKAGE_ELEMENT_PTR = GAME.getDataElement( KEY_GAME_PACKAGE );
        const auto  NAME_ELEMENT_PTR = GAME.getDataElement( KEY_GAME_NAME );
        if( AT_ELEMENT_PTR != nullptr && NAME_ELEMENT_PTR != nullptr ) {
            const auto &    AT_ELEMENT = *AT_ELEMENT_PTR;
            const auto &    NAME_ELEMENT = *NAME_ELEMENT_PTR;

            game.existsPath = true;

            game.at = createGameAt( AT_ELEMENT );
            game.package = createGamePackage(
                game.existsPackage
                , PACKAGE_ELEMENT_PTR
            );
            game.name = createGameName( NAME_ELEMENT );
        } else if( AT_ELEMENT_PTR == nullptr && PACKAGE_ELEMENT_PTR == nullptr && NAME_ELEMENT_PTR == nullptr ) {
            game.existsPath = false;
        } else {
            throw std::runtime_error( "ゲーム設定のパス情報が不十分" );
        }

        game.save = createGameSave( GAME );

        return game;
    }
}

namespace candymaker {
    std::string TemplateFile::relativeFilePathToString(
        const Path &    _PACKAGE_PATH
        , const Path &  _FILE_PATH
    )
    {
        auto    pathString = candymaker::PackageFile::relativeFilePathToString(
            _PACKAGE_PATH
            , _FILE_PATH
        );

        pathString.append( SUFFIX );

        return pathString;
    }

    TemplateFile::TemplateFile(
        Templates &&                    _templates
        , ExecuteFile::Packages &&      _packages
        , ExecuteFile::Basesystem &&    _basesystem
        , ExecuteFile::Game &&          _game
    )
        : templates( std::move( _templates ) )
        , packages( std::move( _packages ) )
        , basesystem( std::move( _basesystem ) )
        , game( std::move( _game ) )
    {
    }

    TemplateFile::ConstUnique TemplateFile::create(
        const std::string & _FILE_PATH
    )
    {
        const auto  ELEMENT_UNIQUE = createDataElement( _FILE_PATH );

        const auto  MAP_PTR = ELEMENT_UNIQUE->getDataElementMap();
        if( MAP_PTR == nullptr ) {
            throw std::runtime_error( "テンプレートファイルがマップ要素ではない" );
        }
        const auto &    MAP = *MAP_PTR;

        auto    templates = createTemplates( MAP );
        auto    packages = createPackages( MAP );
        auto    basesystem = createBasesystem( MAP );
        auto    game = createGame( MAP );

        return new TemplateFile(
            std::move( templates )
            , std::move( packages )
            , std::move( basesystem )
            , std::move( game )
        );
    }
}
