﻿#include "fg/util/test.h"
#include "candymaker/core/file/path.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/string.h"

TEST(
    PathTest
    , Create
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    listUnique->addDataElement( fg::DataElementString::create( "path1" ) );
    listUnique->addDataElement( fg::DataElementString::create( "path2" ) );

    const auto  PATH = candymaker::createPath( LIST );
    ASSERT_EQ( 2, PATH.size() );
    ASSERT_STREQ( "path1", PATH[ 0 ].c_str() );
    ASSERT_STREQ( "path2", PATH[ 1 ].c_str() );
}

TEST(
    PathTest
    , AbsoluteFilePathToString
)
{
    const auto  PATH = candymaker::Path(
        {
            "path1",
            "path2",
        }
    );

    ASSERT_STREQ( "/path1/path2", candymaker::absoluteFilePathToString( PATH ).c_str() );
}

TEST(
    PathTest
    , RelativeFilePathToString
)
{
    const auto  PATH = candymaker::Path(
        {
            "path1",
            "path2",
        }
    );

    ASSERT_STREQ( "path1/path2", candymaker::relativeFilePathToString( PATH ).c_str() );
}

TEST(
    PathTest
    , AbsoluteDirectoryPathToString
)
{
    const auto  PATH = candymaker::Path(
        {
            "path1",
            "path2",
        }
    );

    ASSERT_STREQ( "/path1/path2/", candymaker::absoluteDirectoryPathToString( PATH ).c_str() );
}

TEST(
    PathTest
    , RelativeDirectoryPathToString
)
{
    const auto  PATH = candymaker::Path(
        {
            "path1",
            "path2",
        }
    );

    ASSERT_STREQ( "path1/path2/", candymaker::relativeDirectoryPathToString( PATH ).c_str() );
}

TEST(
    PathTest
    , CreateDirectoryName
)
{
    ASSERT_STREQ( "path1/", candymaker::createDirectoryName( "path1/path2" ).c_str() );
}

TEST(
    PathTest
    , CreateDirectoryPath
)
{
    ASSERT_STREQ( "path/", candymaker::createDirectoryPath( "path" ).c_str() );
}

TEST(
    PathTest
    , CreateDirectoryPath_containPathSplitter
)
{
    ASSERT_STREQ( "path/", candymaker::createDirectoryPath( "path/" ).c_str() );
}
