﻿#include "fg/util/test.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/core/dataelement/dataelement.h"

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_package
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGE = EXECUTE_FILE_UNIQUE->packages[ 0 ];

    ASSERT_STREQ( "packageSavePath1/packageSavePath2/", PACKAGE.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_packageWithDefaultSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withdefaultsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGE = EXECUTE_FILE_UNIQUE->packages[ 0 ];

    ASSERT_STREQ( "packagePath1/packagePath2/", PACKAGE.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_packageWithoutSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withoutsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGE = EXECUTE_FILE_UNIQUE->packages[ 0 ];

    ASSERT_STREQ( "packagePath1/packagePath2/", PACKAGE.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_basesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;

    ASSERT_STREQ( "basesystemSavePath1/basesystemSavePath2/", BASESYSTEM.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_basesystemWithDefaultSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withdefaultsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;

    ASSERT_STREQ( "basesystemPackage1/basesystemPackage2/", BASESYSTEM.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_basesystemWithoutSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withoutsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;

    ASSERT_STREQ( "basesystemPackage1/basesystemPackage2/", BASESYSTEM.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_game
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;

    ASSERT_STREQ( "gameSavePath1/gameSavePath2/", GAME.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_gameWithDefaultSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withdefaultsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;

    ASSERT_STREQ( "gamePackage1/gamePackage2/", GAME.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativeSaveDirectoryPathToString_gameWithoutSaveDirectoryPath
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/savedirectorypathtostringtest_withoutsavedirectory.fge"
        , "systemdirectory"
        , "currentdirectory"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;

    ASSERT_STREQ( "gamePackage1/gamePackage2/", GAME.relativeSaveDirectoryPathToString().c_str() );
}

TEST(
    ExecuteFileTest
    , RelativePathToString
)
{
    ASSERT_STREQ(
        "executes/path1/path2.fge"
        , candymaker::ExecuteFile::relativePathToString(
            candymaker::Path(
                {
                    "path1",
                    "path2",
                }
            )
        ).c_str()
    );
}

TEST(
    ExecuteFileTest
    , Create_withCurrentDirectory
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_withcurrentdirectory.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_withoutCurrentDirectory
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_withoutcurrentdirectory.fge"
        , "executetest/system/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 1, PACKAGE_PATH.size() );
    ASSERT_STREQ( "package", PACKAGE_PATH[ 0 ].c_str() );
    const auto &    MODULES = PACKAGE.modules;
    ASSERT_EQ( 2, MODULES.size() );
    const auto  MODULES_END = MODULES.end();
    ASSERT_NE( MODULES_END, MODULES.find( "systemModule1" ) );
    ASSERT_FALSE( MODULES.at( "systemModule1" ) );
    ASSERT_NE( MODULES_END, MODULES.find( "systemModule2" ) );
    ASSERT_TRUE( MODULES.at( "systemModule2" ) );
    ASSERT_FALSE( PACKAGE.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "system", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 1, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "package", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemBasesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "system", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 1, GAME_PACKAGE.size() );
    ASSERT_STREQ( "package", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemGameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentPackages
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentpackages.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentBasesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentbasesystem.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentGame
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentgame.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentSelfTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentselftemplate.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentSelfPackage
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentselfpackage.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 1, PACKAGE_PATH.size() );
    ASSERT_STREQ( "package", PACKAGE_PATH[ 0 ].c_str() );
    const auto &    MODULES = PACKAGE.modules;
    ASSERT_EQ( 2, MODULES.size() );
    const auto  MODULES_END = MODULES.end();
    ASSERT_NE( MODULES_END, MODULES.find( "module1" ) );
    ASSERT_FALSE( MODULES.at( "module1" ) );
    ASSERT_NE( MODULES_END, MODULES.find( "module2" ) );
    ASSERT_TRUE( MODULES.at( "module2" ) );
    ASSERT_FALSE( PACKAGE.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentSelfBasesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentselfbasesystem.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 1, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "package", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atCurrentSelfGame
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atcurrentselfgame.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 1, GAME_PACKAGE.size() );
    ASSERT_STREQ( "package", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemPackages
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystempackages.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "systemPackagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_1" ) );
    ASSERT_FALSE( MODULES1.at( "systemModule1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_2" ) );
    ASSERT_TRUE( MODULES1.at( "systemModule1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "system", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "systemPackagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemBasesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystembasesystem.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "system", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "systemBasesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemBasesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "systemBasesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemGame
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemgame.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "system", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "systemGamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemGamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "systemGameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemCurrentTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemcurrenttemplate.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "systemPackagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_1" ) );
    ASSERT_FALSE( MODULES1.at( "systemModule1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_2" ) );
    ASSERT_TRUE( MODULES1.at( "systemModule1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "system", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "systemPackagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemCurrentPackages
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemcurrentpackages.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "systemPackagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_1" ) );
    ASSERT_FALSE( MODULES1.at( "systemModule1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_2" ) );
    ASSERT_TRUE( MODULES1.at( "systemModule1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "system", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "systemPackagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemCurrentBasesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemcurrentbasesystem.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "system", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "systemBasesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemBasesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "systemBasesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemCurrentGame
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemcurrentgame.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "system", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "systemGamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemGamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "systemGameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemSelfTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemselftemplate.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "systemPackagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_1" ) );
    ASSERT_FALSE( MODULES1.at( "systemModule1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "systemModule1_2" ) );
    ASSERT_TRUE( MODULES1.at( "systemModule1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "system", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "systemPackagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "systemPackagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemSelfPackage
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemselfpackage.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "system", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 1, PACKAGE_PATH.size() );
    ASSERT_STREQ( "package", PACKAGE_PATH[ 0 ].c_str() );
    const auto &    MODULES = PACKAGE.modules;
    ASSERT_EQ( 2, MODULES.size() );
    const auto  MODULES_END = MODULES.end();
    ASSERT_NE( MODULES_END, MODULES.find( "systemModule1" ) );
    ASSERT_FALSE( MODULES.at( "systemModule1" ) );
    ASSERT_NE( MODULES_END, MODULES.find( "systemModule2" ) );
    ASSERT_TRUE( MODULES.at( "systemModule2" ) );
    ASSERT_FALSE( PACKAGE.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemSelfBasesystem
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemselfbasesystem.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "system", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 1, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "package", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemBasesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_atSystemSelfGame
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_atsystemselfgame.fge"
        , "executetest/system/"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    ASSERT_EQ( 0, EXECUTE_FILE_UNIQUE->packages.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "system", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 1, GAME_PACKAGE.size() );
    ASSERT_STREQ( "package", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "systemGameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergePackages
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergepackages.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 3, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    PACKAGE3 = PACKAGES[ 2 ];
    ASSERT_STREQ( "current", PACKAGE3.at.c_str() );
    ASSERT_TRUE( PACKAGE3.existsPath );
    const auto &    PACKAGE3_PATH = PACKAGE3.path;
    ASSERT_EQ( 2, PACKAGE3_PATH.size() );
    ASSERT_STREQ( "packagePath3_1", PACKAGE3_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath3_2", PACKAGE3_PATH[ 1 ].c_str() );
    const auto &    MODULES3 = PACKAGE3.modules;
    ASSERT_EQ( 2, MODULES3.size() );
    const auto  MODULES3_END = MODULES3.end();
    ASSERT_NE( MODULES3_END, MODULES3.find( "module3_1" ) );
    ASSERT_TRUE( MODULES3.at( "module3_1" ) );
    ASSERT_NE( MODULES3_END, MODULES3.find( "module3_2" ) );
    ASSERT_FALSE( MODULES3.at( "module3_2" ) );
    ASSERT_FALSE( PACKAGE3.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergePackageModules
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergepackagemodules.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 2, PACKAGE_PATH.size() );
    ASSERT_STREQ( "packagePath1", PACKAGE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2", PACKAGE_PATH[ 1 ].c_str() );
    const auto &    MODULES = PACKAGE.modules;
    ASSERT_EQ( 3, MODULES.size() );
    const auto  MODULES_END = MODULES.end();
    ASSERT_NE( MODULES_END, MODULES.find( "module1" ) );
    ASSERT_TRUE( MODULES.at( "module1" ) );
    ASSERT_NE( MODULES_END, MODULES.find( "module2" ) );
    ASSERT_FALSE( MODULES.at( "module2" ) );
    ASSERT_NE( MODULES_END, MODULES.find( "module3" ) );
    ASSERT_FALSE( MODULES.at( "module3" ) );
    ASSERT_FALSE( PACKAGE.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergePackageSave
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergepackagesave.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 1, PACKAGES.size() );

    const auto &    PACKAGE = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE.at.c_str() );
    ASSERT_TRUE( PACKAGE.existsPath );
    const auto &    PACKAGE_PATH = PACKAGE.path;
    ASSERT_EQ( 2, PACKAGE_PATH.size() );
    ASSERT_STREQ( "packagePath1", PACKAGE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2", PACKAGE_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE.modules.size() );
    const auto &    SAVE = PACKAGE.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_TRUE( SAVE.defaultPath );
    ASSERT_EQ( 0, SAVE.path.size() );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeBasesystemPathAtTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergebasesystempathattemplate.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage2_1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2_2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName2", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeBasesystemPathAtExecute
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergebasesystempathatexecute.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeBasesystemSaveAtTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergebasesystemsaveattemplate.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    const auto &    SAVE = BASESYSTEM.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "basesystemSavePath2_1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemSavePath2_2", SAVE_PATH[ 1 ].c_str() );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeBasesystemSaveAtExecute
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergebasesystemsaveatexecute.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    const auto &    SAVE = BASESYSTEM.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "basesystemSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemSavePath2", SAVE_PATH[ 1 ].c_str() );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeGamePathAtTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergegamepathattemplate.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage2_1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2_2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName2", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeGamePathAtExecute
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergegamepathatexecute.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    ASSERT_FALSE( GAME.save.existsPath );
}

TEST(
    ExecuteFileTest
    , Create_mergeGameSaveAtTemplate
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergegamesaveattemplate.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    const auto &    SAVE = GAME.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "gameSavePath2_1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "gameSavePath2_2", SAVE_PATH[ 1 ].c_str() );
}

TEST(
    ExecuteFileTest
    , Create_mergeGameSaveAtExecute
)
{
    const auto  EXECUTE_FILE_UNIQUE = candymaker::ExecuteFile::create(
        "executetest/createtest_mergegamesaveatexecute.fge"
        , "systemdirectory"
        , "executetest/"
    );
    ASSERT_NE( nullptr, EXECUTE_FILE_UNIQUE.get() );

    const auto &    PACKAGES = EXECUTE_FILE_UNIQUE->packages;
    ASSERT_EQ( 2, PACKAGES.size() );

    const auto &    PACKAGE1 = PACKAGES[ 0 ];
    ASSERT_STREQ( "current", PACKAGE1.at.c_str() );
    ASSERT_TRUE( PACKAGE1.existsPath );
    const auto &    PACKAGE1_PATH = PACKAGE1.path;
    ASSERT_EQ( 2, PACKAGE1_PATH.size() );
    ASSERT_STREQ( "packagePath1_1", PACKAGE1_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath1_2", PACKAGE1_PATH[ 1 ].c_str() );
    const auto &    MODULES1 = PACKAGE1.modules;
    ASSERT_EQ( 2, MODULES1.size() );
    const auto  MODULES1_END = MODULES1.end();
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_1" ) );
    ASSERT_FALSE( MODULES1.at( "module1_1" ) );
    ASSERT_NE( MODULES1_END, MODULES1.find( "module1_2" ) );
    ASSERT_TRUE( MODULES1.at( "module1_2" ) );
    ASSERT_FALSE( PACKAGE1.save.existsPath );

    const auto &    PACKAGE2 = PACKAGES[ 1 ];
    ASSERT_STREQ( "current", PACKAGE2.at.c_str() );
    ASSERT_TRUE( PACKAGE2.existsPath );
    const auto &    PACKAGE2_PATH = PACKAGE2.path;
    ASSERT_EQ( 2, PACKAGE2_PATH.size() );
    ASSERT_STREQ( "packagePath2_1", PACKAGE2_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "packagePath2_2", PACKAGE2_PATH[ 1 ].c_str() );
    ASSERT_EQ( 0, PACKAGE2.modules.size() );
    ASSERT_FALSE( PACKAGE2.save.existsPath );

    const auto &    BASESYSTEM = EXECUTE_FILE_UNIQUE->basesystem;
    ASSERT_TRUE( BASESYSTEM.existsPath );
    ASSERT_STREQ( "current", BASESYSTEM.at.c_str() );
    ASSERT_TRUE( BASESYSTEM.existsPackage );
    const auto &    BASESYSTEM_PACKAGE = BASESYSTEM.package;
    ASSERT_EQ( 2, BASESYSTEM_PACKAGE.size() );
    ASSERT_STREQ( "basesystemPackage1", BASESYSTEM_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "basesystemPackage2", BASESYSTEM_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "basesystemName", BASESYSTEM.name.c_str() );
    ASSERT_FALSE( BASESYSTEM.save.existsPath );

    const auto &    GAME = EXECUTE_FILE_UNIQUE->game;
    ASSERT_TRUE( GAME.existsPath );
    ASSERT_STREQ( "current", GAME.at.c_str() );
    ASSERT_TRUE( GAME.existsPackage );
    const auto &    GAME_PACKAGE = GAME.package;
    ASSERT_EQ( 2, GAME_PACKAGE.size() );
    ASSERT_STREQ( "gamePackage1", GAME_PACKAGE[ 0 ].c_str() );
    ASSERT_STREQ( "gamePackage2", GAME_PACKAGE[ 1 ].c_str() );
    ASSERT_STREQ( "gameName", GAME.name.c_str() );
    const auto &    SAVE = GAME.save;
    ASSERT_TRUE( SAVE.existsPath );
    ASSERT_FALSE( SAVE.defaultPath );
    const auto &    SAVE_PATH = SAVE.path;
    ASSERT_EQ( 2, SAVE_PATH.size() );
    ASSERT_STREQ( "gameSavePath1", SAVE_PATH[ 0 ].c_str() );
    ASSERT_STREQ( "gameSavePath2", SAVE_PATH[ 1 ].c_str() );
}

TEST(
    ExecuteFileTest
    , Create_failedNotExistsBasesystemPath
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failednotexistsbasesystempath.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedNotExistsGamePath
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failednotexistsgamepath.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedDuplicateTemplate
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedduplicatetemplate.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExistsTemplatePathAtSelf
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexiststemplatepathatself.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExistsPackagePathAtSelf
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexistspackagepathatself.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExistsBasesystemPathAtSelf
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexistsbasesystempathatself.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExistsGamePathAtSelf
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexistsgamepathatself.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExecuteFileSelfTemplate
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexecutefileselftemplate.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExecuteFileSelfPackage
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexecutefileselfpackage.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExecuteFileSelfBasesystem
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexecutefileselfbasesystem.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ExecuteFileTest
    , Create_failedExecuteFileSelfGame
)
{
    try {
        candymaker::ExecuteFile::create(
            "executetest/createtest_failedexecutefileselfgame.fge"
            , "systemdirectory"
            , "executetest/"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
