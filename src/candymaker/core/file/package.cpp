﻿#include "candymaker/core/file/package.h"
#include "candymaker/core/file/path.h"
#include "candymaker/core/file/common.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/map/map.h"
#include "fg/core/dataelement/dataelement.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/string.h"
#include "fg/core/dataelement/map/map.h"
#include "fg/core/dataelement/map/keys.h"

#include <utility>
#include <string>
#include <stdexcept>
#include <cstddef>

namespace {
    const auto  PACKAGES_DIRECTORY = "packages";
    const auto  PACKAGE_FILE_NAME = "package.fgp";

    const auto  MODULE_NAME_PREFIX = std::string( "lib" );
    const auto  MODULE_NAME_SUFFIX = std::string( ".so" );

    const auto  KEY_MODULES = "modules";
    const auto  KEY_MODULE_AT = "at";
    const auto  KEY_MODULE_PATH = "path";
    const auto  KEY_MODULE_INITIALIZER = "initializer";
    const auto  KEY_MODULE_INTERFACES = "interfaces";
    const auto  KEY_MODULE_INTERFACES_PACKAGE = "package";
    const auto  KEY_MODULE_INTERFACES_MODULE = "module";
    const auto  KEY_MODULE_DEPENDS = "depends";
    const auto  KEY_MODULE_DEPENDS_PACKAGE = "package";
    const auto  KEY_MODULE_DEPENDS_MODULE = "module";

    const auto  KEY_BASESYSTEMS = "basesystems";
    const auto  KEY_BASESYSTEM_MODULE = "module";
    const auto  KEY_BASESYSTEM_INITIALIZER = "initializer";

    const auto  KEY_GAMES = "games";
    const auto  KEY_GAME_MODULE = "module";
    const auto  KEY_GAME_INITIALIZER = "initializer";

    auto createModulePath(
        const candymaker::Path &    _MODULE_PATH
    )
    {
        const auto  MODULE_PATH_SIZE = _MODULE_PATH.size();
        if( MODULE_PATH_SIZE <= 0 ) {
            throw std::runtime_error( "モジュールパスリストの内容が空" );
        }

        auto    modulePath = _MODULE_PATH;

        auto &  moduleName = modulePath[ MODULE_PATH_SIZE - 1 ];

        moduleName = MODULE_NAME_PREFIX + moduleName + MODULE_NAME_SUFFIX;

        return modulePath;
    }

    auto createAt(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  AT_PTR = _MAP.getDataElementString( KEY_MODULE_AT );
        if( AT_PTR == nullptr ) {
            throw std::runtime_error( "モジュール情報に文字列のat要素が存在しない" );
        }
        const auto &    AT = *AT_PTR;

        return std::string( AT.getString() );
    }

    auto createPath(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  PATH_PTR = _MAP.getDataElementList( KEY_MODULE_PATH );
        if( PATH_PTR == nullptr ) {
            throw std::runtime_error( "モジュール情報にリストのpath要素が存在しない" );
        }
        const auto &    PATH = *PATH_PTR;

        return candymaker::createPath( PATH );
    }

    auto initInitializer(
        std::string &                   _initializer
        , const fg::DataElementMap &    _MAP
    )
    {
        const auto  INITIALIZER_ELEMENT_PTR = _MAP.getDataElement( KEY_MODULE_INITIALIZER );
        if( INITIALIZER_ELEMENT_PTR == nullptr ) {
            return false;
        }

        const auto  INITIALIZER_PTR = INITIALIZER_ELEMENT_PTR->getDataElementString();
        if( INITIALIZER_PTR == nullptr ) {
            throw std::runtime_error( "イニシャライザが文字列ではない" );
        }

        _initializer = INITIALIZER_PTR->getString();

        return true;
    }

    void addInterface(
        candymaker::PackageFile::Module::Interfaces &   _interfaces
        , const fg::DataElementMap &                    _MAP
    )
    {
        auto    existsPackage = bool();
        auto    package = std::string();

        const auto  PACKAGE_ELEMENT_PTR = _MAP.getDataElementString( KEY_MODULE_INTERFACES_PACKAGE );
        if( PACKAGE_ELEMENT_PTR != nullptr ) {
            existsPackage = true;
            package = PACKAGE_ELEMENT_PTR->getString();
        } else {
            existsPackage = false;
        }

        const auto  MODULE_ELEMENT_PTR = _MAP.getDataElementString( KEY_MODULE_INTERFACES_MODULE );
        if( MODULE_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "インターフェース情報に文字列のmodule要素が存在しない" );
        }

        auto    module = MODULE_ELEMENT_PTR->getString();

        _interfaces.push_back(
            candymaker::PackageFile::Module::Interface{
                std::move( existsPackage ),
                std::move( package ),
                std::move( module ),
            }
        );
    }

    auto createInterfaces(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  INTERFACES_ELEMENT_PTR = _MAP.getDataElement( KEY_MODULE_INTERFACES );
        if( INTERFACES_ELEMENT_PTR == nullptr ) {
            return candymaker::PackageFile::Module::Interfaces();
        }

        const auto  INTERFACES_PTR = INTERFACES_ELEMENT_PTR->getDataElementList();
        if( INTERFACES_PTR == nullptr ) {
            throw std::runtime_error( "モジュール情報のinterfacesがリスト要素ではない" );
        }

        const auto  SIZE = INTERFACES_PTR->getSize();

        auto    interfaces = candymaker::PackageFile::Module::Interfaces();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  INTERFACE_MAP_PTR = INTERFACES_PTR->getDataElementMap( i );
            if( INTERFACE_MAP_PTR == nullptr ) {
                throw std::runtime_error( "インターフェース情報の要素がマップ要素ではない" );
            }
            const auto &    INTERFACE_MAP = *INTERFACE_MAP_PTR;

            addInterface(
                interfaces
                , INTERFACE_MAP
            );
        }

        return interfaces;
    }

    void addDepend(
        candymaker::PackageFile::Module::Depends &  _depends
        , const fg::DataElementMap &                _MAP
    )
    {
        auto    existsPackage = bool();
        auto    package = std::string();

        const auto  PACKAGE_ELEMENT_PTR = _MAP.getDataElementString( KEY_MODULE_DEPENDS_PACKAGE );
        if( PACKAGE_ELEMENT_PTR != nullptr ) {
            existsPackage = true;
            package = PACKAGE_ELEMENT_PTR->getString();
        } else {
            existsPackage = false;
        }

        const auto  MODULE_ELEMENT_PTR = _MAP.getDataElementString( KEY_MODULE_DEPENDS_MODULE );
        if( MODULE_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "依存モジュール情報に文字列のmodule要素が存在しない" );
        }

        auto    module = MODULE_ELEMENT_PTR->getString();

        _depends.push_back(
            candymaker::PackageFile::Module::Depend{
                std::move( existsPackage ),
                std::move( package ),
                std::move( module ),
            }
        );
    }

    auto createDepends(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  DEPENDS_ELEMENT_PTR = _MAP.getDataElement( KEY_MODULE_DEPENDS );
        if( DEPENDS_ELEMENT_PTR == nullptr ) {
            return candymaker::PackageFile::Module::Depends();
        }

        const auto  DEPENDS_PTR = DEPENDS_ELEMENT_PTR->getDataElementList();
        if( DEPENDS_PTR == nullptr ) {
            throw std::runtime_error( "モジュール情報のdependsがリスト要素ではない" );
        }

        const auto  SIZE = DEPENDS_PTR->getSize();

        auto    depends = candymaker::PackageFile::Module::Depends();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  DEPEND_MAP_PTR = DEPENDS_PTR->getDataElementMap( i );
            if( DEPEND_MAP_PTR == nullptr ) {
                throw std::runtime_error( "依存モジュール情報の要素がマップ要素ではない" );
            }
            const auto &    DEPEND_MAP = *DEPEND_MAP_PTR;

            addDepend(
                depends
                , DEPEND_MAP
            );
        }

        return depends;
    }

    void addModule(
        candymaker::PackageFile::Modules &  _modules
        , const std::string &               _MODULE_NAME
        , const fg::DataElementMap &        _MAP
    )
    {
        auto    at = createAt( _MAP );

        auto    path = createPath( _MAP );

        auto    initializer = std::string();
        auto    existsInitializer = initInitializer(
            initializer
            , _MAP
        );

        auto    interfaces = createInterfaces( _MAP );

        auto    depends = createDepends( _MAP );

        const auto  RESULT = _modules.insert(
            std::make_pair(
                _MODULE_NAME
                , candymaker::PackageFile::Module{
                    std::move( at ),
                    std::move( path ),
                    std::move( existsInitializer ),
                    std::move( initializer ),
                    std::move( interfaces ),
                    std::move( depends ),
                }
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "モジュール情報集合への要素追加に失敗" );
        }
    }

    auto createModules(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  MODULES_ELEMENT_PTR = _MAP.getDataElement( KEY_MODULES );
        if( MODULES_ELEMENT_PTR == nullptr ) {
            return candymaker::PackageFile::Modules();
        }

        const auto  MODULES_PTR = MODULES_ELEMENT_PTR->getDataElementMap();
        if( MODULES_PTR == nullptr ) {
            throw std::runtime_error( "modules要素がマップ要素ではない" );
        }
        const auto &    MODULES = *MODULES_PTR;

        const auto  MODULE_KEYS_UNIQUE = fg::DataElementMapKeys::create( MODULES );

        const auto  MODULE_KEYS_SIZE = MODULE_KEYS_UNIQUE->getSize();

        auto    modules = candymaker::PackageFile::Modules();
        for( auto i = std::size_t( 0 ) ; i < MODULE_KEYS_SIZE ; i++ ) {
            const auto  MODULE_NAME = MODULE_KEYS_UNIQUE->getKey( i );
            if( MODULE_NAME == nullptr ) {
                throw std::runtime_error( "モジュールマップのキーリストからのキー取得に失敗" );
            }

            const auto  MODULE_MAP_PTR = MODULES.getDataElementMap( MODULE_NAME );
            if( MODULE_MAP_PTR == nullptr ) {
                throw std::runtime_error( "モジュールマップからマップ要素の取得に失敗" );
            }

            addModule(
                modules
                , MODULE_NAME
                , *MODULE_MAP_PTR
            );
        }

        return modules;
    }

    void addBasesystem(
        candymaker::PackageFile::Basesystems &  _basesystems
        , const std::string &                   _BASESYSTEM_NAME
        , const fg::DataElementMap &            _MAP
    )
    {
        const auto  MODULE_ELEMENT_PTR = _MAP.getDataElementString( KEY_BASESYSTEM_MODULE );
        if( MODULE_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "ベースシステム情報の文字列のmodule要素が存在しない" );
        }

        const auto  INITIALIZER_ELEMENT_PTR = _MAP.getDataElementString( KEY_BASESYSTEM_INITIALIZER );
        if( INITIALIZER_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "ベースシステム情報の文字列のinitializer要素が存在しない" );
        }

        const auto  RESULT = _basesystems.insert(
            std::make_pair(
                _BASESYSTEM_NAME
                , candymaker::PackageFile::Basesystem{
                    MODULE_ELEMENT_PTR->getString(),
                    INITIALIZER_ELEMENT_PTR->getString(),
                }
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "ベースシステム情報集合への要素追加に失敗" );
        }
    }

    auto createBasesystems(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  BASESYSTEMS_ELEMENT_PTR = _MAP.getDataElement( KEY_BASESYSTEMS );
        if( BASESYSTEMS_ELEMENT_PTR == nullptr ) {
            return candymaker::PackageFile::Basesystems();
        }

        const auto  BASESYSTEMS_PTR = BASESYSTEMS_ELEMENT_PTR->getDataElementMap();
        if( BASESYSTEMS_PTR == nullptr ) {
            throw std::runtime_error( "basesystems要素がマップ要素ではない" );
        }

        const auto  BASESYSTEM_KEYS_UNIQUE = fg::DataElementMapKeys::create( *BASESYSTEMS_PTR );

        const auto  SIZE = BASESYSTEM_KEYS_UNIQUE->getSize();

        auto    basesystems = candymaker::PackageFile::Basesystems();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  BASESYSTEM_NAME = BASESYSTEM_KEYS_UNIQUE->getKey( i );
            if( BASESYSTEM_NAME == nullptr ) {
                throw std::runtime_error( "ベースシステムマップのキーリストからのキー取得に失敗" );
            }

            const auto  BASESYSTEM_MAP_PTR = BASESYSTEMS_PTR->getDataElementMap( BASESYSTEM_NAME );
            if( BASESYSTEM_MAP_PTR == nullptr ) {
                throw std::runtime_error( "ベースシステムマップからマップ要素の取得に失敗" );
            }
            const auto &    BASESYSTEM_MAP = *BASESYSTEM_MAP_PTR;

            addBasesystem(
                basesystems
                , BASESYSTEM_NAME
                , BASESYSTEM_MAP
            );
        }

        return basesystems;
    }

    void addGame(
        candymaker::PackageFile::Games &    _games
        , const std::string &               _GAME_NAME
        , const fg::DataElementMap &        _MAP
    )
    {
        const auto  MODULE_ELEMENT_PTR = _MAP.getDataElementString( KEY_GAME_MODULE );
        if( MODULE_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "ゲーム情報の文字列のmodule要素が存在しない" );
        }

        const auto  INITIALIZER_ELEMENT_PTR = _MAP.getDataElementString( KEY_GAME_INITIALIZER );
        if( INITIALIZER_ELEMENT_PTR == nullptr ) {
            throw std::runtime_error( "ゲーム情報の文字列のinitializer要素が存在しない" );
        }

        const auto  RESULT = _games.insert(
            std::make_pair(
                _GAME_NAME
                , candymaker::PackageFile::Game{
                    MODULE_ELEMENT_PTR->getString(),
                    INITIALIZER_ELEMENT_PTR->getString(),
                }
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "ゲーム情報集合への要素追加に失敗" );
        }
    }

    auto createGames(
        const fg::DataElementMap &  _MAP
    )
    {
        const auto  GAMES_PTR = _MAP.getDataElementMap( KEY_GAMES );
        if( GAMES_PTR == nullptr ) {
            return candymaker::PackageFile::Games();
        }

        const auto  GAME_KEYS_UNIQUE = fg::DataElementMapKeys::create( *GAMES_PTR );

        const auto  SIZE = GAME_KEYS_UNIQUE->getSize();

        auto    games = candymaker::PackageFile::Games();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  GAME_NAME = GAME_KEYS_UNIQUE->getKey( i );
            if( GAME_NAME == nullptr ) {
                throw std::runtime_error( "ゲームマップのキーリストからのキー取得に失敗" );
            }

            const auto  GAME_MAP_PTR = GAMES_PTR->getDataElementMap( GAME_NAME );
            if( GAME_MAP_PTR == nullptr ) {
                throw std::runtime_error( "ゲームマップからマップ要素の取得に失敗" );
            }
            const auto &    GAME_MAP = *GAME_MAP_PTR;

            addGame(
                games
                , GAME_NAME
                , GAME_MAP
            );
        }

        return games;
    }
}

namespace candymaker {
    bool PackageFile::Module::Depend::isLocalPackage(
    ) const
    {
        return this->existsPackage == false;
    }

    const std::string & PackageFile::Module::Depend::getPackage(
    ) const
    {
        return this->package;
    }

    const std::string & PackageFile::Module::getAt(
    ) const
    {
        return this->at;
    }

    const Path & PackageFile::Module::getPath(
    ) const
    {
        return this->path;
    }

    bool PackageFile::Module::isExistsInitializer(
    ) const
    {
        return this->existsInitializer;
    }

    const std::string & PackageFile::Module::getInitializer(
    ) const
    {
        if( this->isExistsInitializer() == false ) {
            throw std::runtime_error( "イニシャライザが存在しない" );
        }

        return this->initializer;
    }

    const PackageFile::Module::Depends & PackageFile::Module::getDepends(
    ) const
    {
        return this->depends;
    }

    bool PackageFile::Module::isImplementedModule(
        const Depend &  _DEPEND
    ) const
    {
        for( const auto & INTERFACE : this->interfaces ) {
            if( INTERFACE.package != _DEPEND.package ) {
                continue;
            }

            if( INTERFACE.module != _DEPEND.module ) {
                continue;
            }

            return true;
        }

        return false;
    }

    const std::string & PackageFile::Basesystem::getModule(
    ) const
    {
        return this->module;
    }

    const std::string & PackageFile::Basesystem::getInitializer(
    ) const
    {
        return this->initializer;
    }

    const std::string & PackageFile::Game::getModule(
    ) const
    {
        return this->module;
    }

    const std::string & PackageFile::Game::getInitializer(
    ) const
    {
        return this->initializer;
    }

    std::string PackageFile::getFileName(
    )
    {
        return PACKAGE_FILE_NAME;
    }

    std::string PackageFile::relativeDirectoryPathToString(
        const Path &    _PACKAGE_PATH
    )
    {
        auto    path = Path( { PACKAGES_DIRECTORY } );
        path.insert(
            path.end()
            , _PACKAGE_PATH.begin()
            , _PACKAGE_PATH.end()
        );

        return candymaker::relativeDirectoryPathToString( path );
    }

    std::string PackageFile::relativeFilePathToString(
        const Path &    _PACKAGE_PATH
        , const Path &  _FILE_PATH
    )
    {
        auto    path = Path( { PACKAGES_DIRECTORY } );
        path.insert(
            path.end()
            , _PACKAGE_PATH.begin()
            , _PACKAGE_PATH.end()
        );
        path.insert(
            path.end()
            , _FILE_PATH.begin()
            , _FILE_PATH.end()
        );

        return candymaker::relativeFilePathToString( path );
    }

    std::string PackageFile::absoluteModulePathToString(
        const Path &    _MODULE_PATH
    )
    {
        const auto  MODULE_PATH = createModulePath( _MODULE_PATH );

        return candymaker::absoluteFilePathToString( MODULE_PATH );
    }

    std::string PackageFile::relativeModulePathToString(
        const Path &    _MODULE_PATH
    )
    {
        const auto  MODULE_PATH = createModulePath( _MODULE_PATH );

        return candymaker::relativeFilePathToString( MODULE_PATH );
    }

    PackageFile::PackageFile(
        Modules &&          _modules
        , Basesystems &&    _basesystems
        , Games &&          _games
    )
        : modules( std::move( _modules ) )
        , basesystems( std::move( _basesystems ) )
        , games( std::move( _games ) )
    {
    }

    PackageFile::ConstUnique PackageFile::create(
        const std::string & _FILE_PATH
    )
    {
        const auto  ELEMENT_UNIQUE = createDataElement( _FILE_PATH );

        const auto  MAP_PTR = ELEMENT_UNIQUE->getDataElementMap();
        if( MAP_PTR == nullptr ) {
            throw std::runtime_error( "パッケージファイルがマップ要素ではない" );
        }
        const auto &    MAP = *MAP_PTR;

        auto    modules = createModules( MAP );

        auto    basesystems = createBasesystems( MAP );

        auto    games = createGames( MAP );

        return new PackageFile(
            std::move( modules )
            , std::move( basesystems )
            , std::move( games )
        );
    }

    const PackageFile::Module & PackageFile::getModule(
        const std::string & _NAME
    ) const
    {
        return this->modules.at( _NAME );
    }

    const PackageFile::Basesystem & PackageFile::getBasesystem(
        const std::string & _NAME
    ) const
    {
        return this->basesystems.at( _NAME );
    }

    const PackageFile::Game & PackageFile::getGame(
        const std::string & _NAME
    ) const
    {
        return this->games.at( _NAME );
    }
}
