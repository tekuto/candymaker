﻿#include "candymaker/core/file/path.h"
#include "fg/core/dataelement/string.h"

#include <string>
#include <algorithm>
#include <stdexcept>
#include <cstddef>
#include <libgen.h>

namespace {
    const auto  PATH_SPLITTER = '/';

    void appendPathString(
        std::string &                               _pathString
        , const candymaker::Path::const_iterator &  _BEGIN
        , const candymaker::Path::const_iterator &  _END
    )
    {
        std::for_each(
            _BEGIN
            , _END
            , [
                &_pathString
            ]
            (
                const std::string & _PART
            )
            {
                _pathString.append( _PART );
                _pathString.push_back( PATH_SPLITTER );
            }
        );
    }

    struct Absolute
    {
        void operator()(
            std::string &   _pathString
        ) const
        {
            _pathString.push_back( PATH_SPLITTER );
        }
    };

    struct Relative
    {
        void operator()(
            std::string &
        ) const
        {
        }
    };

    template< typename BEFORE_PROC_T >
    auto filePathToString(
        const candymaker::Path &    _PATH
    )
    {
        if( _PATH.size() <= 0 ) {
            return std::string();
        }

        auto    pathString = std::string();

        BEFORE_PROC_T()( pathString );

        const auto  DIRECTORY_END = _PATH.end() - 1;    // appendPathString()で処理するのはディレクトリ部分まで

        appendPathString(
            pathString
            , _PATH.begin()
            , DIRECTORY_END
        );

        const auto &    FILE = DIRECTORY_END;

        pathString.append( *FILE );

        return pathString;
    }

    template< typename BEFORE_PROC_T >
    auto directoryPathToString(
        const candymaker::Path &    _PATH
    )
    {
        if( _PATH.size() <= 0 ) {
            return std::string();
        }

        auto    pathString = std::string();

        BEFORE_PROC_T()( pathString );

        appendPathString(
            pathString
            , _PATH.begin()
            , _PATH.end()
        );

        return pathString;
    }
}

namespace candymaker {
    Path createPath(
        const fg::DataElementList & _PATH
    )
    {
        const auto  SIZE = _PATH.getSize();
        if( SIZE <= 0 ) {
            throw std::runtime_error( "パスリストの内容が空" );
        }

        auto    path = Path();
        for( auto i = std::size_t( 0 ) ; i < SIZE ; i++ ) {
            const auto  STRING_PTR = _PATH.getDataElementString( i );
            if( STRING_PTR == nullptr ) {
                throw std::runtime_error( "パスリストの要素に文字列以外が含まれている" );
            }
            const auto &    STRING = *STRING_PTR;

            path.push_back( STRING.getString() );
        }

        return path;
    }

    std::string absoluteFilePathToString(
        const Path &    _PATH
    )
    {
        return filePathToString< Absolute >( _PATH );
    }

    std::string relativeFilePathToString(
        const Path &    _PATH
    )
    {
        return filePathToString< Relative >( _PATH );
    }

    std::string absoluteDirectoryPathToString(
        const Path &    _PATH
    )
    {
        return directoryPathToString< Absolute >( _PATH );
    }

    std::string relativeDirectoryPathToString(
        const Path &    _PATH
    )
    {
        return directoryPathToString< Relative >( _PATH );
    }

    std::string createDirectoryName(
        const std::string & _PATH
    )
    {
        auto    tmp = _PATH;
        auto    tmpPtr = tmp.data();

        const auto  DIRECTORY_NAME_PTR = dirname( tmpPtr );

        auto    directoryName = std::string( DIRECTORY_NAME_PTR );

        directoryName.push_back( PATH_SPLITTER );

        return directoryName;
    }

    std::string createDirectoryPath(
        const std::string & _PATH
    )
    {
        if( _PATH.size() > 0 && _PATH.back() == PATH_SPLITTER ) {
            return _PATH;
        }

        return _PATH + PATH_SPLITTER;
    }
}
