﻿#include "fg/util/test.h"
#include "candymaker/core/file/common.h"
#include "candymaker/core/dataelement/dataelement.h"

TEST(
    CoreFileCommonTest
    , CreateDataElement_string
)
{
    const auto  ELEMENT_UNIQUE = candymaker::createDataElement( "filecommontest/stringtest.txt" );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_NE( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    CoreFileCommonTest
    , CreateDataElement_list
)
{
    const auto  ELEMENT_UNIQUE = candymaker::createDataElement( "filecommontest/listtest.txt" );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    CoreFileCommonTest
    , CreateDataElement_map
)
{
    const auto  ELEMENT_UNIQUE = candymaker::createDataElement( "filecommontest/maptest.txt" );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->mapUnique.get() );
}
