﻿#include "fg/util/test.h"
#include "candymaker/core/save/writenotify.h"

TEST(
    SaveWriteNotifyTest
    , GetState
)
{
    auto    writeNotifyImpl = FgSaveWriteNotify{
        *reinterpret_cast< FgState * >( 10 )
        , true
    };
    auto &  writeNotify = reinterpret_cast< fg::SaveWriteNotify<> & >( writeNotifyImpl );

    ASSERT_EQ( reinterpret_cast< const fg::State<> * >( 10 ), &( writeNotify.getState() ) );
}

TEST(
    SaveWriteNotifyTest
    , Wrote
)
{
    const auto  WRITE_NOTIFY_IMPL = FgSaveWriteNotify{
        *reinterpret_cast< FgState * >( 10 )
        , true
    };
    const auto &    WRITE_NOTIFY = reinterpret_cast< const fg::SaveWriteNotify<> & >( WRITE_NOTIFY_IMPL );

    ASSERT_TRUE( WRITE_NOTIFY.wrote() );
}
