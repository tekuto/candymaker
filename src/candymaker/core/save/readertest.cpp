﻿#include "fg/util/test.h"
#include "candymaker/core/save/reader.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/state/state.h"
#include "fg/core/save/save.h"
#include "fg/core/save/readnotify.h"
#include "fg/core/dataelement/dataelement.h"
#include "fg/core/dataelement/string.h"

#include <mutex>
#include <condition_variable>
#include <utility>

TEST(
    SaveReaderTest
    , Read
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVE_UNIQUE = fg::SaveReader::read(
        MODULE_CONTEXT
        , "savename"
    );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );

    const auto  STRING_ELEMENT_PTR = SAVE_UNIQUE->getDataElement().getDataElementString();
    ASSERT_NE( nullptr, STRING_ELEMENT_PTR );

    ASSERT_STREQ( "READERTEST", STRING_ELEMENT_PTR->getString() );
}

TEST(
    SaveReaderTest
    , Read_failedNotExists
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVE_UNIQUE = fg::SaveReader::read(
        MODULE_CONTEXT
        , "notexists"
    );
    ASSERT_EQ( nullptr, SAVE_UNIQUE.get() );
}

struct CreateTestData
{
    std::mutex              mutex;
    std::condition_variable cond;
    bool                    ended = false;
    fg::Save::ConstUnique   saveUnique;
};

void dummyDestroy(
    void *
)
{
}

void createTestProc(
    fg::SaveReadNotify< CreateTestData > &  _readNotify
)
{
    auto &  state = _readNotify.getState();
    auto &  data = state.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.ended = true;
    const auto  SAVE_PTR = _readNotify.getSave();
    if( SAVE_PTR == nullptr ) {
        return;
    }
    data.saveUnique = SAVE_PTR->clone();

    data.cond.notify_one();
}

TEST(
    SaveReaderTest
    , CreateForModuleContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , MODULE_CONTEXT
        , "savename"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    const auto  STRING_ELEMENT_PTR = data.saveUnique->getDataElement().getDataElementString();
    ASSERT_NE( nullptr, STRING_ELEMENT_PTR );

    ASSERT_STREQ( "READERTEST", STRING_ELEMENT_PTR->getString() );
}

TEST(
    SaveReaderTest
    , CreateForModuleContext_failedNotExists
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , MODULE_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_EQ( nullptr, data.saveUnique.get() );
}

TEST(
    SaveReaderTest
    , CreateForBasesystemContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState->setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , BASESYSTEM_CONTEXT
        , "savename"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    const auto  STRING_ELEMENT_PTR = data.saveUnique->getDataElement().getDataElementString();
    ASSERT_NE( nullptr, STRING_ELEMENT_PTR );

    ASSERT_STREQ( "READERTEST", STRING_ELEMENT_PTR->getString() );
}

TEST(
    SaveReaderTest
    , CreateForBasesystemContext_failedNotExists
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState->setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , BASESYSTEM_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_EQ( nullptr, data.saveUnique.get() );
}

TEST(
    SaveReaderTest
    , CreateForGameContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , GAME_CONTEXT
        , "savename"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    const auto  STRING_ELEMENT_PTR = data.saveUnique->getDataElement().getDataElementString();
    ASSERT_NE( nullptr, STRING_ELEMENT_PTR );

    ASSERT_STREQ( "READERTEST", STRING_ELEMENT_PTR->getString() );
}

TEST(
    SaveReaderTest
    , CreateForGameContext_failedNotExists
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savereadertest"
        , "savereadertest/libsavereadertest.so"
        , "savereadertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  READER_UNIQUE = fg::SaveReader::create(
        state
        , GAME_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, READER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_EQ( nullptr, data.saveUnique.get() );
}
