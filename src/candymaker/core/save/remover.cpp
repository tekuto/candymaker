﻿#include "fg/util/export.h"
#include "candymaker/core/save/remover.h"
#include "candymaker/core/save/removenotify.h"
#include "candymaker/core/save/save.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "fg/core/state/threadbackground.h"

#include <string>
#include <unistd.h>

namespace {
    auto createFilePath(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
    )
    {
        auto    filePath = _HOME_DIRECTORY_PATH;
        filePath.append(
            FgSave::createFilePath(
                _SAVE_DIRECTORY_PATH
                , _FILE_NAME
            )
        );

        return filePath;
    }

    auto remove_(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
    )
    {
        const auto  FILE_PATH = createFilePath(
            _HOME_DIRECTORY_PATH
            , _SAVE_DIRECTORY_PATH
            , _FILE_NAME
        );

        return unlink( FILE_PATH.c_str() ) == 0;
    }

    void removeThread(
        fg::StateThreadBackground< void, FgSaveRemover > &  _thread
    )
    {
        auto &  remover = _thread.getData();

        const auto  REMOVED = remove_(
            remover.HOME_DIRECTORY_PATH
            , remover.SAVE_DIRECTORY_PATH
            , remover.FILE_NAME
        );

        auto    removeNotify = FgSaveRemoveNotify{
            *( remover.state ),
            REMOVED,
        };
        remover.removeNotifyProc( &removeNotify );
    }
}

bool fgSaveRemoverRemove(
    const FgModuleContext * _MODULE_CONTEXT_PTR
    , const char *          _FILE_NAME
)
{
    return remove_(
        _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
        , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
    );
}

FgSaveRemover * fgSaveRemoverCreateForModuleContext(
    FgState *                   _statePtr
    , const FgModuleContext *   _MODULE_CONTEXT_PTR
    , const char *              _FILE_NAME
    , FgSaveRemoveNotifyProc    _removeNotifyProc
)
{
    return new FgSaveRemover(
        _statePtr
        , _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
        , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _removeNotifyProc
    );
}

FgSaveRemover * fgSaveRemoverCreateForBasesystemContext(
    FgState *                       _statePtr
    , const FgBasesystemContext *   _BASESYSTEM_CONTEXT_PTR
    , const char *                  _FILE_NAME
    , FgSaveRemoveNotifyProc        _removeNotifyProc
)
{
    return new FgSaveRemover(
        _statePtr
        , _BASESYSTEM_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _BASESYSTEM_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _removeNotifyProc
    );
}

FgSaveRemover * fgSaveRemoverCreateForGameContext(
    FgState *                   _statePtr
    , const FgGameContext *     _GAME_CONTEXT_PTR
    , const char *              _FILE_NAME
    , FgSaveRemoveNotifyProc    _removeNotifyProc
)
{
    return new FgSaveRemover(
        _statePtr
        , _GAME_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _GAME_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _removeNotifyProc
    );
}

void fgSaveRemoverDestroy(
    FgSaveRemover * _implPtr
)
{
    delete _implPtr;
}

FgSaveRemover::FgSaveRemover(
    FgState *                   _statePtr
    , const std::string &       _HOME_DIRECTORY_PATH
    , const std::string &       _SAVE_DIRECTORY_PATH
    , const char *              _FILE_NAME
    , FgSaveRemoveNotifyProc    _removeNotifyProc
)
    : state( reinterpret_cast< fg::State<> & >( *_statePtr ) )
    , HOME_DIRECTORY_PATH( _HOME_DIRECTORY_PATH )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
    , FILE_NAME( _FILE_NAME )
    , removeNotifyProc( _removeNotifyProc )
    , joinerUnique( fg::StateJoiner::create() )
{
    this->state.execute(
        removeThread
        , *( this->joinerUnique )
        , *this
    );
}
