﻿#include "fg/util/export.h"
#include "candymaker/core/save/readnotify.h"

FgState * fgSaveReadNotifyGetState(
    FgSaveReadNotify *  _implPtr
)
{
    return &( _implPtr->state );
}

const FgSave * fgSaveReadNotifyGetSave(
    const FgSaveReadNotify *    _IMPL_PTR
)
{
    return &**( _IMPL_PTR->saveUnique );
}
