﻿#include "fg/util/test.h"
#include "candymaker/core/save/save.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "fg/core/dataelement/dataelement.h"
#include "fg/core/dataelement/string.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/map/map.h"
#include "fg/core/dataelement/map/keys.h"

#include <utility>

TEST(
    SaveTest
    , CreateFilePath
)
{
    const auto  FILE_PATH = FgSave::createFilePath(
        "SAVE_DIRECTORY_PATH/"
        , "FILE_NAME"
    );
    ASSERT_STREQ( "saves/SAVE_DIRECTORY_PATH/FILE_NAME.fgs", FILE_PATH.c_str() );
}

TEST(
    SaveTest
    , Create_candymaker
)
{
    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    auto    dataElementUnique = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, dataElementUnique.get() );

    const auto  SAVE_UNIQUE = FgSave::create( std::move( dataElementUnique ) );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto &    DATA_ELEMENT_UNIQUE = SAVE->DATA_ELEMENT_UNIQUE;
    ASSERT_NE( nullptr, DATA_ELEMENT_UNIQUE.get() );

    const auto  STRING_PTR = DATA_ELEMENT_UNIQUE->getDataElementString();
    ASSERT_NE( nullptr, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    SaveTest
    , CreateForString
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( STRING );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto &    DATA_ELEMENT_UNIQUE = SAVE->DATA_ELEMENT_UNIQUE;
    ASSERT_NE( nullptr, DATA_ELEMENT_UNIQUE.get() );

    const auto  STRING_PTR = DATA_ELEMENT_UNIQUE->getDataElementString();
    ASSERT_NE( nullptr, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    SaveTest
    , CreateForList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    auto &  list = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    list.addDataElement( std::move( stringUnique ) );

    const auto  SAVE_UNIQUE = fg::Save::create( list );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto &    DATA_ELEMENT_UNIQUE = SAVE->DATA_ELEMENT_UNIQUE;
    ASSERT_NE( nullptr, DATA_ELEMENT_UNIQUE.get() );

    const auto  LIST_PTR = DATA_ELEMENT_UNIQUE->getDataElementList();
    ASSERT_NE( nullptr, LIST_PTR );
    ASSERT_EQ( 1, LIST_PTR->getSize() );
    const auto  STRING_PTR = LIST_PTR->getDataElementString( 0 );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    SaveTest
    , CreateForMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    map.addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto  SAVE_UNIQUE = fg::Save::create( map );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto &    DATA_ELEMENT_UNIQUE = SAVE->DATA_ELEMENT_UNIQUE;
    ASSERT_NE( nullptr, DATA_ELEMENT_UNIQUE.get() );

    const auto  MAP_PTR = DATA_ELEMENT_UNIQUE->getDataElementMap();
    ASSERT_NE( nullptr, MAP_PTR );
    const auto &    MAP = *MAP_PTR;

    const auto  KEYS_UNIQUE = fg::DataElementMapKeys::create( MAP );
    ASSERT_NE( nullptr, KEYS_UNIQUE.get() );
    ASSERT_EQ( 1, KEYS_UNIQUE->getSize() );

    const auto  STRING_PTR = MAP_PTR->getDataElementString( "KEY" );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    SaveTest
    , Clone
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( STRING );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );

    const auto  CLONE_UNIQUE = SAVE_UNIQUE->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    DATA_ELEMENT_UNIQUE = CLONE->DATA_ELEMENT_UNIQUE;
    ASSERT_NE( nullptr, DATA_ELEMENT_UNIQUE.get() );

    const auto  STRING_PTR = DATA_ELEMENT_UNIQUE->getDataElementString();
    ASSERT_NE( nullptr, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    SaveTest
    , GetDataElement
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( STRING );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    ASSERT_EQ( SAVE->DATA_ELEMENT_UNIQUE.get(), &( SAVE.getDataElement() ) );
}
