﻿#include "fg/util/export.h"
#include "candymaker/core/save/save.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/list.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/file/path.h"

#include <utility>

namespace {
    const auto  SAVES_DIRECTORY = "saves";

    const auto  SAVE_NAME_SUFFIX = ".fgs";

    template< typename DATA_ELEMENT_T >
    auto create(
        const DATA_ELEMENT_T *  _DATA_ELEMENT_PTR
    )
    {
        return const_cast< FgSave * >( &**( FgSave::create( FgDataElement::create( _DATA_ELEMENT_PTR->clone() ) ).release() ) );
    }
}

FgSave * fgSaveCreateForString(
    const FgDataElementString * _STRING_PTR
)
{
    return create( _STRING_PTR );
}

FgSave * fgSaveCreateForList(
    const FgDataElementList *   _LIST_PTR
)
{
    return create( _LIST_PTR );
}

FgSave * fgSaveCreateForMap(
    const FgDataElementMap *    _MAP_PTR
)
{
    return create( _MAP_PTR );
}

FgSave * fgSaveClone(
    const FgSave *  _ORG_PTR
)
{
    return const_cast< FgSave * >( &**( FgSave::create( ( *( _ORG_PTR->DATA_ELEMENT_UNIQUE ) )->clone() ) ).release() );
}

void fgSaveDestroy(
    FgSave *    _implPtr
)
{
    delete _implPtr;
}

const FgDataElement * fgSaveGetDataElement(
    const FgSave *  _IMPL_PTR
)
{
    return &**( _IMPL_PTR->DATA_ELEMENT_UNIQUE );
}

std::string FgSave::createFilePath(
    const std::string &     _SAVE_DIRECTORY_PATH
    , const std::string &   _FILE_NAME
)
{
    auto    filePath = candymaker::relativeDirectoryPathToString( candymaker::Path( { SAVES_DIRECTORY } ) );
    filePath.append( _SAVE_DIRECTORY_PATH );
    filePath.append( _FILE_NAME );
    filePath.append( SAVE_NAME_SUFFIX );

    return filePath;
}

fg::Save::ConstUnique FgSave::create(
    fg::DataElement::ConstUnique && _dataElementUnique
)
{
    return new FgSave{
        std::move( _dataElementUnique ),
    };
}
