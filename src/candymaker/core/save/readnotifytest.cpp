﻿#include "fg/util/test.h"
#include "candymaker/core/save/readnotify.h"
#include "fg/core/save/save.h"
#include "fg/core/dataelement/string.h"

#include <utility>

TEST(
    SaveReadNotifyTest
    , GetState
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    auto    saveUnique = fg::Save::create( STRING );
    ASSERT_NE( nullptr, saveUnique.get() );

    auto    readNotifyImpl = FgSaveReadNotify{
        *reinterpret_cast< FgState * >( 10 ),
        std::move( saveUnique ),
    };
    auto &  readNotify = reinterpret_cast< fg::SaveReadNotify<> & >( readNotifyImpl );

    ASSERT_EQ( reinterpret_cast< const fg::State<> * >( 10 ), &( readNotify.getState() ) );
}

TEST(
    SaveReadNotifyTest
    , GetSave
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    auto    saveUnique = fg::Save::create( STRING );
    ASSERT_NE( nullptr, saveUnique.get() );
    const auto &    SAVE = *saveUnique;

    const auto  READ_NOTIFY_IMPL = FgSaveReadNotify{
        *reinterpret_cast< FgState * >( 10 ),
        std::move( saveUnique ),
    };
    const auto &    READ_NOTIFY = reinterpret_cast< const fg::SaveReadNotify<> & >( READ_NOTIFY_IMPL );

    ASSERT_EQ( &SAVE, READ_NOTIFY.getSave() );
}
