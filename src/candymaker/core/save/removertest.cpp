﻿#include "fg/util/test.h"
#include "candymaker/core/save/remover.h"
#include "candymaker/core/save/removenotify.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/state/state.h"

#include <cstdio>
#include <mutex>
#include <condition_variable>

struct CloseFile
{
    void operator()(
        FILE *  _filePtr
    ) const
    {
        std::fclose( _filePtr );
    }
};

using FileUnique = std::unique_ptr<
    FILE
    , CloseFile
>;

TEST(
    SaveRemoverTest
    , Remove
)
{
    CloseFile()(
        std::fopen(
            "removertest/saves/savedirectory/savename.fgs"
            , "w"
        )
    );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    ASSERT_TRUE(
        fg::SaveRemover::remove(
            MODULE_CONTEXT
            , "savename"
        )
    );

    const auto  FILE_UNIQUE = FileUnique(
        std::fopen(
            "removertest/saves/savedirectory/savename.fgs"
            , "r"
        )
    );
    ASSERT_EQ( nullptr, FILE_UNIQUE.get() );
}

TEST(
    SaveRemoverTest
    , Remove_failed
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    ASSERT_FALSE(
        fg::SaveRemover::remove(
            MODULE_CONTEXT
            , "notexists"
        )
    );
}

struct CreateTestData
{
    std::mutex              mutex;
    std::condition_variable cond;
    bool                    ended = false;
    bool                    removed = false;
};

void dummyDestroy(
    void *
)
{
}

void createTestProc(
    fg::SaveRemoveNotify< CreateTestData > &    _removeNotify
)
{
    auto &  state = _removeNotify.getState();
    auto &  data = state.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.ended = true;
    data.removed = _removeNotify.removed();

    data.cond.notify_one();
}

TEST(
    SaveRemoverTest
    , CreateForModuleContext
)
{
    CloseFile()(
        std::fopen(
            "removertest/saves/savedirectory/createformodulecontext.fgs"
            , "w"
        )
    );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , MODULE_CONTEXT
        , "createformodulecontext"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.removed );

    const auto  FILE_UNIQUE = FileUnique(
        std::fopen(
            "removertest/saves/savedirectory/createformodulecontext.fgs"
            , "r"
        )
    );
    ASSERT_EQ( nullptr, FILE_UNIQUE.get() );
}

TEST(
    SaveRemoverTest
    , CreateForModuleContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , MODULE_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.removed );
}

TEST(
    SaveRemoverTest
    , CreateForBasesystemContext
)
{
    CloseFile()(
        std::fopen(
            "removertest/saves/savedirectory/createforbasesystemcontext.fgs"
            , "w"
        )
    );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , BASESYSTEM_CONTEXT
        , "createforbasesystemcontext"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.removed );
}

TEST(
    SaveRemoverTest
    , CreateForBasesystemContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , BASESYSTEM_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.removed );
}

TEST(
    SaveRemoverTest
    , CreateForGameContext
)
{
    CloseFile()(
        std::fopen(
            "removertest/saves/savedirectory/createforgamecontext.fgs"
            , "w"
        )
    );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , GAME_CONTEXT
        , "createforgamecontext"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.removed );
}

TEST(
    SaveRemoverTest
    , CreateForGameContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "removertest"
        , "removertest/libremovertest.so"
        , "removertest/"
        , "notexists/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  REMOVER_UNIQUE = fg::SaveRemover::create(
        state
        , GAME_CONTEXT
        , "notexists"
        , createTestProc
    );
    ASSERT_NE( nullptr, REMOVER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.removed );
}
