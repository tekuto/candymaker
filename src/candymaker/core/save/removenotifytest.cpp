﻿#include "fg/util/test.h"
#include "candymaker/core/save/removenotify.h"

TEST(
    SaveRemoveNotifyTest
    , GetState
)
{
    auto    removeNotifyImpl = FgSaveRemoveNotify{
        *reinterpret_cast< FgState * >( 10 )
        , true
    };
    auto &  removeNotify = reinterpret_cast< fg::SaveRemoveNotify<> & >( removeNotifyImpl );

    ASSERT_EQ( reinterpret_cast< const fg::State<> * >( 10 ), &( removeNotify.getState() ) );
}

TEST(
    SaveRemoveNotifyTest
    , Removed
)
{
    const auto  REMOVE_NOTIFY_IMPL = FgSaveRemoveNotify{
        *reinterpret_cast< FgState * >( 10 )
        , true
    };
    const auto &    REMOVE_NOTIFY = reinterpret_cast< const fg::SaveRemoveNotify<> & >( REMOVE_NOTIFY_IMPL );

    ASSERT_TRUE( REMOVE_NOTIFY.removed() );
}
