﻿#include "fg/util/export.h"
#include "candymaker/core/save/reader.h"
#include "candymaker/core/save/readnotify.h"
#include "candymaker/core/save/save.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/common/filedata.h"
#include "fg/core/state/threadbackground.h"

#include <string>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    auto createFilePath(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
    )
    {
        auto    filePath = _HOME_DIRECTORY_PATH;
        filePath.append(
            FgSave::createFilePath(
                _SAVE_DIRECTORY_PATH
                , _FILE_NAME
            )
        );

        return filePath;
    }

    auto read(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
    )
    {
        const auto  FILE_PATH = createFilePath(
            _HOME_DIRECTORY_PATH
            , _SAVE_DIRECTORY_PATH
            , _FILE_NAME
        );

        auto    fileData = candymaker::FileData();
        if( candymaker::readFile(
            fileData
            , FILE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:ファイルが存在しない\n" );
#endif  // DEBUG

            return fg::Save::ConstUnique();
        }

        auto    dataElementUnique = FgDataElement::create( fileData );

        return FgSave::create( std::move( dataElementUnique ) );
    }

    void readThread(
        fg::StateThreadBackground< void, FgSaveReader > &   _thread
    )
    {
        auto &  reader = _thread.getData();

        auto    saveUnique = read(
            reader.HOME_DIRECTORY_PATH
            , reader.SAVE_DIRECTORY_PATH
            , reader.FILE_NAME
        );

        auto    readNotify = FgSaveReadNotify{
            *( reader.state ),
            std::move( saveUnique ),
        };
        reader.readNotifyProc( &readNotify );
    }
}

FgSave * fgSaveReaderRead(
    const FgModuleContext * _MODULE_CONTEXT_PTR
    , const char *          _FILE_NAME
)
{
    return const_cast< FgSave * >(
        &**(
            read(
                _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
                , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
                , _FILE_NAME
            ).release()
        )
    );
}

FgSaveReader * fgSaveReaderCreateForModuleContext(
    FgState *                   _statePtr
    , const FgModuleContext *   _MODULE_CONTEXT_PTR
    , const char *              _FILE_NAME
    , FgSaveReadNotifyProc      _readNotifyProc
)
{
    return new FgSaveReader(
        _statePtr
        , _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
        , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _readNotifyProc
    );
}

FgSaveReader * fgSaveReaderCreateForBasesystemContext(
    FgState *                       _statePtr
    , const FgBasesystemContext *   _BASESYSTEM_CONTEXT_PTR
    , const char *                  _FILE_NAME
    , FgSaveReadNotifyProc          _readNotifyProc
)
{
    return new FgSaveReader(
        _statePtr
        , _BASESYSTEM_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _BASESYSTEM_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _readNotifyProc
    );
}

FgSaveReader * fgSaveReaderCreateForGameContext(
    FgState *               _statePtr
    , const FgGameContext * _GAME_CONTEXT_PTR
    , const char *          _FILE_NAME
    , FgSaveReadNotifyProc  _readNotifyProc
)
{
    return new FgSaveReader(
        _statePtr
        , _GAME_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _GAME_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _readNotifyProc
    );
}

void fgSaveReaderDestroy(
    FgSaveReader *  _implPtr
)
{
    delete _implPtr;
}

FgSaveReader::FgSaveReader(
    FgState *               _statePtr
    , const std::string &   _HOME_DIRECTORY_PATH
    , const std::string &   _SAVE_DIRECTORY_PATH
    , const char *          _FILE_NAME
    , FgSaveReadNotifyProc  _readNotifyProc
)
    : state( reinterpret_cast< fg::State<> & >( *_statePtr ) )
    , HOME_DIRECTORY_PATH( _HOME_DIRECTORY_PATH )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
    , FILE_NAME( _FILE_NAME )
    , readNotifyProc( _readNotifyProc )
    , joinerUnique( fg::StateJoiner::create() )
{
    this->state.execute(
        readThread
        , *( this->joinerUnique )
        , *this
    );
}
