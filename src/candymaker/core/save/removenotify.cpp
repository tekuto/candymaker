﻿#include "fg/util/export.h"
#include "candymaker/core/save/removenotify.h"

FgState * fgSaveRemoveNotifyGetState(
    FgSaveRemoveNotify *    _implPtr
)
{
    return &( _implPtr->state );
}

bool fgSaveRemoveNotifyRemoved(
    const FgSaveRemoveNotify *  _IMPL_PTR
)
{
    return _IMPL_PTR->removed;
}
