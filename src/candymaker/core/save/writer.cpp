﻿#include "fg/util/export.h"
#include "candymaker/core/save/writer.h"
#include "candymaker/core/save/writenotify.h"
#include "candymaker/core/save/save.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/common/filedata.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "fg/core/save/save.h"
#include "fg/core/state/threadbackground.h"

#include <string>
#include <cerrno>
#include <libgen.h>
#include <sys/stat.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    auto createFilePath(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
    )
    {
        auto    filePath = _HOME_DIRECTORY_PATH;
        filePath.append(
            FgSave::createFilePath(
                _SAVE_DIRECTORY_PATH
                , _FILE_NAME
            )
        );

        return filePath;
    }

    using statStruct = struct stat;

    auto createDirectory(
        const std::string & _PATH
    )
    {
        auto    path = _PATH;
        auto    pathPtr = path.data();

        dirname( pathPtr );

        if( path == _PATH ) {
            return true;
        }

        auto    stat_ = statStruct();
        errno = 0;
        if( stat(
            pathPtr
            , &stat_
        ) != 0 ) {
            if( errno != ENOENT ) {
#ifdef  DEBUG
                std::printf( "D:stat()がENOENT以外で失敗\n" );
#endif  // DEBUG

                return false;
            }

            if( createDirectory( path ) == false ) {
                return false;
            }

            if( mkdir(
                pathPtr
                , 0755
            ) != 0 ) {
#ifdef  DEBUG
                std::printf( "D:mkdir()が失敗\n" );
#endif  // DEBUG

                return false;
            }
        }

        return true;
    }

    auto write(
        const std::string &     _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _FILE_NAME
        , const fg::Save &      _SAVE
    )
    {
        const auto  FILE_PATH = createFilePath(
            _HOME_DIRECTORY_PATH
            , _SAVE_DIRECTORY_PATH
            , _FILE_NAME
        );

        if( createDirectory( FILE_PATH ) == false ) {
#ifdef  DEBUG
            std::printf( "D:セーブディレクトリ作成に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return candymaker::writeFile(
            FILE_PATH
            , _SAVE.getDataElement()->toFileData()
        );
    }

    void writeThread(
        fg::StateThreadBackground< void, FgSaveWriter > &   _thread
    )
    {
        auto &  writer = _thread.getData();

        const auto  WROTE = write(
            writer.HOME_DIRECTORY_PATH
            , writer.SAVE_DIRECTORY_PATH
            , writer.FILE_NAME
            , *( writer.saveUnique )
        );

        auto    writeNotify = FgSaveWriteNotify{
            *( writer.state ),
            WROTE,
        };
        writer.writeNotifyProc( &writeNotify );
    }
}

bool fgSaveWriterWrite(
    const FgModuleContext * _MODULE_CONTEXT_PTR
    , const char *          _FILE_NAME
    , const FgSave *        _SAVE_PTR
)
{
    return write(
        _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
        , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , reinterpret_cast< const fg::Save & >( *_SAVE_PTR )
    );
}

FgSaveWriter * fgSaveWriterCreateForModuleContext(
    FgState *                   _statePtr
    , const FgModuleContext *   _MODULE_CONTEXT_PTR
    , const char *              _FILE_NAME
    , const FgSave *            _SAVE_PTR
    , FgSaveWriteNotifyProc     _writeNotifyProc
)
{
    return new FgSaveWriter(
        _statePtr
        , _MODULE_CONTEXT_PTR->getHomeDirectoryPath()
        , _MODULE_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _SAVE_PTR
        , _writeNotifyProc
    );
}

FgSaveWriter * fgSaveWriterCreateForBasesystemContext(
    FgState *                       _statePtr
    , const FgBasesystemContext *   _BASESYSTEM_CONTEXT_PTR
    , const char *                  _FILE_NAME
    , const FgSave *                _SAVE_PTR
    , FgSaveWriteNotifyProc         _writeNotifyProc
)
{
    return new FgSaveWriter(
        _statePtr
        , _BASESYSTEM_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _BASESYSTEM_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _SAVE_PTR
        , _writeNotifyProc
    );
}

FgSaveWriter * fgSaveWriterCreateForGameContext(
    FgState *               _statePtr
    , const FgGameContext * _GAME_CONTEXT_PTR
    , const char *          _FILE_NAME
    , const FgSave *        _SAVE_PTR
    , FgSaveWriteNotifyProc _writeNotifyProc
)
{
    return new FgSaveWriter(
        _statePtr
        , _GAME_CONTEXT_PTR->getModuleContext().getHomeDirectoryPath()
        , _GAME_CONTEXT_PTR->getSaveDirectoryPath()
        , _FILE_NAME
        , _SAVE_PTR
        , _writeNotifyProc
    );
}

void fgSaveWriterDestroy(
    FgSaveWriter *  _implPtr
)
{
    delete _implPtr;
}

FgSaveWriter::FgSaveWriter(
    FgState *               _statePtr
    , const std::string &   _HOME_DIRECTORY_PATH
    , const std::string &   _SAVE_DIRECTORY_PATH
    , const char *          _FILE_NAME
    , const FgSave *        _SAVE_PTR
    , FgSaveWriteNotifyProc _writeNotifyProc
)
    : state( reinterpret_cast< fg::State<> & >( *_statePtr ) )
    , HOME_DIRECTORY_PATH( _HOME_DIRECTORY_PATH )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
    , FILE_NAME( _FILE_NAME )
    , saveUnique( reinterpret_cast< const fg::Save & >( *_SAVE_PTR ).clone() )
    , writeNotifyProc( _writeNotifyProc )
    , joinerUnique( fg::StateJoiner::create() )
{
    this->state.execute(
        writeThread
        , *( this->joinerUnique )
        , *this
    );
}
