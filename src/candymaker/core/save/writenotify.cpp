﻿#include "fg/util/export.h"
#include "candymaker/core/save/writenotify.h"

FgState * fgSaveWriteNotifyGetState(
    FgSaveWriteNotify * _implPtr
)
{
    return &( _implPtr->state );
}

bool fgSaveWriteNotifyWrote(
    const FgSaveWriteNotify *   _IMPL_PTR
)
{
    return _IMPL_PTR->wrote;
}
