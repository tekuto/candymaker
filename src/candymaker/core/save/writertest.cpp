﻿#include "fg/util/test.h"
#include "candymaker/core/save/writer.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/state/state.h"
#include "candymaker/core/common/filedata.h"
#include "fg/core/save/save.h"
#include "fg/core/save/writenotify.h"
#include "fg/core/dataelement/string.h"

#include <filesystem>
#include <mutex>
#include <condition_variable>
#include <sys/stat.h>

TEST(
    SaveWriterTest
    , Write
)
{
    std::filesystem::remove( "savewritertest/saves/savedirectory/savename.fgs" );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    ASSERT_TRUE(
        fg::SaveWriter::write(
            MODULE_CONTEXT
            , "savename"
            , SAVE
        )
    );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "savewritertest/saves/savedirectory/savename.fgs"
        )
    );
    const auto  FILE_DATA_STRING = std::string(
        fileData.begin()
        , fileData.end()
    );
    ASSERT_STREQ( "\"SAVEDATA\"", FILE_DATA_STRING.c_str() );
}

TEST(
    SaveWriterTest
    , Write_withCreateDirectory
)
{
    std::filesystem::remove_all( "savewritertest/savewithcreatedirectory" );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/savewithcreatedirectory/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    ASSERT_TRUE(
        fg::SaveWriter::write(
            MODULE_CONTEXT
            , "savename"
            , SAVE
        )
    );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "savewritertest/savewithcreatedirectory/saves/savedirectory/savename.fgs"
        )
    );
    const auto  FILE_DATA_STRING = std::string(
        fileData.begin()
        , fileData.end()
    );
    ASSERT_STREQ( "\"SAVEDATA\"", FILE_DATA_STRING.c_str() );
}

TEST(
    SaveWriterTest
    , Write_failed
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/savefailed/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    ASSERT_FALSE(
        fg::SaveWriter::write(
            MODULE_CONTEXT
            , "savename"
            , SAVE
        )
    );

    struct stat stat_;
    ASSERT_EQ(
        0
        , stat(
            "savewritertest/savefailed/saves/savedirectory/savename.fgs"
            , &stat_
        )
    );
    ASSERT_FALSE( S_ISREG( stat_.st_mode ) );
}

struct CreateTestData
{
    std::mutex              mutex;
    std::condition_variable cond;
    bool                    ended = false;
    bool                    wrote = false;
};

void dummyDestroy(
    void *
)
{
}

void createTestProc(
    fg::SaveWriteNotify< CreateTestData > & _writeNotify
)
{
    auto &  state = _writeNotify.getState();
    auto &  data = state.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.ended = true;
    data.wrote = _writeNotify.wrote();

    data.cond.notify_one();
}

TEST(
    SaveWriterTest
    , CreateForModuleContext
)
{
    std::filesystem::remove_all( "savewritertest/saves/createformodulecontext" );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/"
        , "createformodulecontext/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , MODULE_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.wrote );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "savewritertest/saves/createformodulecontext/savename.fgs"
        )
    );
    const auto  FILE_DATA_STRING = std::string(
        fileData.begin()
        , fileData.end()
    );
    ASSERT_STREQ( "\"SAVEDATA\"", FILE_DATA_STRING.c_str() );
}

TEST(
    SaveWriterTest
    , CreateForModuleContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    // 次のステートが存在する場合でも処理を行う
    auto    nextStateUnique = FgState::create(
        threadCache
        , &*state
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );

    state->setNextState( std::move( nextStateUnique ) );

    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/savefailed/"
        , "savedirectory/"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = reinterpret_cast< const fg::ModuleContext & >( *MODULE_CONTEXT_SHARED );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , MODULE_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.wrote );
}

TEST(
    SaveWriterTest
    , CreateForBasesystemContext
)
{
    std::filesystem::remove_all( "savewritertest/saves/createforbasesystemcontext" );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/"
        , "createformodulecontext/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "createforbasesystemcontext/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , BASESYSTEM_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.wrote );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "savewritertest/saves/createformodulecontext/savename.fgs"
        )
    );
    const auto  FILE_DATA_STRING = std::string(
        fileData.begin()
        , fileData.end()
    );
    ASSERT_STREQ( "\"SAVEDATA\"", FILE_DATA_STRING.c_str() );
}

TEST(
    SaveWriterTest
    , CreateForBasesystemContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/savefailed/"
        , "createformodulecontext/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< void > & >( BASESYSTEM_CONTEXT_IMPL );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , BASESYSTEM_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.wrote );
}

TEST(
    SaveWriterTest
    , CreateForGameContext
)
{
    std::filesystem::remove_all( "savewritertest/saves/createforgamecontext" );

    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/"
        , "createformodulecontext/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "createforgamecontext/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , GAME_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.wrote );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "savewritertest/saves/createformodulecontext/savename.fgs"
        )
    );
    const auto  FILE_DATA_STRING = std::string(
        fileData.begin()
        , fileData.end()
    );
    ASSERT_STREQ( "\"SAVEDATA\"", FILE_DATA_STRING.c_str() );
}

TEST(
    SaveWriterTest
    , CreateForGameContext_failed
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    data = CreateTestData();

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , &data
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< CreateTestData > & >( *stateUnique );

    rootState.setNextState( std::move( stateUnique ) );

    auto    moduleContextShared = FgModuleContext::create(
        "savewritertest"
        , "savewritertest/libsavewritertest.so"
        , "savewritertest/savefailed/"
        , "createformodulecontext/"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = *moduleContextShared;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        rootState
        , moduleContext
        , "savedirectory/"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    const auto  SAVEDATA_UNIQUE = fg::DataElementString::create( "SAVEDATA" );
    ASSERT_NE( nullptr, SAVEDATA_UNIQUE.get() );
    const auto &    SAVEDATA = *SAVEDATA_UNIQUE;

    const auto  SAVE_UNIQUE = fg::Save::create( SAVEDATA );
    ASSERT_NE( nullptr, SAVE_UNIQUE.get() );
    const auto &    SAVE = *SAVE_UNIQUE;

    const auto  WRITER_UNIQUE = fg::SaveWriter::create(
        state
        , GAME_CONTEXT
        , "savename"
        , SAVE
        , createTestProc
    );
    ASSERT_NE( nullptr, WRITER_UNIQUE.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended != true ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( data.ended );
    ASSERT_FALSE( data.wrote );
}
