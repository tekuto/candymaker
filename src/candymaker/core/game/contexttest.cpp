﻿#include "fg/util/test.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/state/state.h"
#include "candymaker/core/thread/cache.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/enterevent.h"
#include "fg/common/unique.h"

void dummyDestroy(
    void *
)
{
}

struct GameContextTestData : public fg::UniqueWrapper< GameContextTestData >
{
    static Unique create(
    )
    {
        return new GameContextTestData;
    }
};

TEST(
    GameContextTest
    , Constructor
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContext = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );

    ASSERT_EQ( &state, &( gameContext.state ) );
    ASSERT_EQ( reinterpret_cast< FgModuleContext * >( 10 ), &( gameContext.moduleContext ) );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", gameContext.SAVE_DIRECTORY_PATH.c_str() );
}

TEST(
    GameContextTest
    , GetModuleContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    ASSERT_EQ( reinterpret_cast< const FgModuleContext * >( 10 ), &( GAME_CONTEXT->getModuleContext() ) );
}

TEST(
    GameContextTest
    , GetSaveDirectoryPath
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    const auto  GAME_CONTEXT_IMPL = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    GAME_CONTEXT = reinterpret_cast< const fg::GameContext<> & >( GAME_CONTEXT_IMPL );

    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", GAME_CONTEXT->getSaveDirectoryPath().c_str() );
}

void startTestProc(
    fg::StateEnterEvent< int > &    _event
)
{
    _event.getState().getData() = 30;
}

TEST(
    GameContextTest
    , Start
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , &i
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContext = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 20 )
        , "SAVE_DIRECTORY_PATH"
    );

    state.setEnterEventProc( reinterpret_cast< candymaker::StateEvent::Proc >( startTestProc ) );

    gameContext.start();

    state.threadJoinerUnique->join();

    ASSERT_FALSE( state.disabled );
    ASSERT_EQ( 30, i );
}

struct SetStateDataTestData : public fg::UniqueWrapper< SetStateDataTestData >
{
    int &   i;

    SetStateDataTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new SetStateDataTestData( _i );
    }

    static void destroy(
        SetStateDataTestData *  _this
    )
    {
        _this->i = 20;

        delete _this;
    }
};

TEST(
    GameContextTest
    , SetStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextImpl = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  gameContext = reinterpret_cast< fg::GameContext< SetStateDataTestData > & >( gameContextImpl );

    auto    i = 10;

    auto    dataUnique = SetStateDataTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    gameContext.setStateData(
        dataUnique.release()
        , SetStateDataTestData::destroy
    );

    stateUnique.destroy();

    ASSERT_EQ( 20, i );
}

TEST(
    GameContextTest
    , GetCreatingState
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextImpl = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  gameContext = reinterpret_cast< fg::GameContext< GameContextTestData > & >( gameContextImpl );

    ASSERT_EQ( reinterpret_cast< fg::CreatingState< GameContextTestData > * >( &state ), &( gameContext.getCreatingState() ) );
}

TEST(
    GameContextTest
    , GetCreatingState_withoutStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameContextImpl = FgGameContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  gameContext = reinterpret_cast< fg::GameContext<> & >( gameContextImpl );

    ASSERT_EQ( reinterpret_cast< fg::CreatingState<> * >( &state ), &( gameContext.getCreatingState() ) );
}
