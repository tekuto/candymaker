﻿#include "fg/util/export.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/state/state.h"

FgGameContext::FgGameContext(
    FgState &               _state
    , FgModuleContext &     _moduleContext
    , const std::string &   _SAVE_DIRECTORY_PATH
)
    : state( _state )
    , moduleContext( _moduleContext )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
{
}

const FgModuleContext & FgGameContext::getModuleContext(
) const
{
    return this->moduleContext;
}

const std::string & FgGameContext::getSaveDirectoryPath(
) const
{
    return this->SAVE_DIRECTORY_PATH;
}

void FgGameContext::start(
)
{
    this->state.executeEnterThread();
}

void fgGameContextSetStateData(
    FgGameContext *                     _implPtr
    , void *                            _dataPtr
    , FgGameContextStateDataDestroyProc _dataDestroyProcPtr
)
{
    _implPtr->state.setData(
        _dataPtr
        , _dataDestroyProcPtr
    );
}

FgCreatingState * fgGameContextGetCreatingState(
    FgGameContext * _implPtr
)
{
    return reinterpret_cast< FgCreatingState * >( &( _implPtr->state ) );
}
