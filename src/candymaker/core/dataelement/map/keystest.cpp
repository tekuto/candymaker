﻿#include "fg/util/test.h"
#include "candymaker/core/dataelement/map/keys.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/common/filedata.h"

#include <string>
#include <utility>

TEST(
    DataElementMapKeysTest
    , Create
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    auto    mapKeysUnique = fg::DataElementMapKeys::create( MAP );
    ASSERT_NE( nullptr, mapKeysUnique.get() );
    const auto &    MAP_KEYS = *mapKeysUnique;

    const auto &    KEYS = MAP_KEYS->keys;
    ASSERT_EQ( 1, KEYS.size() );
    ASSERT_STREQ( "KEY", KEYS[ 0 ].c_str() );
}

TEST(
    DataElementMapKeysTest
    , GetSize
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    auto    mapKeysUnique = fg::DataElementMapKeys::create( MAP );
    ASSERT_NE( nullptr, mapKeysUnique.get() );

    ASSERT_EQ( 1, mapKeysUnique->getSize() );
}

TEST(
    DataElementMapKeysTest
    , GetKey
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    auto    mapKeysUnique = fg::DataElementMapKeys::create( MAP );
    ASSERT_NE( nullptr, mapKeysUnique.get() );

    ASSERT_STREQ( "KEY", mapKeysUnique->getKey( 0 ) );
}

TEST(
    DataElementMapKeysTest
    , GetKey_failed
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    auto    mapKeysUnique = fg::DataElementMapKeys::create( MAP );
    ASSERT_NE( nullptr, mapKeysUnique.get() );

    try {
        mapKeysUnique->getKey( 10 );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
