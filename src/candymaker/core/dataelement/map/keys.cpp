﻿#include "fg/util/export.h"
#include "candymaker/core/dataelement/map/keys.h"
#include "candymaker/core/dataelement/map/map.h"

#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

FgDataElementMapKeys * fgDataElementMapKeysCreate(
    const FgDataElementMap *    _MAP_PTR
)
{
    auto    keys = FgDataElementMapKeys::Keys();
    for( const auto & PAIR : _MAP_PTR->map ) {
        const auto &    KEY = PAIR.first;

        keys.push_back( KEY );
    }

    return new FgDataElementMapKeys{
        std::move( keys ),
    };
}

void fgDataElementMapKeysDestroy(
    FgDataElementMapKeys *  _implPtr
)
{
    delete _implPtr;
}

size_t fgDataElementMapKeysGetSize(
    const FgDataElementMapKeys *    _IMPL_PTR
)
{
    return _IMPL_PTR->keys.size();
}

const char * fgDataElementMapKeysGetKey(
    const FgDataElementMapKeys *    _IMPL_PTR
    , size_t                        _index
)
{
    return _IMPL_PTR->keys.at( _index ).c_str();
}
