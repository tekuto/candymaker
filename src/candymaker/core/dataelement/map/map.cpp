﻿#include "fg/util/export.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/list.h"

#include <string>
#include <utility>
#include <stdexcept>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_MAP_ELEMENT = '{';
    const auto  BEGIN_MAP_ELEMENT_LENGTH = 1;
    const auto  END_MAP_ELEMENT = '}';
    const auto  END_MAP_ELEMENT_LENGTH = 1;

    const auto  MAP_ELEMENT_SEPARATOR = ':';
    const auto  MAP_ELEMENT_SEPARATOR_LENGTH = 1;

    const auto  SEPARATOR = ',';
    const auto  SEPARATOR_LENGTH = 1;

    template< typename GET_VALUE_PROC_T >
    auto getDataElement(
        const FgDataElementMap *    _IMPL_PTR
        , const char *              _KEY
    )
    {
        const auto &    MAP = _IMPL_PTR->map;

        const auto  IT = MAP.find( _KEY );
        if( IT == MAP.end() ) {
#ifdef  DEBUG
            std::printf( "D:キーに対応する値が存在しない\n" );
#endif  // DEBUG

            return static_cast< decltype( GET_VALUE_PROC_T()( nullptr ) ) >( nullptr );
        }

        return GET_VALUE_PROC_T()( &**( IT->second ) );
    }

    template< typename ELEMENT_UNIQUE_T >
    void addDataElement(
        FgDataElementMap *      _implPtr
        , const char *          _KEY
        , ELEMENT_UNIQUE_T &&   _elementUnique
    )
    {
        auto &  map = _implPtr->map;

        auto    it = map.find( _KEY );
        if( it != map.end() ) {
#ifdef  DEBUG
            std::printf( "D:マップの要素に重複があるので追加済みの要素を削除\n" );
#endif  // DEBUG

            map.erase( it );
        }

        const auto  RESULT = map.insert(
            std::make_pair(
                _KEY
                , FgDataElement::create( std::move( _elementUnique ) )
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "マップへの要素追加に失敗" );
        }
    }

    template< typename ELEMENT_T >
    void addDataElement(
        FgDataElementMap *  _implPtr
        , const char *      _KEY
        , const ELEMENT_T * _ELEMENT_PTR
    )
    {
        addDataElement(
            _implPtr
            , _KEY
            , _ELEMENT_PTR->clone()
        );
    }

    template<
        typename T
        , typename ELEMENT_T
    >
    void addDataElement(
        FgDataElementMap *  _implPtr
        , const char *      _KEY
        , const ELEMENT_T * _ELEMENT_PTR
    )
    {
        addDataElement(
            _implPtr
            , _KEY
            , typename T::ConstUnique( const_cast< ELEMENT_T * >( _ELEMENT_PTR ) )
        );
    }

    FgDataElementMap::Map cloneMap(
        const FgDataElementMap::Map &   _ORG_MAP
    )
    {
        auto    map = FgDataElementMap::Map();
        for( const auto & PAIR : _ORG_MAP ) {
            const auto &    KEY = PAIR.first;
            const auto &    ELEMENT = *( PAIR.second );

            const auto  RESULT = map.insert(
                std::make_pair(
                    KEY
                    , ELEMENT->clone()
                )
            );
            if( RESULT.second == false ) {
                throw std::runtime_error( "マップへの要素追加に失敗" );
            }
        }

        return map;
    }

    auto getBegin(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        if( *_IT != BEGIN_MAP_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:{で始まっていないためマップ要素ではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + BEGIN_MAP_ELEMENT_LENGTH;
    }

    auto getEndMap(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT > _END ) {
            throw std::runtime_error( "マップ要素の終端文字が出現する前に文字列の最後に達している" );
        }

        if( *_IT != END_MAP_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素の終端文字ではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + END_MAP_ELEMENT_LENGTH;
    }

    auto initMapKey(
        std::string &                                   _key
        , const candymaker::FileData::const_iterator &  _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    it = _IT;

        auto    keyUnique = FgDataElementString::generate(
            it
            , _END
        );
        if( keyUnique.get() == nullptr ) {
            throw std::runtime_error( "マップ要素のキー生成に失敗" );
        }

        _key = keyUnique->getString();

        return it;
    }

    auto getEndMapElementSeparator(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
            throw std::runtime_error( "マップ要素の区切り文字が出現する前に終端に達している" );
        }

        if( *_IT != MAP_ELEMENT_SEPARATOR ) {
            throw std::runtime_error( "マップ要素の区切り文字が存在しない" );
        }

        return _IT + MAP_ELEMENT_SEPARATOR_LENGTH;
    }

    void insert(
        FgDataElementMap::Map &             _map
        , std::string &&                    _key
        , fg::DataElement::ConstUnique &&   _valueUnique
    )
    {
        auto    it = _map.find( _key );
        if( it != _map.end() ) {
#ifdef  DEBUG
            std::printf( "D:マップの要素に重複があるので追加済みの要素を削除\n" );
#endif  // DEBUG

            _map.erase( it );
        }

        const auto  RESULT = _map.insert(
            std::make_pair(
                std::move( _key )
                , std::move( _valueUnique )
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "マップへの要素追加に失敗" );
        }
    }

    auto addDataElement(
        FgDataElementMap::Map &                         _map
        , const candymaker::FileData::const_iterator &  _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    key = std::string();

        const auto  END_KEY = initMapKey(
            key
            , _IT
            , _END
        );

        const auto  BEGIN_ELEMENT_SEPARATOR = FgDataElement::getNextToken(
            END_KEY
            , _END
        );

        const auto  END_ELEMENT_SEPARATOR = getEndMapElementSeparator(
            BEGIN_ELEMENT_SEPARATOR
            , _END
        );

        const auto  BEGIN_VALUE = FgDataElement::getNextToken(
            END_ELEMENT_SEPARATOR
            , _END
        );

        auto    it = BEGIN_VALUE;

        auto    valueUnique = FgDataElement::create(
            it
            , _END
        );

        insert(
            _map
            , std::move( key )
            , std::move( valueUnique )
        );

        return it;
    }

    auto getEndSeparator(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
            throw std::runtime_error( "マップのセパレータが出現する前に終端に達している" );
        }

        if( *_IT != SEPARATOR ) {
#ifdef  DEBUG
            std::printf( "D:マップのセパレータではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + SEPARATOR_LENGTH;
    }

    auto initDataElements(
        FgDataElementMap::Map &                         _map
        , const candymaker::FileData::const_iterator &  _BEGIN
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    it = _BEGIN;

        while( 1 ) {
            const auto  BEGIN_ELEMENT = FgDataElement::getNextToken(
                it
                , _END
            );

            {
                const auto  END_MAP = getEndMap(
                    BEGIN_ELEMENT
                    , _END
                );
                if( END_MAP != candymaker::FileData::const_iterator() ) {
                    return END_MAP;
                }
            }

            const auto  END_ELEMENT = addDataElement(
                _map
                , BEGIN_ELEMENT
                , _END
            );

            const auto  BEGIN_SEPARATOR = FgDataElement::getNextToken(
                END_ELEMENT
                , _END
            );

            const auto  END_SEPARATOR = getEndSeparator(
                BEGIN_SEPARATOR
                , _END
            );
            if( END_SEPARATOR != candymaker::FileData::const_iterator() ) {
                it = END_SEPARATOR;

                continue;
            }

            {
                const auto  END_MAP = getEndMap(
                    BEGIN_SEPARATOR
                    , _END
                );
                if( END_MAP != candymaker::FileData::const_iterator() ) {
                    return END_MAP;
                }
            }

            throw std::runtime_error( "マップの内部要素の後にセパレータかマップ要素の終端が存在しない" );
        }
    }

    bool initMap(
        FgDataElementMap::Map &                         _map
        , candymaker::FileData::const_iterator &        _it
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        const auto  BEGIN = getBegin(
            _it
            , _END
        );
        if( BEGIN == candymaker::FileData::const_iterator() ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素開始位置への移動に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    map = FgDataElementMap::Map();

        const auto  END_MAP = initDataElements(
            map
            , BEGIN
            , _END
        );

        _map = std::move( map );

        _it = END_MAP;

        return true;
    }
}

FgDataElementMap * fgDataElementMapCreate(
)
{
    return new FgDataElementMap;
}

void fgDataElementMapDestroy(
    FgDataElementMap *  _implPtr
)
{
    delete _implPtr;
}

const FgDataElement * fgDataElementMapGetDataElement(
    const FgDataElementMap *    _IMPL_PTR
    , const char *              _KEY
)
{
    struct GetValue
    {
        auto operator()(
            const FgDataElement *   _ELEMENT_PTR
        ) const
        {
            return _ELEMENT_PTR;
        }
    };

    return getDataElement< GetValue >(
        _IMPL_PTR
        , _KEY
    );
}

const FgDataElementString * fgDataElementMapGetDataElementString(
    const FgDataElementMap *    _IMPL_PTR
    , const char *              _KEY
)
{
    struct GetValue
    {
        auto operator()(
            const FgDataElement *   _ELEMENT_PTR
        ) const
        {
            return fgDataElementGetDataElementString( _ELEMENT_PTR );
        }
    };

    return getDataElement< GetValue >(
        _IMPL_PTR
        , _KEY
    );
}

const FgDataElementList * fgDataElementMapGetDataElementList(
    const FgDataElementMap *    _IMPL_PTR
    , const char *              _KEY
)
{
    struct GetValue
    {
        auto operator()(
            const FgDataElement *   _ELEMENT_PTR
        ) const
        {
            return fgDataElementGetDataElementList( _ELEMENT_PTR );
        }
    };

    return getDataElement< GetValue >(
        _IMPL_PTR
        , _KEY
    );
}

const FgDataElementMap * fgDataElementMapGetDataElementMap(
    const FgDataElementMap *    _IMPL_PTR
    , const char *              _KEY
)
{
    struct GetValue
    {
        auto operator()(
            const FgDataElement *   _ELEMENT_PTR
        ) const
        {
            return fgDataElementGetDataElementMap( _ELEMENT_PTR );
        }
    };

    return getDataElement< GetValue >(
        _IMPL_PTR
        , _KEY
    );
}

void fgDataElementMapAddDataElementString(
    FgDataElementMap *              _implPtr
    , const char *                  _KEY
    , const FgDataElementString *   _STRING_PTR
)
{
    addDataElement(
        _implPtr
        , _KEY
        , _STRING_PTR
    );
}

void fgDataElementMapAddMoveDataElementString(
    FgDataElementMap *              _implPtr
    , const char *                  _KEY
    , const FgDataElementString *   _STRING_PTR
)
{
    addDataElement< fg::DataElementString >(
        _implPtr
        , _KEY
        , _STRING_PTR
    );
}

void fgDataElementMapAddDataElementList(
    FgDataElementMap *          _implPtr
    , const char *              _KEY
    , const FgDataElementList * _LIST_PTR
)
{
    addDataElement(
        _implPtr
        , _KEY
        , _LIST_PTR
    );
}

void fgDataElementMapAddMoveDataElementList(
    FgDataElementMap *          _implPtr
    , const char *              _KEY
    , const FgDataElementList * _LIST_PTR
)
{
    addDataElement< fg::DataElementList >(
        _implPtr
        , _KEY
        , _LIST_PTR
    );
}

void fgDataElementMapAddDataElementMap(
    FgDataElementMap *          _implPtr
    , const char *              _KEY
    , const FgDataElementMap *  _MAP_PTR
)
{
    addDataElement(
        _implPtr
        , _KEY
        , _MAP_PTR
    );
}

void fgDataElementMapAddMoveDataElementMap(
    FgDataElementMap *          _implPtr
    , const char *              _KEY
    , const FgDataElementMap *  _MAP_PTR
)
{
    addDataElement< fg::DataElementMap >(
        _implPtr
        , _KEY
        , _MAP_PTR
    );
}

FgDataElementMap::FgDataElementMap(
    const FgDataElementMap &    _ORG
)
    : map( cloneMap( _ORG.map ) )
{
}

FgDataElementMap::FgDataElementMap(
    Map &&  _map
)
    : map( std::move( _map ) )
{
}

fg::DataElementMap::ConstUnique FgDataElementMap::generate(
    candymaker::FileData::const_iterator &          _it
    , const candymaker::FileData::const_iterator &  _END
)
{
    auto    it = _it;

    auto    map = FgDataElementMap::Map();
    if( initMap(
        map
        , it
        , _END
    ) == false ) {
#ifdef  DEBUG
        std::printf( "D:マップの要素初期化に失敗\n" );
#endif  // DEBUG

        return nullptr;
    }

    auto    thisUnique = fg::DataElementMap::ConstUnique(
        new FgDataElementMap(
            std::move( map )
        )
    );

    _it = std::move( it );

    return thisUnique;
}

candymaker::FileData FgDataElementMap::toFileData(
) const
{
    const auto &    MAP = this->map;

    const auto  BEGIN = MAP.begin();
    const auto  END = MAP.end();

    auto    fileData = candymaker::FileData();
    fileData.push_back( BEGIN_MAP_ELEMENT );

    for( auto it = BEGIN ; it != END ; it++ ) {
        if( it != BEGIN ) {
            fileData.push_back( SEPARATOR );
        }

        const auto  KEY = FgDataElementString::stringToFileData( it->first );

        const auto  VALUE = ( *( it->second ) )->toFileData();

        fileData.insert(
            fileData.end()
            , KEY.begin()
            , KEY.end()
        );
        fileData.push_back( MAP_ELEMENT_SEPARATOR );
        fileData.insert(
            fileData.end()
            , VALUE.begin()
            , VALUE.end()
        );
    }

    fileData.push_back( END_MAP_ELEMENT );

    return fileData;
}
