﻿#include "fg/util/test.h"
#include "fg/core/dataelement/map/map.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/common/filedata.h"

#include <string>
#include <utility>
#include <cstddef>

TEST(
    DataElementMapTest
    , Create
)
{
    const auto  MAP_UNIQUE = fg::DataElementMap::create();
    ASSERT_NE( nullptr, MAP_UNIQUE.get() );
    const auto &    MAP = *MAP_UNIQUE;

    ASSERT_TRUE( MAP->map.empty() );
}

TEST(
    DataElementMapTest
    , GetDataElement
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto  ELEMENT_PTR = mapUnique->getDataElement( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  STRING_PTR = ELEMENT_PTR->getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "VALUE", STRING_PTR->getString() );
}

TEST(
    DataElementMapTest
    , GetDataElement_failedNotFound
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    ASSERT_EQ( nullptr, mapUnique->getDataElement( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementString
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto  ELEMENT_PTR = mapUnique->getDataElementString( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    ASSERT_STREQ( "VALUE", ELEMENT_PTR->getString() );
}

TEST(
    DataElementMapTest
    , GetDataElementString_failedNotFound
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    ASSERT_EQ( nullptr, mapUnique->getDataElementString( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementString_failedDiffType
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( listUnique )
    );

    ASSERT_EQ( nullptr, mapUnique->getDataElementString( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementList
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    mapUnique->addDataElement(
        "KEY"
        , std::move( listUnique )
    );

    const auto  ELEMENT_PTR = mapUnique->getDataElementList( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    ASSERT_EQ( &LIST, ELEMENT_PTR );
}

TEST(
    DataElementMapTest
    , GetDataElementList_failedNotFound
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    ASSERT_EQ( nullptr, mapUnique->getDataElementList( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementList_failedDiffType
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    ASSERT_EQ( nullptr, mapUnique->getDataElementList( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    map2Unique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, map2Unique.get() );
    const auto &    MAP2 = *map2Unique;

    mapUnique->addDataElement(
        "KEY"
        , std::move( map2Unique )
    );

    const auto  ELEMENT_PTR = mapUnique->getDataElementMap( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    ASSERT_EQ( &MAP2, ELEMENT_PTR );
}

TEST(
    DataElementMapTest
    , GetDataElementMap_failedNotFound
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    ASSERT_EQ( nullptr, mapUnique->getDataElementMap( "KEY" ) );
}

TEST(
    DataElementMapTest
    , GetDataElementMap_failedDiffType
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    ASSERT_EQ( nullptr, mapUnique->getDataElementMap( "KEY" ) );
}

TEST(
    DataElementMapTest
    , AddDataElementString
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    map.addDataElement(
        "KEY"
        , STRING
    );

    const auto &    MAP = map->map;
    ASSERT_EQ( 1, MAP.size() );
    const auto  IT = MAP.find( "KEY" );
    ASSERT_NE( MAP.end(), IT );
    const auto &    STRING_UNIQUE = ( *( IT->second ) )->stringUnique;
    ASSERT_NE( &STRING, STRING_UNIQUE.get() );
    ASSERT_STREQ( "VALUE", ( *STRING_UNIQUE )->string.c_str() );
}

TEST(
    DataElementMapTest
    , AddDataElementString_overwrite
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    string1Unique = fg::DataElementString::create( "VALUE1" );
    ASSERT_NE( nullptr, string1Unique.get() );
    const auto &    STRING1 = *string1Unique;

    map.addDataElement(
        "KEY"
        , STRING1
    );

    auto    string2Unique = fg::DataElementString::create( "VALUE2" );
    ASSERT_NE( nullptr, string2Unique.get() );
    const auto &    STRING2 = *string2Unique;

    map.addDataElement(
        "KEY"
        , STRING2
    );

    const auto &    MAP = map->map;
    ASSERT_EQ( 1, MAP.size() );
    const auto  IT = MAP.find( "KEY" );
    ASSERT_NE( MAP.end(), IT );
    const auto &    STRING_UNIQUE = ( *( IT->second ) )->stringUnique;
    ASSERT_NE( &STRING2, STRING_UNIQUE.get() );
    ASSERT_STREQ( "VALUE2", ( *STRING_UNIQUE )->string.c_str() );
}

TEST(
    DataElementMapTest
    , AddMoveDataElementString
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    map.addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto &    MAP = map->map;
    ASSERT_EQ( 1, MAP.size() );
    const auto  IT = MAP.find( "KEY" );
    ASSERT_NE( MAP.end(), IT );
    const auto &    STRING_UNIQUE = ( *( IT->second ) )->stringUnique;
    ASSERT_EQ( &STRING, STRING_UNIQUE.get() );
    ASSERT_STREQ( "VALUE", ( *STRING_UNIQUE )->string.c_str() );
}

TEST(
    DataElementMapTest
    , AddDataElementList
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( std::move( stringUnique ) );

    mapUnique->addDataElement(
        "KEY"
        , LIST
    );

    const auto  ELEMENT_PTR = MAP.getDataElement( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  LIST_PTR = ELEMENT_PTR->getDataElementList();
    ASSERT_NE( &LIST, LIST_PTR );

    ASSERT_EQ( 1, LIST_PTR->getSize() );
    const auto  STRING_PTR = LIST_PTR->getDataElement( 0 ).getDataElementString();
    ASSERT_NE( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementMapTest
    , AddMoveDataElementList
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( std::move( stringUnique ) );

    mapUnique->addDataElement(
        "KEY"
        , std::move( listUnique )
    );

    const auto  ELEMENT_PTR = MAP.getDataElement( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  LIST_PTR = ELEMENT_PTR->getDataElementList();
    ASSERT_EQ( &LIST, LIST_PTR );

    ASSERT_EQ( 1, LIST_PTR->getSize() );
    const auto  STRING_PTR = LIST_PTR->getDataElement( 0 ).getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementMapTest
    , AddDataElementMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    addMapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, addMapUnique.get() );
    const auto &    ADD_MAP = *addMapUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    addMapUnique->addDataElement(
        "KEY1"
        , std::move( stringUnique )
    );

    mapUnique->addDataElement(
        "KEY2"
        , ADD_MAP
    );

    const auto  MAP_ELEMENT_PTR = MAP.getDataElement( "KEY2" );
    ASSERT_NE( nullptr, MAP_ELEMENT_PTR );
    const auto  ADD_MAP_PTR = MAP_ELEMENT_PTR->getDataElementMap();
    ASSERT_NE( &ADD_MAP, ADD_MAP_PTR );

    const auto  ELEMENT_PTR = ADD_MAP_PTR->getDataElement( "KEY1" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  STRING_PTR = ELEMENT_PTR->getDataElementString();
    ASSERT_NE( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementMapTest
    , AddMoveDataElementMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    addMapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, addMapUnique.get() );
    const auto &    ADD_MAP = *addMapUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    addMapUnique->addDataElement(
        "KEY1"
        , std::move( stringUnique )
    );

    mapUnique->addDataElement(
        "KEY2"
        , std::move( addMapUnique )
    );

    const auto  MAP_ELEMENT_PTR = MAP.getDataElement( "KEY2" );
    ASSERT_NE( nullptr, MAP_ELEMENT_PTR );
    const auto  ADD_MAP_PTR = MAP_ELEMENT_PTR->getDataElementMap();
    ASSERT_EQ( &ADD_MAP, ADD_MAP_PTR );

    const auto  ELEMENT_PTR = ADD_MAP_PTR->getDataElement( "KEY1" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  STRING_PTR = ELEMENT_PTR->getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementMapTest
    , Clone
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    map.addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto  CLONE_UNIQUE = map->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    MAP = CLONE->map;
    ASSERT_EQ( 1, MAP.size() );
    const auto  IT = MAP.find( "KEY" );
    ASSERT_NE( MAP.end(), IT );
    const auto &    STRING_UNIQUE = ( *( IT->second ) )->stringUnique;
    ASSERT_NE( &STRING, STRING_UNIQUE.get() );
    ASSERT_STREQ( "VALUE", ( *STRING_UNIQUE )->string.c_str() );
}

void testGenerate(
    const std::string &                             _DATA
    , const std::map< std::string, std::string > &  _MAP
    , std::size_t                                   _elementSize
)
{
    const auto  FILE_DATA = candymaker::FileData(
        _DATA.begin()
        , _DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementMapUnique = FgDataElementMap::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, elementMapUnique.get() );
    const auto &    ELEMENT_MAP = *elementMapUnique;

    const auto &    MAP = ELEMENT_MAP->map;

    const auto  MAP_SIZE = _MAP.size();
    ASSERT_EQ( MAP_SIZE, MAP.size() );

    const auto  END_MAP = MAP.end();
    for( const auto & PAIR : _MAP ) {
        const auto &    KEY = PAIR.first;
        const auto &    VALUE = PAIR.second;

        const auto  IT = MAP.find( KEY );
        ASSERT_NE( END_MAP, IT );

        ASSERT_STREQ( VALUE.c_str(), ( *( ( *( IT->second ) )->stringUnique ) )->string.c_str() );
    }

    ASSERT_EQ( BEGIN + _elementSize, it );
}

TEST(
    DataElementMapTest
    , Generate
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "{ \"KEY\" : \"VALUE\" }   "
            , {
                { "KEY", "VALUE" },
            }
            , 19
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "{ \"KEY1\" : \"VALUE1\" , \"KEY2\" : \"VALUE2\" , }   "
            , {
                { "KEY1", "VALUE1" },
                { "KEY2", "VALUE2" },
            }
            , 43
        )
    );
}

TEST(
    DataElementMapTest
    , Generate_failed
)
{
    const auto  DATA = std::string( "\"STRING1\"" );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementMapUnique = FgDataElementMap::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_EQ( nullptr, elementMapUnique.get() );

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementMapTest
    , Generate_failedException
)
{
    const auto  DATA = std::string( "{ \"KEY\" : \"VALUE\" " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    try {
        FgDataElementMap::generate(
            it
            , FILE_DATA.end()
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementMapTest
    , ToFileData
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    string1Unique = fg::DataElementString::create( "VALUE1" );
    ASSERT_NE( nullptr, string1Unique.get() );

    mapUnique->addDataElement(
        "KEY1"
        , std::move( string1Unique )
    );

    auto    string2Unique = fg::DataElementString::create( "VALUE2" );
    ASSERT_NE( nullptr, string2Unique.get() );

    mapUnique->addDataElement(
        "KEY2"
        , std::move( string2Unique )
    );

    const auto  FILE_DATA = MAP->toFileData();

    ASSERT_STREQ(
        "{\"KEY1\":\"VALUE1\",\"KEY2\":\"VALUE2\"}"
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}
