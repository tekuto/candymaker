﻿#include "fg/util/export.h"
#include "fg/core/dataelement/list.h"
#include "candymaker/core/dataelement/list.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/map/map.h"

#include <utility>
#include <stdexcept>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_LIST_ELEMENT = '[';
    const auto  BEGIN_LIST_ELEMENT_LENGTH = 1;
    const auto  END_LIST_ELEMENT = ']';
    const auto  END_LIST_ELEMENT_LENGTH = 1;

    const auto  SEPARATOR = ',';
    const auto  SEPARATOR_LENGTH = 1;

    template< typename ELEMENT_UNIQUE_T >
    void addDataElement(
        FgDataElementList *     _implPtr
        , ELEMENT_UNIQUE_T &&   _elementUnique
    )
    {
        _implPtr->list.push_back( FgDataElement::create( std::move( _elementUnique ) ) );
    }

    template< typename ELEMENT_T >
    void addDataElement(
        FgDataElementList * _implPtr
        , const ELEMENT_T * _ELEMENT_PTR
    )
    {
        addDataElement(
            _implPtr
            , _ELEMENT_PTR->clone()
        );
    }

    template<
        typename T
        , typename ELEMENT_T
    >
    void addDataElement(
        FgDataElementList * _implPtr
        , const ELEMENT_T * _ELEMENT_PTR
    )
    {
        addDataElement(
            _implPtr
            , typename T::ConstUnique( const_cast< ELEMENT_T * >( _ELEMENT_PTR ) )
        );
    }

    FgDataElementList::List cloneList(
        const FgDataElementList::List & _ORG_LIST
    )
    {
        auto    list = FgDataElementList::List();
        for( const auto & ELEMENT_UNIQUE : _ORG_LIST ) {
            const auto &    ELEMENT = *ELEMENT_UNIQUE;

            list.push_back( ELEMENT->clone() );
        }

        return list;
    }

    auto getBegin(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素の始端文字が出現する前に終端に達している\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        if( *_IT != BEGIN_LIST_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:[で始まっていないためリスト要素ではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + BEGIN_LIST_ELEMENT_LENGTH;
    }

    auto getEndList(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
            throw std::runtime_error( "リスト要素の終端文字が出現する前に終端に達している" );
        }

        if( *_IT != END_LIST_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素の終端文字ではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + END_LIST_ELEMENT_LENGTH;
    }

    auto addDataElement(
        FgDataElementList::List &                       _list
        , const candymaker::FileData::const_iterator &  _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    it = _IT;

        auto    elementUnique = FgDataElement::create(
            it
            , _END
        );

        _list.push_back( std::move( elementUnique ) );

        return it;
    }

    auto getEndSeparator(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
#ifdef  DEBUG
            std::printf( "D:リストのセパレータが出現する前に終端に達している\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        if( *_IT != SEPARATOR ) {
#ifdef  DEBUG
            std::printf( "D:リストのセパレータではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + SEPARATOR_LENGTH;
    }

    auto initDataElements(
        FgDataElementList::List &                       _list
        , const candymaker::FileData::const_iterator &  _BEGIN
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    it = _BEGIN;

        while( 1 ) {
            const auto  BEGIN_ELEMENT = FgDataElement::getNextToken(
                it
                , _END
            );

            {
                const auto  END_LIST = getEndList(
                    BEGIN_ELEMENT
                    , _END
                );
                if( END_LIST != candymaker::FileData::const_iterator() ) {
                    return END_LIST;
                }
            }

            const auto  END_ELEMENT = addDataElement(
                _list
                , BEGIN_ELEMENT
                , _END
            );

            const auto  BEGIN_SEPARATOR = FgDataElement::getNextToken(
                END_ELEMENT
                , _END
            );

            const auto  END_SEPARATOR = getEndSeparator(
                BEGIN_SEPARATOR
                , _END
            );
            if( END_SEPARATOR != candymaker::FileData::const_iterator() ) {
                it = END_SEPARATOR;

                continue;
            }

            {
                const auto  END_LIST = getEndList(
                    BEGIN_SEPARATOR
                    , _END
                );
                if( END_LIST != candymaker::FileData::const_iterator() ) {
                    return END_LIST;
                }
            }

            throw std::runtime_error( "リストの内部要素の後にセパレータかリスト要素の終端が存在しない" );
        }
    }

    bool initList(
        FgDataElementList::List &                       _list
        , candymaker::FileData::const_iterator &        _it
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        const auto  BEGIN = getBegin(
            _it
            , _END
        );
        if( BEGIN == candymaker::FileData::const_iterator() ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素開始位置の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    list = FgDataElementList::List();

        const auto  END_LIST = initDataElements(
            list
            , BEGIN
            , _END
        );

        _list = std::move( list );

        _it = END_LIST;

        return true;
    }
}

FgDataElementList * fgDataElementListCreate(
)
{
    return new FgDataElementList;
}

void fgDataElementListDestroy(
    FgDataElementList * _implPtr
)
{
    delete _implPtr;
}

size_t fgDataElementListGetSize(
    const FgDataElementList *   _IMPL_PTR
)
{
    return _IMPL_PTR->list.size();
}

const FgDataElement * fgDataElementListGetDataElement(
    const FgDataElementList *   _IMPL_PTR
    , size_t                    _index
)
{
    return &**( _IMPL_PTR->list.at( _index ) );
}

const FgDataElementString * fgDataElementListGetDataElementString(
    const FgDataElementList *   _IMPL_PTR
    , size_t                    _index
)
{
    return fgDataElementGetDataElementString(
        fgDataElementListGetDataElement(
            _IMPL_PTR
            , _index
        )
    );
}

const FgDataElementList * fgDataElementListGetDataElementList(
    const FgDataElementList *   _IMPL_PTR
    , size_t                    _index
)
{
    return fgDataElementGetDataElementList(
        fgDataElementListGetDataElement(
            _IMPL_PTR
            , _index
        )
    );
}

const FgDataElementMap * fgDataElementListGetDataElementMap(
    const FgDataElementList *   _IMPL_PTR
    , size_t                    _index
)
{
    return fgDataElementGetDataElementMap(
        fgDataElementListGetDataElement(
            _IMPL_PTR
            , _index
        )
    );
}

void fgDataElementListAddDataElementString(
    FgDataElementList *             _implPtr
    , const FgDataElementString *   _STRING_PTR
)
{
    addDataElement(
        _implPtr
        , _STRING_PTR
    );
}

void fgDataElementListAddMoveDataElementString(
    FgDataElementList *             _implPtr
    , const FgDataElementString *   _STRING_PTR
)
{
    addDataElement< fg::DataElementString >(
        _implPtr
        , _STRING_PTR
    );
}

void fgDataElementListAddDataElementList(
    FgDataElementList *         _implPtr
    , const FgDataElementList * _LIST_PTR
)
{
    addDataElement(
        _implPtr
        , _LIST_PTR
    );
}

void fgDataElementListAddMoveDataElementList(
    FgDataElementList *         _implPtr
    , const FgDataElementList * _LIST_PTR
)
{
    addDataElement< fg::DataElementList >(
        _implPtr
        , _LIST_PTR
    );
}

void fgDataElementListAddDataElementMap(
    FgDataElementList *         _implPtr
    , const FgDataElementMap *  _MAP_PTR
)
{
    addDataElement(
        _implPtr
        , _MAP_PTR
    );
}

void fgDataElementListAddMoveDataElementMap(
    FgDataElementList *         _implPtr
    , const FgDataElementMap *  _MAP_PTR
)
{
    addDataElement< fg::DataElementMap >(
        _implPtr
        , _MAP_PTR
    );
}

FgDataElementList::FgDataElementList(
    const FgDataElementList &   _ORG
)
    : list( cloneList( _ORG.list ) )
{
}

FgDataElementList::FgDataElementList(
    List && _list
)
    : list( std::move( _list ) )
{
}

fg::DataElementList::ConstUnique FgDataElementList::generate(
    candymaker::FileData::const_iterator &          _it
    , const candymaker::FileData::const_iterator &  _END
)
{
    auto    it = _it;

    auto    list = FgDataElementList::List();
    if( initList(
        list
        , it
        , _END
    ) == false ) {
#ifdef  DEBUG
        std::printf( "D:リストの要素初期化に失敗\n" );
#endif  // DEBUG

        return nullptr;
    }

    auto    thisUnique = fg::DataElementList::ConstUnique(
        new FgDataElementList(
            std::move( list )
        )
    );

    _it = std::move( it );

    return thisUnique;
}

candymaker::FileData FgDataElementList::toFileData(
) const
{
    const auto &    LIST = this->list;

    const auto  BEGIN = LIST.begin();
    const auto  END = LIST.end();

    auto    fileData = candymaker::FileData();
    fileData.push_back( BEGIN_LIST_ELEMENT );

    for( auto it = BEGIN ; it != END ; it++ ) {
        if( it != BEGIN ) {
            fileData.push_back( SEPARATOR );
        }

        const auto  LIST_ITEM = ( **it )->toFileData();

        fileData.insert(
            fileData.end()
            , LIST_ITEM.begin()
            , LIST_ITEM.end()
        );
    }

    fileData.push_back( END_LIST_ELEMENT );

    return fileData;
}
