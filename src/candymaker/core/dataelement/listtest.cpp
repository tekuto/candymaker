﻿#include "fg/util/test.h"
#include "fg/core/dataelement/list.h"
#include "candymaker/core/dataelement/list.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/common/filedata.h"

#include <utility>
#include <string>
#include <vector>
#include <cstddef>

TEST(
    DataElementListTest
    , Create
)
{
    const auto  LIST_UNIQUE = fg::DataElementList::create();
    ASSERT_NE( nullptr, LIST_UNIQUE.get() );
    const auto &    LIST = *LIST_UNIQUE;

    ASSERT_TRUE( LIST->list.empty() );
}

TEST(
    DataElementListTest
    , GetSize
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    listUnique->addDataElement( std::move( stringUnique ) );

    ASSERT_EQ( 1, listUnique->getSize() );
}

TEST(
    DataElementListTest
    , GetDataElement
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( std::move( stringUnique ) );

    const auto &    ELEMENT = listUnique->getDataElement( 0 );
    ASSERT_NE( nullptr, &ELEMENT );

    ASSERT_EQ( &STRING, ELEMENT.getDataElementString() );
}

TEST(
    DataElementListTest
    , GetDataElement_failed
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    listUnique->addDataElement( std::move( stringUnique ) );

    try {
        listUnique->getDataElement( 10 );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    DataElementListTest
    , GetDataElementString
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( std::move( stringUnique ) );

    ASSERT_EQ( &STRING, listUnique->getDataElementString( 0 ) );
}

TEST(
    DataElementListTest
    , GetDataElementString_failedOutOfSize
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    listUnique->addDataElement( std::move( stringUnique ) );

    try {
        listUnique->getDataElementString( 10 );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    DataElementListTest
    , GetDataElementString_failedDiffType
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    list2Unique = fg::DataElementList::create();
    ASSERT_NE( nullptr, list2Unique.get() );

    listUnique->addDataElement( std::move( list2Unique ) );

    ASSERT_EQ( nullptr, listUnique->getDataElementString( 0 ) );
}

TEST(
    DataElementListTest
    , GetDataElementList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    list2Unique = fg::DataElementList::create();
    ASSERT_NE( nullptr, list2Unique.get() );
    const auto &    LIST2 = *list2Unique;

    listUnique->addDataElement( std::move( list2Unique ) );

    ASSERT_EQ( &LIST2, listUnique->getDataElementList( 0 ) );
}

TEST(
    DataElementListTest
    , GetDataElementList_failedOutOfSize
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    list2Unique = fg::DataElementList::create();
    ASSERT_NE( nullptr, list2Unique.get() );

    listUnique->addDataElement( std::move( list2Unique ) );

    try {
        listUnique->getDataElementList( 10 );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    DataElementListTest
    , GetDataElementList_failedDiffType
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    listUnique->addDataElement( std::move( stringUnique ) );

    ASSERT_EQ( nullptr, listUnique->getDataElementList( 0 ) );
}

TEST(
    DataElementListTest
    , GetDataElementMap
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    listUnique->addDataElement( std::move( mapUnique ) );

    ASSERT_EQ( &MAP, listUnique->getDataElementMap( 0 ) );
}

TEST(
    DataElementListTest
    , GetDataElementMap_failedOutOfSize
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );

    listUnique->addDataElement( std::move( mapUnique ) );

    try {
        listUnique->getDataElementMap( 10 );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    DataElementListTest
    , GetDataElementMap_failedDiffType
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );

    listUnique->addDataElement( std::move( stringUnique ) );

    ASSERT_EQ( nullptr, listUnique->getDataElementMap( 0 ) );
}

TEST(
    DataElementListTest
    , AddDataElementString
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( STRING );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  STRING_PTR = LIST.getDataElement( 0 ).getDataElementString();
    ASSERT_NE( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , AddMoveDataElementString
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    listUnique->addDataElement( std::move( stringUnique ) );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  STRING_PTR = LIST.getDataElement( 0 ).getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , AddDataElementList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    addListUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, addListUnique.get() );
    const auto &    ADD_LIST = *addListUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    addListUnique->addDataElement( std::move( stringUnique ) );

    listUnique->addDataElement( ADD_LIST );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  LIST_PTR = LIST.getDataElement( 0 ).getDataElementList();
    ASSERT_NE( &ADD_LIST, LIST_PTR );

    ASSERT_EQ( 1, LIST_PTR->getSize() );
    const auto  STRING_PTR = LIST_PTR->getDataElement( 0 ).getDataElementString();
    ASSERT_NE( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , AddMoveDataElementList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    addListUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, addListUnique.get() );
    const auto &    ADD_LIST = *addListUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    addListUnique->addDataElement( std::move( stringUnique ) );

    listUnique->addDataElement( std::move( addListUnique ) );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  LIST_PTR = LIST.getDataElement( 0 ).getDataElementList();
    ASSERT_EQ( &ADD_LIST, LIST_PTR );

    ASSERT_EQ( 1, LIST_PTR->getSize() );
    const auto  STRING_PTR = LIST_PTR->getDataElement( 0 ).getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "STRING", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , AddDataElementMap
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    listUnique->addDataElement( MAP );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  MAP_PTR = LIST.getDataElement( 0 ).getDataElementMap();
    ASSERT_NE( &MAP, MAP_PTR );
    const auto  ELEMENT_PTR = MAP_PTR->getDataElement( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  STRING_PTR = ELEMENT_PTR->getDataElementString();
    ASSERT_NE( &STRING, STRING_PTR );
    ASSERT_STREQ( "VALUE", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , AddMoveDataElementMap
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    mapUnique->addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    listUnique->addDataElement( std::move( mapUnique ) );

    ASSERT_EQ( 1, LIST.getSize() );
    const auto  MAP_PTR = LIST.getDataElement( 0 ).getDataElementMap();
    ASSERT_EQ( &MAP, MAP_PTR );
    const auto  ELEMENT_PTR = MAP_PTR->getDataElement( "KEY" );
    ASSERT_NE( nullptr, ELEMENT_PTR );
    const auto  STRING_PTR = ELEMENT_PTR->getDataElementString();
    ASSERT_EQ( &STRING, STRING_PTR );
    ASSERT_STREQ( "VALUE", STRING_PTR->getString() );
}

TEST(
    DataElementListTest
    , Clone
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    auto &  list = *listUnique;

    auto    stringUnique = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    list.addDataElement( std::move( stringUnique ) );

    const auto  CLONE_UNIQUE = list->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    LIST = CLONE->list;
    ASSERT_EQ( 1, LIST.size() );
    const auto &    STRING_UNIQUE = ( *( LIST[ 0 ] ) )->stringUnique;
    ASSERT_NE( &STRING, STRING_UNIQUE.get() );
    ASSERT_STREQ( "STRING", ( *STRING_UNIQUE )->string.c_str() );
}

void testGenerate(
    const std::string &                     _DATA
    , const std::vector< std::string > &    _LIST
    , std::size_t                           _elementSize
)
{
    const auto  FILE_DATA = candymaker::FileData(
        _DATA.begin()
        , _DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementListUnique = FgDataElementList::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, elementListUnique.get() );
    const auto &    ELEMENT_LIST = *elementListUnique;

    const auto &    LIST = ELEMENT_LIST->list;

    const auto  LIST_SIZE = _LIST.size();
    ASSERT_EQ( LIST_SIZE, LIST.size() );
    for( auto i = std::size_t( 0 ) ; i < LIST_SIZE ; i++ ) {
        ASSERT_STREQ( _LIST[ i ].c_str(), ( *( ( *LIST[ i ] )->stringUnique ) )->string.c_str() );
    }

    ASSERT_EQ( BEGIN + _elementSize, it );
}

TEST(
    DataElementListTest
    , Generate
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "[ \"STRING1\" , \"STRING2\" ]   "
            , {
                "STRING1",
                "STRING2",
            }
            , 25
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "[ \"STRING1\" , \"STRING2\" , \"STRING3\" , ]   "
            , {
                "STRING1",
                "STRING2",
                "STRING3",
            }
            , 39
        )
    );
}

TEST(
    DataElementListTest
    , Generate_failed
)
{
    const auto  DATA = std::string( "\"STRING1\"" );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementListUnique = FgDataElementList::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_EQ( nullptr, elementListUnique.get() );

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementListTest
    , Generate_failedException
)
{
    const auto  DATA = std::string( "[ \"STRING1\",  \"STRING2\"    " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    try {
        FgDataElementList::generate(
            it
            , FILE_DATA.end()
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementListTest
    , ToFileData
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    auto    string1Unique = fg::DataElementString::create( "STRING1" );
    ASSERT_NE( nullptr, string1Unique.get() );

    auto    string2Unique = fg::DataElementString::create( "STRING2" );
    ASSERT_NE( nullptr, string2Unique.get() );

    listUnique->addDataElement( std::move( string1Unique ) );
    listUnique->addDataElement( std::move( string2Unique ) );

    const auto  FILE_DATA = LIST->toFileData();

    ASSERT_STREQ(
        "[\"STRING1\",\"STRING2\"]"
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}
