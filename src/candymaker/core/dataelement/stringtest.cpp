﻿#include "fg/util/test.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/common/filedata.h"

#include <string>
#include <cstddef>

TEST(
    DataElementStringTest
    , Create
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    ASSERT_STREQ( "STRING", STRING->string.c_str() );
}

TEST(
    DataElementStringTest
    , GetString
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );

    ASSERT_STREQ( "STRING", STRING_UNIQUE->getString() );
}

TEST(
    DataElementStringTest
    , Clone
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  CLONE_UNIQUE = STRING->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    ASSERT_STREQ( "STRING", CLONE->string.c_str() );
}

void testGenerate(
    const std::string &     _DATA
    , const std::string &   _STRING
    , std::size_t           _elementSize
)
{
    const auto  FILE_DATA = candymaker::FileData(
        _DATA.begin()
        , _DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    const auto  STRING_UNIQUE = FgDataElementString::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    ASSERT_STREQ( _STRING.c_str(), STRING->string.c_str() );

    ASSERT_EQ( BEGIN + _elementSize, it );
}

TEST(
    DataElementStringTest
    , Generate
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "\"STRING\" "
            , "STRING"
            , 8
        )
    );
}

TEST(
    DataElementStringTest
    , Generate_escapedString
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "\"\\\"STRING\\\"\"   "
            , "\"STRING\""
            , 12
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGenerate(
            "\"\\\\1,234,567\"    "
            , "\\1,234,567"
            , 13
        )
    );
}

TEST(
    DataElementStringTest
    , Generate_failed
)
{
    const auto  DATA = std::string( "[]" );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    const auto  STRING_UNIQUE = FgDataElementString::generate(
        it
        , FILE_DATA.end()
    );
    ASSERT_EQ( nullptr, STRING_UNIQUE.get() );

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementStringTest
    , Generate_failedException
)
{
    const auto  DATA = std::string( "\"FAILED" );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    try {
        FgDataElementString::generate(
            it
            , FILE_DATA.end()
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    ASSERT_EQ( BEGIN, it );
}

TEST(
    DataElementStringTest
    , ToFileData
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "STRING" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  FILE_DATA = STRING->toFileData();

    ASSERT_STREQ(
        "\"STRING\""
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}

TEST(
    DataElementStringTest
    , ToFileData_escape
)
{
    const auto  STRING_UNIQUE = fg::DataElementString::create( "\"\\" );
    ASSERT_NE( nullptr, STRING_UNIQUE.get() );
    const auto &    STRING = *STRING_UNIQUE;

    const auto  FILE_DATA = STRING->toFileData();

    ASSERT_STREQ(
        "\"\\\"\\\\\""
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}

TEST(
    DataElementStringTest
    , StringToFileData
)
{
    const auto  FILE_DATA = FgDataElementString::stringToFileData( "STRING" );

    const auto  STRING = std::string(
        FILE_DATA.begin()
        , FILE_DATA.end()
    );
    ASSERT_STREQ( "\"STRING\"", STRING.c_str() );
}

TEST(
    DataElementStringTest
    , StringToFileData_escape
)
{
    const auto  FILE_DATA = FgDataElementString::stringToFileData( "\"\\" );

    const auto  STRING = std::string(
        FILE_DATA.begin()
        , FILE_DATA.end()
    );
    ASSERT_STREQ( "\"\\\"\\\\\"", STRING.c_str() );
}
