﻿#include "fg/util/test.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/list.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/common/filedata.h"

#include <utility>
#include <string>

TEST(
    DataElementTest
    , GetDataElementString
)
{
    auto    stringUnique = fg::DataElementString::create( "DataElementTest" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( &STRING, ELEMENT_UNIQUE->getDataElementString() );
}

TEST(
    DataElementTest
    , GetDataElementString_failed
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( listUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( nullptr, ELEMENT_UNIQUE->getDataElementString() );
}

TEST(
    DataElementTest
    , GetDataElementList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( listUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( &LIST, ELEMENT_UNIQUE->getDataElementList() );
}

TEST(
    DataElementTest
    , GetDataElementList_failed
)
{
    auto    stringUnique = fg::DataElementString::create( "DataElementTest" );
    ASSERT_NE( nullptr, stringUnique.get() );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( nullptr, ELEMENT_UNIQUE->getDataElementList() );
}

TEST(
    DataElementTest
    , GetDataElementMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( mapUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( &MAP, ELEMENT_UNIQUE->getDataElementMap() );
}

TEST(
    DataElementTest
    , GetDataElementMap_failed
)
{
    auto    stringUnique = fg::DataElementString::create( "DataElementTest" );
    ASSERT_NE( nullptr, stringUnique.get() );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );

    ASSERT_EQ( nullptr, ELEMENT_UNIQUE->getDataElementMap() );
}

TEST(
    DataElementTest
    , CreateElementStringUnique
)
{
    auto    stringUnique = fg::DataElementString::create( "DataElementTest" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( &STRING, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CreateElementListUnique
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    const auto &    LIST = *listUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( listUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( &LIST, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CreateElementMapUnique
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    const auto &    MAP = *mapUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( mapUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( &MAP, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CloneString
)
{
    auto    stringUnique = fg::DataElementString::create( "DataElementTest" );
    ASSERT_NE( nullptr, stringUnique.get() );
    const auto &    STRING = *stringUnique;

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( stringUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  CLONE_UNIQUE = ELEMENT->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    CLONE_STRING_UNIQUE = CLONE->stringUnique;
    ASSERT_NE( &STRING, CLONE_STRING_UNIQUE.get() );
    ASSERT_STREQ( "DataElementTest", CLONE_STRING_UNIQUE->getString() );
    ASSERT_EQ( nullptr, CLONE->listUnique.get() );
    ASSERT_EQ( nullptr, CLONE->mapUnique.get() );
}

TEST(
    DataElementTest
    , CloneList
)
{
    auto    listUnique = fg::DataElementList::create();
    ASSERT_NE( nullptr, listUnique.get() );
    auto &  list = *listUnique;

    auto    string1Unique = fg::DataElementString::create( "STRING1" );
    ASSERT_NE( nullptr, string1Unique.get() );

    auto    string2Unique = fg::DataElementString::create( "STRING2" );
    ASSERT_NE( nullptr, string2Unique.get() );

    list.addDataElement( std::move( string1Unique ) );
    list.addDataElement( std::move( string2Unique ) );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( listUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  CLONE_UNIQUE = ELEMENT->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    ASSERT_EQ( nullptr, CLONE->stringUnique.get() );
    const auto &    CLONE_LIST_UNIQUE = CLONE->listUnique;
    ASSERT_NE( &list, CLONE_LIST_UNIQUE.get() );
    const auto &    LIST = ( *CLONE_LIST_UNIQUE )->list;
    ASSERT_EQ( 2, LIST.size() );
    ASSERT_STREQ( "STRING1", ( *( ( *( LIST[ 0 ] ) )->stringUnique ) )->string.c_str() );
    ASSERT_STREQ( "STRING2", ( *( ( *( LIST[ 1 ] ) )->stringUnique ) )->string.c_str() );
    ASSERT_EQ( nullptr, CLONE->mapUnique.get() );
}

TEST(
    DataElementTest
    , CloneMap
)
{
    auto    mapUnique = fg::DataElementMap::create();
    ASSERT_NE( nullptr, mapUnique.get() );
    auto &  map = *mapUnique;

    auto    stringUnique = fg::DataElementString::create( "VALUE" );
    ASSERT_NE( nullptr, stringUnique.get() );

    map.addDataElement(
        "KEY"
        , std::move( stringUnique )
    );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( std::move( mapUnique ) );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  CLONE_UNIQUE = ELEMENT->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    ASSERT_EQ( nullptr, CLONE->stringUnique.get() );
    ASSERT_EQ( nullptr, CLONE->listUnique.get() );
    const auto &    CLONE_MAP_UNIQUE = CLONE->mapUnique;
    ASSERT_NE( &map, CLONE_MAP_UNIQUE.get() );
    const auto &    MAP = ( *CLONE_MAP_UNIQUE )->map;
    ASSERT_EQ( 1, MAP.size() );
    const auto  IT = MAP.find( "KEY" );
    ASSERT_NE( MAP.end(), IT );
    ASSERT_STREQ( "VALUE", ( *( ( *( IT->second ) )->stringUnique ) )->string.c_str() );
}

TEST(
    DataElementTest
    , CreateFileData_string
)
{
    const auto  DATA = std::string( "   \"STRING\"   " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( FILE_DATA );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_NE( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CreateFileData_list
)
{
    const auto  DATA = std::string( " [ [], [], ] " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( FILE_DATA );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CreateFileData_map
)
{
    const auto  DATA = std::string( " { \"KEY\" : {}, } " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  ELEMENT_UNIQUE = FgDataElement::create( FILE_DATA );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->mapUnique.get() );
}

TEST(
    DataElementTest
    , CreateIterator_string
)
{
    const auto  DATA = std::string( "\"STRING\"   " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementUnique = FgDataElement::create(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, elementUnique.get() );
    const auto &    ELEMENT = *elementUnique;

    ASSERT_NE( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );

    ASSERT_EQ( BEGIN + 8, it );
}

TEST(
    DataElementTest
    , CreateIterator_list
)
{
    const auto  DATA = std::string( "[ [], [], ] " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementUnique = FgDataElement::create(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, elementUnique.get() );
    const auto &    ELEMENT = *elementUnique;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->listUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->mapUnique.get() );

    ASSERT_EQ( BEGIN + 11, it );
}

TEST(
    DataElementTest
    , CreateIterator_map
)
{
    const auto  DATA = std::string( "{ \"KEY\" : {}, } " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    auto    it = BEGIN;

    auto    elementUnique = FgDataElement::create(
        it
        , FILE_DATA.end()
    );
    ASSERT_NE( nullptr, elementUnique.get() );
    const auto &    ELEMENT = *elementUnique;

    ASSERT_EQ( nullptr, ELEMENT->stringUnique.get() );
    ASSERT_EQ( nullptr, ELEMENT->listUnique.get() );
    ASSERT_NE( nullptr, ELEMENT->mapUnique.get() );

    ASSERT_EQ( BEGIN + 15, it );
}

TEST(
    DataElementTest
    , GetNextToken
)
{
    const auto  DATA = std::string( "   \"STRING\"" );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    const auto  BEGIN = FILE_DATA.begin();

    ASSERT_EQ(
        BEGIN + 3
        , FgDataElement::getNextToken(
            BEGIN
            , FILE_DATA.end()
        )
    );
}

TEST(
    DataElementTest
    , GetNextToken_failed
)
{
    const auto  DATA = std::string( "    " );

    const auto  FILE_DATA = candymaker::FileData(
        DATA.begin()
        , DATA.end()
    );

    try {
        FgDataElement::getNextToken(
            FILE_DATA.begin()
            , FILE_DATA.end()
        );

        ASSERT_FALSE( true );  // ここには到達しない
    } catch( ... ) {}
}

TEST(
    DataElementTest
    , ToFileDataFromString
)
{
    const auto  DATA = std::string( "   \"STRING\"   " );

    const auto  ELEMENT_UNIQUE = FgDataElement::create(
        candymaker::FileData(
            DATA.begin()
            , DATA.end()
        )
    );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  FILE_DATA = ELEMENT->toFileData();

    ASSERT_STREQ(
        "\"STRING\""
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}

TEST(
    DataElementTest
    , ToFileDataFromList
)
{
    const auto  DATA = std::string( "[ [], [], ] " );

    const auto  ELEMENT_UNIQUE = FgDataElement::create(
        candymaker::FileData(
            DATA.begin()
            , DATA.end()
        )
    );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  FILE_DATA = ELEMENT->toFileData();

    ASSERT_STREQ(
        "[[],[]]"
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}

TEST(
    DataElementTest
    , ToFileDataFromMap
)
{
    const auto  DATA = std::string( "{ \"KEY1\" : {} , \"KEY2\" : {} , } " );

    const auto  ELEMENT_UNIQUE = FgDataElement::create(
        candymaker::FileData(
            DATA.begin()
            , DATA.end()
        )
    );
    ASSERT_NE( nullptr, ELEMENT_UNIQUE.get() );
    const auto &    ELEMENT = *ELEMENT_UNIQUE;

    const auto  FILE_DATA = ELEMENT->toFileData();

    ASSERT_STREQ(
        "{\"KEY1\":{},\"KEY2\":{}}"
        , std::string(
            FILE_DATA.begin()
            , FILE_DATA.end()
        ).c_str()
    );
}
