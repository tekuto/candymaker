﻿#include "fg/util/export.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/dataelement.h"

#include <string>
#include <algorithm>
#include <utility>
#include <stdexcept>
#include <cstddef>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_STRING_ELEMENT = '"';
    const auto  BEGIN_STRING_ELEMENT_LENGTH = 1;
    const auto  END_STRING_ELEMENT = '"';
    const auto  END_STRING_ELEMENT_LENGTH = 1;

    const auto  ESCAPE = '\\';

    const auto  ESCAPE_CHARACTERS = std::string( "\\\"" );
    const auto  UNESCAPE_CHARACTERS = std::string( "\\\"" );

    auto getBegin(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
#ifdef  DEBUG
            std::printf( "D:文字列要素の始端文字が出現する前に終端に達している\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        if( *_IT != BEGIN_STRING_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:\"で始まっていないため文字列要素ではない\n" );
#endif  // DEBUG

            return candymaker::FileData::const_iterator();
        }

        return _IT + BEGIN_STRING_ELEMENT_LENGTH;
    }

    auto getEnd(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    escaped = false;

        const auto  IT = std::find_if(
            _IT
            , _END
            , [
                &escaped
            ]
            (
                char    _c
            )
            {
                if( escaped == false ) {
                    if( _c == END_STRING_ELEMENT ) {
                        return true;
                    } else if( _c == ESCAPE ) {
                        escaped = true;
                    }
                } else {
                    escaped = false;
                }

                return false;
            }
        );

        if( IT == _END ) {
            throw std::runtime_error( "文字列要素の終端文字が存在しない" );
        }

        return IT;
    }

    auto getEndElement(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        if( _IT >= _END ) {
            throw std::runtime_error( "既にデータの終端に達している" );
        }

        return _IT + END_STRING_ELEMENT_LENGTH;
    }

    char getUnescaped(
        char    _escaped
    )
    {
        const auto  POSITION = ESCAPE_CHARACTERS.find_first_of( _escaped );
        if( POSITION == std::string::npos ) {
            throw std::runtime_error( "エスケープ対象の文字ではない" );
        }

        return UNESCAPE_CHARACTERS.at( POSITION );
    }

    std::string getUnescapeString(
        const std::string & _STRING
    )
    {
        auto    unescapedString = std::string();

        const auto  SIZE = _STRING.size();

        auto    beforePosition = std::size_t( 0 );
        while( 1 ) {
            const auto  POSITION = _STRING.find(
                ESCAPE
                , beforePosition
            );
            if( POSITION == std::string::npos ) {
                break;
            }

            const auto  UNESCAPED = getUnescaped( _STRING.at( POSITION + 1 ) );

            unescapedString.append(
                _STRING
                , beforePosition
                , POSITION - beforePosition
            );
            unescapedString.push_back( UNESCAPED );

            beforePosition = POSITION + 2;
        }

        unescapedString.append(
            _STRING
            , beforePosition
            , SIZE - beforePosition
        );

        return unescapedString;
    }

    bool initString(
        std::string &                                   _string
        , candymaker::FileData::const_iterator &        _it
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        const auto  BEGIN = getBegin(
            _it
            , _END
        );
        if( BEGIN == candymaker::FileData::const_iterator() ) {
#ifdef  DEBUG
            std::printf( "D:文字列開始位置の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        const auto  END = getEnd(
            BEGIN
            , _END
        );

        const auto  END_ELEMENT = getEndElement(
            END
            , _END
        );

        const auto  STRING = std::string(
            BEGIN
            , END
        );

        auto    unescapedString = getUnescapeString( STRING );

        _string = std::move( unescapedString );

        _it = END_ELEMENT;

        return true;
    }

    char getEscaped(
        char    _unescaped
    )
    {
        return ESCAPE_CHARACTERS.at( UNESCAPE_CHARACTERS.find_first_of( _unescaped ) );
    }

    std::string escape(
        const std::string & _STRING
    )
    {
        auto    escapedString = std::string();

        const auto  SIZE = _STRING.size();

        auto    beforePosition = std::size_t( 0 );
        while( 1 ) {
            const auto  POSITION = _STRING.find_first_of(
                ESCAPE_CHARACTERS
                , beforePosition
            );
            if( POSITION == std::string::npos ) {
                break;
            }

            auto    escaped = getEscaped( _STRING.at( POSITION ) );

            escapedString.append(
                _STRING
                , beforePosition
                , POSITION - beforePosition
            );
            escapedString.push_back( ESCAPE );
            escapedString.push_back( escaped );

            beforePosition = POSITION + 1;
        }

        escapedString.append(
            _STRING
            , beforePosition
            , SIZE - beforePosition
        );

        return escapedString;
    }
}

FgDataElementString * fgDataElementStringCreate(
    const char *    _STRING
)
{
    return new FgDataElementString{
        _STRING,
    };
}

void fgDataElementStringDestroy(
    FgDataElementString *   _implPtr
)
{
    delete _implPtr;
}

const char * fgDataElementStringGetString(
    const FgDataElementString * _IMPL_PTR
)
{
    return _IMPL_PTR->string.c_str();
}

fg::DataElementString::ConstUnique FgDataElementString::generate(
    candymaker::FileData::const_iterator &          _it
    , const candymaker::FileData::const_iterator &  _END
)
{
    auto    it = _it;

    auto    string = std::string();
    if( initString(
        string
        , it
        , _END
    ) == false ) {
#ifdef  DEBUG
        std::printf( "D:文字列の初期化に失敗\n" );
#endif  // DEBUG

        return nullptr;
    }

    auto    thisUnique = fg::DataElementString::ConstUnique(
        new FgDataElementString{
            std::move( string ),
        }
    );

    _it = std::move( it );

    return thisUnique;
}

candymaker::FileData FgDataElementString::toFileData(
) const
{
    return stringToFileData( this->string );
}

candymaker::FileData FgDataElementString::stringToFileData(
    const std::string & _STRING
)
{
    auto    escapedString = escape( _STRING );

    auto    fileData = candymaker::FileData();
    fileData.push_back( BEGIN_STRING_ELEMENT );
    fileData.insert(
        fileData.end()
        , escapedString.begin()
        , escapedString.end()
    );
    fileData.push_back( END_STRING_ELEMENT );

    return fileData;
}
