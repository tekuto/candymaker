﻿#include "fg/util/export.h"
#include "candymaker/core/dataelement/dataelement.h"
#include "candymaker/core/dataelement/string.h"
#include "candymaker/core/dataelement/list.h"
#include "candymaker/core/dataelement/map/map.h"
#include "candymaker/core/common/filedata.h"

#include <string>
#include <utility>
#include <cctype>
#include <stdexcept>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    template< typename UNIQUE_T >
    UNIQUE_T clone_(
        const UNIQUE_T &    _UNIQUE
    )
    {
        if( _UNIQUE.get() == nullptr ) {
            return nullptr;
        }

        return ( *_UNIQUE )->clone();
    }

    auto findNextToken(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        auto    it = _IT;

        while( it < _END ) {
            if( std::isspace( *it ) == 0 ) {
                break;
            }

            it++;
        }

        return it;
    }

    bool isExistsNextToken(
        const candymaker::FileData::const_iterator &    _IT
        , const candymaker::FileData::const_iterator &  _END
    )
    {
        return findNextToken(
            _IT
            , _END
        ) != _END;
    }
}

const FgDataElementString * fgDataElementGetDataElementString(
    const FgDataElement *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->stringUnique );
}

const FgDataElementList * fgDataElementGetDataElementList(
    const FgDataElement *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->listUnique );
}

const FgDataElementMap * fgDataElementGetDataElementMap(
    const FgDataElement *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->mapUnique );
}

FgDataElement::FgDataElement(
    fg::DataElementString::ConstUnique &&   _stringUnique
)
    : stringUnique( std::move( _stringUnique ) )
{
}

FgDataElement::FgDataElement(
    fg::DataElementList::ConstUnique && _listUnique
)
    : listUnique( std::move( _listUnique ) )
{
}

FgDataElement::FgDataElement(
    fg::DataElementMap::ConstUnique &&  _mapUnique
)
    : mapUnique( std::move( _mapUnique ) )
{
}

FgDataElement::FgDataElement(
    const FgDataElement &   _ORG
)
    : stringUnique( ::clone_( _ORG.stringUnique ) )
    , listUnique( ::clone_( _ORG.listUnique ) )
    , mapUnique( ::clone_( _ORG.mapUnique ) )
{
}

fg::DataElement::ConstUnique FgDataElement::create(
    const candymaker::FileData &    _FILE_DATA
)
{
    const auto  END = _FILE_DATA.end();

    auto    it = FgDataElement::getNextToken(
        _FILE_DATA.begin()
        , END
    );

    auto    thisUnique = FgDataElement::create(
        it
        , END
    );

    if( isExistsNextToken(
        it
        , END
    ) == true ) {
        throw std::runtime_error( "要素終了位置よりも後に字句が存在する" );
    }

    return thisUnique;
}

fg::DataElement::ConstUnique FgDataElement::create(
    candymaker::FileData::const_iterator &          _it
    , const candymaker::FileData::const_iterator &  _END
)
{
    auto    stringUnique = FgDataElementString::generate(
        _it
        , _END
    );
    if( stringUnique.get() != nullptr ) {
        return FgDataElement::create( std::move( stringUnique ) );
    }

    auto    listUnique = FgDataElementList::generate(
        _it
        , _END
    );
    if( listUnique.get() != nullptr ) {
        return FgDataElement::create( std::move( listUnique ) );
    }

    auto    mapUnique = FgDataElementMap::generate(
        _it
        , _END
    );
    if( mapUnique.get() != nullptr ) {
        return FgDataElement::create( std::move( mapUnique ) );
    }

    throw std::runtime_error( "データ要素の生成に失敗" );
}

candymaker::FileData::const_iterator FgDataElement::getNextToken(
    const candymaker::FileData::const_iterator &    _IT
    , const candymaker::FileData::const_iterator &  _END
)
{
    const auto  IT = findNextToken(
        _IT
        , _END
    );
    if( IT == _END ) {
        throw std::runtime_error( "次の字句が存在しない" );
    }

    return IT;
}

candymaker::FileData FgDataElement::toFileData(
) const
{
    const auto &    STRING_UNIQUE = this->stringUnique;
    if( STRING_UNIQUE.get() != nullptr ) {
        return ( *STRING_UNIQUE )->toFileData();
    }

    const auto &    LIST_UNIQUE = this->listUnique;
    if( LIST_UNIQUE.get() != nullptr ) {
        return ( *LIST_UNIQUE )->toFileData();
    }

    const auto &    MAP_UNIQUE = this->mapUnique;
    if( MAP_UNIQUE.get() != nullptr ) {
        return ( *MAP_UNIQUE )->toFileData();
    }

    throw std::runtime_error( "非対応の要素" );
}
