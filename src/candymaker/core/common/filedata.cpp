﻿#include "candymaker/core/common/filedata.h"

#include <array>
#include <memory>
#include <cstdio>
#include <tuple>

namespace {
    enum {
        BUFFER_SIZE = 1024,
    };

    using Buffer = std::array<
        char
        , BUFFER_SIZE
    >;

    struct CloseFile
    {
        void operator()(
            std::FILE * _filePtr
        ) const
        {
            std::fclose( _filePtr );
        }
    };

    using FileUnique = std::unique_ptr<
        std::FILE
        , CloseFile
    >;

    bool readFileData(
        candymaker::FileData &  _fileData
        , std::FILE &           _file
    )
    {
        auto    fileData = candymaker::FileData();
        while( std::feof( &_file ) == 0 ) {
            auto    buffer = Buffer();

            const auto  READ_SIZE = std::fread(
                buffer.data()
                , 1
                , std::tuple_size< decltype( buffer ) >::value
                , &_file
            );
            if( std::ferror( &_file ) != 0 ) {
#ifdef  DEBUG
                std::printf( "D:fread()が失敗\n" );
#endif  // DEBUG

                return false;
            }

            auto    it = buffer.begin();

            fileData.insert(
                fileData.end()
                , it
                , it + READ_SIZE
            );
        }

        _fileData = std::move( fileData );

        return true;
    }

    bool writeFileData(
        std::FILE &                     _file
        , const candymaker::FileData &  _FILE_DATA
    )
    {
        std::fwrite(
            _FILE_DATA.data()
            , _FILE_DATA.size()
            , 1
            , &_file
        );
        if( std::ferror( &_file ) != 0 ) {
#ifdef  DEBUG
            std::printf( "D:fwrite()が失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

namespace candymaker {
    bool readFile(
        FileData &              _fileData
        , const std::string &   _FILE_PATH
    )
    {
        auto    fileUnique = FileUnique(
            fopen64(
                _FILE_PATH.c_str()
                , "rb"
            )
        );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:fopen64()が失敗\n" );
#endif  // DEBUG

            return false;
        }

        return readFileData(
            _fileData
            , *fileUnique
        );
    }

    bool writeFile(
        const std::string & _FILE_PATH
        , const FileData &  _FILE_DATA
    )
    {
        auto    fileUnique = FileUnique(
            fopen64(
                _FILE_PATH.c_str()
                , "wb"
            )
        );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:fopen64()が失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  file = *fileUnique;

        return writeFileData(
            file
            , _FILE_DATA
        );
    }
}
