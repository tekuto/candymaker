﻿#include "fg/util/test.h"
#include "candymaker/core/common/filedata.h"

#include <string>

TEST(
    FileDataTest
    , ReadFile
)
{
    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "filedatatest/readdatatest.txt"
        )
    );

    auto    fileDataString = std::string(
        fileData.begin()
        , fileData.end()
    );

    ASSERT_STREQ( "READDATA\n", fileDataString.c_str() );
}

TEST(
    FileDataTest
    , ReadFile_failedWhenFileNotExists
)
{
    auto    fileData = candymaker::FileData();
    ASSERT_FALSE(
        candymaker::readFile(
            fileData
            , "filedatatest/notfound.txt"
        )
    );

    ASSERT_EQ( 0, fileData.size() );
}

TEST(
    FileDataTest
    , WriteFile
)
{
    const auto  WRITE_DATA = std::string( "WRITEDATA" );

    const auto  FILE_DATA = candymaker::FileData(
        WRITE_DATA.begin()
        , WRITE_DATA.end()
    );

    ASSERT_TRUE(
        candymaker::writeFile(
            "filedatatest/writefiletest"
            , FILE_DATA
        )
    );

    auto    fileData = candymaker::FileData();
    ASSERT_TRUE(
        candymaker::readFile(
            fileData
            , "filedatatest/writefiletest"
        )
    );

    auto    fileDataString = std::string(
        fileData.begin()
        , fileData.end()
    );

    ASSERT_STREQ( "WRITEDATA", fileDataString.c_str() );
}

TEST(
    FileDataTest
    , WriteFile_failedWhenDirectoryNotExists
)
{
    const auto  WRITE_DATA = std::string( "WRITEDATA" );

    const auto  FILE_DATA = candymaker::FileData(
        WRITE_DATA.begin()
        , WRITE_DATA.end()
    );

    ASSERT_FALSE(
        candymaker::writeFile(
            "filedatatest/notfound/notfound"
            , FILE_DATA
        )
    );

    auto    fileData = candymaker::FileData();
    ASSERT_FALSE(
        candymaker::readFile(
            fileData
            , "filedatatest/notfound/notfound"
        )
    );
}
