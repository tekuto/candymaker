﻿#include "fg/util/test.h"
#include "candymaker/core/thread/thread.h"
#include "candymaker/core/thread/joiner.h"

#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>

struct ThreadImplTest
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended;
    bool    enabled;

    candymaker::ThreadProcImpl      procPtr;
    void *                          userDataPtr;
    candymaker::ThreadJoiner::Ender procEnder;

    std::thread thread;

    std::unique_ptr< std::thread >      joiner;
    std::unique_ptr< ThreadImplTest >   ender;
};

void createTestProc(
    candymaker::ThreadImpl &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
}

TEST(
    ThreadImplTest
    , Create
)
{
    auto    i = 10;

    auto    joinerUnique = candymaker::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    threadUnique = candymaker::ThreadImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    const auto &    THREAD = reinterpret_cast< const ThreadImplTest & >( *threadUnique );

    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( createTestProc, THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );
}

void resetTestProc(
    candymaker::ThreadImpl &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
}

TEST(
    ThreadImplTest
    , Reset
)
{
    auto    i = 10;

    auto    joinerUnique = candymaker::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    auto    joiner2Unique = candymaker::ThreadJoiner::create();
    ASSERT_NE( nullptr, joiner2Unique.get() );
    auto &  joiner2 = *joiner2Unique;

    auto    threadUnique = candymaker::ThreadImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;
    const auto &    THREAD = reinterpret_cast< const ThreadImplTest & >( thread );

    ASSERT_FALSE(
        thread.reset(
            resetTestProc
            , &j
            , joiner2
        )
    );

    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( createTestProc, THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner, THREAD.procEnder.get() );

    joiner.join();

    ASSERT_TRUE(
        thread.reset(
            resetTestProc
            , &j
            , joiner2
        )
    );

    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( resetTestProc, THREAD.procPtr );
    ASSERT_EQ( 20, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner2, THREAD.procEnder.get() );
}

TEST(
    ThreadImplTest
    , GetData
)
{
    auto    i = 10;

    auto    joinerUnique = candymaker::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    threadUnique = candymaker::ThreadImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;

    ASSERT_NE( nullptr, thread.getData() );
    ASSERT_EQ( 10, reinterpret_cast< candymaker::Thread< int > & >( thread ).getData() );
}
