﻿#include "fg/util/test.h"
#include "candymaker/core/module/contexts.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/module/info.h"
#include "candymaker/core/module/infouniques.h"

#include <utility>

TEST(
    ModuleContextsTest
    , Create
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libnewtest.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfoUnique ) );

    const auto  MODULE_CONTEXTS_UNIQUE = candymaker::ModuleContexts::create(
        moduleManager
        , moduleInfoUniques
    );
    ASSERT_NE( nullptr, MODULE_CONTEXTS_UNIQUE.get() );

    ASSERT_EQ( &moduleManager, &( MODULE_CONTEXTS_UNIQUE->manager ) );
    ASSERT_NE( nullptr, &( MODULE_CONTEXTS_UNIQUE->manager ) );
    ASSERT_EQ( 1, MODULE_CONTEXTS_UNIQUE->contextShares.size() );
    ASSERT_EQ( MODULE_CONTEXTS_UNIQUE.get(), MODULE_CONTEXTS_UNIQUE->unloader.get() );
}

TEST(
    ModuleContextsTest
    , Destroy
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libdeletetest.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfoUnique ) );

    auto    moduleContextsUnique = candymaker::ModuleContexts::create(
        moduleManager
        , moduleInfoUniques
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto  PROC_PTR = moduleContextsUnique->getProc< void ( * )( int & ) >( "_ZN10candymaker7setDataERi" );

    auto    i = 0;
    PROC_PTR( i );

    moduleContextsUnique.destroy();

    ASSERT_EQ( 10, i );
}

TEST(
    ModuleContextsTest
    , GetMainModuleContext
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libnewtest.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfoUnique ) );

    const auto  MODULE_CONTEXTS_UNIQUE = candymaker::ModuleContexts::create(
        moduleManager
        , moduleInfoUniques
    );
    ASSERT_NE( nullptr, MODULE_CONTEXTS_UNIQUE.get() );

    ASSERT_EQ( MODULE_CONTEXTS_UNIQUE->contextShares[ 0 ].get(), &( MODULE_CONTEXTS_UNIQUE->getMainModuleContext() ) );
}

TEST(
    ModuleContextsTest
    , GetProc
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libgetproctest1.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfoUnique ) );

    auto    moduleContextsUnique = candymaker::ModuleContexts::create(
        moduleManager
        , moduleInfoUniques
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    const auto  PROC_PTR = moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );

    ASSERT_EQ( 10, PROC_PTR() );
}

TEST(
    ModuleContextsTest
    , GetProc_failed
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );
    auto &  moduleManager = *moduleManagerUnique;

    auto    moduleInfo1Unique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libgetproctest1.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfo1Unique.get() );

    auto    moduleInfo2Unique = candymaker::ModuleInfo::create(
        "modulecontextstest"
        , "modulecontextstest/libgetproctest2.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfo2Unique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfo1Unique ) );
    moduleInfoUniques.push_back( std::move( moduleInfo2Unique ) );

    auto    moduleContextsUnique = candymaker::ModuleContexts::create(
        moduleManager
        , moduleInfoUniques
    );
    ASSERT_NE( nullptr, moduleContextsUnique.get() );

    try {
        moduleContextsUnique->getProc< int ( * )() >( "_ZN10candymaker8getData2Ev" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
