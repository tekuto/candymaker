﻿#include "candymaker/core/module/contexts.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/module/manager.h"

#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace candymaker {
    void ModuleContexts::UnloadModuleContextShares::operator()(
        ModuleContexts *    _moduleContextsPtr
    ) const
    {
        _moduleContextsPtr->manager.unloadModules( _moduleContextsPtr->contextShares );
    }

    ModuleContexts::ModuleContexts(
        ModuleManager & _manager
    )
        : manager( _manager )
    {
    }

    ModuleContexts::Unique ModuleContexts::create(
        ModuleManager &             _manager
        , const ModuleInfoUniques & _INFO_UNIQUES
    )
    {
        auto    thisUnique = Unique( new ModuleContexts( _manager ) );
        auto &  contexts = *thisUnique;

        auto    contextShares = ModuleContextShares();
        contexts.manager.loadModules(
            contextShares
            , _INFO_UNIQUES
        );

        contexts.contextShares = std::move( contextShares );
        contexts.unloader.reset( &contexts );

        return thisUnique;
    }

    const FgModuleContext & ModuleContexts::getMainModuleContext(
    ) const
    {
        return const_cast< ModuleContexts * >( this )->getMainModuleContext();
    }

    FgModuleContext & ModuleContexts::getMainModuleContext(
    )
    {
        return *( this->contextShares[ 0 ] );
    }

    void * ModuleContexts::getProc_(
        const std::string & _SYMBOL
    )
    {
        return this->getMainModuleContext().getProc_( _SYMBOL );
    }
}
