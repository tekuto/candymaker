﻿#include "fg/util/test.h"
#include "fg/core/module/context.h"
#include "candymaker/core/module/context.h"

#include <memory>

TEST(
    ModuleContextTest
    , Create
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );
    const auto &    MODULE_CONTEXT = *MODULE_CONTEXT_SHARED;

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_CONTEXT.PACKAGE_PATH.c_str() );
    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_CONTEXT.HOME_DIRECTORY_PATH.c_str() );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", MODULE_CONTEXT.SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_NE( nullptr, MODULE_CONTEXT.handleUnique.get() );
}

TEST(
    ModuleContextTest
    , Initialize
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libinitializetest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );

    moduleContextShared->initialize( "_ZN10candymaker10initializeERN2fg13ModuleContextE" );

    const auto  PROC_PTR = moduleContextShared->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 10, PROC_PTR() );
}

TEST(
    ModuleContextTest
    , Initialize_failed
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libinitializetest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );

    try {
        moduleContextShared->initialize( "notfound" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    ModuleContextTest
    , Finalize
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libfinalizetest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );

    moduleContextShared->initialize( "_ZN10candymaker10initializeERN2fg13ModuleContextE" );

    moduleContextShared->finalize();

    const auto  PROC_PTR = moduleContextShared->getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 10, PROC_PTR() );
}

TEST(
    ModuleContextTest
    , GetPackagePath
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_CONTEXT_SHARED->getPackagePath().c_str() );
}

TEST(
    ModuleContextTest
    , GetHomeDirectoryPath
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_CONTEXT_SHARED->getHomeDirectoryPath().c_str() );
}

TEST(
    ModuleContextTest
    , GetSaveDirectoryPath
)
{
    const auto  MODULE_CONTEXT_SHARED = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_CONTEXT_SHARED.get() );

    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", MODULE_CONTEXT_SHARED->getSaveDirectoryPath().c_str() );
}

TEST(
    ModuleContextTest
    , GetProc
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libgetproctest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );

    const auto  PROC_PTR = moduleContextShared->getProc< void ( * )( int & ) >( "_ZN10candymaker4procERi" );

    auto    i = 0;
    PROC_PTR( i );

    ASSERT_EQ( 10, i );
}

TEST(
    ModuleContextTest
    , GetProc_failed
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libgetproctest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );

    try {
        moduleContextShared->getProc< void ( * )( int & ) >( "notfound" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

struct GetDataTestData
{
};

void freeGetDataTestData(
    GetDataTestData *   _this
)
{
    delete _this;
}

struct FreeGetDataTestData
{
    void operator()(
        GetDataTestData *   _dataPtr
    ) const
    {
        freeGetDataTestData( _dataPtr );
    }
};

using GetDataTestDataUnique = std::unique_ptr<
    GetDataTestData
    , FreeGetDataTestData
>;

TEST(
    ModuleContextTest
    , GetData
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = reinterpret_cast< fg::ModuleContext & >( *moduleContextShared );

    auto    dataUnique = GetDataTestDataUnique( new GetDataTestData );
    ASSERT_NE( nullptr, dataUnique.get() );
    auto &  data = *dataUnique;

    moduleContext.setData(
        dataUnique.release()
        , freeGetDataTestData
    );

    ASSERT_EQ( &data, &( moduleContext.getData< GetDataTestData >() ) );
}

struct SetDataTestData
{
};

void freeSetDataTestData(
    SetDataTestData *   _this
)
{
    delete _this;
}

struct FreeSetDataTestData
{
    void operator()(
        SetDataTestData *   _dataPtr
    ) const
    {
        freeSetDataTestData( _dataPtr );
    }
};

using SetDataTestDataUnique = std::unique_ptr<
    SetDataTestData
    , FreeSetDataTestData
>;

TEST(
    ModuleContextTest
    , SetData
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = reinterpret_cast< fg::ModuleContext & >( *moduleContextShared );

    auto    dataUnique = SetDataTestDataUnique( new SetDataTestData );
    ASSERT_NE( nullptr, dataUnique.get() );
    auto &  data = *dataUnique;

    moduleContext.setData(
        dataUnique.release()
        , freeSetDataTestData
    );

    ASSERT_EQ( &data, moduleContextShared->dataPtr );
    ASSERT_EQ( reinterpret_cast< FgModuleContextDataDestroyProc >( freeSetDataTestData ), moduleContextShared->dataDestroyProcPtr );
    ASSERT_EQ( moduleContextShared.get(), moduleContextShared->dataDestroyer.get() );
}

struct SetDataDestroyTestDataA
{
    int i = 0;
};

struct SetDataDestroyTestDataB
{
    SetDataDestroyTestDataA &   dataA;
};

void freeSetDataDestroyTestDataB(
    SetDataDestroyTestDataB *   _this
)
{
    _this->dataA.i = 10;

    delete _this;
}

struct FreeSetDataDestroyTestDataB
{
    void operator()(
        SetDataDestroyTestDataB *   _dataPtr
    ) const
    {
        freeSetDataDestroyTestDataB( _dataPtr );
    }
};

using SetDataDestroyTestDataBUnique = std::unique_ptr<
    SetDataDestroyTestDataB
    , FreeSetDataDestroyTestDataB
>;

void freeSetDataDestroyTestDataB2(
    SetDataDestroyTestDataB *   _this
)
{
    _this->dataA.i = 20;

    delete _this;
}

struct FreeSetDataDestroyTestDataB2
{
    void operator()(
        SetDataDestroyTestDataB *   _dataPtr
    ) const
    {
        freeSetDataDestroyTestDataB2( _dataPtr );
    }
};

using SetDataDestroyTestDataB2Unique = std::unique_ptr<
    SetDataDestroyTestDataB
    , FreeSetDataDestroyTestDataB2
>;

TEST(
    ModuleContextTest
    , SetData_destroy
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = reinterpret_cast< fg::ModuleContext & >( *moduleContextShared );

    auto    dataA = SetDataDestroyTestDataA();

    auto    dataBUnique = SetDataDestroyTestDataBUnique(
        new SetDataDestroyTestDataB{
            dataA,
        }
    );
    ASSERT_NE( nullptr, dataBUnique.get() );

    moduleContext.setData(
        dataBUnique.release()
        , freeSetDataDestroyTestDataB
    );

    moduleContextShared.reset();

    ASSERT_EQ( 10, dataA.i );
}

TEST(
    ModuleContextTest
    , SetData_setOtherData
)
{
    auto    moduleContextShared = FgModuleContext::create(
        "PACKAGE_PATH"
        , "modulecontexttest/libnewtest.so"
        , "HOME_DIRECTORY_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleContextShared.get() );
    auto &  moduleContext = reinterpret_cast< fg::ModuleContext & >( *moduleContextShared );

    auto    dataA = SetDataDestroyTestDataA();

    auto    dataBUnique = SetDataDestroyTestDataBUnique(
        new SetDataDestroyTestDataB{
            dataA,
        }
    );
    ASSERT_NE( nullptr, dataBUnique.get() );

    moduleContext.setData(
        dataBUnique.release()
        , freeSetDataDestroyTestDataB
    );

    auto    dataA2 = SetDataDestroyTestDataA();

    auto    dataB2Unique = SetDataDestroyTestDataB2Unique(
        new SetDataDestroyTestDataB{
            dataA2,
        }
    );
    ASSERT_NE( nullptr, dataB2Unique.get() );

    moduleContext.setData(
        dataB2Unique.release()
        , freeSetDataDestroyTestDataB2
    );

    ASSERT_EQ( 10, dataA.i );
}
