﻿#include "candymaker/core/module/manager.h"
#include "candymaker/core/module/context.h"
#include "candymaker/core/module/info.h"

#include <utility>
#include <vector>
#include <algorithm>
#include <memory>
#include <stdexcept>

namespace {
    void removeUnusedContextShares(
        candymaker::ModuleContextShares &           _unusedContextShares
        , candymaker::ModuleManager::ModuleMap &    _map
    )
    {
        const auto  END = _map.end();
        for( auto it = _map.begin() ; it != END ; ) {
            auto    eraseIt = it;
            it++;   // イテレータが有効のうちにインクリメントする

            auto &  contextShared = eraseIt->second;

            if( contextShared.use_count() > 1 ) {
                continue;
            }

            _unusedContextShares.push_back( std::move( contextShared ) );

            _map.erase( eraseIt );
        }
    }

    void finalize(
        candymaker::ModuleContextShares &   _unusedContextShares
    )
    {
        for( auto & unusedContextShared : _unusedContextShares ) {
            unusedContextShared->finalize();
        }
    }

    struct CleanModuleMap
    {
        void operator()(
            candymaker::ModuleManager::ModuleMap *  _mapPtr
        ) const
        {
            auto &  map = *_mapPtr;

            auto    contextShares = candymaker::ModuleContextShares();

            removeUnusedContextShares(
                contextShares
                , map
            );

            finalize( contextShares );
        }
    };

    using ModuleMapCleaner = std::unique_ptr<
        candymaker::ModuleManager::ModuleMap
        , CleanModuleMap
    >;

    using ModulePair = std::pair<
        const candymaker::ModuleInfo &
        , FgModuleContext &
    >;

    using ModulePairs = std::vector< ModulePair >;

    candymaker::ModuleManager::ModuleMap::const_iterator findModule(
        const candymaker::ModuleManager::ModuleMap &    _MAP
        , const candymaker::ModuleInfo &                _INFO
    )
    {
        return std::find_if(
            _MAP.begin()
            , _MAP.end()
            , [
                &_INFO
            ]
            (
                const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
            )
            {
                const auto &    KEY = _PAIR.first;

                if( KEY.PACKAGE_PATH != _INFO.getPackagePath() ) {
                    return false;
                }

                if( KEY.MODULE_PATH != _INFO.getModulePath() ) {
                    return false;
                }

                return true;
            }
        );
    }

    bool existsModule(
        const candymaker::ModuleManager::ModuleMap &    _MAP
        , const candymaker::ModuleInfo &                _INFO
    )
    {
        if( findModule(
            _MAP
            , _INFO
        ) == _MAP.end() ) {
            return false;
        }

        return true;
    }

    void addModuleContexts(
        ModulePairs &                           _newModulePairs
        , candymaker::ModuleManager &           _manager
        , const candymaker::ModuleInfoUniques & _INFO_UNIQUES
    )
    {
        const auto &    HOME_DIRECTOPY_PATH = _manager.getHomeDirectoryPath();

        auto &  map = _manager.moduleMap;

        const auto  END = _INFO_UNIQUES.rend();
        for( auto it = _INFO_UNIQUES.rbegin() ; it != END ; it++ ) {
            const auto &    INFO = **it;

            if( existsModule(
                map
                , INFO
            ) == true ) {
                continue;
            }

            const auto &    PACKAGE_PATH = INFO.getPackagePath();
            const auto &    MODULE_PATH = INFO.getModulePath();

            auto    key = candymaker::ModuleManager::ModuleMapKey{
                PACKAGE_PATH,
                MODULE_PATH,
            };

            auto    contextShared = FgModuleContext::create(
                PACKAGE_PATH
                , MODULE_PATH
                , HOME_DIRECTOPY_PATH
                , INFO.getSaveDirectoryPath()
            );
            auto &  context = *contextShared;

            const auto  RESULT = map.insert(
                std::make_pair(
                    std::move( key )
                    , std::move( contextShared )
                )
            );
            if( RESULT.second == false ) {
                throw std::runtime_error( "モジュールマップへの要素追加に失敗" );
            }

            _newModulePairs.push_back(
                ModulePair(
                    INFO
                    , context
                )
            );
        }
    }

    void initializeModuleContexts(
        ModulePairs &   _pairs
    )
    {
        for( auto & pair : _pairs ) {
            const auto &    INFO = pair.first;
            auto &          context = pair.second;

            if( INFO.isExistsInitializerSymbol() == true ) {
                context.initialize( INFO.getInitializerSymbol() );
            }
        }
    }

    void updateModuleMap(
        candymaker::ModuleManager &             _manager
        , const candymaker::ModuleInfoUniques & _INFO_UNIQUES
    )
    {
        auto    newModulePairs = ModulePairs();

        addModuleContexts(
            newModulePairs
            , _manager
            , _INFO_UNIQUES
        );

        initializeModuleContexts( newModulePairs );
    }

    void initializeModuleContextShares(
        candymaker::ModuleContextShares &           _contextShares
        , candymaker::ModuleManager::ModuleMap &    _map
        , const candymaker::ModuleInfoUniques &     _INFO_UNIQUES
    )
    {
        const auto  END = _map.end();

        for( const auto & INFO_UNIQUE : _INFO_UNIQUES ) {
            const auto &    INFO = *INFO_UNIQUE;

            auto    it = findModule(
                _map
                , INFO
            );
            if( it == END ) {
                throw std::runtime_error( "モジュールマップ内に対象のモジュールが存在しない" );
            }
            auto &  contextShared = it->second;

            _contextShares.push_back( contextShared );
        }
    }
}

namespace candymaker {
    bool ModuleManager::LessModuleMapKey::operator()(
        const ModuleMapKey &    _KEY1
        , const ModuleMapKey &  _KEY2
    ) const
    {
        const auto  COMPARE_PACKAGE_PATH = _KEY1.PACKAGE_PATH.compare( _KEY2.PACKAGE_PATH );
        if( COMPARE_PACKAGE_PATH < 0 ) {
            return true;
        } else if( COMPARE_PACKAGE_PATH > 0 ) {
            return false;
        }

        const auto  COMPARE_MODULE_PATH = _KEY1.MODULE_PATH.compare( _KEY2.MODULE_PATH );
        if( COMPARE_MODULE_PATH < 0 ) {
            return true;
        } else if( COMPARE_MODULE_PATH > 0 ) {
            return false;
        }

        return false;
    }

    ModuleManager::ModuleManager(
        const std::string & _HOME_DIRECTORY_PATH
    )
        : HOME_DIRECTORY_PATH( _HOME_DIRECTORY_PATH )
    {
    }

    ModuleManager::Unique ModuleManager::create(
        const std::string & _HOME_DIRECTORY_PATH
    )
    {
        return new ModuleManager( _HOME_DIRECTORY_PATH );
    }

    const std::string & ModuleManager::getHomeDirectoryPath(
    ) const
    {
        return this->HOME_DIRECTORY_PATH;
    }

    void ModuleManager::loadModules(
        ModuleContextShares &       _moduleContextShares
        , const ModuleInfoUniques & _INFO_UNIQUES
    )
    {
        auto    moduleMapCleaner = ModuleMapCleaner( &( this->moduleMap ) );

        updateModuleMap(
            *this
            , _INFO_UNIQUES
        );

        auto    moduleContextShares = ModuleContextShares();
        initializeModuleContextShares(
            moduleContextShares
            , this->moduleMap
            , _INFO_UNIQUES
        );

        _moduleContextShares = std::move( moduleContextShares );
    }

    void ModuleManager::unloadModules(
        ModuleContextShares &   _moduleContextShares
    )
    {
        auto    moduleMapCleaner = ModuleMapCleaner( &( this->moduleMap ) );

        _moduleContextShares.clear();
    }
}
