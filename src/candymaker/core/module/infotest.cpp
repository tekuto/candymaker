﻿#include "fg/util/test.h"
#include "candymaker/core/module/info.h"

#include <string>

TEST(
    ModuleInfoTest
    , Create
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
        , "INITIALIZER_SYMBOL"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_INFO_UNIQUE->PACKAGE_PATH.c_str() );
    ASSERT_STREQ( "MODULE_PATH", MODULE_INFO_UNIQUE->MODULE_PATH.c_str() );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", MODULE_INFO_UNIQUE->SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_TRUE( MODULE_INFO_UNIQUE->EXISTS_INITIALIZER_SYMBOL );
    ASSERT_STREQ( "INITIALIZER_SYMBOL", MODULE_INFO_UNIQUE->INITIALIZER_SYMBOL.c_str() );
}

TEST(
    ModuleInfoTest
    , Create_withoutInitializerSymbol
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_INFO_UNIQUE->PACKAGE_PATH.c_str() );
    ASSERT_STREQ( "MODULE_PATH", MODULE_INFO_UNIQUE->MODULE_PATH.c_str() );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", MODULE_INFO_UNIQUE->SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_FALSE( MODULE_INFO_UNIQUE->EXISTS_INITIALIZER_SYMBOL );
}

TEST(
    ModuleInfoTest
    , GetPackagePath
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_INFO_UNIQUE->getPackagePath().c_str() );
}

TEST(
    ModuleInfoTest
    , GetModulePath
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "MODULE_PATH", MODULE_INFO_UNIQUE->getModulePath().c_str() );
}

TEST(
    ModuleInfoTest
    , GetSaveDirectoryPath
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", MODULE_INFO_UNIQUE->getSaveDirectoryPath().c_str() );
}

TEST(
    ModuleInfoTest
    , IsExistsInitializerSymbol
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
        , "initializerSymbol"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_TRUE( MODULE_INFO_UNIQUE->isExistsInitializerSymbol() );
}

TEST(
    ModuleInfoTest
    , IsExistsInitializerSymbol_notExists
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_FALSE( MODULE_INFO_UNIQUE->isExistsInitializerSymbol() );
}

TEST(
    ModuleInfoTest
    , GetInitializerSymbol
)
{
    const auto  MODULE_INFO_UNIQUE = candymaker::ModuleInfo::create(
        "PACKAGE_PATH"
        , "MODULE_PATH"
        , "SAVE_DIRECTORY_PATH"
        , "initializerSymbol"
    );
    ASSERT_NE( nullptr, MODULE_INFO_UNIQUE.get() );

    ASSERT_STREQ( "initializerSymbol", MODULE_INFO_UNIQUE->getInitializerSymbol().c_str() );
}
