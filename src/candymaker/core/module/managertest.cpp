﻿#include "fg/util/test.h"
#include "candymaker/core/module/manager.h"
#include "candymaker/core/module/info.h"
#include "candymaker/core/module/context.h"

#include <utility>
#include <algorithm>

TEST(
    ModuleManagerTest
    , Create
)
{
    const auto  MODULE_MANAGER_UNIQUE = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, MODULE_MANAGER_UNIQUE.get() );

    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_MANAGER_UNIQUE->HOME_DIRECTORY_PATH.c_str() );
    ASSERT_EQ( 0, MODULE_MANAGER_UNIQUE->moduleMap.size() );
}

TEST(
    ModuleManagerTest
    , GetHomeDirectoryPath
)
{
    const auto  MODULE_MANAGER_UNIQUE = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, MODULE_MANAGER_UNIQUE.get() );

    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_MANAGER_UNIQUE->getHomeDirectoryPath().c_str() );
}

void testModuleMapElement(
    const candymaker::ModuleManager::ModuleMap &    _MODULE_MAP
    , const std::string &                           _PACKAGE_PATH
    , const std::string &                           _MODULE_PATH
    , const FgModuleContext &                       _MODULE_CONTEXT
)
{
    const auto  END = _MODULE_MAP.end();

    const auto  IT = std::find_if(
        _MODULE_MAP.begin()
        , END
        , [
            &_PACKAGE_PATH
            , &_MODULE_PATH
        ]
        (
            const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
        )
        {
            const auto &    KEY = _PAIR.first;

            if( KEY.PACKAGE_PATH != _PACKAGE_PATH ) {
                return false;
            }

            if( KEY.MODULE_PATH != _MODULE_PATH ) {
                return false;
            }

            return true;
        }
    );
    ASSERT_NE( IT, END );
    const auto &    MODULE_CONTEXT_SHARED = IT->second;

    ASSERT_EQ( 2, MODULE_CONTEXT_SHARED.use_count() );
    ASSERT_EQ( &_MODULE_CONTEXT, MODULE_CONTEXT_SHARED.get() );
}

void testModuleMapElement(
    const candymaker::ModuleManager::ModuleMap &    _MODULE_MAP
    , const std::string &                           _PACKAGE_PATH
    , const std::string &                           _MODULE_PATH
    , const FgModuleContext &                       _MODULE_CONTEXT
    , const FgModuleContext &                       _OLD_MODULE_CONTEXT
)
{
    const auto  END = _MODULE_MAP.end();

    const auto  IT = std::find_if(
        _MODULE_MAP.begin()
        , END
        , [
            &_PACKAGE_PATH
            , &_MODULE_PATH
        ]
        (
            const candymaker::ModuleManager::ModuleMap::value_type &    _PAIR
        )
        {
            const auto &    KEY = _PAIR.first;

            if( KEY.PACKAGE_PATH != _PACKAGE_PATH ) {
                return false;
            }

            if( KEY.MODULE_PATH != _MODULE_PATH ) {
                return false;
            }

            return true;
        }
    );
    ASSERT_NE( IT, END );
    const auto &    MODULE_CONTEXT_SHARED = IT->second;

    ASSERT_EQ( 3, MODULE_CONTEXT_SHARED.use_count() );
    ASSERT_EQ( &_MODULE_CONTEXT, MODULE_CONTEXT_SHARED.get() );
    ASSERT_EQ( &_OLD_MODULE_CONTEXT, &_MODULE_CONTEXT );
}

TEST(
    ModuleManagerTest
    , LoadModules
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );

    auto    moduleInfo1Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadtest1.so"
        , "SAVE_DIRECTORY_PATH1"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo1Unique.get() );

    auto    moduleInfo2Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadtest2.so"
        , "SAVE_DIRECTORY_PATH2"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo2Unique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfo1Unique ) );
    moduleInfoUniques.push_back( std::move( moduleInfo2Unique ) );

    auto    moduleContextShares = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares
        , moduleInfoUniques
    );

    ASSERT_EQ( 2, moduleContextShares.size() );
    const auto &    MODULE_CONTEXT1 = *( moduleContextShares[ 0 ] );
    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_CONTEXT1.HOME_DIRECTORY_PATH.c_str() );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH1", MODULE_CONTEXT1.SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_EQ( 10, *static_cast< const int * >( MODULE_CONTEXT1.dataPtr ) );
    const auto &    MODULE_CONTEXT2 = *( moduleContextShares[ 1 ] );
    ASSERT_STREQ( "HOME_DIRECTORY_PATH", MODULE_CONTEXT2.HOME_DIRECTORY_PATH.c_str() );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH2", MODULE_CONTEXT2.SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_EQ( 20, *static_cast< const int * >( MODULE_CONTEXT2.dataPtr ) );

    const auto &    MODULE_MAP = moduleManagerUnique->moduleMap;
    ASSERT_EQ( 2, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleMapElement(
            MODULE_MAP
            , "modulemanagertest"
            , "modulemanagertest/libloadtest1.so"
            , MODULE_CONTEXT1
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testModuleMapElement(
            MODULE_MAP
            , "modulemanagertest"
            , "modulemanagertest/libloadtest2.so"
            , MODULE_CONTEXT2
        )
    );
}

TEST(
    ModuleManagerTest
    , LoadModules_loadOrder
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );

    auto    moduleInfo1Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadordertest1.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfo1Unique.get() );

    auto    moduleInfo2Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadordertest2.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfo2Unique.get() );

    auto    moduleInfo3Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadordertest3.so"
        , "SAVE_DIRECTORY_PATH"
    );
    ASSERT_NE( nullptr, moduleInfo3Unique.get() );

    auto    moduleInfoUniques = candymaker::ModuleInfoUniques();
    moduleInfoUniques.push_back( std::move( moduleInfo1Unique ) );
    moduleInfoUniques.push_back( std::move( moduleInfo2Unique ) );
    moduleInfoUniques.push_back( std::move( moduleInfo3Unique ) );

    auto    moduleContextShares = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares
        , moduleInfoUniques
    );

    const auto  GET_DATA_PROC_PTR = moduleContextShares[ 2 ]->getProc< int & ( * )() >( "_ZN10candymaker7getDataEv" );

    auto &  i = GET_DATA_PROC_PTR();
    ASSERT_EQ( 200, i );
}

TEST(
    ModuleManagerTest
    , LoadModules_containAlreadyLoadedModules
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadtest3.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques1 = candymaker::ModuleInfoUniques();
    moduleInfoUniques1.push_back( std::move( moduleInfoUnique ) );

    auto    moduleContextShares1 = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares1
        , moduleInfoUniques1
    );

    ASSERT_EQ( 1, moduleContextShares1.size() );
    const auto &    MODULE_CONTEXT = *( moduleContextShares1[ 0 ] );

    auto    moduleInfo1Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadtest3.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo1Unique.get() );

    auto    moduleInfo2Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libloadtest4.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo2Unique.get() );

    auto    moduleInfoUniques2 = candymaker::ModuleInfoUniques();
    moduleInfoUniques2.push_back( std::move( moduleInfo1Unique ) );
    moduleInfoUniques2.push_back( std::move( moduleInfo2Unique ) );

    auto    moduleContextShares2 = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares2
        , moduleInfoUniques2
    );

    ASSERT_EQ( 2, moduleContextShares2.size() );
    auto &  moduleContext1 = *( moduleContextShares2[ 0 ] );
    auto &  moduleContext2 = *( moduleContextShares2[ 1 ] );

    const auto &    MODULE_MAP = moduleManagerUnique->moduleMap;
    ASSERT_EQ( 2, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleMapElement(
            MODULE_MAP
            , "modulemanagertest"
            , "modulemanagertest/libloadtest3.so"
            , moduleContext1
            , MODULE_CONTEXT
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testModuleMapElement(
            MODULE_MAP
            , "modulemanagertest"
            , "modulemanagertest/libloadtest4.so"
            , moduleContext2
        )
    );

    const auto  PROC_PTR1 = moduleContext1.getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 10, PROC_PTR1() );

    const auto  PROC_PTR2 = moduleContext2.getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 20, PROC_PTR2() );
}

//TODO LoadModules_failed

TEST(
    ModuleManagerTest
    , UnloadModules
)
{
    auto    moduleManagerUnique = candymaker::ModuleManager::create( "HOME_DIRECTORY_PATH" );
    ASSERT_NE( nullptr, moduleManagerUnique.get() );

    auto    moduleInfoUnique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libunloadtest1.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfoUnique.get() );

    auto    moduleInfoUniques1 = candymaker::ModuleInfoUniques();
    moduleInfoUniques1.push_back( std::move( moduleInfoUnique ) );

    auto    moduleContextShares1 = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares1
        , moduleInfoUniques1
    );

    ASSERT_EQ( 1, moduleContextShares1.size() );
    auto &  moduleContext = *( moduleContextShares1[ 0 ] );

    auto    moduleInfo1Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libunloadtest1.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo1Unique.get() );

    auto    moduleInfo2Unique = candymaker::ModuleInfo::create(
        "modulemanagertest"
        , "modulemanagertest/libunloadtest2.so"
        , "SAVE_DIRECTORY_PATH"
        , "_ZN10candymaker10initializeERN2fg13ModuleContextE"
    );
    ASSERT_NE( nullptr, moduleInfo2Unique.get() );

    auto    moduleInfoUniques2 = candymaker::ModuleInfoUniques();
    moduleInfoUniques2.push_back( std::move( moduleInfo1Unique ) );
    moduleInfoUniques2.push_back( std::move( moduleInfo2Unique ) );

    auto    moduleContextShares2 = candymaker::ModuleContextShares();
    moduleManagerUnique->loadModules(
        moduleContextShares2
        , moduleInfoUniques2
    );

    moduleManagerUnique->unloadModules( moduleContextShares2 );

    ASSERT_EQ( 0, moduleContextShares2.size() );

    const auto &    MODULE_MAP = moduleManagerUnique->moduleMap;
    ASSERT_EQ( 1, MODULE_MAP.size() );

    ASSERT_NO_FATAL_FAILURE(
        testModuleMapElement(
            MODULE_MAP
            , "modulemanagertest"
            , "modulemanagertest/libunloadtest1.so"
            , moduleContext
        )
    );

    const auto  PROC_PTR = moduleContext.getProc< int ( * )() >( "_ZN10candymaker7getDataEv" );
    ASSERT_EQ( 10, PROC_PTR() );
}
