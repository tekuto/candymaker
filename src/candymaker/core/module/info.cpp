﻿#include "candymaker/core/module/info.h"

namespace candymaker {
    ModuleInfo::ModuleInfo(
        const std::string &     _PACKAGE_PATH
        , const std::string &   _MODULE_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
        , const std::string &   _INITIALIZER_SYMBOL
    )
        : PACKAGE_PATH( _PACKAGE_PATH )
        , MODULE_PATH( _MODULE_PATH )
        , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
        , EXISTS_INITIALIZER_SYMBOL( true )
        , INITIALIZER_SYMBOL( _INITIALIZER_SYMBOL )
    {
    }

    ModuleInfo::ModuleInfo(
        const std::string &     _PACKAGE_PATH
        , const std::string &   _MODULE_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
    )
        : PACKAGE_PATH( _PACKAGE_PATH )
        , MODULE_PATH( _MODULE_PATH )
        , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
        , EXISTS_INITIALIZER_SYMBOL( false )
    {
    }

    const std::string & ModuleInfo::getPackagePath(
    ) const
    {
        return this->PACKAGE_PATH;
    }

    const std::string & ModuleInfo::getModulePath(
    ) const
    {
        return this->MODULE_PATH;
    }

    const std::string & ModuleInfo::getSaveDirectoryPath(
    ) const
    {
        return this->SAVE_DIRECTORY_PATH;
    }

    bool ModuleInfo::isExistsInitializerSymbol(
    ) const
    {
        return this->EXISTS_INITIALIZER_SYMBOL;
    }

    const std::string & ModuleInfo::getInitializerSymbol(
    ) const
    {
        return this->INITIALIZER_SYMBOL;
    }
}
