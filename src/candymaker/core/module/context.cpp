﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"
#include "candymaker/core/module/context.h"

#include <string>
#include <stdexcept>
#include <dlfcn.h>

namespace {
    FgModuleContext::HandleUnique createModuleHandle(
        const std::string & _MODULE_PATH
    )
    {
        auto    thisUnique = FgModuleContext::HandleUnique(
            dlopen(
                _MODULE_PATH.c_str()
                , RTLD_LAZY | RTLD_GLOBAL
            )
        );
        if( thisUnique.get() == nullptr ) {
            throw std::runtime_error( "dlopen()に失敗" );
        }

        return thisUnique;
    }
}

void FgModuleContext::UnloadHandle::operator()(
    void *  _handlePtr
) const
{
    dlclose( _handlePtr );
}

void FgModuleContext::FreeData::operator()(
    FgModuleContext *   _implPtr
) const
{
    _implPtr->dataDestroyProcPtr( _implPtr->dataPtr );
}

FgModuleContext::FgModuleContext(
    const std::string &     _PACKAGE_PATH
    , const std::string &   _MODULE_PATH
    , const std::string &   _HOME_DIRECTORY_PATH
    , const std::string &   _SAVE_DIRECTORY_PATH
)
    : PACKAGE_PATH( _PACKAGE_PATH )
    , HOME_DIRECTORY_PATH( _HOME_DIRECTORY_PATH )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
    , handleUnique( createModuleHandle( _MODULE_PATH ) )
{
}

void FgModuleContext::initialize(
    const std::string & _INITIALIZER_SYMBOL
)
{
    const auto  PROC_PTR = this->getProc< FgModuleContextInitializeProc >( _INITIALIZER_SYMBOL );

    PROC_PTR( this );
}

void FgModuleContext::finalize(
)
{
    this->dataDestroyer.reset();
}

const std::string & FgModuleContext::getPackagePath(
) const
{
    return this->PACKAGE_PATH;
}

const std::string & FgModuleContext::getHomeDirectoryPath(
) const
{
    return this->HOME_DIRECTORY_PATH;
}

const std::string & FgModuleContext::getSaveDirectoryPath(
) const
{
    return this->SAVE_DIRECTORY_PATH;
}

void * FgModuleContext::getProc_(
    const std::string & _SYMBOL
)
{
    dlerror();
    const auto  PROC_PTR = dlsym(
        this->handleUnique.get()
        , _SYMBOL.c_str()
    );
    const auto  ERROR = dlerror();
    if( ERROR != nullptr ) {
        throw std::runtime_error( ERROR );
    }

    return PROC_PTR;
}

void * fgModuleContextGetData(
    FgModuleContext *   _implPtr
)
{
    return _implPtr->dataPtr;
}

void fgModuleContextSetData(
    FgModuleContext *                   _implPtr
    , void *                            _dataPtr
    , FgModuleContextDataDestroyProc    _procPtr
)
{
    _implPtr->dataDestroyer.reset( _implPtr );
    _implPtr->dataPtr = _dataPtr;
    _implPtr->dataDestroyProcPtr = _procPtr;
}
