﻿#include "fg/util/test.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/state/state.h"
#include "candymaker/core/thread/cache.h"
#include "fg/common/unique.h"

void dummyDestroy(
    void *
)
{
}

struct BasesystemContextTestData : public fg::UniqueWrapper< BasesystemContextTestData >
{
    static Unique create(
    )
    {
        return new BasesystemContextTestData;
    }
};

TEST(
    BasesystemContextTest
    , Constructor
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContext = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );

    ASSERT_EQ( &state, &( basesystemContext.state ) );
    ASSERT_EQ( reinterpret_cast< FgModuleContext * >( 10 ), &( basesystemContext.moduleContext ) );
    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", basesystemContext.SAVE_DIRECTORY_PATH.c_str() );
    ASSERT_EQ( nullptr, basesystemContext.waiter.get() );
}

struct SetStateDataTestData : public fg::UniqueWrapper< SetStateDataTestData >
{
    int &   i;

    SetStateDataTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new SetStateDataTestData( _i );
    }

    static void destroy(
        SetStateDataTestData *  _this
    )
    {
        _this->i = 20;

        delete _this;
    }
};

TEST(
    BasesystemContextTest
    , SetStateData
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextImpl = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  basesystemContext = reinterpret_cast< fg::BasesystemContext< SetStateDataTestData > & >( basesystemContextImpl );

    auto    i = 10;

    auto    dataUnique = SetStateDataTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    basesystemContext.setStateData(
        dataUnique.release()
        , SetStateDataTestData::destroy
    );

    stateUnique.destroy();

    ASSERT_EQ( 20, i );
}

void setWaitEndProcTestProc(
    BasesystemContextTestData &
)
{
}

TEST(
    BasesystemContextTest
    , SetWaitEndProc
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextImpl = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  basesystemContext = reinterpret_cast< fg::BasesystemContext< BasesystemContextTestData > & >( basesystemContextImpl );

    auto    dataUnique = BasesystemContextTestData::create();
    ASSERT_NE( nullptr, dataUnique.get() );
    auto &  data = *dataUnique;

    basesystemContext.setWaitEndProc(
        setWaitEndProcTestProc
        , data
    );

    ASSERT_EQ( reinterpret_cast< FgBasesystemContextWaitEndProc >( setWaitEndProcTestProc ), basesystemContextImpl.waitEndProcPtr );
    ASSERT_EQ( &data, basesystemContextImpl.waitEndDataPtr );
    ASSERT_EQ( &basesystemContextImpl, basesystemContextImpl.waiter.get() );
}

TEST(
    BasesystemContextTest
    , GetState
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< BasesystemContextTestData > & >( *stateUnique );

    auto    basesystemContextImpl = FgBasesystemContext(
        *state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  basesystemContext = reinterpret_cast< fg::BasesystemContext< BasesystemContextTestData > & >( basesystemContextImpl );

    ASSERT_EQ( &state, &( basesystemContext.getState() ) );
}

TEST(
    BasesystemContextTest
    , GetModuleContext
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< BasesystemContextTestData > & >( BASESYSTEM_CONTEXT_IMPL );

    ASSERT_EQ( reinterpret_cast< const FgModuleContext * >( 10 ), &( BASESYSTEM_CONTEXT->getModuleContext() ) );
}

TEST(
    BasesystemContextTest
    , GetSaveDirectoryPath
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    const auto  BASESYSTEM_CONTEXT_IMPL = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    const auto &    BASESYSTEM_CONTEXT = reinterpret_cast< const fg::BasesystemContext< BasesystemContextTestData > & >( BASESYSTEM_CONTEXT_IMPL );

    ASSERT_STREQ( "SAVE_DIRECTORY_PATH", BASESYSTEM_CONTEXT->getSaveDirectoryPath().c_str() );
}

void waitEndTestProc(
    int &  _i
)
{
    _i = 20;
}

TEST(
    BasesystemContextTest
    , WaitEnd
)
{
    auto    threadCacheUnique = candymaker::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemContextImpl = FgBasesystemContext(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
        , "SAVE_DIRECTORY_PATH"
    );
    auto &  basesystemContext = reinterpret_cast< fg::BasesystemContext< int > & >( basesystemContextImpl );

    auto    i = 10;

    basesystemContext.setWaitEndProc(
        waitEndTestProc
        , i
    );

    basesystemContextImpl.waitEnd();

    ASSERT_EQ( 20, i );
}
