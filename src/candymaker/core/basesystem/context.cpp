﻿#include "fg/util/export.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/state/state.h"
#include "fg/core/state/state.h"

void FgBasesystemContext::WaitEnd::operator()(
    FgBasesystemContext *   _basesystemStatePtr
) const
{
    _basesystemStatePtr->waitEndProcPtr( _basesystemStatePtr->waitEndDataPtr );
}

FgBasesystemContext::FgBasesystemContext(
    FgState &               _state
    , FgModuleContext &     _moduleContext
    , const std::string &   _SAVE_DIRECTORY_PATH
)
    : state( _state )
    , moduleContext( _moduleContext )
    , SAVE_DIRECTORY_PATH( _SAVE_DIRECTORY_PATH )
    , waitEndProcPtr( nullptr )
    , waitEndDataPtr( nullptr )
    , waiter( nullptr )
{
}

const FgModuleContext & FgBasesystemContext::getModuleContext(
) const
{
    return this->moduleContext;
}

const std::string & FgBasesystemContext::getSaveDirectoryPath(
) const
{
    return this->SAVE_DIRECTORY_PATH;
}

void FgBasesystemContext::waitEnd(
)
{
    this->waiter.reset();
}

void fgBasesystemContextSetStateData(
    FgBasesystemContext *                       _implPtr
    , void *                                    _dataPtr
    , FgBasesystemContextStateDataDestroyProc   _dataDestroyProcPtr
)
{
    _implPtr->state.setData(
        _dataPtr
        , _dataDestroyProcPtr
    );
}

void fgBasesystemContextSetWaitEndProc(
    FgBasesystemContext *               _implPtr
    , FgBasesystemContextWaitEndProc    _procPtr
    , void *                            _dataPtr
)
{
    _implPtr->waiter.reset( _implPtr );
    _implPtr->waitEndProcPtr = _procPtr;
    _implPtr->waitEndDataPtr = _dataPtr;
}

FgState * fgBasesystemContextGetState(
    FgBasesystemContext *   _implPtr
)
{
    return &( _implPtr->state );
}
