﻿#ifndef CANDYMAKER_MAIN_BASESYSTEMCONTEXTMANAGER_H
#define CANDYMAKER_MAIN_BASESYSTEMCONTEXTMANAGER_H

#include "candymaker/def/main/basesystemcontextmanager.h"
#include "candymaker/core/module/contexts.h"
#include "candymaker/core/basesystem/context.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/core/state/state.h"
#include "fg/common/unique.h"

#include <string>

namespace candymaker {
    class BasesystemContextManager : public fg::UniqueWrapper< BasesystemContextManager >
    {
        ModuleContexts::Unique  moduleContextsUnique;
        FgBasesystemContext     basesystemContext;

        BasesystemContextManager(
            ModuleContexts::Unique &&
            , FgState &
            , const std::string &
            , const std::string &
        );

    public:
        static Unique create(
            const ExecuteFile &
            , const std::string &
            , const std::string &
            , ModuleManager &
            , FgState &
        );

        static Unique create(
            const ExecuteFile &
            , const std::string &
            , ModuleManager &
            , FgState &
        );

        void waitEnd(
        );
    };
}

#endif  // CANDYMAKER_MAIN_BASESYSTEMCONTEXTMANAGER_H
