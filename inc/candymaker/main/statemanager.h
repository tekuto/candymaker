﻿#ifndef CANDYMAKER_MAIN_STATEMANAGER_H
#define CANDYMAKER_MAIN_STATEMANAGER_H

#include "candymaker/def/main/statemanager.h"
#include "candymaker/core/state/state.h"
#include "candymaker/def/core/thread/cache.h"
#include "fg/common/unique.h"

namespace candymaker {
    class StateManager : public fg::UniqueWrapper< StateManager >
    {
        FgState::Unique stateUnique;

        StateManager(
            ThreadCache &
        );

    public:
        static Unique create(
            ThreadCache &
        );

        FgState & getBasesystemState(
        );

        FgState & getGameState(
        );
    };
}

#endif  // CANDYMAKER_MAIN_STATEMANAGER_H
