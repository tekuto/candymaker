﻿#ifndef CANDYMAKER_MAIN_GAMECONTEXTMANAGER_H
#define CANDYMAKER_MAIN_GAMECONTEXTMANAGER_H

#include "candymaker/def/main/gamecontextmanager.h"
#include "candymaker/core/module/contexts.h"
#include "candymaker/core/game/context.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/core/state/state.h"
#include "fg/common/unique.h"

#include <string>

namespace candymaker {
    class GameContextManager : public fg::UniqueWrapper< GameContextManager >
    {
        ModuleContexts::Unique  moduleContextsUnique;
        FgGameContext           gameContext;

        GameContextManager(
            ModuleContexts::Unique &&
            , FgState &
            , const std::string &
            , const std::string &
        );

    public:
        static Unique create(
            const ExecuteFile &
            , const std::string &
            , const std::string &
            , ModuleManager &
            , FgState &
        );

        static Unique create(
            const ExecuteFile &
            , const std::string &
            , ModuleManager &
            , FgState &
        );

        void start(
        );
    };
}

#endif  // CANDYMAKER_MAIN_GAMECONTEXTMANAGER_H
