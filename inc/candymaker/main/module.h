﻿#ifndef CANDYMAKER_MAIN_MODULE_H
#define CANDYMAKER_MAIN_MODULE_H

#include "candymaker/def/core/module/manager.h"
#include "candymaker/core/module/contexts.h"
#include "candymaker/core/file/execute.h"
#include "candymaker/def/core/file/path.h"

#include <string>

namespace candymaker {
    ModuleContexts::Unique createModuleContexts(
        ModuleManager &
        , const std::string &
        , const std::string &
        , const ExecuteFile::Packages &
        , const std::string &
        , const Path &
        , const std::string &
    );

    ModuleContexts::Unique createModuleContexts(
        ModuleManager &
        , const std::string &
        , const ExecuteFile::Packages &
        , const std::string &
        , const Path &
        , const std::string &
    );
}

#endif  // CANDYMAKER_MAIN_MODULE_H
