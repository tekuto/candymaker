﻿#ifndef CANDYMAKER_MAIN_ARGS_H
#define CANDYMAKER_MAIN_ARGS_H

#include "candymaker/def/main/args.h"
#include "fg/common/unique.h"

#include <string>

namespace candymaker {
    class Args : public fg::UniqueWrapper< Args >
    {
        bool        existsShortcutFilePath;
        std::string shortcutFilePath;

        bool        existsSystemDirectoryPath;
        std::string systemDirectoryPath;
        bool        existsHomeDirectoryPath;
        std::string homeDirectoryPath;

        Args(
            bool
            , std::string &&
            , bool
            , std::string &&
            , bool
            , std::string &&
        );

    public:
        static ConstUnique create(
            int
            , char **
        );

        bool isExistsShortcutFilePath(
        ) const;

        const std::string & getShortcutFilePath(
        ) const;

        bool isExistsSystemDirectoryPath(
        ) const;

        const std::string & getSystemDirectoryPath(
        ) const;

        bool isExistsHomeDirectoryPath(
        ) const;

        const std::string & getHomeDirectoryPath(
        ) const;
    };
}

#endif  // CANDYMAKER_MAIN_ARGS_H
