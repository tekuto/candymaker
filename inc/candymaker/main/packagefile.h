﻿#ifndef CANDYMAKER_MAIN_PACKAGEFILE_H
#define CANDYMAKER_MAIN_PACKAGEFILE_H

#include "candymaker/core/file/package.h"
#include "candymaker/core/file/path.h"

#include <string>

namespace candymaker {
    std::string createPackageDirectoryString(
        const std::string &
        , const std::string &
        , const std::string &
        , const candymaker::Path &
    );

    std::string createPackageDirectoryString(
        const std::string &
        , const std::string &
        , const candymaker::Path &
    );

    PackageFile::ConstUnique createPackageFile(
        const std::string &
    );

    PackageFile::ConstUnique createPackageFile(
        const std::string &
        , const std::string &
        , const std::string &
        , const candymaker::Path &
    );

    PackageFile::ConstUnique createPackageFile(
        const std::string &
        , const std::string &
        , const candymaker::Path &
    );
}

#endif  // CANDYMAKER_MAIN_PACKAGEFILE_H
