﻿#ifndef CANDYMAKER_MAIN_MAIN_H
#define CANDYMAKER_MAIN_MAIN_H

#include "fg/util/import.h"

namespace candymaker {
    FG_FUNCTION_BOOL(
        main(
            int
            , char **
        )
    )
}

#endif  // CANDYMAKER_MAIN_MAIN_H
