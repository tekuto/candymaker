﻿#ifndef CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST2_H
#define CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST2_H

#include "fg/def/core/module/context.h"
#include "fg/util/import.h"

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData2(
        )
    )
}

#endif  // CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST2_H
