﻿#ifndef CANDYMAKER_TEST_MODULEMANAGERTEST_LOADORDERTEST3_H
#define CANDYMAKER_TEST_MODULEMANAGERTEST_LOADORDERTEST3_H

#include "fg/util/import.h"

namespace candymaker {
    FG_FUNCTION_REF(
        int
        , getData(
        )
    )
}

#endif  // CANDYMAKER_TEST_MODULEMANAGERTEST_LOADORDERTEST3_H
