﻿#ifndef CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST1_H
#define CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST1_H

#include "fg/def/core/module/context.h"
#include "fg/util/import.h"

namespace candymaker {
    FG_FUNCTION_VOID(
        initialize(
            fg::ModuleContext &
        )
    )

    FG_FUNCTION_NUM(
        int
        , getData1(
        )
    )
}

#endif  // CANDYMAKER_TEST_MODULEMANAGERTEST_LOADTEST1_H
