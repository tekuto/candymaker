﻿#ifndef CANDYMAKER_TEST_MAINMODULETEST_PACKAGES_PACKAGE_MAINMODULETEST_DEPENDLOCALMODULE_H
#define CANDYMAKER_TEST_MAINMODULETEST_PACKAGES_PACKAGE_MAINMODULETEST_DEPENDLOCALMODULE_H

#include "fg/util/import.h"

namespace candymaker {
    FG_FUNCTION_VOID(
        dependLocalFunc(
        )
    )
}

#endif  // CANDYMAKER_TEST_MAINMODULETEST_PACKAGES_PACKAGE_MAINMODULETEST_DEPENDLOCALMODULE_H
