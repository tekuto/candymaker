﻿#ifndef CANDYMAKER_CORE_PACKAGE_RESOURCE_H
#define CANDYMAKER_CORE_PACKAGE_RESOURCE_H

#include "fg/def/core/package/resource.h"
#include "candymaker/def/core/common/filedata.h"

struct FgPackageResource
{
    candymaker::FileData    fileData;
};

#endif  // CANDYMAKER_CORE_PACKAGE_RESOURCE_H
