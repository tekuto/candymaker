﻿#ifndef CANDYMAKER_CORE_STATE_THREADIMPL_H
#define CANDYMAKER_CORE_STATE_THREADIMPL_H

#include "candymaker/def/core/state/threadimpl.h"
#include "fg/core/state/state.h"

namespace candymaker {
    class StateThread
    {
        FgState &   state;
        void *      userDataPtr;

    public:
        StateThread(
            FgState &
            , void *
        );

        FgState * getState(
        );

        void * getData(
        );
    };
}

#endif  // CANDYMAKER_CORE_STATE_THREADIMPL_H
