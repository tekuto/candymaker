﻿#ifndef CANDYMAKER_CORE_STATE_REENTEREVENT_H
#define CANDYMAKER_CORE_STATE_REENTEREVENT_H

#include "fg/core/state/reenterevent.h"
#include "fg/core/state/creating.h"

struct FgStateReenterEventData
{
    FgCreatingStateDataDestroyProc  prevDataDestroyProcPtr;

    FgStateReenterEventData(
        FgCreatingStateDataDestroyProc
    );
};

#endif  // CANDYMAKER_CORE_STATE_REENTEREVENT_H
