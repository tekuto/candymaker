﻿#ifndef CANDYMAKER_CORE_STATE_EVENTIMPL_H
#define CANDYMAKER_CORE_STATE_EVENTIMPL_H

#include "candymaker/def/core/state/eventimpl.h"
#include "fg/core/state/state.h"

namespace candymaker {
    class StateEvent
    {
        FgState &   state;
        void *      eventDataPtr;

    public:
        using Proc = void ( * )(
            StateEvent &
        );

        StateEvent(
            FgState &
            , void *
        );

        FgState * getState(
        );

        void * getData(
        );
    };
}

#endif  // CANDYMAKER_CORE_STATE_EVENTIMPL_H
