﻿#ifndef CANDYMAKER_CORE_STATE_EVENTREGISTERMANAGER_H
#define CANDYMAKER_CORE_STATE_EVENTREGISTERMANAGER_H

#include "fg/core/state/eventregistermanager.h"
#include "candymaker/core/state/eventmanager.h"

struct FgStateEventRegisterManager
{
    FgStateEventManager::EventProcUnregisterer  unregisterer;
};

#endif  // CANDYMAKER_CORE_STATE_EVENTREGISTERMANAGER_H
