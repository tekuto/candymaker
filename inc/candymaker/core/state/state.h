﻿#ifndef CANDYMAKER_CORE_STATE_STATE_H
#define CANDYMAKER_CORE_STATE_STATE_H

#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/threadsingleton.h"
#include "fg/core/state/joiner.h"
#include "candymaker/core/state/eventimpl.h"
#include "candymaker/core/state/joiner.h"
#include "candymaker/core/state/eventmanager.h"
#include "candymaker/core/thread/cache.h"
#include "candymaker/core/thread/joiner.h"
#include "fg/common/unique.h"

#include <memory>
#include <set>
#include <mutex>

struct FgState : public fg::UniqueWrapper< FgState >
{
    struct DestroyData
    {
        void operator()(
            FgState *
        ) const;
    };

    using DataDestroyer = std::unique_ptr<
        FgState
        , DestroyData
    >;

    struct ThreadSingletonData
    {
        FgState &                   state;
        FgStateThreadSingletonProc  threadProcPtr;
        FgStateJoiner::Ender        ender;
        void *                      userDataPtr;

        bool    reexecute;

        ThreadSingletonData(
            FgState &
            , FgStateThreadSingletonProc
            , FgStateJoiner::Ender &&
            , void *
        );
    };

    struct LessThreadSingletonData
    {
        bool operator()(
            const ThreadSingletonData &
            , const ThreadSingletonData &
        ) const;
    };

    using ThreadSingletonDataSet = std::set<
        ThreadSingletonData
        , LessThreadSingletonData
    >;

    struct EndState
    {
        void operator()(
            FgState *
        ) const;
    };

    using Ender = std::unique_ptr<
        FgState
        , EndState
    >;

    candymaker::ThreadCache &   threadCache;

    FgState *   prevStatePtr;

    std::mutex  disabledMutex;

    bool    disabled;

    void *                          dataPtr;
    FgCreatingStateDataDestroyProc  dataDestroyProcPtr;
    DataDestroyer                   dataDestroyer;

    bool                            existsEnterEvent;
    candymaker::StateEvent::Proc    enterEventProcPtr;
    bool                            existsExitEvent;
    candymaker::StateEvent::Proc    exitEventProcPtr;
    bool                            existsReenterEvent;
    candymaker::StateEvent::Proc    reenterEventProcPtr;
    bool                            existsLeaveEvent;
    candymaker::StateEvent::Proc    leaveEventProcPtr;

    std::mutex  threadSingletonMutex;

    ThreadSingletonDataSet  threadSingletonDataSet;

    candymaker::ThreadJoiner::Unique    threadJoinerBackgroundUnique;
    candymaker::ThreadJoiner::Unique    threadJoinerUnique;

    Ender   ender;

    FgState::Unique nextStateUnique;

    FgState(
        candymaker::ThreadCache &
        , FgState *
        , bool
        , void *
        , FgCreatingStateDataDestroyProc
    );

    static Unique create(
        candymaker::ThreadCache &
        , FgState *
        , bool
        , void *
        , FgCreatingStateDataDestroyProc
    );

    void setData(
        void *
        , FgCreatingStateDataDestroyProc
    );

    void setEnterEventProc(
        candymaker::StateEvent::Proc
    );

    void setExitEventProc(
        candymaker::StateEvent::Proc
    );

    void setReenterEventProc(
        candymaker::StateEvent::Proc
    );

    void setLeaveEventProc(
        candymaker::StateEvent::Proc
    );

    FgState & getNextState(
    );

    void setNextState(
        FgState::Unique &&
    );

    FgState & getRootState(
    );

    void executeEnterThread(
    );

    void executeEvent(
        candymaker::StateEvent::Proc
        , fg::StateJoiner &
        , FgStateJoiner &
        , void *
    );

    void executeEventBackground(
        candymaker::StateEvent::Proc
        , fg::StateJoiner &
        , FgStateJoiner &
        , void *
    );
};

#endif  // CANDYMAKER_CORE_STATE_STATE_H
