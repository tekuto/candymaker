﻿#ifndef CANDYMAKER_CORE_DATAELEMENT_STRING_H
#define CANDYMAKER_CORE_DATAELEMENT_STRING_H

#include "fg/core/dataelement/string.h"
#include "candymaker/def/core/common/filedata.h"

#include <string>

struct FgDataElementString
{
    std::string string;

    inline fg::DataElementString::ConstUnique clone(
    ) const
    {
        return new FgDataElementString( *this );
    }

    static fg::DataElementString::ConstUnique generate(
        candymaker::FileData::const_iterator &
        , const candymaker::FileData::const_iterator &
    );

    candymaker::FileData toFileData(
    ) const;

    static candymaker::FileData stringToFileData(
        const std::string &
    );
};

#endif  // CANDYMAKER_CORE_DATAELEMENT_STRING_H
