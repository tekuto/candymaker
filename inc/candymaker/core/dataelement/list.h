﻿#ifndef CANDYMAKER_CORE_DATAELEMENT_LIST_H
#define CANDYMAKER_CORE_DATAELEMENT_LIST_H

#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/dataelement.h"
#include "candymaker/def/core/common/filedata.h"

#include <vector>

struct FgDataElementList
{
    using List = std::vector< fg::DataElement::ConstUnique >;

    List    list;

    FgDataElementList(
    ) = default;

    FgDataElementList(
        const FgDataElementList &
    );

    FgDataElementList(
        List &&
    );

    inline fg::DataElementList::ConstUnique clone(
    ) const
    {
        return new FgDataElementList( *this );
    }

    static fg::DataElementList::ConstUnique generate(
        candymaker::FileData::const_iterator &
        , const candymaker::FileData::const_iterator &
    );

    candymaker::FileData toFileData(
    ) const;
};

#endif  // CANDYMAKER_CORE_DATAELEMENT_LIST_H
