﻿#ifndef CANDYMAKER_CORE_DATAELEMENT_MAP_MAP_H
#define CANDYMAKER_CORE_DATAELEMENT_MAP_MAP_H

#include "fg/core/dataelement/map/map.h"
#include "fg/core/dataelement/dataelement.h"
#include "candymaker/def/core/common/filedata.h"

#include <map>
#include <string>

struct FgDataElementMap
{
    using Map = std::map<
        std::string
        , fg::DataElement::ConstUnique
    >;

    Map map;

    FgDataElementMap(
    ) = default;

    FgDataElementMap(
        const FgDataElementMap &
    );

    FgDataElementMap(
        Map &&
    );

    inline fg::DataElementMap::ConstUnique clone(
    ) const
    {
        return new FgDataElementMap( *this );
    }

    static fg::DataElementMap::ConstUnique generate(
        candymaker::FileData::const_iterator &
        , const candymaker::FileData::const_iterator &
    );

    candymaker::FileData toFileData(
    ) const;
};

#endif  // CANDYMAKER_CORE_DATAELEMENT_MAP_MAP_H
