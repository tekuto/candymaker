﻿#ifndef CANDYMAKER_CORE_DATAELEMENT_MAP_KEYS_H
#define CANDYMAKER_CORE_DATAELEMENT_MAP_KEYS_H

#include "fg/core/dataelement/map/keys.h"

#include <vector>
#include <string>

struct FgDataElementMapKeys
{
    using Keys = std::vector< std::string >;

    Keys    keys;
};

#endif  // CANDYMAKER_CORE_DATAELEMENT_MAP_KEYS_H
