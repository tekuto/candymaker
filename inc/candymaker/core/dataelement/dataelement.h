﻿#ifndef CANDYMAKER_CORE_DATAELEMENT_DATAELEMENT_H
#define CANDYMAKER_CORE_DATAELEMENT_DATAELEMENT_H

#include "fg/core/dataelement/dataelement.h"
#include "fg/core/dataelement/string.h"
#include "fg/core/dataelement/list.h"
#include "fg/core/dataelement/map/map.h"
#include "candymaker/def/core/common/filedata.h"

#include <utility>
#include <string>

struct FgDataElement
{
    fg::DataElementString::ConstUnique  stringUnique;
    fg::DataElementList::ConstUnique    listUnique;
    fg::DataElementMap::ConstUnique     mapUnique;

    FgDataElement(
        fg::DataElementString::ConstUnique &&
    );

    FgDataElement(
        fg::DataElementList::ConstUnique &&
    );

    FgDataElement(
        fg::DataElementMap::ConstUnique &&
    );

    FgDataElement(
        const FgDataElement &
    );

    inline static fg::DataElement::ConstUnique create(
        fg::DataElementString::ConstUnique &&   _stringUnique
    )
    {
        return new FgDataElement( std::move( _stringUnique ) );
    }

    inline static fg::DataElement::ConstUnique create(
        fg::DataElementList::ConstUnique && _listUnique
    )
    {
        return new FgDataElement( std::move( _listUnique ) );
    }

    inline static fg::DataElement::ConstUnique create(
        fg::DataElementMap::ConstUnique &&  _mapUnique
    )
    {
        return new FgDataElement( std::move( _mapUnique ) );
    }

    inline fg::DataElement::ConstUnique clone(
    ) const
    {
        return new FgDataElement( *this );
    }

    static fg::DataElement::ConstUnique create(
        const candymaker::FileData &
    );

    static fg::DataElement::ConstUnique create(
        candymaker::FileData::const_iterator &
        , const candymaker::FileData::const_iterator &
    );

    static candymaker::FileData::const_iterator getNextToken(
        const candymaker::FileData::const_iterator &
        , const candymaker::FileData::const_iterator &
    );

    candymaker::FileData toFileData(
    ) const;
};

#endif  // CANDYMAKER_CORE_DATAELEMENT_DATAELEMENT_H
