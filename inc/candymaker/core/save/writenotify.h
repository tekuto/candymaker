﻿#ifndef CANDYMAKER_CORE_SAVE_WRITENOTIFY_H
#define CANDYMAKER_CORE_SAVE_WRITENOTIFY_H

#include "fg/core/save/writenotify.h"
#include "fg/core/state/state.h"

struct FgSaveWriteNotify
{
    FgState &   state;
    bool        wrote;
};

#endif  // CANDYMAKER_CORE_SAVE_WRITENOTIFY_H
