﻿#ifndef CANDYMAKER_CORE_SAVE_WRITER_H
#define CANDYMAKER_CORE_SAVE_WRITER_H

#include "fg/core/save/writer.h"
#include "fg/core/save/save.h"
#include "fg/core/save/writenotify.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"

#include <string>

struct FgSaveWriter
{
    fg::State<> &           state;
    const std::string       HOME_DIRECTORY_PATH;
    const std::string       SAVE_DIRECTORY_PATH;
    const std::string       FILE_NAME;
    fg::Save::ConstUnique   saveUnique;
    FgSaveWriteNotifyProc   writeNotifyProc;
    fg::StateJoiner::Unique joinerUnique;

    FgSaveWriter(
        FgState *
        , const std::string &
        , const std::string &
        , const char *
        , const FgSave *
        , FgSaveWriteNotifyProc
    );
};

#endif  // CANDYMAKER_CORE_SAVE_WRITER_H
