﻿#ifndef CANDYMAKER_CORE_SAVE_REMOVENOTIFY_H
#define CANDYMAKER_CORE_SAVE_REMOVENOTIFY_H

#include "fg/core/save/removenotify.h"
#include "fg/core/state/state.h"

struct FgSaveRemoveNotify
{
    FgState &   state;
    bool        removed;
};

#endif  // CANDYMAKER_CORE_SAVE_REMOVENOTIFY_H
