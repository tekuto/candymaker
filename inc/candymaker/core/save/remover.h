﻿#ifndef CANDYMAKER_CORE_SAVE_REMOVER_H
#define CANDYMAKER_CORE_SAVE_REMOVER_H

#include "fg/core/save/remover.h"
#include "fg/core/save/removenotify.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"

#include <string>

struct FgSaveRemover
{
    fg::State<> &           state;
    const std::string       HOME_DIRECTORY_PATH;
    const std::string       SAVE_DIRECTORY_PATH;
    const std::string       FILE_NAME;
    FgSaveRemoveNotifyProc  removeNotifyProc;
    fg::StateJoiner::Unique joinerUnique;

    FgSaveRemover(
        FgState *
        , const std::string &
        , const std::string &
        , const char *
        , FgSaveRemoveNotifyProc
    );
};

#endif  // CANDYMAKER_CORE_SAVE_REMOVER_H
