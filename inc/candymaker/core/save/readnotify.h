﻿#ifndef CANDYMAKER_CORE_SAVE_READNOTIFY_H
#define CANDYMAKER_CORE_SAVE_READNOTIFY_H

#include "fg/core/save/readnotify.h"
#include "fg/core/save/save.h"
#include "fg/core/state/state.h"

struct FgSaveReadNotify
{
    FgState &               state;
    fg::Save::ConstUnique   saveUnique;
};

#endif  // CANDYMAKER_CORE_SAVE_READNOTIFY_H
