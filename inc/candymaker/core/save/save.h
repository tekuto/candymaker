﻿#ifndef CANDYMAKER_CORE_SAVE_SAVE_H
#define CANDYMAKER_CORE_SAVE_SAVE_H

#include "fg/core/save/save.h"
#include "fg/core/dataelement/dataelement.h"

#include <string>

struct FgSave
{
    fg::DataElement::ConstUnique    DATA_ELEMENT_UNIQUE;

    static std::string createFilePath(
        const std::string &
        , const std::string &
    );

    static fg::Save::ConstUnique create(
        fg::DataElement::ConstUnique &&
    );
};

#endif  // CANDYMAKER_CORE_SAVE_SAVE_H
