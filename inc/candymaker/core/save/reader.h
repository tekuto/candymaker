﻿#ifndef CANDYMAKER_CORE_SAVE_READER_H
#define CANDYMAKER_CORE_SAVE_READER_H

#include "fg/core/save/reader.h"
#include "fg/core/save/readnotify.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"

#include <string>

struct FgSaveReader
{
    fg::State<> &           state;
    const std::string       HOME_DIRECTORY_PATH;
    const std::string       SAVE_DIRECTORY_PATH;
    const std::string       FILE_NAME;
    FgSaveReadNotifyProc    readNotifyProc;
    fg::StateJoiner::Unique joinerUnique;

    FgSaveReader(
        FgState *
        , const std::string &
        , const std::string &
        , const char *
        , FgSaveReadNotifyProc
    );
};

#endif  // CANDYMAKER_CORE_SAVE_READER_H
