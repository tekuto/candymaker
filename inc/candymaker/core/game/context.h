﻿#ifndef CANDYMAKER_CORE_GAME_CONTEXT_H
#define CANDYMAKER_CORE_GAME_CONTEXT_H

#include "fg/core/game/context.h"
#include "fg/core/state/state.h"
#include "fg/core/module/context.h"

#include <string>

struct FgGameContext
{
    FgState &           state;
    FgModuleContext &   moduleContext;
    const std::string   SAVE_DIRECTORY_PATH;

    FgGameContext(
        FgState &
        , FgModuleContext &
        , const std::string &
    );

    const FgModuleContext & getModuleContext(
    ) const;

    const std::string & getSaveDirectoryPath(
    ) const;

    void start(
    );
};

#endif  // CANDYMAKER_CORE_GAME_CONTEXT_H
