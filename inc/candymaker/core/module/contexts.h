﻿#ifndef CANDYMAKER_CORE_MODULE_CONTEXTS_H
#define CANDYMAKER_CORE_MODULE_CONTEXTS_H

#include "candymaker/def/core/module/contexts.h"
#include "candymaker/def/core/module/manager.h"
#include "candymaker/def/core/module/infouniques.h"
#include "candymaker/def/core/module/contextshares.h"
#include "fg/core/module/context.h"
#include "fg/common/unique.h"

#include <memory>
#include <string>

namespace candymaker {
    struct ModuleContexts : public fg::UniqueWrapper< ModuleContexts >
    {
        struct UnloadModuleContextShares
        {
            void operator()(
                ModuleContexts *
            ) const;
        };

        using ModuleContextSharesUnloader = std::unique_ptr<
            ModuleContexts
            , UnloadModuleContextShares
        >;

        ModuleManager & manager;

        ModuleContextShares         contextShares;
        ModuleContextSharesUnloader unloader;

        ModuleContexts(
            ModuleManager &
        );

        static Unique create(
            ModuleManager &
            , const ModuleInfoUniques &
        );

        const FgModuleContext & getMainModuleContext(
        ) const;

        FgModuleContext & getMainModuleContext(
        );

        void * getProc_(
            const std::string &
        );

        template< typename PROC_PTR_T >
        PROC_PTR_T getProc(
            const std::string & _SYMBOL
        )
        {
            return reinterpret_cast< PROC_PTR_T >( this->getProc_( _SYMBOL ) );
        }
    };
}

#endif  // CANDYMAKER_CORE_MODULE_CONTEXTS_H
