﻿#ifndef CANDYMAKER_CORE_MODULE_MANAGER_H
#define CANDYMAKER_CORE_MODULE_MANAGER_H

#include "candymaker/def/core/module/manager.h"
#include "candymaker/def/core/module/contextshares.h"
#include "candymaker/def/core/module/infouniques.h"
#include "fg/common/unique.h"

#include <string>
#include <map>

namespace candymaker {
    struct ModuleManager : public fg::UniqueWrapper< ModuleManager >
    {
        struct ModuleMapKey
        {
            const std::string   PACKAGE_PATH;
            const std::string   MODULE_PATH;
        };

        struct LessModuleMapKey
        {
            bool operator()(
                const ModuleMapKey &
                , const ModuleMapKey &
            ) const;
        };

        using ModuleMap = std::map<
            ModuleMapKey
            , ModuleContextShared
            , LessModuleMapKey
        >;

        const std::string   HOME_DIRECTORY_PATH;

        ModuleMap   moduleMap;

        ModuleManager(
            const std::string &
        );

        static Unique create(
            const std::string &
        );

        const std::string & getHomeDirectoryPath(
        ) const;

        void loadModules(
            ModuleContextShares &
            , const ModuleInfoUniques &
        );

        void unloadModules(
            ModuleContextShares &
        );
    };
}

#endif  // CANDYMAKER_CORE_MODULE_MANAGER_H
