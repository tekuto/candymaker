﻿#ifndef CANDYMAKER_CORE_MODULE_CONTEXT_H
#define CANDYMAKER_CORE_MODULE_CONTEXT_H

#include "candymaker/def/core/module/context.h"
#include "fg/def/core/module/context.h"

#include <memory>
#include <string>

struct FgModuleContext
{
    struct UnloadHandle
    {
        void operator()(
            void *
        ) const;
    };

    using HandleUnique = std::unique_ptr<
        void
        , UnloadHandle
    >;

    struct FreeData
    {
        void operator()(
            FgModuleContext *
        ) const;
    };

    using DataDestroyer = std::unique_ptr<
        FgModuleContext
        , FreeData
    >;

    const std::string   PACKAGE_PATH;
    const std::string   HOME_DIRECTORY_PATH;
    const std::string   SAVE_DIRECTORY_PATH;

    HandleUnique    handleUnique;

    void *                          dataPtr;
    FgModuleContextDataDestroyProc  dataDestroyProcPtr;
    DataDestroyer                   dataDestroyer;

    FgModuleContext(
        const std::string &
        , const std::string &
        , const std::string &
        , const std::string &
    );

    inline static auto create(
        const std::string &     _PACKAGE_PATH
        , const std::string &   _MODULE_PATH
        , const std::string &   _HOME_DIRECTORY_PATH
        , const std::string &   _SAVE_DIRECTORY_PATH
    )
    {
        return candymaker::ModuleContextShared(
            new FgModuleContext(
                _PACKAGE_PATH
                , _MODULE_PATH
                , _HOME_DIRECTORY_PATH
                , _SAVE_DIRECTORY_PATH
            )
        );
    }

    void initialize(
        const std::string &
    );

    void finalize(
    );

    const std::string & getPackagePath(
    ) const;

    const std::string & getHomeDirectoryPath(
    ) const;

    const std::string & getSaveDirectoryPath(
    ) const;

    void * getProc_(
        const std::string &
    );

    template< typename PROC_PTR_T >
    PROC_PTR_T getProc(
        const std::string & _SYMBOL
    )
    {
        return reinterpret_cast< PROC_PTR_T >( this->getProc_( _SYMBOL ) );
    }
};

#endif  // CANDYMAKER_CORE_MODULE_CONTEXT_H
