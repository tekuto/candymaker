﻿#ifndef CANDYMAKER_CORE_MODULE_INFO_H
#define CANDYMAKER_CORE_MODULE_INFO_H

#include "candymaker/def/core/module/info.h"
#include "fg/common/unique.h"

#include <string>

namespace candymaker {
    struct ModuleInfo : public fg::UniqueWrapper< ModuleInfo >
    {
        const std::string   PACKAGE_PATH;
        const std::string   MODULE_PATH;

        const std::string   SAVE_DIRECTORY_PATH;

        const bool          EXISTS_INITIALIZER_SYMBOL;
        const std::string   INITIALIZER_SYMBOL;

        ModuleInfo(
            const std::string &
            , const std::string &
            , const std::string &
            , const std::string &
        );

        ModuleInfo(
            const std::string &
            , const std::string &
            , const std::string &
        );

        inline static ConstUnique create(
            const std::string &     _PACKAGE_PATH
            , const std::string &   _MODULE_PATH
            , const std::string &   _SAVE_DIRECTORY_PATH
            , const std::string &   _INITIALIZER_SYMBOL
        )
        {
            return new ModuleInfo(
                _PACKAGE_PATH
                , _MODULE_PATH
                , _SAVE_DIRECTORY_PATH
                , _INITIALIZER_SYMBOL
            );
        }

        inline static ConstUnique create(
            const std::string &     _PACKAGE_PATH
            , const std::string &   _MODULE_PATH
            , const std::string &   _SAVE_DIRECTORY_PATH
        )
        {
            return new ModuleInfo(
                _PACKAGE_PATH
                , _MODULE_PATH
                , _SAVE_DIRECTORY_PATH
            );
        }

        const std::string & getPackagePath(
        ) const;

        const std::string & getModulePath(
        ) const;

        const std::string & getSaveDirectoryPath(
        ) const;

        bool isExistsInitializerSymbol(
        ) const;

        const std::string & getInitializerSymbol(
        ) const;
    };
}

#endif  // CANDYMAKER_CORE_MODULE_INFO_H
