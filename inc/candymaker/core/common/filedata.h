﻿#ifndef CANDYMAKER_CORE_COMMON_FILEDATA_H
#define CANDYMAKER_CORE_COMMON_FILEDATA_H

#include "candymaker/def/core/common/filedata.h"

#include <string>

namespace candymaker {
    bool readFile(
        FileData &
        , const std::string &
    );

    bool writeFile(
        const std::string &
        , const FileData &
    );
}

#endif  // CANDYMAKER_CORE_COMMON_FILEDATA_H
