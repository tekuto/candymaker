﻿#ifndef CANDYMAKER_CORE_FILE_PACKAGE_H
#define CANDYMAKER_CORE_FILE_PACKAGE_H

#include "candymaker/def/core/file/package.h"
#include "candymaker/def/core/file/path.h"
#include "fg/def/common/unique.h"

#include <string>
#include <vector>
#include <map>

namespace candymaker {
    struct PackageFile : public fg::UniqueWrapper< PackageFile >
    {
        struct Module
        {
            struct Interface
            {
                bool        existsPackage;
                std::string package;
                std::string module;
            };

            using Interfaces = std::vector< Interface >;

            struct Depend
            {
                bool        existsPackage;
                std::string package;
                std::string module;

                bool isLocalPackage(
                ) const;

                const std::string & getPackage(
                ) const;
            };

            using Depends = std::vector< Depend >;

            std::string at;
            Path        path;
            bool        existsInitializer;
            std::string initializer;
            Interfaces  interfaces;
            Depends     depends;

            const std::string & getAt(
            ) const;

            const Path & getPath(
            ) const;

            bool isExistsInitializer(
            ) const;

            const std::string & getInitializer(
            ) const;

            const Depends & getDepends(
            ) const;

            bool isImplementedModule(
                const Depend &
            ) const;
        };

        using Modules = std::map<
            std::string
            , Module
        >;

        struct Basesystem
        {
            std::string module;
            std::string initializer;

            const std::string & getModule(
            ) const;

            const std::string & getInitializer(
            ) const;
        };

        using Basesystems = std::map<
            std::string
            , Basesystem
        >;

        struct Game
        {
            std::string module;
            std::string initializer;

            const std::string & getModule(
            ) const;

            const std::string & getInitializer(
            ) const;
        };

        using Games = std::map<
            std::string
            , Game
        >;

        Modules     modules;
        Basesystems basesystems;
        Games       games;

        static std::string getFileName(
        );

        static std::string relativeDirectoryPathToString(
            const Path &
        );

        static std::string relativeFilePathToString(
            const Path &
            , const Path &
        );

        static std::string absoluteModulePathToString(
            const Path &
        );

        static std::string relativeModulePathToString(
            const Path &
        );

        PackageFile(
            Modules &&
            , Basesystems &&
            , Games &&
        );

        static ConstUnique create(
            const std::string &
        );

        const Module & getModule(
            const std::string &
        ) const;

        const Basesystem & getBasesystem(
            const std::string &
        ) const;

        const Game & getGame(
            const std::string &
        ) const;
    };
}

#endif  // CANDYMAKER_CORE_FILE_PACKAGE_H
