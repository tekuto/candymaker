﻿#ifndef CANDYMAKER_CORE_FILE_TEMPLATE_H
#define CANDYMAKER_CORE_FILE_TEMPLATE_H

#include "candymaker/def/core/file/template.h"
#include "candymaker/def/core/file/path.h"
#include "candymaker/core/file/execute.h"
#include "fg/common/unique.h"

#include <string>
#include <vector>

namespace candymaker {
    struct TemplateFile : public fg::UniqueWrapper< TemplateFile >
    {
        struct Template
        {
            std::string at;
            bool        existsPackage;
            Path        package;
            Path        file;
        };

        using Templates = std::vector< Template >;

        Templates   templates;

        ExecuteFile::Packages   packages;
        ExecuteFile::Basesystem basesystem;
        ExecuteFile::Game       game;

        static std::string relativeFilePathToString(
            const Path &
            , const Path &
        );

        TemplateFile(
            Templates &&
            , ExecuteFile::Packages &&
            , ExecuteFile::Basesystem &&
            , ExecuteFile::Game &&
        );

        static ConstUnique create(
            const std::string &
        );
    };
}

#endif  // CANDYMAKER_CORE_FILE_TEMPLATE_H
