﻿#ifndef CANDYMAKER_CORE_FILE_COMMON_H
#define CANDYMAKER_CORE_FILE_COMMON_H

#include "fg/core/dataelement/dataelement.h"

#include <string>

namespace candymaker {
    fg::DataElement::ConstUnique createDataElement(
        const std::string &
    );
}

#endif  // CANDYMAKER_CORE_FILE_COMMON_H
