﻿#ifndef CANDYMAKER_CORE_FILE_SHORTCUT_H
#define CANDYMAKER_CORE_FILE_SHORTCUT_H

#include "candymaker/def/core/file/shortcut.h"
#include "candymaker/def/core/file/path.h"
#include "fg/common/unique.h"

#include <string>

namespace candymaker {
    struct ShortcutFile : public fg::UniqueWrapper< ShortcutFile >
    {
        std::string at;
        bool        existsCurrentDirectory;
        Path        currentDirectory;
        Path        executeFile;

        ShortcutFile(
            std::string &&
            , bool &&
            , Path &&
            , Path &&
        );

        static ConstUnique create(
            const std::string &
        );

        bool atSystem(
        ) const;

        bool atHome(
        ) const;

        std::string createCurrentDirectory(
            const std::string &
        ) const;

        std::string createExecuteFilePath(
        ) const;
    };
}

#endif  // CANDYMAKER_CORE_FILE_SHORTCUT_H
