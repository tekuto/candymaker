﻿#ifndef CANDYMAKER_CORE_FILE_PATH_H
#define CANDYMAKER_CORE_FILE_PATH_H

#include "candymaker/def/core/file/path.h"
#include "fg/core/dataelement/list.h"

#include <string>

namespace candymaker {
    Path createPath(
        const fg::DataElementList &
    );

    std::string absoluteFilePathToString(
        const Path &
    );

    std::string relativeFilePathToString(
        const Path &
    );

    std::string absoluteDirectoryPathToString(
        const Path &
    );

    std::string relativeDirectoryPathToString(
        const Path &
    );

    std::string createDirectoryName(
        const std::string &
    );

    std::string createDirectoryPath(
        const std::string &
    );
}

#endif  // CANDYMAKER_CORE_FILE_PATH_H
