﻿#ifndef CANDYMAKER_CORE_FILE_EXECUTE_H
#define CANDYMAKER_CORE_FILE_EXECUTE_H

#include "candymaker/def/core/file/execute.h"
#include "candymaker/def/core/file/path.h"
#include "fg/common/unique.h"

#include <string>
#include <vector>
#include <map>

namespace candymaker {
    struct ExecuteFile : public fg::UniqueWrapper< ExecuteFile >
    {
        struct Save
        {
            bool    existsPath;
            bool    defaultPath;
            Path    path;
        };

        struct Package
        {
            using Modules = std::map<
                std::string
                , bool
            >;

            std::string at;
            bool        existsPath;
            Path        path;
            Modules     modules;

            Save    save;

            std::string relativeSaveDirectoryPathToString(
            ) const;
        };

        using Packages = std::vector< Package >;

        struct Basesystem
        {
            bool        existsPath;
            std::string at;
            bool        existsPackage;
            Path        package;
            std::string name;

            Save    save;

            std::string relativeSaveDirectoryPathToString(
            ) const;
        };

        struct Game
        {
            bool        existsPath;
            std::string at;
            bool        existsPackage;
            Path        package;
            std::string name;

            Save    save;

            std::string relativeSaveDirectoryPathToString(
            ) const;
        };

        Packages    packages;
        Basesystem  basesystem;
        Game        game;

        static std::string relativePathToString(
            const Path &
        );

        ExecuteFile(
            Packages &&
            , Basesystem &&
            , Game &&
        );

        static ConstUnique create(
            const std::string &
            , const std::string &
            , const std::string &
        );

        static ConstUnique create(
            const std::string &
            , const std::string &
        );
    };
}

#endif  // CANDYMAKER_CORE_FILE_EXECUTE_H
