﻿#ifndef CANDYMAKER_CORE_BASESYSTEM_CONTEXT_H
#define CANDYMAKER_CORE_BASESYSTEM_CONTEXT_H

#include "fg/core/basesystem/context.h"
#include "fg/core/state/state.h"
#include "fg/core/module/context.h"

#include <memory>

struct FgBasesystemContext
{
    struct WaitEnd
    {
        void operator()(
            FgBasesystemContext *
        ) const;
    };

    using Waiter = std::unique_ptr<
        FgBasesystemContext
        , WaitEnd
    >;

    FgState &           state;
    FgModuleContext &   moduleContext;
    const std::string   SAVE_DIRECTORY_PATH;

    FgBasesystemContextWaitEndProc  waitEndProcPtr;
    void *                          waitEndDataPtr;
    Waiter                          waiter;

    FgBasesystemContext(
        FgState &
        , FgModuleContext &
        , const std::string &
    );

    const FgModuleContext & getModuleContext(
    ) const;

    const std::string & getSaveDirectoryPath(
    ) const;

    void waitEnd(
    );
};

#endif  // CANDYMAKER_CORE_BASESYSTEM_CONTEXT_H
