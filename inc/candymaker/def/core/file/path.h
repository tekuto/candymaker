﻿#ifndef CANDYMAKER_DEF_CORE_FILE_PATH_H
#define CANDYMAKER_DEF_CORE_FILE_PATH_H

#include <vector>
#include <string>

namespace candymaker {
    using Path = std::vector< std::string >;
}

#endif  // CANDYMAKER_DEF_CORE_FILE_PATH_H
