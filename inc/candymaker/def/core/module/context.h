﻿#ifndef CANDYMAKER_DEF_CORE_MODULE_CONTEXT_H
#define CANDYMAKER_DEF_CORE_MODULE_CONTEXT_H

#include "fg/def/core/module/context.h"

#include <memory>

namespace candymaker {
    using ModuleContextShared = std::shared_ptr< FgModuleContext >;
}

#endif  // CANDYMAKER_DEF_CORE_MODULE_CONTEXT_H
