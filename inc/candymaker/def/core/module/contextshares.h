﻿#ifndef CANDYMAKER_DEF_CORE_MODULE_CONTEXTSHARES_H
#define CANDYMAKER_DEF_CORE_MODULE_CONTEXTSHARES_H

#include "candymaker/def/core/module/context.h"

#include <vector>

namespace candymaker {
    using ModuleContextShares = std::vector< ModuleContextShared >;
}

#endif  // CANDYMAKER_DEF_CORE_MODULE_CONTEXTSHARES_H
