﻿#ifndef CANDYMAKER_DEF_CORE_MODULE_INFOUNIQUES_H
#define CANDYMAKER_DEF_CORE_MODULE_INFOUNIQUES_H

#include "candymaker/core/module/info.h"

#include <vector>

namespace candymaker {
    using ModuleInfoUniques = std::vector< ModuleInfo::ConstUnique >;
}

#endif  // CANDYMAKER_DEF_CORE_MODULE_INFOUNIQUES_H
