﻿#ifndef CANDYMAKER_DEF_CORE_COMMON_FILEDATA_H
#define CANDYMAKER_DEF_CORE_COMMON_FILEDATA_H

#include <vector>

namespace candymaker {
    using FileData = std::vector< char >;
}

#endif  // CANDYMAKER_DEF_CORE_COMMON_FILEDATA_H
