﻿#ifndef CANDYMAKER_DEF_CORE_THREAD_THREAD_H
#define CANDYMAKER_DEF_CORE_THREAD_THREAD_H

namespace candymaker {
    class ThreadImpl;

    using ThreadProcImpl = void ( * )(
        ThreadImpl &
    );

    template< typename >
    class Thread;

    template< typename USER_DATA_T >
    using ThreadProc = void ( * )(
        Thread< USER_DATA_T > &
    );
}

#endif  // CANDYMAKER_DEF_CORE_THREAD_THREAD_H
