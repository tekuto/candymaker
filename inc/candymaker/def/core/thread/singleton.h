﻿#ifndef CANDYMAKER_DEF_CORE_THREAD_SINGLETON_H
#define CANDYMAKER_DEF_CORE_THREAD_SINGLETON_H

namespace candymaker {
    class ThreadSingletonImpl;

    using ThreadSingletonProcImpl = void ( * )(
        ThreadSingletonImpl &
    );

    template< typename >
    class ThreadSingleton;

    template< typename USER_DATA_T >
    using ThreadSingletonProc = void ( * )(
        ThreadSingleton< USER_DATA_T > &
    );
}

#endif  // CANDYMAKER_DEF_CORE_THREAD_SINGLETON_H
