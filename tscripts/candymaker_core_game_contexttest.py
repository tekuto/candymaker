# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-game-contexttest'

module.SOURCE = {
    'core' : {
        'game' : [
            'contexttest.cpp',
            'context.cpp',
        ],
        'state' : [
            'state.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
            'event.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
    },
}
