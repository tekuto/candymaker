# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-state-eventbackgroundtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'eventbackgroundtest.cpp',
            'eventbackground.cpp',
            'eventimpl.cpp',
        ],
    },
}
