# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_executetest',
    'candymaker_testdata_executetest_packages_package',
    'candymaker_testdata_executetest_system_packages_package',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-file-executetest'

module.SOURCE = {
    'core' : {
        'file' : [
            'executetest.cpp',
            'execute.cpp',
            'template.cpp',
            'package.cpp',
            'path.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
    },
}
