# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_test_modulecontexttest_newtest',
    'candymaker_test_modulecontexttest_getproctest',
    'candymaker_test_modulecontexttest_initializetest',
    'candymaker_test_modulecontexttest_finalizetest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-module-contexttest'

module.SOURCE = {
    'core' : {
        'module' : [
            'contexttest.cpp',
            'context.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
