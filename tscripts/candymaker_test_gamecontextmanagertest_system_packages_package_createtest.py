# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'gamecontextmanagertest',
    'system',
    'packages',
    'package',
    'createtest',
)

module.SOURCE = {
    'test' : {
        'gamecontextmanagertest' : {
            'system' : {
                'packages' : {
                    'package' : [
                        'createtest.cpp',
                    ],
                },
            },
        },
    },
}
