# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-dataelement-listtest'

module.SOURCE = {
    'core' : {
        'dataelement' : [
            'listtest.cpp',
            'list.cpp',
            'dataelement.cpp',
            'string.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
    }
}
