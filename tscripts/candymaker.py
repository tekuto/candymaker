# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_main_argstest',
    'candymaker_main_statemanagertest',
    'candymaker_main_basesystemcontextmanagertest',
    'candymaker_main_gamecontextmanagertest',
    'candymaker_main_moduletest',
    'candymaker_main_packagefiletest',
    'candymaker_core_module_infotest',
    'candymaker_core_module_contexttest',
    'candymaker_core_module_managertest',
    'candymaker_core_module_contextstest',
    'candymaker_core_basesystem_contexttest',
    'candymaker_core_game_contexttest',
    'candymaker_core_package_resourcetest',
    'candymaker_core_dataelement_dataelementtest',
    'candymaker_core_dataelement_stringtest',
    'candymaker_core_dataelement_listtest',
    'candymaker_core_dataelement_map_maptest',
    'candymaker_core_dataelement_map_keystest',
    'candymaker_core_state_statetest',
    'candymaker_core_state_creatingtest',
    'candymaker_core_state_threadimpltest',
    'candymaker_core_state_threadtest',
    'candymaker_core_state_threadsingletontest',
    'candymaker_core_state_threadbackgroundtest',
    'candymaker_core_state_threadbackgroundsingletontest',
    'candymaker_core_state_eventimpltest',
    'candymaker_core_state_eventtest',
    'candymaker_core_state_eventbackgroundtest',
    'candymaker_core_state_reentereventtest',
    'candymaker_core_state_joinertest',
    'candymaker_core_state_eventmanagertest',
    'candymaker_core_state_eventregistermanagertest',
    'candymaker_core_save_savetest',
    'candymaker_core_save_writertest',
    'candymaker_core_save_writenotifytest',
    'candymaker_core_save_readertest',
    'candymaker_core_save_readnotifytest',
    'candymaker_core_save_removertest',
    'candymaker_core_save_removenotifytest',
    'candymaker_core_thread_threadtest',
    'candymaker_core_thread_singletontest',
    'candymaker_core_thread_joinertest',
    'candymaker_core_thread_cachetest',
    'candymaker_core_file_shortcuttest',
    'candymaker_core_file_templatetest',
    'candymaker_core_file_executetest',
    'candymaker_core_file_packagetest',
    'candymaker_core_file_pathtest',
    'candymaker_core_file_commontest',
    'candymaker_core_common_filedatatest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'candymaker'

module.SOURCE = {
    'main' : [
        'main.cpp',
        'args.cpp',
        'statemanager.cpp',
        'basesystemcontextmanager.cpp',
        'gamecontextmanager.cpp',
        'module.cpp',
        'packagefile.cpp',
    ],
    'core' : {
        'module' : [
            'contexts.cpp',
            'info.cpp',
            'context.cpp',
            'manager.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'package' : [
            'resource.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'state' : [
            'creating.cpp',
            'event.cpp',
            'eventbackground.cpp',
            'eventimpl.cpp',
            'eventmanager.cpp',
            'eventregistermanager.cpp',
            'joiner.cpp',
            'reenterevent.cpp',
            'state.cpp',
            'thread.cpp',
            'threadbackground.cpp',
            'threadbackgroundsingleton.cpp',
            'threadimpl.cpp',
            'threadsingleton.cpp',
        ],
        'save' : [
            'save.cpp',
            'writer.cpp',
            'writenotify.cpp',
            'reader.cpp',
            'readnotify.cpp',
            'remover.cpp',
            'removenotify.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'singleton.cpp',
            'thread.cpp',
        ],
        'file' : [
            'shortcut.cpp',
            'template.cpp',
            'execute.cpp',
            'package.cpp',
            'path.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
