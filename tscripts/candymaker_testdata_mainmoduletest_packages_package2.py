# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = os.path.join(
    'mainmoduletest',
    'packages',
    'package2',
)

module.SOURCE = {
    'testdata' : {
        'mainmoduletest' : {
            'packages' : {
                'package2' : [
                    'package.fgp',
                ]
            }
        }
    }
}
