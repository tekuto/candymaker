# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_test_modulecontextstest_newtest',
    'candymaker_test_modulecontextstest_getproctest1',
    'candymaker_test_modulecontextstest_getproctest2',
    'candymaker_test_modulecontextstest_deletetest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-module-contextstest'

module.SOURCE = {
    'core' : {
        'module' : [
            'contextstest.cpp',
            'contexts.cpp',
            'manager.cpp',
            'context.cpp',
            'info.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
