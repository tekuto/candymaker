# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_test_packageresourcetest_newtest',
    'candymaker_testdata_packageresourcetest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-package-resourcetest'

module.SOURCE = {
    'core' : {
        'package' : [
            'resourcetest.cpp',
            'resource.cpp',
        ],
        'module' : [
            'context.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'state' : [
            'state.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
        'file' : [
            'path.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
        'common' : [
            'filedata.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
