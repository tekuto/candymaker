# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = os.path.join(
    'savewritertest',
    'savefailed',
    'saves',
    'savedirectory',
    'savename.fgs',
)

module.SOURCE = {
    'testdata' : {
        'savewritertest' : {
            'savefailed' : {
                'saves' : {
                    'savedirectory' : {
                        'savename.fgs' : [
                            'dummy.txt',
                        ]
                    }
                }
            }
        }
    }
}
