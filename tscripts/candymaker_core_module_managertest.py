# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_test_modulemanagertest_loadtest1',
    'candymaker_test_modulemanagertest_loadtest2',
    'candymaker_test_modulemanagertest_loadtest3',
    'candymaker_test_modulemanagertest_loadtest4',
    'candymaker_test_modulemanagertest_loadtest5',
    'candymaker_test_modulemanagertest_unloadtest1',
    'candymaker_test_modulemanagertest_unloadtest2',
    'candymaker_test_modulemanagertest_loadordertest1',
    'candymaker_test_modulemanagertest_loadordertest2',
    'candymaker_test_modulemanagertest_loadordertest3',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-module-managertest'

module.SOURCE = {
    'core' : {
        'module' : [
            'managertest.cpp',
            'manager.cpp',
            'context.cpp',
            'info.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
