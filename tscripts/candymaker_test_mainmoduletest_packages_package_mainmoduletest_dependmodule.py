# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'mainmoduletest',
    'packages',
    'package',
    'mainmoduletest_dependmodule',
)

module.SOURCE = {
    'test' : {
        'mainmoduletest' : {
            'packages' : {
                'package' : [
                    'mainmoduletest_dependmodule.cpp',
                ],
            },
        },
    },
}
