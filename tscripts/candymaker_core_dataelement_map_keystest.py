# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-dataelement-map-keystest'

module.SOURCE = {
    'core' : {
        'dataelement' : [
            {
                'map' : [
                    'keystest.cpp',
                    'keys.cpp',
                    'map.cpp',
                ],
            },
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
        ],
    }
}
