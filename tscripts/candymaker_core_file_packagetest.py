# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_packagetest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-file-packagetest'

module.SOURCE = {
    'core' : {
        'file' : [
            'packagetest.cpp',
            'package.cpp',
            'path.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
    },
}
