# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'basesystemcontextmanagertest',
    'packages',
    'package',
    'waitendtest',
)

module.SOURCE = {
    'test' : {
        'basesystemcontextmanagertest' : {
            'packages' : {
                'package' : [
                    'waitendtest.cpp',
                ],
            },
        },
    },
}
