# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-readnotifytest'

module.SOURCE = {
    'core' : {
        'save' : [
            'readnotifytest.cpp',
            'readnotify.cpp',
            'save.cpp',
        ],
        'file' : [
            'path.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
    }
}
