# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'modulemanagertest',
    'loadtest4',
)

module.SOURCE = {
    'test' : {
        'modulemanagertest' : [
            'loadtest4.cpp',
        ],
    },
}
