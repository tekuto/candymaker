# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-savetest'

module.SOURCE = {
    'core' : {
        'save' : [
            'savetest.cpp',
            'save.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'file' : [
            'path.cpp',
        ],
    }
}
