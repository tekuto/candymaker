# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_mainpackagefiletest_packages_package',
    'candymaker_testdata_mainpackagefiletest_system_packages_package',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-main-packagefiletest'

module.SOURCE = {
    'main' : [
        'packagefiletest.cpp',
        'packagefile.cpp',
    ],
    'core' : {
        'file' : [
            'package.cpp',
            'path.cpp',
            'common.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'common' : [
            'filedata.cpp',
        ],
    },
}
