# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'basesystemcontextmanagertest'

module.SOURCE = {
    'testdata' : {
        'basesystemcontextmanagertest' : [
            'createtest.fge',
            'createtest_withoutcurrentdirectory.fge',
            'waitendtest.fge',
        ]
    }
}
