# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_basesystemcontextmanagertest',
    'candymaker_testdata_basesystemcontextmanagertest_packages_package',
    'candymaker_testdata_basesystemcontextmanagertest_system_packages_package',
    'candymaker_test_basesystemcontextmanagertest_packages_package_createtest',
    'candymaker_test_basesystemcontextmanagertest_system_packages_package_createtest',
    'candymaker_test_basesystemcontextmanagertest_packages_package_waitendtest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-main-basesystemcontextmanagertest'

module.SOURCE = {
    'main' : [
        'basesystemcontextmanagertest.cpp',
        'basesystemcontextmanager.cpp',
        'module.cpp',
        'packagefile.cpp',
    ],
    'core' : {
        'basesystem' : [
            'context.cpp',
        ],
        'file' : [
            'execute.cpp',
            'template.cpp',
            'path.cpp',
            'package.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'module' : [
            'manager.cpp',
            'context.cpp',
            'contexts.cpp',
            'info.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'thread.cpp',
            'singleton.cpp',
            'joiner.cpp',
        ],
        'state' : [
            'state.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'joiner.cpp',
            'reenterevent.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
