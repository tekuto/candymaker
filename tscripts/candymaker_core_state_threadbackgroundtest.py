# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-state-threadbackgroundtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'threadbackgroundtest.cpp',
            'threadbackground.cpp',
            'threadimpl.cpp',
        ],
    },
}
