# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_savereadertest_saves_savedirectory',
    'candymaker_test_savereadertest_savereadertest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-readertest'

module.SOURCE = {
    'core' : {
        'save' : [
            'readertest.cpp',
            'reader.cpp',
            'save.cpp',
            'readnotify.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
        'module' : [
            'context.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'state' : [
            'state.cpp',
            'threadbackground.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
        'file' : [
            'path.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
    }
}

module.LIB = [
    'dl',
]
