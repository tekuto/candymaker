# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'basesystemcontextmanagertest',
    'packages',
    'package',
    'createtest',
)

module.SOURCE = {
    'test' : {
        'basesystemcontextmanagertest' : {
            'packages' : {
                'package' : [
                    'createtest.cpp',
                ],
            },
        },
    },
}
