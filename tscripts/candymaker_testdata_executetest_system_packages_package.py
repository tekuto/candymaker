# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = os.path.join(
    'executetest',
    'system',
    'packages',
    'package',
)

module.SOURCE = {
    'testdata' : {
        'executetest' : {
            'system' : {
                'packages' : {
                    'package' : [
                        'createtest_packages.fgt',
                        'createtest_basesystem.fgt',
                        'createtest_game.fgt',
                        'createtest_currenttemplate.fgt',
                        'createtest_currentpackages.fgt',
                        'createtest_currentbasesystem.fgt',
                        'createtest_currentgame.fgt',
                        'createtest_selftemplate.fgt',
                        'createtest_selfpackage.fgt',
                        'createtest_selfbasesystem.fgt',
                        'createtest_selfgame.fgt',
                    ],
                },
            },
        },
    },
}
