# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-state-eventregistermanagertest'

module.SOURCE = {
    'core' : {
        'state' : [
            'eventregistermanagertest.cpp',
            'eventregistermanager.cpp',
            'eventmanager.cpp',
            'state.cpp',
            'joiner.cpp',
            'creating.cpp',
            'eventimpl.cpp',
            'event.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'joiner.cpp',
            'cache.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
    },
}
