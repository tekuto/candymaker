# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'shortcuttest'

module.SOURCE = {
    'testdata' : {
        'shortcuttest' : [
            'createtest.fgs',
            'createtest_withoutcurrentdirpath.fgs',
            'atsystemtest_true.fgs',
            'atsystemtest_false.fgs',
            'atsystemtest_failed.fgs',
            'athometest_true.fgs',
            'athometest_false.fgs',
            'athometest_failed.fgs',
            'createcurrentdirectorytest_atabsolute.fgs',
            'createcurrentdirectorytest_atcurrent.fgs',
            'createcurrentdirectorytest_failed.fgs',
            'createcurrentdirectorytest_failedatabsolute.fgs',
            'createcurrentdirectorytest_failedatcurrent.fgs',
            'createexecutefilepathtest.fgs',
        ]
    }
}
