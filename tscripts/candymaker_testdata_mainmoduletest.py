# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'mainmoduletest'

module.SOURCE = {
    'testdata' : {
        'mainmoduletest' : [
            'mainmoduletest_currentpackage.fge',
            'mainmoduletest_systempackage.fge',
            'mainmoduletest_absolutemodulepath.fge',
            'mainmoduletest_withinitializer.fge',
            'mainmoduletest_withdependmodules.fge',
            'mainmoduletest_withdependlocalmodules.fge',
            'mainmoduletest_failedilligalpackageat.fge',
            'mainmoduletest_failedilligalmoduleat.fge',
            'mainmoduletest_faileddisablemodule.fge',
            'mainmoduletest_failedconflictdepend.fge',
        ]
    }
}
