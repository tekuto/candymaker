# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_filedatatest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-common-filedatatest'

module.SOURCE = {
    'core' : {
        'common' : [
            'filedatatest.cpp',
            'filedata.cpp',
        ],
    },
}
