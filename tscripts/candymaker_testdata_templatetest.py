# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'templatetest'

module.SOURCE = {
    'testdata' : {
        'templatetest' : [
            'createtest_templates.fge',
            'createtest_templatewithoutpackagepath.fge',
            'createtest_packages.fge',
            'createtest_packagewithsavepath.fge',
            'createtest_packagedefaultsavepath.fge',
            'createtest_packagewithoutpackagepath.fge',
            'createtest_basesystem.fge',
            'createtest_basesystemwithsavepath.fge',
            'createtest_basesystemonlysavepath.fge',
            'createtest_basesystemdefaultsavepath.fge',
            'createtest_basesystemwithoutpackagepath.fge',
            'createtest_game.fge',
            'createtest_gamewithsavepath.fge',
            'createtest_gameonlysavepath.fge',
            'createtest_gamedefaultsavepath.fge',
            'createtest_gamewithoutpackagepath.fge',
        ]
    }
}
