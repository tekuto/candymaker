# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-main-argstest'

module.SOURCE = {
    'main' : [
        'argstest.cpp',
        'args.cpp',
    ],
}
