# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-writenotifytest'

module.SOURCE = {
    'core' : {
        'save' : [
            'writenotifytest.cpp',
            'writenotify.cpp',
        ],
    }
}
