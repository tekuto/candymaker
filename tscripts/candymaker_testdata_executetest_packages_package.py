# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = os.path.join(
    'executetest',
    'packages',
    'package',
)

module.SOURCE = {
    'testdata' : {
        'executetest' : {
            'packages' : {
                'package' : [
                    'createtest_packages.fgt',
                    'createtest_basesystem.fgt',
                    'createtest_game.fgt',
                    'createtest_selftemplate.fgt',
                    'createtest_selfpackage.fgt',
                    'createtest_selfbasesystem.fgt',
                    'createtest_selfgame.fgt',
                    'createtest_mergepackage1.fgt',
                    'createtest_mergepackage2.fgt',
                    'createtest_mergepackagemodules1.fgt',
                    'createtest_mergepackagemodules2.fgt',
                    'createtest_mergepackagesave1.fgt',
                    'createtest_mergepackagesave2.fgt',
                    'createtest_mergebasesystempath1.fgt',
                    'createtest_mergebasesystempath2.fgt',
                    'createtest_mergebasesystempath3.fgt',
                    'createtest_mergebasesystemsave1.fgt',
                    'createtest_mergebasesystemsave2.fgt',
                    'createtest_mergebasesystemsave3.fgt',
                    'createtest_mergegamepath1.fgt',
                    'createtest_mergegamepath2.fgt',
                    'createtest_mergegamepath3.fgt',
                    'createtest_mergegamesave1.fgt',
                    'createtest_mergegamesave2.fgt',
                    'createtest_mergegamesave3.fgt',
                    'createtest_failedduplicatetemplate1.fgt',
                    'createtest_failedduplicatetemplate2.fgt',
                    'createtest_failedduplicatetemplate3.fgt',
                    'createtest_failedexiststemplatepathatself.fgt',
                    'createtest_failedexistspackagepathatself.fgt',
                    'createtest_failedexistsbasesystempathatself.fgt',
                    'createtest_failedexistsgamepathatself.fgt',
                ],
            },
        },
    },
}
