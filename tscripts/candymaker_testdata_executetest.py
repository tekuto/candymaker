# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'executetest'

module.SOURCE = {
    'testdata' : {
        'executetest' : [
            'createtest_withcurrentdirectory.fge',
            'createtest_withoutcurrentdirectory.fge',
            'createtest_atcurrentpackages.fge',
            'createtest_atcurrentbasesystem.fge',
            'createtest_atcurrentgame.fge',
            'createtest_atcurrentselftemplate.fge',
            'createtest_atcurrentselfpackage.fge',
            'createtest_atcurrentselfbasesystem.fge',
            'createtest_atcurrentselfgame.fge',
            'createtest_atsystempackages.fge',
            'createtest_atsystembasesystem.fge',
            'createtest_atsystemgame.fge',
            'createtest_atsystemcurrenttemplate.fge',
            'createtest_atsystemcurrentpackages.fge',
            'createtest_atsystemcurrentbasesystem.fge',
            'createtest_atsystemcurrentgame.fge',
            'createtest_atsystemselftemplate.fge',
            'createtest_atsystemselfpackage.fge',
            'createtest_atsystemselfbasesystem.fge',
            'createtest_atsystemselfgame.fge',
            'createtest_mergepackages.fge',
            'createtest_mergepackagemodules.fge',
            'createtest_mergepackagesave.fge',
            'createtest_mergebasesystempathattemplate.fge',
            'createtest_mergebasesystempathatexecute.fge',
            'createtest_mergebasesystemsaveattemplate.fge',
            'createtest_mergebasesystemsaveatexecute.fge',
            'createtest_mergegamepathattemplate.fge',
            'createtest_mergegamepathatexecute.fge',
            'createtest_mergegamesaveattemplate.fge',
            'createtest_mergegamesaveatexecute.fge',
            'createtest_failednotexistsbasesystempath.fge',
            'createtest_failednotexistsgamepath.fge',
            'createtest_failedduplicatetemplate.fge',
            'createtest_failedexiststemplatepathatself.fge',
            'createtest_failedexistspackagepathatself.fge',
            'createtest_failedexistsbasesystempathatself.fge',
            'createtest_failedexistsgamepathatself.fge',
            'createtest_failedexecutefileselftemplate.fge',
            'createtest_failedexecutefileselfpackage.fge',
            'createtest_failedexecutefileselfbasesystem.fge',
            'createtest_failedexecutefileselfgame.fge',
            'savedirectorypathtostringtest.fge',
            'savedirectorypathtostringtest_withdefaultsavedirectory.fge',
            'savedirectorypathtostringtest_withoutsavedirectory.fge',
        ]
    }
}
