# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_removertest_saves_savedirectory',
    'candymaker_test_removertest_removertest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-removertest'

module.SOURCE = {
    'core' : {
        'save' : [
            'removertest.cpp',
            'remover.cpp',
            'save.cpp',
            'removenotify.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
        'module' : [
            'context.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'state' : [
            'state.cpp',
            'threadbackground.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
        'file' : [
            'path.cpp',
        ],
    }
}

module.LIB = [
    'dl',
]
