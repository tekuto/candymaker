# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_mainmoduletest',
    'candymaker_testdata_mainmoduletest_packages_package',
    'candymaker_testdata_mainmoduletest_packages_package2',
    'candymaker_testdata_mainmoduletest_system_packages_package',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_withinitializer',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_withdependmodules',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_withdependlocalmodules',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_dependmodule',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_dependmodule2',
    'candymaker_test_mainmoduletest_packages_package_mainmoduletest_dependlocalmodule',
    'candymaker_test_mainmoduletest_system_packages_package_mainmoduletest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-main-moduletest'

module.SOURCE = {
    'main' : [
        'moduletest.cpp',
        'module.cpp',
        'packagefile.cpp',
    ],
    'core' : {
        'module' : [
            'manager.cpp',
            'context.cpp',
            'info.cpp',
            'contexts.cpp',
        ],
        'file' : [
            'execute.cpp',
            'template.cpp',
            'package.cpp',
            'path.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
    },
}

module.LIB = [
    'dl',
]
