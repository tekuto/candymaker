# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'modulemanagertest',
    'loadordertest3',
)

module.SOURCE = {
    'test' : {
        'modulemanagertest' : [
            'loadordertest3.cpp',
        ],
    },
}
