# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

import os.path

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = os.path.join(
    'packageresourcetest',
    'newtest',
)

module.SOURCE = {
    'test' : {
        'packageresourcetest' : [
            'newtest.cpp',
        ],
    },
}
