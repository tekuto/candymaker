# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_gamecontextmanagertest',
    'candymaker_testdata_gamecontextmanagertest_packages_package',
    'candymaker_testdata_gamecontextmanagertest_system_packages_package',
    'candymaker_test_gamecontextmanagertest_packages_package_createtest',
    'candymaker_test_gamecontextmanagertest_system_packages_package_createtest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-main-gamecontextmanagertest'

module.SOURCE = {
    'main' : [
        'gamecontextmanagertest.cpp',
        'gamecontextmanager.cpp',
        'module.cpp',
        'packagefile.cpp',
    ],
    'core' : {
        'game' : [
            'context.cpp',
        ],
        'file' : [
            'execute.cpp',
            'template.cpp',
            'path.cpp',
            'package.cpp',
            'common.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'module' : [
            'manager.cpp',
            'context.cpp',
            'contexts.cpp',
            'info.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'thread.cpp',
            'singleton.cpp',
            'joiner.cpp',
        ],
        'state' : [
            'state.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'joiner.cpp',
            'reenterevent.cpp',
            'event.cpp',
        ],
    },
}

module.LIB = [
    'dl',
]
