# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-dataelement-stringtest'

module.SOURCE = {
    'core' : {
        'dataelement' : [
            'stringtest.cpp',
            'string.cpp',
            'dataelement.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
    }
}
