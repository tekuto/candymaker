# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_filecommontest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-file-commontest'

module.SOURCE = {
    'core' : {
        'file' : [
            'commontest.cpp',
            'common.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                ],
            },
        ],
        'common' : [
            'filedata.cpp',
        ],
    },
}
