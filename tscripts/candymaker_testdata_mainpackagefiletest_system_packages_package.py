# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = os.path.join(
    'mainpackagefiletest',
    'system',
    'packages',
    'package',
)

module.SOURCE = {
    'testdata' : {
        'mainpackagefiletest' : {
            'system' : {
                'packages' : {
                    'package' : [
                        'package.fgp',
                    ]
                }
            }
        }
    }
}
