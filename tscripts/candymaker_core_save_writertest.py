# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'candymaker_testdata_savewritertest_saves_savedirectory',
    'candymaker_testdata_savewritertest_savefailed_saves_savedirectory_savename_fgs',
    'candymaker_test_savewritertest_savewritertest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'candymaker-core-save-writertest'

module.SOURCE = {
    'core' : {
        'save' : [
            'writertest.cpp',
            'writer.cpp',
            'save.cpp',
            'writenotify.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'module' : [
            'context.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'state' : [
            'state.cpp',
            'threadbackground.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
        'file' : [
            'path.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
    }
}

module.LIB = [
    'stdc++fs',
    'dl',
]
