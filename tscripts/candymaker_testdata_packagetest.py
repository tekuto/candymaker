# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'packagetest'

module.SOURCE = {
    'testdata' : {
        'packagetest' : [
            'createtest_modules.fgp',
            'createtest_modulewithoutinitializer.fgp',
            'createtest_modulewithoutinterfacepackage.fgp',
            'createtest_modulewithoutdependpackage.fgp',
            'createtest_basesystems.fgp',
            'createtest_games.fgp',
        ]
    }
}
